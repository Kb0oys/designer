/* Name ClearCommand.java
 *
 * What:
 *  The command replaces the Selection area from the Layout with a block
 *  of the same dimensions, but blank.  The old Selection is saved to the
 *  clipboard.
 */
package designer.command;

import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JMenuItem;

import designer.layout.Layout;
import designer.layout.RangeBlk;

/**
 * Command to clear the selection area from the layout to the clipboard,
 * replacing the selection area with blank nodes.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A model railroad dispatching program
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */
public class ClearCommand extends AbstractCommand {

	private Layout Layout; // The Layout to copy a Block of Nodes from

	private RangeBlk PrevCopy; // the old contents of the clipboard

	private Point OldCursor; // the Cursor prior to the command

	static final String CmdLabel = "Clear"; // String appended to Undo

	/**
	 * Constructor for the DeleteColumn Prototype.
	 */
	ClearCommand() {
	}

	/**
	 * Perform the command encapsulated by this object.
	 *
	 * @return true if this call to doIt was successful and can be undone
	 */
	public boolean doIt() {
		Rectangle sel = Layout.getSelection();
		OldCursor = Layout.getXY();
		RangeBlk copy;
		if (sel != null) {
			try {
				copy = new RangeBlk(Layout.createBlk(sel.width, sel.height),
						sel.width, sel.height);
				Layout.exchange(Layout.locate(sel.y, sel.x), copy);
				PrevCopy = CommandFactory.MyClipboard.toClipBoard(copy);
				Layout.gotoXY(OldCursor);
				return true;
			} catch (Exception e) {
			} // try
		}
		return false;
	} // doIt()

	/**
	 * Undo the command encapsulated by this object.
	 *
	 * @return true if the undo was successful
	 */
	public boolean undoIt() {
		try {
			RangeBlk fromClip = CommandFactory.MyClipboard
					.toClipBoard(PrevCopy);
			Layout.exchange(Layout.locate(OldCursor.y, OldCursor.x), fromClip);
			Layout.gotoXY(OldCursor);
			Layout.setSelection(new Rectangle(OldCursor.x, OldCursor.y,
					fromClip.Width, fromClip.Height));
		} catch (Exception e) {
			return false;
		} // try
		return true;
	} // undoIt()

	/**
	 * creates a new ClearCommand, appropriate for state of the Layout.
	 *
	 * @param layout
	 *            is the layout that the command operates on.
	 *
	 * @return a new DeleteColumn object.
	 */
	public AbstractCommand duplicate(Layout layout) {
		ClearCommand newCmd = new ClearCommand();
		newCmd.Layout = layout;
		return newCmd;
	}

	/**
	 * fetches the label for the concrete class.
	 *
	 * @return the label associated with the concrete class.
	 */
	public String findLabel() {
		return CmdLabel;
	}

	/**
	 * stores the JMenuItem of the trigger of the class constructor. By knowing
	 * the JMenuItem, the concrete class can alter the appearance of the
	 * JMenuItem. For example, Paste can disable itself, if there is nothing to
	 * Paste.
	 *
	 * @param cmd
	 *            is a member of the class created by the JMenuItem.
	 */
	public void saveJMenuItem(AbstractCommand cmd, JMenuItem item) {
	}

} // class ClearCommand
