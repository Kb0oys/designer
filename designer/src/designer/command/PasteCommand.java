/* Name PasteCommand.java
 *
 * What:
 *  The command pastes the clipboard into the Layout, replacing the
 *  Selection Block.
 */
package designer.command;

import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JMenuItem;

import designer.layout.Layout;
import designer.layout.Node;
import designer.layout.RangeBlk;

/**
 * Command to Paste the Clipboard into the layout. If the Selection area is not
 * the same size, the operation fails.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A model railroad dispatching program
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */
public class PasteCommand extends AbstractCommand {

	private Layout Layout; // The Layout to delete from

	private RangeBlk OldBlk; // the block replaced

	private Node NewBlk; // the block retrieved from the clip board

	static final String CmdLabel = "Paste"; // String appended to Undo

	/**
	 * Constructor for the DeleteColumn Prototype.
	 */
	PasteCommand() {
	}

	/**
	 * Perform the command encapsulated by this object.
	 *
	 * @return true if this call to doIt was successful and can be undone
	 */
	public boolean doIt() {
		Dimension size = Layout.getSize();
		Point sel = Layout.getXY();
		int w = size.width - sel.x + 1;
		int h = size.height - sel.y + 1;
		OldBlk = CommandFactory.MyClipboard.fromClipBoard();
		if (OldBlk != null) {
			try {
				if ((OldBlk.Width <= w) && (OldBlk.Height <= h)) {
					NewBlk = Layout.exchange(Layout.locate(sel.y, sel.x),
							OldBlk);
					Layout.gotoXY(sel);
				}
				return true;
			} catch (Exception e) {
			} // try
		}
		return false;
	} // doIt()

	/**
	 * Undo the command encapsulated by this object.
	 *
	 * @return true if the undo was successful
	 */
	public boolean undoIt() {
		try {
			Layout.exchange(NewBlk, OldBlk);
			//      Layout.showMe();
		} catch (Exception e) {
			return false;
		} // try
		return true;
	} // undoIt()

	/**
	 * creates a new PasteCommand, appropriate for state of the Layout.
	 *
	 * @param layout
	 *            is the layout that the command operates on.
	 *
	 * @return a new DeleteColumn object.
	 */
	public AbstractCommand duplicate(Layout layout) {
		PasteCommand newCmd = new PasteCommand();
		newCmd.Layout = layout;
		return newCmd;
	}

	/**
	 * fetches the label for the concrete class.
	 *
	 * @return the label associated with the concrete class.
	 */
	public String findLabel() {
		return CmdLabel;
	}

	/**
	 * stores the JMenuItem of the trigger of the class constructor. By knowing
	 * the JMenuItem, the concrete class can alter the appearance of the
	 * JMenuItem. For example, Paste can disable itself, if there is nothing to
	 * Paste.
	 *
	 * @param cmd
	 *            is a member of the class created by the JMenuItem.
	 */
	public void saveJMenuItem(AbstractCommand cmd, JMenuItem item) {
		CommandFactory.MyClipboard.setMenuItem(item);
	}

}
/* @(#)PasteCommand.java */
