/* Name: UndoCommand.java
 *
 * What:
 *  This class is simply a marker to the CommandManger, letting it take
 *  special action - undo the last command, but do not put this in the
 *  history list.
 */
package designer.command;

/**
 * Instances of this class represent undo commands.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * @author Rodney Black
 * @version $Revision$
 */
class UndoCommand implements Undo {
    /**
     * This implementation of doIt does not actually do anything.  The
     * logic for undo is in the CommandManager class.  A CommandManager
     * object knows that it is supposed to invoke its undo logic when it
     * sees an instance of this class because this class implements the
     * Undo interface.  The Undo interface is a semantic interface that
     * is used to mark a class as representing an undo command.
     * @return nothing is ever returned because an Exception is thrown.
     */
    public boolean doIt() {
        // This method should never be called
        throw new NoSuchMethodError();
    } // doIt()

    /**
     * This implementation of undoIt does not actually do anything.
     * Undo commands are not undone.  Instead a redo command is issued.
     * @return nothing is ever returned because an Exception is thrown.
     */
    public boolean undoIt() {
        // This method should never be called
        throw new NoSuchMethodError();
    } // undoIt()
}
/* @(#)UndoCommand.java */
