/* Name: ImportCommand.java
 *
 * What:
 *  This command class handles importing files.
 */
package designer.command;

import java.awt.Rectangle;

import javax.swing.JMenuItem;

import designer.layout.Layout;
import designer.layout.RangeBlk;

/**
 *  This command class handles importing files.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ImportCommand extends AbstractCommand  {
    static private CommandFactory ImportHandler;  //Singleton handler 
    static final String CmdLabel = "Import"; // String appended to Undo
    private Layout ActiveLayout; // The Layout to copy a Block of Nodes from
    private RangeBlk PrevCopy;   // the old contents of the active layout
    private Rectangle PrevPoint; // the description of the old contents
    private int RowsAdded;       // number of rows added so that the import would fit
    private int ColsAdded;       // number of columns added so that the import would fit
    
    /**
     * Constructor for the ImportCommand Prototype.
     */
    ImportCommand() {
    }
    
    /**
     * is invoked to retrieve the CommandFactory that will handle the details
     * of running the command.  If the CommandFactory does not exist, it
     * will be created.
     * @return the CommandFactory for executing the import command
     */
    static public CommandFactory getHandler() {
        if (ImportHandler == null) {
            ImportHandler = new CommandFactory(new ImportCommand());
        }
        return ImportHandler;
    }
    
    /**
     * holds a reference to the Singleton command handler
     */
    /**
     * Imports a Grid Array into the current cursor location.  The upper left
     * corner of the imported Grid Array is placed at the upper left corner
     * of the selected Grid Array.  All Grids in the current Grid Array are
     * replaced by the contents of the imported Grid Array.  Columns and rows are
     * added, as needed. 
     * @return true if this call to doIt was successful and can be undone
     */
    public boolean doIt() {
        PrevPoint = ActiveLayout.getSelection();
        Layout importLayout = Layout.getConstruction();
        int width;
        int height;
        int activeWidth;
        int activeHeight;
        
        if ((PrevPoint != null) && (importLayout != null)) {
            try {
                ActiveLayout.setSelection(null);
                
//              // remove the selection columns
//              head = ActiveLayout.delColumn(PrevPoint.x, PrevPoint.width);
//              if (head != null) {
//              PrevCopy = new RangeBlk(head, PrevPoint.width, ActiveLayout.getSize().height);
//              
//              // ensure the layout has enough rows
//              if (ActiveLayout.getSize().height < (importLayout.getSize().height + PrevPoint.y)) {
//              RowsAdded = (importLayout.getSize().height + PrevPoint.y) - ActiveLayout.getSize().height;
//              ActiveLayout.newRow(ActiveLayout.locate(ActiveLayout.getSize().height, 1),
//              RowsAdded);
//              }
//              
//              // add in the number of imported columns
//              ColsAdded = importLayout.getSize().width;
//              ActiveLayout.newColumn(ActiveLayout.locate(1, PrevPoint.x - 1), ColsAdded);
//              
//              // replace the newly added, blank columns
//              ActiveLayout.exchange(ActiveLayout.locate(PrevPoint.y, PrevPoint.x), new RangeBlk(importLayout.locate(1, 1),
//              ColsAdded, importLayout.getSize().height));
                width = importLayout.getSize().width;
                height = importLayout.getSize().height;
                activeWidth = ActiveLayout.getSize().width;
                activeHeight = ActiveLayout.getSize().height;
                
                // check if any columns should be added
                ColsAdded = (PrevPoint.x + width - 1) - activeWidth;
                if (ColsAdded > 0) {
                    ActiveLayout.newColumn(ActiveLayout.locate(1, activeWidth), ColsAdded);
                }
                
                // check if any rows should be added
                RowsAdded = (PrevPoint.y + height - 1) - activeHeight;
                if (RowsAdded > 0) {
                    ActiveLayout.newRow(ActiveLayout.locate(activeHeight, 1), RowsAdded);
                }
                
                // replace the existing Sections with those from the imported file.  The contents
                // of PrevCopy will get replaced with the contents of the ActiveLayout
                PrevCopy = new RangeBlk(importLayout.locate(1, 1), width, height);
                ActiveLayout.exchange(ActiveLayout.locate(PrevPoint.y, PrevPoint.x),
                        PrevCopy);
                ActiveLayout.setSelection(new Rectangle(PrevPoint.x, PrevPoint.y, width, height));
                ActiveLayout.showMe();
                return true;
            }
            catch (Exception e) {
            } // try
        }
        return false;
    } // doIt()
    
    /**
     * Undo the command encapsulated by this object.
     * @return true if the undo was successful
     */
    public boolean undoIt() {
        try {
            
            // remove the columns added
            ActiveLayout.setSelection(null);
//          ActiveLayout.delColumn(PrevPoint.x, ColsAdded);
//          
//          // remove the rows added
//          ActiveLayout.delRow(PrevPoint.y, RowsAdded);
//          
//          // insert enough blank columns
//          ActiveLayout.newColumn(ActiveLayout.locate(PrevPoint.y, PrevPoint.x - 1), PrevPoint.width);
//          
//          // copy back the saved region
//          ActiveLayout.exchange(ActiveLayout.locate(PrevPoint.y, PrevPoint.x), PrevCopy);
//          
            // copy back the saved region
            ActiveLayout.exchange(ActiveLayout.locate(PrevPoint.y, PrevPoint.x), PrevCopy);
            
            // remove any rows added
            if (RowsAdded > 0) {
                ActiveLayout.delRow(ActiveLayout.getSize().height - RowsAdded + 1, RowsAdded);
            }
            
            // remove any columns added
            if (ColsAdded > 0) {
                ActiveLayout.delColumn(ActiveLayout.getSize().width - ColsAdded + 1, ColsAdded);
            }
            // reset the selection
            ActiveLayout.setSelection(PrevPoint);
            ActiveLayout.showMe();
        }
        catch (Exception e) {
            return false;
        } // try
        return true;
    } // undoIt()
    
    /**
     * creates a new ImportCommand, appropriate for state of the Layout.
     *
     * @param layout is the layout that the command operates on.
     *
     * @return a new ImportCommand object.
     */
    public AbstractCommand duplicate(Layout layout) {
        ImportCommand newCmd = new ImportCommand();
        newCmd.ActiveLayout = layout;
        return newCmd;
    }
    
    /**
     * fetches the label for the concrete class.
     *
     * @return the label associated with the concrete class.
     */
    public String findLabel() {
        return CmdLabel;
    }
    
    /**
     * stores the JMenuItem of the trigger of the class constructor.  By
     * knowing the JMenuItem, the concrete class can alter the appearance
     * of the JMenuItem.  For example, Paste can disable itself, if there
     * is nothing to Paste.
     *
     * @param cmd is a member of the class created by the JMenuItem.
     */
    public void saveJMenuItem(AbstractCommand cmd, JMenuItem item) {
    }   
}
/* @(#)ImportCommand.java */