/* Name DeleteColumn.java
 *
 * What:
 *  The command that deletes the column the cursor is on.
 */
package designer.command;

import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JMenuItem;

import designer.layout.Layout;
import designer.layout.Node;
import designer.layout.RangeBlk;

/**
 * Command to delete a column from the layout.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class DeleteColumn
    extends AbstractCommand {

  private Layout Layout;   // The Layout to delete from
  private RangeBlk DelBlk; // The old block from the ClipBoard
  private Point OldPos;    // initial cursor position.
  private Point NewPos;    // final cursor position.
  static final String CmdLabel = "Delete Column";  // String appended to Undo
  static JMenuItem MyTrigger;

  /**
   * Constructor for the DeleteColumn Prototype.
   */
  DeleteColumn() {
  }

  /**
   * Perform the command encapsulated by this object.
   * @return true if this call to doIt was successful and can be undone
   */
  public boolean doIt() {
    Node head;
    Rectangle selection = Layout.getSelection();
    OldPos = Layout.getXY();
    try {
      Layout.setSelection(null);
      head = Layout.delColumn(OldPos.x, selection.width);
      if (head != null) {
        DelBlk = new RangeBlk(head, selection.width, Layout.getSize().height);
        if (OldPos.x <= Layout.getSize().width) {
          NewPos = new Point(OldPos);
        }
        else {
          NewPos = new Point(OldPos.x - 1, OldPos.y);
        }
        Layout.gotoXY(NewPos);
        Layout.setSelection(new Rectangle(NewPos.x, NewPos.y, 1, 1));
        Layout.showMe();
        return true;
      }
    }
    catch (Exception e) {
    } // try
    return false;
  } // doIt()

  /**
   * Undo the command encapsulated by this object.
   * @return true if the undo was successful
   */
  public boolean undoIt() {
    try {
      Layout.setSelection(null);
      Layout.addColumn(OldPos.x - 1, DelBlk.UpperLeft);
      Layout.gotoXY(OldPos);
      Layout.showMe();
    }
    catch (Exception e) {
      return false;
    } // try
    return true;
  } // undoIt()

  /**
   * creates a new DeleteColumn, appropriate for state of the Layout.
   *
   * @param layout is the layout that the command operates on.
   *
   * @return a new DeleteColumn object.
   */
  public AbstractCommand duplicate(Layout layout) {
    DeleteColumn newCmd = new DeleteColumn();
    newCmd.Layout = layout;
    return newCmd;
  }

      /**
       * fetches the label for the concrete class.
       *
       * @return the label associated with the concrete class.
       */
      public String findLabel()
      {
        return CmdLabel;
      }

      /**
       * stores the JMenuItem of the trigger of the class constructor.  By
       * knowing the JMenuItem, the concrete class can alter the appearance
       * of the JMenuItem.  For example, Paste can disable itself, if there
       * is nothing to Paste.
       *
       * @param cmd is a member of the class created by the JMenuItem.
       */
      public void saveJMenuItem(AbstractCommand cmd, JMenuItem item)
      {
      }

}
/* @(#)DeleteColumn.java */
