/* Name InsRowAbove.java
 *
 * What:
 *  The command that inserts a row above the current cursor.
 */
package designer.command;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JMenuItem;

import designer.layout.Layout;
import designer.layout.Node;

/**
 * Command to insert a row above the current cursor.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A model railroad dispatching program
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */
public class InsRowAbove extends AbstractCommand {

	private Layout Layout; // The Layout to add the row(s) to

	private Node Pos; // The Node to add the row above

	private Node NewBlk; // The newly ceated Nodes

	private Rectangle Sel; // The Nodes selected prior to the insert

	private Point Cursor;

	static final String CmdLabel = "Insert Row Above"; // String appended to
													   // Undo

	/**
	 * Constructor
	 */
	InsRowAbove() {
	} // Constructor()

	/**
	 * Perform the command encapsulated by this object.
	 *
	 * @return true if this call to doIt was successful and can be undone
	 */
	public boolean doIt() {
		try {
			Dimension size = Layout.getSize();
			Cursor = Layout.getXY();
			Pos = Layout.locate(Cursor.y - 1, Cursor.x);
			if (NewBlk == null) {
				Sel = Layout.getSelection();
				NewBlk = Layout.createBlk(size.width, Sel.height);
			}
			Layout.setSelection(null);
			Layout.addRow(Pos, NewBlk);
			Layout.setSelection(new Rectangle(Cursor, new Dimension(1, 1)));
			Layout.gotoXY(Cursor);
			Layout.showMe();
		} catch (Exception e) {
			return false;
		} // try
		return true;
	} // doIt()

	/**
	 * Undo the command encapsulated by this object.
	 *
	 * @return true if the undo was successful
	 */
	public boolean undoIt() {
		try {
			Layout.setSelection(null);
			Layout.delRow(Pos, Sel.width);
			Layout.gotoXY(Cursor);
			Layout.setSelection(Sel);
			Layout.showMe();
		} catch (Exception e) {
			return false;
		} // try
		return true;
	} // undoIt()

	/**
	 * creates a new insRowAbove, appropriate for state of the Layout.
	 *
	 * @param layout
	 *            is the layout that the command operates on.
	 *
	 * @return a new InsRowAbove object.
	 */
	public AbstractCommand duplicate(Layout layout) {
		InsRowAbove newCmd = new InsRowAbove();
		newCmd.Layout = layout;
		return newCmd;
	}

	/**
	 * fetches the label for the concrete class.
	 *
	 * @return the label associated with the concrete class.
	 */
	public String findLabel() {
		return CmdLabel;
	}

	/**
	 * stores the JMenuItem of the trigger of the class constructor. By knowing
	 * the JMenuItem, the concrete class can alter the appearance of the
	 * JMenuItem. For example, Paste can disable itself, if there is nothing to
	 * Paste.
	 *
	 * @param cmd
	 *            is a member of the class created by the JMenuItem.
	 */
	public void saveJMenuItem(AbstractCommand cmd, JMenuItem item) {
	}

}
/* @(#)InsRowAbove.java */
