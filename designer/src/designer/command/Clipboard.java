/* Name: Clipboard.java
 *
 * What:
 *   Implements a clip board for holding a rectangular block of Nodes.
 */

package designer.command;

import designer.layout.*;
import javax.swing.JMenuItem;

/**
 * implements a clip board for holding a rectangular block of Nodes.
 * <p>
 * When the user copies or deletes a block, it is given to the clip board
 * so that the paste command has the most recent block to add.  This class
 * is a Singleton, created when the program begins.  It should survive
 * loading new files so that one can add something to it, bring up a new
 * file, and paste the clipboard contents in the new file.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * @author Rodney Black
 * @version $Revision$
 */
class Clipboard {
  RangeBlk SaveBlk; // the clipboard
  JMenuItem Menu;

  /**
   * the constructor.
   */
  public Clipboard() {
    SaveBlk = null;
  }

  /**
   * returns a copy of the Block saved.
   *
   * @return a copy of the Block saved.
   */
  RangeBlk fromClipBoard() {
    return SaveBlk.copy();
  }

  /**
   * replaces the saved Block.
   *
   * @param newBlk is the new contents of the ClipBoard
   *
   * @return the old contents of the ClipBoard
   */
  RangeBlk toClipBoard(RangeBlk newBlk) {
    RangeBlk oldBlk = SaveBlk;
    SaveBlk = newBlk;
    if (newBlk != null)
    {
      Menu.setEnabled(true);
    }
    else {
      Menu.setEnabled(false);
    }
    return oldBlk;
  }

  /**
   * tells the Clipboard where the JMenuItem is so that it can be
   * disabled if there is nothing in the Clipboard or enabled if
   * there is.
   * @param item is the JMenuItem to disable.
   */
  void setMenuItem(JMenuItem item)
  {
    Menu = item;
    item.setEnabled(false);
  }
}
/* @(#)Clipboard.java */
