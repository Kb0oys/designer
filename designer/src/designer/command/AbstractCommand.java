/* Name: AbstractCommand.java
 *
 * What:
 *  The super class for all command objects.
 */
package designer.command;

import designer.layout.Layout;
import javax.swing.JMenuItem;

/**
 * This class is the superclass of classes that encapsulate commands for
 * the layout editor.  It uses the Command pattern to provide a do/undo
 * history list.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * @author Rodney Black
 * @version $Revision$
 */
public abstract class AbstractCommand {
    /**
     * The CommandManager that manages command objects.
     */
    public final static CommandManager manager = new CommandManager();

    /**
     * The string which modifes the Undo label, so the user knows what
     * will be undone.
     */
    final static String CmdLabel = "Bad Label";

    /**
     * The JmenuItem which triggers creation of this class of command.
     */
    static JMenuItem MyTrigger;

    /**
     * Perform the command encapsulated by this object.
     * @return true if sucessful and can be undone.
     */
    public abstract boolean doIt();

    /**
     * Undo the last invocation of doIt.
     * @return true if the unndo was successful
     */
    public abstract boolean undoIt();

    /**
     * duplicates the concrete Command.
     *
     * @param layout is the Layout that the command operates on.
     * @return a copy of the command
     *
     * @see designer.layout.Layout
     */
    public abstract AbstractCommand duplicate(Layout layout);

    /**
     * fetches the label for the concrete class.
     *
     * @return the label associated with the concrete class.
     */
    public String findLabel()
    {
      return CmdLabel;
    }

    /**
     * stores the JMenuItem of the trigger of the class constructor.  By
     * knowing the JMenuItem, the concrete class can alter the appearance
     * of the JMenuItem.  For example, Paste can disable itself, if there
     * is nothing to Paste.
     *
     * @param cmd is a member of the class created by the JMenuItem.
     * @param item is the JMenuItem triggering the command.
     */
    public void saveJMenuItem(AbstractCommand cmd, JMenuItem item)
    {
      AbstractCommand.MyTrigger = item;
    }
}
/* @(#)AbstractCommand.java */
