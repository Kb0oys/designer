/* Name: CommandTrio
 *
 * What:
 *  A simple data structure for associating an AbstractCommand with a String.
 */

package designer.command;

/**
 * is a data structure for associating an AbstractCommand with a String
 * and an accelerator key.
 * The string is intended to be the label on a JMenuItem to identify the
 * CommandFactory for AbstractCommands of a particular type.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CommandTrio
{
    /**
     * is a template for the command Object.
     */
    public AbstractCommand Command;
    
    /**
     * is the name of the command, which is used to find the template.
     */
    public String Name;

    /**
     * is the accelerator (shortcut) key.  When instantiated,
     * the "ctrl" metakey will be added.
     */
    public int Accelerator;
    
    /**
     * constructs the trio.
     *
     * @param cmd is the AbstractCommand.
     * @param name is the String.
     * @param acc is the accelerator sequence.  0 means that there
     * is none.
     */
    public CommandTrio(AbstractCommand cmd, String name, int acc)
    {
	Command = cmd;
	Name = name;
    Accelerator = acc;
    }

    /**
     * constructs the trio without an accelerator
     * @param cmd is the AbstractCommand
     * @param name is the Label
     */
    public CommandTrio(AbstractCommand cmd, String name) {
        this(cmd, name, 0);
    }
}
/* @(#)CommandTrio.java */
