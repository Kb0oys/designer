/* Name CopyCommand.java
 *
 * What:
 *  The command copies the Selection area from the Layout, to the clipboard.
 */
package designer.command;

import java.awt.Rectangle;

import javax.swing.JMenuItem;

import designer.layout.Layout;
import designer.layout.RangeBlk;

/**
 * Command to copy the selection area from the layout to the clipboard.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CopyCommand
    extends AbstractCommand {

  private Layout Layout; // The Layout to copy a Block of Nodes from
  private RangeBlk PrevCopy; // the old contents of the clipboard
  static final String CmdLabel = "Copy"; // String appended to Undo

  /**
   * Constructor for the DeleteColumn Prototype.
   */
  CopyCommand() {
  }

  /**
   * Perform the command encapsulated by this object.
   * @return true if this call to doIt was successful and can be undone
   */
  public boolean doIt() {
    Rectangle sel = Layout.getSelection();
    RangeBlk copy;
    if (sel != null) {
      try {
        copy = (new RangeBlk(Layout.locate(sel.y, sel.x), sel.width,
                             sel.height));
        copy = copy.shallowCopy();
        PrevCopy = CommandFactory.MyClipboard.toClipBoard(copy);
        return true;
      }
      catch (Exception e) {
          System.out.println("Excption in copy:" + e.toString());
      } // try
    }
    return false;
  } // doIt()

  /**
   * Undo the command encapsulated by this object.
   * @return true if the undo was successful
   */
  public boolean undoIt() {
    try {
      CommandFactory.MyClipboard.toClipBoard(PrevCopy);
    }
    catch (Exception e) {
      return false;
    } // try
    return true;
  } // undoIt()

  /**
   * creates a new CopyCommand, appropriate for state of the Layout.
   *
   * @param layout is the layout that the command operates on.
   *
   * @return a new CopyCommand object.
   */
  public AbstractCommand duplicate(Layout layout) {
    CopyCommand newCmd = new CopyCommand();
    newCmd.Layout = layout;
    return newCmd;
  }

  /**
   * fetches the label for the concrete class.
   *
   * @return the label associated with the concrete class.
   */
  public String findLabel() {
    return CmdLabel;
  }

  /**
   * stores the JMenuItem of the trigger of the class constructor.  By
   * knowing the JMenuItem, the concrete class can alter the appearance
   * of the JMenuItem.  For example, Paste can disable itself, if there
   * is nothing to Paste.
   *
   * @param cmd is a member of the class created by the JMenuItem.
   */
  public void saveJMenuItem(AbstractCommand cmd, JMenuItem item) {
  }

}
/* @(#)CopyCommand.java */
