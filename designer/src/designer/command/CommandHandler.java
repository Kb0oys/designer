/* Name: CommandHandler.java
 *
 * What:
 *   This is an extension of the ActionListener interface to provide a way
 *   for the implementation to know of the JMenuItem which triggers
 *   the creation of a Command.
 */
package designer.command;

import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 * is an extension of ActionListener for the CommandFactory.  This extension
 * provides a way for the Panel to tell the CommandFactory what JMenuItem
 * triggers creation of a Command; thus, allowing the CommandFactory to
 * alter the presentation of the JMenuItem.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface CommandHandler extends ActionListener
{

    /**
     * is the additional method for telling the CommandFactory who the
     * JMenuItem is that triggers creation of a command.
     *
     * @param item is the JMenuItem which calls the ActionListener event
     *             which, in turn, creates a new command.
     */
public void passItem(JMenuItem item);

}
