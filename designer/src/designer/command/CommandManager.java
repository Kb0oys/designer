/* Name: CommandManager.java
 *
 * What: the singleton object responsible for managing the execution
 *  of commands.  It maintains a history list of commands executed, so
 *  that commands can be undone.
 */
package designer.command;

import java.util.LinkedList;
import javax.swing.JMenuItem;

/**
 * Instances of this class are responsible for managing the execution of
 * commands.  More specifically, for the purposes of this editor
 * program, instances of this class are responsible for maintaining a
 * command history for undo and redo.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CommandManager {
  /**
   * is the Singleton CommandManager
   */
  static CommandManager TheManager = new CommandManager();

  // The maximum number of commands to keep in the history
  private int maxHistoryLength = 100;

  private LinkedList<AbstractCommand> history = new LinkedList<AbstractCommand>();
  private LinkedList<AbstractCommand> redoList = new LinkedList<AbstractCommand>();
  static private JMenuItem UndoItem; // The menu item for the Undo command.
  static private JMenuItem RedoItem; // The menu item for the Redo command.

  /**
   * saves the JMenuItem for Undo.
   *
   * @param item is the JMenuItem which selects Undo.
   */
  public static void setUndoItem(JMenuItem item) {
    UndoItem = item;
    item.setEnabled(false);
  }

    /**
     * saves the JMenuItem for Redo.
     *
     * @param item is the JMenuItem which selects Redo.
     */
    public static void setRedoItem(JMenuItem item) {
      RedoItem = item;
      item.setEnabled(false);
    }

  /**
   * invokes a command and add it to the history,
   *<p>
   * If the command object's doIt method was previously called, then
   * it is expected to return false.
   * @param command is the command to be invoked
   */
  public void invokeCommand(AbstractCommand command) {
    String undoLabel;
    if (command instanceof Undo) {
      undo();
      return;
    } // if undo
    if (command instanceof Redo) {
      redo();
      return;
    } // if redo
    if (command.doIt()) {
      // doIt returned true, which means it can be undone
      addToHistory(command);
      undoLabel = new String("undo " + command.findLabel());
      UndoItem.setText(undoLabel);
      UndoItem.setEnabled(true);
    }
    else { // command cannot be undone
      history.clear();
      UndoItem.setText("undo");
      UndoItem.setEnabled(false);
    } // if
    // After a command that is not an undo or a redo, ensure that
    // the redo list is empty.
    if (redoList.size() > 0) {
      redoList.clear();
    }
  } // invokeCommand(AbstractCommand)

  /**
   * Undo the most recent command in the commmand history.
   */
  private void undo() {
    if (history.size() > 0) { // If there are commands in the history
      AbstractCommand undoCommand;
      undoCommand = history.removeFirst();
      undoCommand.undoIt();
      redoList.addFirst(undoCommand);
      RedoItem.setText("redo " + undoCommand.findLabel());
      RedoItem.setEnabled(true);
      if (history.size() > 0) {
        undoCommand =history.getFirst();
        UndoItem.setText("undo " + undoCommand.findLabel());
      }
      else {
        UndoItem.setText("undo");
        UndoItem.setEnabled(false);
      }
    } // if
  } // undo()

  /**
   * Redo the most recently undone command
   */
  private void redo() {
    if (redoList.size() > 0) { // If the redo list is not empty
      AbstractCommand redoCommand;
      redoCommand = redoList.removeFirst();
      redoCommand.doIt();
      history.addFirst(redoCommand);
      UndoItem.setText("undo " + redoCommand.findLabel());
      UndoItem.setEnabled(true);
      if (redoList.size() > 0) {
        redoCommand =redoList.getFirst();
        RedoItem.setText("redo " + redoCommand.findLabel());
      }
      else {
        RedoItem.setText("redo");
        RedoItem.setEnabled(false);
      }
    } // if
  } // redo()

  /**
   * Add a command to the command history.
   * @param command the command to add to the history.
   */
  private void addToHistory(AbstractCommand command) {
    history.addFirst(command);
    // If size of history has exceded maxHistoryLength, remove
    // the oldest command from the history
    if (history.size() > maxHistoryLength) {
      history.removeLast();
    }
  } // addToHistory(AbstractCommand)
}
/* @(#)CommandManager.java */
