/* Name: EditPane.java
 *
 * What:
 *  This file contains the class definition for an EditPane object.  An
 *  EditPane is derived from a ScrollPane, thus, it has scroll bars on the
 *  right and left edges.  It's body is a jPanel with a Grid layout
 *  manager.  The number of columns and rows is the same as the number of
 *  rows and columns in the Layout.  Thus, as the size of the Layout
 *  changes, the size of the jPanel changes.
 *
 *  This class is a Singleton.
 */
package designer.gui;

import designer.layout.Layout;
import designer.layout.Node;
import java.util.Enumeration;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.KeyEvent;

import javax.swing.JScrollPane;
import javax.swing.JPanel;

/**
 * This class provides the surface containing the visible aspects of the
 * Layout.  It is derived from the ScrollPane so that it has scroll bars.
 * However, its contents change as rows and columns are added to the Layout.
 * <p>
 * It is a Singleton.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class EditPane
extends JScrollPane {
    
    private Dimension PanelSize; // The viewable size of the panel
    private Layout LastLayout;
    /**
     * constructs the EditPane.
     *
     * @param fSize is the size of the visible area.
     */
    public EditPane(Dimension fSize) {
        PanelSize = fSize;
        Layout.register(this);
        setOpaque(true);
    }
    
    
    /**
     * reformats the screen presentation whenever the number of rows or
     * columns changes.
     *
     * @param layout is the Layout whose dimensions changed.
     *
     * @see designer.layout.Layout
     */
    public void reformat(Layout layout)
    {
        Point origin = getViewport().getViewPosition();
        GridPanel gp = new GridPanel(layout, PanelSize);
        LastLayout = layout;
        setViewportView(gp);
        getViewport().setViewPosition(origin);
    }
    
    /**
     * reformats the screen presentation, using the last known layout.
     * Warning: the last known layout may not be valid or even non-null!!
     */
    public void reformat() {
        reformat(LastLayout);
    }
    
    /**
     * handles a key.  It ignores everything but arrow keys.
     * 
     * @param key is the KeyEvent containing the key
     */
    public void handleKey(KeyEvent key) {
        if (LastLayout != null) {
            switch (key.getKeyCode()) {
            case KeyEvent.VK_UP:
                LastLayout.moveUp();
                break;
                
            case KeyEvent.VK_DOWN:
                LastLayout.moveDown();
                break;
                
            case KeyEvent.VK_LEFT:
                LastLayout.moveLeft();
                break;
                
            case KeyEvent.VK_RIGHT:
                LastLayout.moveRight();
                break;
                
            default:
            }
        }      
    }

    /**
     * is called to determine if the upper left corner of a 
     * Node is visible in the scroll window.
     *
     * @param n is is the Node in question
     * @return true if it is in the scroll window
     * and false if it is not
     */
    public boolean isVisible(Node n) {
        int row = n.getRow() * GridTile.Size.height;
        int column = n.getColumn() * GridTile.Size.width;
      return getViewport().getViewRect().contains(column, row, GridTile.Size.width,
              GridTile.Size.height);
    }

    /**
     * moves the scroll window
     * @param x is the number of grid squares to move the x (horizontal
     * axis) of the window.
     * @param y is the number of grid squares to move the y (vertical
     * axis) of the window.
     */
    public void scroll(int x, int y) {
        Point p = getViewport().getViewPosition();
        getViewport().setViewPosition(new Point(p.x + (x * GridTile.Size.width),
                p.y + (y * GridTile.Size.height)));
    }
    
    /**
     * the inner class provides the GridLayout for the grid squares.
     */
    private class GridPanel
    extends JPanel {
        
        static final int HGAP = 0; // gap betweem rows
        static final int VGAP = 0; // gap between columns
        
        /**
         * constructs a JPanel with a Grid Layout.
         *
         * @param layout is the layout being drawn
         * @param fSize is the size of the enclosing JFrame.
         */
        GridPanel(Layout layout, Dimension fSize) {
            Enumeration<Node> e = layout.elements();
            int x;
            Node n;
            GridTile gTile;
            Dimension lSize = layout.getSize();
            Dimension pixels = Tile.getGridSize();
//          int rows = lSize.height + 1;  // 1 for preventing scroll bar overlap
            int rows = fSize.height / pixels.height;
            int cols = fSize.width / pixels.width;
            cols = Math.max(cols, lSize.width); // 1 for heading
            rows = Math.max(rows, lSize.height);
            setLayout(new GridLayout(rows + 1, cols + 1, HGAP, VGAP));
            
            /* Set the column headings */
            add(new Tile(""));
            for (x = 0; x < lSize.width; ++x)
            {
                add(new Tile(String.valueOf(x + 1)));
            }
            while (x < cols)
            {
                add(new Tile(""));
                ++x;
            }
            for (int y = 0; y < rows; ++y) {
                if (y < lSize.height) {
                    add(new Tile(String.valueOf(y + 1)));
                }
                else {
                    add(new Tile(""));
                }
                if (e.hasMoreElements()) {
                    for (x = 0; x < lSize.width; ++x) {
                        n = e.nextElement();
                        gTile = n.getTile();
                        add(gTile);
                    }
                }
                else {
                    x = 0;
                }
                while (x < cols) {
                    add(new Tile(""));
                    ++x;
                }
            }
        }
    }
}
/* @(#)EditPane.java */
