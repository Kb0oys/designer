/* Name: ScreenSize.java
 *
 * What;
 *  ScreenSize is a Singleton object with two values - the number
 *  of pixels wide and number of pixels high to make the CATS
 *  screen.
 */
package designer.gui;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import designer.gui.jCustom.AcceptDialog;

/**
 *  ScreenSize is a Singleton object with two values - the number
 *  of pixels wide and number of pixels high to make the CATS
 *  screen.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2007, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ScreenSize {
    
    /**
     * is the default width
     */
    private final int DEFAULT_WIDTH = 640;
    
    /**
     * is the default height
     */
    private final int DEFAULT_HEIGHT = 400;
    
    /**
     * is the default X coordinate of the upper left corner
     */
    private final int DEFAULT_X = 0;
    
    /**
     * is the default Y coordinate of the upper left corner
     */
    private final int DEFAULT_Y = 0;
    
    /**
     * is the singleton.
     */
    public static ScreenSize TheScreenSize;
    
    /**
     * goes on the drop down menu.
     */
    private JMenuItem RectangleMenu = new JMenuItem("Screen Size ...");
    
    /**
     * is the current width
     */
    private int MyWidth;
    
    /**
     * is the current height
     */
    private int MyHeight;
    
    /**
     * is the current X coordinate
     */
    private int MyX;
    
    /**
     * is the current Y coordinate
     */
    private int MyY;
    
    /**
     * the constructor.
     *
     * @param parent is the Panel which will hold the JCheckBox.
     */
    public ScreenSize(DispPanel parent) {
        if (TheScreenSize == null) {
            RectangleMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    JPanel p = new JPanel(new GridLayout(2,2));
                    JPanel wPane = new JPanel(new FlowLayout());
                    JPanel hPane = new JPanel(new FlowLayout());
                    JPanel xPane = new JPanel(new FlowLayout());
                    JPanel yPane = new JPanel(new FlowLayout());
                    JTextField hText = new JTextField(String.valueOf(MyHeight), 5);
                    JTextField wText = new JTextField(String.valueOf(MyWidth), 5);
                    JTextField xText = new JTextField(String.valueOf(MyX), 5);
                    JTextField yText = new JTextField(String.valueOf(MyY), 5);
                    wPane.add(new JLabel("Width:"));
                    hPane.add(new JLabel("Height:"));
                    xPane.add(new JLabel("X:"));
                    yPane.add(new JLabel("Y:"));
                    wPane.add(wText);
                    hPane.add(hText);
                    xPane.add(xText);
                    yPane.add(yText);
                    p.add(wPane);
                    p.add(hPane);
                    p.add(xPane);
                    p.add(yPane);
                    if (AcceptDialog.select(p, "Screen Size")) {
                        int i;
                        try {
                            i = Integer.parseInt(hText.getText());
                            MyHeight = i;
                        }
                        catch (NumberFormatException nfe) {
                            JOptionPane.showMessageDialog( (Component)null,
                                    "Height is not an integer.", "Format Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        try {
                            i = Integer.parseInt(wText.getText());
                            MyWidth = i;
                        }
                        catch (NumberFormatException nfe) {
                            JOptionPane.showMessageDialog( (Component)null,
                                    "Width is not an integer.", "Format Error",
                                    JOptionPane.ERROR_MESSAGE);                            
                        }
                        try {
                            i = Integer.parseInt(xText.getText());
                            MyX = i;
                        }
                        catch (NumberFormatException nfe) {
                            JOptionPane.showMessageDialog( (Component)null,
                                    "X is not an integer.", "Format Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        try {
                            i = Integer.parseInt(yText.getText());
                            MyY = i;
                        }
                        catch (NumberFormatException nfe) {
                            JOptionPane.showMessageDialog( (Component)null,
                                    "Y is not an integer.", "Format Error",
                                    JOptionPane.ERROR_MESSAGE);                            
                        }
                    }
                }
            }
            );
            TheScreenSize = this;
            parent.addAppearanceItem(RectangleMenu);
            MyWidth = DEFAULT_WIDTH;
            MyHeight = DEFAULT_HEIGHT;
            MyX = DEFAULT_X;
            MyY = DEFAULT_Y;
        }
    }
    
    /**
     * returns the current width.
     *
     * @return the width in pixels.
     */
    public int getScreenWidth() {
        return MyWidth;
    }
    
    /**
     * sets the width in pixels.
     * @param width is the screen width
     */
    public void setScreenWidth(int width) {
        if (width > 100) {
            MyWidth = width;
        }
    }
    
    /**
     * returns the current height.
     *
     * @return the height in pixels.
     */
    public int getScreenHeight() {
        return MyHeight;
    }
    
    /**
     * sets the height in pixels.
     * @param height is the screen height
     */
    public void setScreenHeight(int height) {
        if (height > 100) {
            MyHeight = height;
        }
    }
    
    
    /**
     * returns the current X coordinate of the upper left corner.
     *
     * @return the X value in pixels.
     */
    public int getScreenX() {
        return MyX;
    }
    
    /**
     * sets the X coordinate in pixels.
     * @param x is the X coordinate
     */
    public void setScreenX(int x) {
        if (x > 0) {
            MyX = x;
        }
    }
    
    /**
     * returns the current Y coordinate of the upper left corner.
     *
     * @return the Y coordinate in pixels.
     */
    public int getScreenY() {
        return MyY;
    }
    
    /**
     * sets the Y coordinate of the upper left corner in pixels.
     * @param y is the Y coordinate
     */
    public void setScreenY(int y) {
        if (y > 0) {
            MyY = y;
        }
    }
    
    /**
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return (MyHeight == DEFAULT_HEIGHT) && (MyWidth == DEFAULT_WIDTH)
        && (MyX == DEFAULT_X) && (MyY == DEFAULT_Y);
    }
}
/* @(#)ScreenSize.java */
