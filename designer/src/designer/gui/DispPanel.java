/* Name: DispPanel.java
 *
 * What:
 *  This is the root of the screens displayed.
 */
package designer.gui;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;

import designer.command.CommandFactory;
import designer.command.CommandHandler;
import designer.command.CommandManager;
import designer.command.CommandTrio;
import designer.command.ImportCommand;
import designer.gui.items.BlockTable;
import designer.gui.items.ButtonItemDialog;
import designer.gui.items.DepotDialog;
import designer.gui.items.DeviceTable;
import designer.gui.items.ImageItemDialog;
import designer.gui.items.LockTable;
import designer.gui.items.PointsTable;
import designer.gui.items.RouteTable;
import designer.gui.items.SecEdgeDialog;
import designer.gui.items.SecNameDialog;
import designer.gui.items.SignalTable;
import designer.gui.items.TrackGroupDialog;
import designer.gui.jCustom.JScrollDialog;
import designer.gui.jCustom.ManagerEditPane;
import designer.gui.store.LawPane;
import designer.gui.store.StoreMenu;
//import designer.layout.ColorStore;
import designer.layout.ColorList;
import designer.layout.CrewStore;
import designer.layout.FontList;
import designer.layout.Hours;
import designer.layout.JmriNameList;
import designer.layout.JobStore;
import designer.layout.Layout;
import designer.layout.ServiceClient;
import designer.layout.SignalTemplate;
import designer.layout.TemplateStore;
import designer.layout.TrainStore;
import designer.layout.items.ButtonItem;
import designer.layout.items.IOSpecChainManager;
import designer.layout.items.ImageItem;
import designer.layout.items.LoadFilter;
import designer.layout.items.Section;
import designer.layout.xml.XMLReader;

/**
 * is the root object for the GUI. It contains the menu bar, the menu items, and
 * the main drawing surface.
 *
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2004, 2005, 2006, 2009, 2012, 2013, 2015, 2020, 2021
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */

public class DispPanel
extends JFrame {

    /**
     * is the preferred file name extension for layout files.
     */
    static final String XML_EXTENSION = ".xml";
    
    /**
     * the java property tag for the default data folder
     */
    static private final String DEFAULT_FOLDER_TAG = "cats.folder";
    
    /**
     * the java property tag for the user home folder
     */
    static private final String HOME_FOLDER_TAG = "user.home";
    	
    JPanel contentPane;
    
    JMenuBar jMenuBar1 = new JMenuBar();
    
    JMenu jMenuFile = new JMenu();
    
    JMenuItem jMenuFileExit = new JMenuItem();
    
    JMenu jMenuHelp = new JMenu();
    
    JMenuItem jMenuHelpAbout = new JMenuItem();
    
    BorderLayout borderLayout1 = new BorderLayout();
    
    JMenuItem jMenuFileSave = new JMenuItem();
    
    JMenuItem jMenuFileOpen = new JMenuItem();
    
    JMenu jMenuApp = new JMenu();
    
//    JMenu jMenuAppColors = new JMenu();
    JMenuItem jMenuAppColors = new JMenuItem();
    
//    JMenu jMenuAppFonts = new JMenu();
    JMenuItem jMenuAppFonts = new JMenuItem();
    
    CounterFactory jMenuAppAdjustments = new CounterFactory("Adjustments");
    
    JMenu jMenuEdit = new JMenu();
    
    JMenu jMenuDevices = new JMenu();
    
    JMenu jMenuNetwork = new JMenu();
    
    JMenu jMenuTrains = new StoreMenu("Train", TrainStore.TrainKeeper);
    
    JMenu jMenuJobs = new StoreMenu("Job", JobStore.JobsKeeper);
    
    JMenu jMenuCrew = new StoreMenu("Crew", CrewStore.CrewKeeper);
    
    JMenuItem jMenuHours = new JMenuItem();
    
    JMenuItem jMenuFileNew = new JMenuItem();
    
    JMenuItem jMenuFileAs = new JMenuItem();
    
    JMenuItem jMenuFileImport = new JMenuItem();
    
    JMenuItem jMenuUndo = new JMenuItem();
    
    JMenuItem jMenuRedo = new JMenuItem();
    
    JMenuItem jMenuAppSize = new JMenuItem();
    
    JMenuItem jMenuEditRefresh = new JMenuItem();
    
    JMenuItem jMenuDevAddTemp = new JMenuItem();
    
    JMenuItem jMenuDevEditTemp = new JMenuItem();
    
    JMenuItem jMenuDevDelTemp = new JMenuItem();
    
    JMenuItem jMenuDevIOChain = new JMenuItem();
    
    JMenuItem jMenuDevJmriName = new JMenuItem();
    
    JMenuItem jMenuTabBlock = new JMenuItem();
    
    JMenuItem jMenuTabLocks = new JMenuItem();
    
    JMenuItem jMenuTabSignal = new JMenuItem();
    
    JMenuItem jMenuTabPoints = new JMenuItem();
    
    JMenuItem jMenuTabRoutes = new JMenuItem();
    
    JMenuItem jMenuTabAll = new JMenuItem();
    
    JMenu jMenuAppWidth = new JMenu();
    
    JMenu jMenuDet = new JMenu();
    
    JMenuItem jMenuDetName = new JMenuItem();
    
    JMenuItem jMenuDetTracks = new JMenuItem();
    
    JMenuItem jMenuDetDepot = new JMenuItem();
    
    JMenuItem jMenuDetEnds = new JMenuItem();
    
    JMenuItem jMenuDetImage = new JMenuItem();
    
    JMenuItem jMenuDetButton = new JMenuItem();
    
    // components of the Grid Size JDialog
    JDialog GDialog = new JDialog();
    
    final JLabel GwidthLab = new JLabel("Grid Width");
    
    final JLabel GheightLab = new JLabel("Grid Height");
    
    JTextField GwidthText = new JTextField(String
            .valueOf(Tile.getGridSize().width), 3);
    
    JTextField GheightText = new JTextField(String
            .valueOf(Tile.getGridSize().height),
            3);
    
    JButton GAccept = new JButton("Accept");
    
    JButton GCancel = new JButton("Cancel");
    
    JPanel GPanel1 = new JPanel();
    
    JPanel GPanel2 = new JPanel();
    
    JPanel GPanel3 = new JPanel();
    
    boolean Gresult;
    
    JMenu StatNetwork;
    
    JPanel TrainStatPort;
    
    JMenuItem jMenuNetworkOps = new JMenuItem();
    
    JMenuItem jMenuNetworkTSServer = new JMenuItem();
    
    private String FileName; // the name of the file holding the configuration
    
    /**
     * the path to the last file selected through a FileChooser
     */
    static public String FilePath = ".";
    
    EditPane EditPane;
    
    /**
     * construct the frame.
     */
    public DispPanel() {
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        setDefaultLookAndFeelDecorated(true);
        FilePath = getDefaultDataFolder();
        try {
            jbInit();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    //Component initialization
    private void jbInit() throws Exception {
        Enumeration<CommandTrio> itor;
        CommandTrio pair;
        contentPane = (JPanel)this.getContentPane();
        contentPane.setLayout(borderLayout1);
        this.setSize(new Dimension(400, 300));
        this.setTitle("Dispatcher Panel");
        jMenuFile.setText("File");
        jMenuFileExit.setText("Exit");
        jMenuFileExit
        .addActionListener(new DispPanel_jMenuFileExit_ActionAdapter(
                this));
        jMenuHelp.setText("Help");
        jMenuHelpAbout.setText("About");
        jMenuHelpAbout
        .addActionListener(new DispPanel_jMenuHelpAbout_ActionAdapter(
                this));
        jMenuFileSave.setText("Save ...");
        jMenuFileSave
        .addActionListener(new DispPanel_jMenuFileSave_ActionAdapter(
                this));
        jMenuFileSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                KeyEvent.CTRL_MASK));
        jMenuFileOpen.setText("Open ...");
        jMenuFileOpen
        .addActionListener(new DispPanel_jMenuFileOpen_ActionAdapter(
                this));
        jMenuFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
                KeyEvent.CTRL_MASK));
        jMenuEdit.setText("Edit");
        jMenuDevices.setText("Devices");
        jMenuFileNew.setText("New");
        jMenuFileNew
        .addActionListener(new DispPanel_jMenuFileNew_ActionAdapter(
                this));
        jMenuFileNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                KeyEvent.CTRL_MASK));
        jMenuFileAs.setText("Save As ...");
        jMenuFileAs
        .addActionListener(new DispPanel_jMenuFileSaveAs_ActionAdapter(
                this));
        jMenuFileImport.setText("Import ...");
        jMenuFileImport.addActionListener(new DispPanel_jMenuFileImport_ActionAdapter(
                this));
        jMenuEditRefresh.setText("Refresh Screen");
        jMenuUndo.setText("Undo");
        jMenuUndo.addActionListener(new DispPanel_jMenuEditUndo_ActionAdapter(
                this));
        jMenuUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
                KeyEvent.CTRL_MASK));
        
        CommandManager.setUndoItem(jMenuUndo);
        jMenuRedo.setText("Redo");
        jMenuRedo.addActionListener(new DispPanel_jMenuEditRedo_ActionAdapter(
                this));
        CommandManager.setRedoItem(jMenuRedo);
        jMenuDevAddTemp.setText("Add Signal Template ...");
        jMenuDevAddTemp
        .addActionListener(new DispPanel_jMenuAddTemp_ActionAdapter(
                this));
        jMenuDevices.add(jMenuDevAddTemp);
        jMenuDevEditTemp.setText("Edit Signal Template ...");
        jMenuDevEditTemp
        .addActionListener(new DispPanel_jMenuEditTemp_ActionAdapter(
                this));
        jMenuDevices.add(jMenuDevEditTemp);
        jMenuDevDelTemp.setText("Delete Signal Template ...");
        jMenuDevDelTemp
        .addActionListener(new DispPanel_jMenuDelTemp_ActionAdapter(
                this));
        jMenuDevices.add(jMenuDevDelTemp);
        jMenuDevices.addSeparator();
        jMenuDevices.add(jMenuDevIOChain);
        jMenuDevIOChain.setText("Decoder Chain ...");
        jMenuDevIOChain.addActionListener(
                new DispPanel_jMenuIOSpecChain_ActionAdapter(this));
        jMenuDevices.add(jMenuDevJmriName);
        jMenuDevJmriName.setText("Jmri Devices ...");
        jMenuDevJmriName.addActionListener(
                new DispPanel_jMenuJmriNames_ActionAdapter(this));
        jMenuDevices.addSeparator();
        jMenuDevices.add(jMenuTabBlock);
        jMenuTabBlock.setText("Block Table ...");
        jMenuTabBlock.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                BlockTable.showBlocks();
            }
        });
        /*******************************************************
         ****************************** future enhancement *****
        jMenuDevices.add(jMenuTabLocks);
        */
        jMenuTabLocks.setText("Lock Table ...");
        jMenuTabLocks.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
             LockTable.showLocks();
            }
        });
        jMenuDevices.add(jMenuTabSignal);
        jMenuTabSignal.setText("Signal Table ...");
        jMenuTabSignal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                SignalTable.showSignals();
            }
        });
        jMenuDevices.add(jMenuTabPoints);
        jMenuTabPoints.setText("Points Table ...");
        jMenuTabPoints.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                PointsTable.showPoints();
            }
        });
        jMenuDevices.add(jMenuTabRoutes);
        jMenuTabRoutes.setText("Routes Table ...");
        jMenuTabRoutes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                RouteTable.showRoutes();
            }
        });
        jMenuDevices.add(jMenuTabAll);
        jMenuTabAll.setText("All Devices ...");
        jMenuTabAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                DeviceTable.showDevices();
            }
        });
        jMenuApp.setText("Appearance");
        jMenuAppColors.setText("Colors ...");
        jMenuAppColors.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ManagerEditPane.editList(ColorList.instance().createModel())) {
                    ColorList.instance().commit();
                    Ctc.getLayout().showMe();
                }
            }
        });
        jMenuAppFonts.setText("Fonts ...");
        jMenuAppFonts.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ManagerEditPane.editList(FontList.instance().createModel())) {
                    FontList.instance().commit();
                    Ctc.getLayout().showMe();
                }
            }
        });
        jMenuAppSize.setText("Grid Size ...");
        jMenuAppSize
        .addActionListener(new DispPanel_jMenuAppSize_ActionAdapter(
                this));
        jMenuAppWidth.setText("Line Width ...");
        jMenuNetwork.setText("Network");
        jMenuDet.setText("Details");
        jMenuDetName.setText("Name ...");
        jMenuDetName
        .addActionListener(new DispPanel_jMenuDetName_ActionAdapter(
                this));
        jMenuDetEnds.setText("Track Ends ...");
        jMenuDetEnds
        .addActionListener(new DispPanel_jMenuDetEnds_ActionAdapter(
                this));
        jMenuDetEnds.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
                KeyEvent.CTRL_MASK));
        
        jMenuDetTracks.setText("Tracks ...");
        jMenuDetTracks
        .addActionListener(new DispPanel_jMenuDetTracks_ActionAdapter(
                this));
        jMenuDetTracks.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T,
                KeyEvent.CTRL_MASK));
        jMenuDetDepot.setText("Station ...");
        jMenuDetDepot
        .addActionListener(new DispPanel_jMenuDetDepot_ActionAdapter(
                this));
        jMenuDetImage.setText("Picture ...");
        jMenuDetImage
        .addActionListener(new DispPanel_jMenuDetImage_ActionAdapter(
                this));
        jMenuDetButton.setText("Button ...");
        jMenuDetButton
        .addActionListener(new DispPanel_jMenuDetButton_ActionAdapter(
        		this));
        jMenuDet.add(jMenuDetName);
        jMenuDet.add(jMenuDetEnds);
        jMenuDet.add(jMenuDetTracks);
        jMenuDet.add(jMenuDetDepot);
        jMenuDet.add(jMenuDetImage);
        jMenuDet.add(jMenuDetButton);
        Layout.setDetailMenu(jMenuDet);
        jMenuFile.add(jMenuFileNew);
        jMenuFile.add(jMenuFileOpen);
        jMenuFile.add(jMenuFileSave);
        jMenuFile.add(jMenuFileAs);
        jMenuFile.add(jMenuFileImport);
        jMenuFile.addSeparator();
        jMenuFile.add(jMenuFileExit);
        jMenuHelp.add(jMenuHelpAbout);
        jMenuBar1.add(jMenuFile);
        jMenuBar1.add(jMenuEdit);
        jMenuBar1.add(jMenuDevices);
        jMenuBar1.add(jMenuApp);
        jMenuBar1.add(jMenuDet);
        jMenuBar1.add(jMenuNetwork);
        jMenuBar1.add(jMenuTrains);
        jMenuBar1.add(jMenuJobs);
        jMenuHours.setText("Legal Hours ...");
        jMenuHours.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                LawPane.editHours(Hours.getHours());
            }
        });
        jMenuCrew.add(jMenuHours);
        jMenuBar1.add(jMenuCrew);
        jMenuBar1.add(jMenuHelp);
        jMenuApp.add(jMenuAppColors);
        jMenuApp.add(jMenuAppFonts);
        jMenuApp.add(jMenuAppWidth);
        jMenuApp.add(jMenuAppSize);
        jMenuApp.add(jMenuAppAdjustments);
        this.setJMenuBar(jMenuBar1);
        jMenuEdit.add(jMenuUndo);
        jMenuEdit.add(jMenuRedo);
        jMenuEdit.addSeparator();
        for (itor = CommandFactory.getEditList(); itor.hasMoreElements(); ) {
            pair = itor.nextElement();
            jMenuEdit.add(addCommand(new CommandFactory(pair.Command),
                    pair.Name, pair.Accelerator));
        }
        jMenuEdit.add(jMenuEditRefresh);
        jMenuEditRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Ctc.getLayout().showMe();
            }
        });
        jMenuEdit.addSeparator();
        for (itor = CommandFactory.getGridList(); itor.hasMoreElements(); ) {
            pair = itor.nextElement();
            jMenuEdit.add(addCommand(new CommandFactory(pair.Command),
                    pair.Name, pair.Accelerator));
        }
        GAccept.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Gresult = true;
                GDialog.dispose();
            }
        });
        GCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Gresult = false;
                GDialog.dispose();
            }
        });
        GPanel1.add(GwidthLab);
        GPanel1.add(GwidthText);
        GPanel2.add(GheightLab);
        GPanel2.add(GheightText);
        GPanel3.add(GAccept);
        GPanel3.add(GCancel);
        setContentPane((EditPane = new EditPane(getSize())));
        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent key) {
                EditPane.handleKey(key);
            }
        });
        
        StatNetwork = new JMenu("TrainStat Network");
        TrainStatPort = new JPanel();
        TrainStatPort.add(new JLabel("Server Port:"));
        TrainStatPort.add(ServerPort.instance());
        StatNetwork.add(TrainStatPort);
        StatNetwork.add(new StartTrainStat());
        jMenuNetworkOps.setText("Operations ...");
        jMenuNetworkOps
        .addActionListener(new DispPanel_jMenuNetworkOps_ActionAdapter(
                this));
        jMenuNetworkTSServer.setText("TrainStat Server ...");
        jMenuNetworkTSServer
        .addActionListener(new DispPanel_jMenuNetworkTSServer_ActionAdapter(
                this));
        jMenuNetwork.add(StatNetwork);
        jMenuNetwork.add(jMenuNetworkOps);
        jMenuNetwork.add(jMenuNetworkTSServer);
    }
    
    /**
     * File | Exit action performed.
     *
     * @param e
     *            is the event causing the action.
     */
    public void jMenuFileExit_actionPerformed(ActionEvent e) {
        int result;
        if (!Ctc.RootCTC.isSaved()) {
            result = JOptionPane.showConfirmDialog( (Component)null,
                    "Save existing layout?",
                    "Save on exit",
                    JOptionPane.YES_NO_CANCEL_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                if (saveLayout()) {
                    System.exit(0);
                }
            }
            else if (result == JOptionPane.NO_OPTION) {
                System.exit(0);
            }
        }
        else {
            System.exit(0);
        }
    }
    
    /**
     * Help | About action performed
     *
     * @param e
     *            is the event causing the action.
     */
    public void jMenuHelpAbout_actionPerformed(ActionEvent e) {
        DispPanel_AboutBox dlg = new DispPanel_AboutBox(this);
        Dimension dlgSize = dlg.getPreferredSize();
        Dimension frmSize = getSize();
        Point loc = getLocation();
        dlg.setLocation( (frmSize.width - dlgSize.width) / 2 + loc.x,
                (frmSize.height - dlgSize.height) / 2 + loc.y);
        dlg.setModal(true);
        dlg.pack();
        dlg.setVisible(true);
    }
    
    //Overridden so we can exit when window is closed
    protected void processWindowEvent(WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            jMenuFileExit_actionPerformed(null);
        }
    }
    
    /**
     * adds a MenuItem to the Colors pulldown.
     *
     * @param item
     *            is the MenuItem. It should already have an ActionListener
     *            attached to it.
     */
//    public void addColorItem(JMenuItem item) {
//        jMenuAppColors.add(item);
//    }
    
    /**
     * adds a MenuItem to the Width pulldown.
     *
     * @param item
     *            is the MenuItem. It should already have an ActionListener
     *            attached to it.
     */
    public void addLineItem(JMenuItem item) {
        jMenuAppWidth.add(item);
    }
    
    /**
     * adds a JMenuItem to the Appearance Menu.
     *
     * @param comp is the JMenuItem being added.
     */
    public void addAppearanceItem(JMenuItem comp) {
        jMenuApp.add(comp);
    }
    /**
     * adds a MenuItem to the Fonts pulldown.
     *
     * @param item
     *            is the MenuItem. It should already have an ActionListener
     *            attached to it.
     */
//    public void addFontItem(JMenuItem item) {
//        jMenuAppFonts.add(item);
//    }
    
    /**
     * installs a command on the command pulldown.
     *
     * @param cmdHandler
     *            is the ActionListener that creates the command.
     * @param label
     *            is its label on the pulldown
     * @param acc is the accelerator key identifier.
     * @return the newly created JMenuItem for the command pulldown.
     */
    private JMenuItem addCommand(CommandHandler cmdHandler, String label,
            int acc) {
        JMenuItem item = new JMenuItem(label);
        if (acc != 0) {
            item.setAccelerator(KeyStroke.getKeyStroke(acc, KeyEvent.CTRL_MASK));
        }
        item.addActionListener(cmdHandler);
        cmdHandler.passItem(item);
        return item;
    }
    
    /**
     * File | Open menu item handler.
     * <p>
     * This method is used to read in an existing layout description from a
     * file. If the current layout description has not been saved, it first
     * presents a Dialog box to save the current layout. Then it presents a
     * browse window for selecting a new file. If one is selected, then the
     * current layout is thrown away and the selected one read in. If no file is
     * selected, then the current file is untouched.
     * 
     * This wipes out the current layout - everything.  To merge a layout, the user
     * should use Import.
     *
     * @param e
     *            is the event triggering this method.
     */
    void jMenuFileOpen_actionPerformed(ActionEvent e) {
        int result;
        String errReport;
        Layout layout = Ctc.getLayout();
        XmlFileFilter fFilter = new XmlFileFilter(XML_EXTENSION, "XML file");
        if (checkSave(layout)) {
            JFileChooser chooser = new JFileChooser(new File(FilePath));
            chooser.addChoosableFileFilter(fFilter);
            chooser.setFileFilter(fFilter);
            chooser.setDialogTitle("Select file to read:");
            result = chooser.showOpenDialog(null);
            File fileobj = chooser.getSelectedFile();
            if (result == JFileChooser.APPROVE_OPTION) {
                if (fileobj.exists() && fileobj.canRead()) {
                    LoadFilter.instance().setFilter(LoadFilter.TRACKPLAN);
                    FileName = FilePath = fileobj.getAbsolutePath();
//                    FastClock.TheClock.setFlagValue(false);
//                    JmriName.reset();
//                    IOSpecChainManager.IOSpecChainKeeper.init();
                    Ctc.RootCTC.reNew();
                    errReport = XMLReader.parseDocument(fileobj);
                    if (errReport != null) {
                        JOptionPane.showMessageDialog( (Component)null,
                                errReport, "Open Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                    else {
                        Layout.setDetailMenu(jMenuDet);
                        
                        /************************************
                         * This is where to replace the old one
                         ************************************/
                        Ctc.RootCTC.replaceLayout(Layout.getConstruction());
                    }
                }
                else {
                    JOptionPane.showMessageDialog( (Component)null, fileobj
                            + " does not exist", "Open Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            else if (result == JFileChooser.CANCEL_OPTION) {
                System.out.println("Not opening file for reading");
            }
        }
    }
    
    /**
     * File | Save menu item handler.
     * <p>
     * This method is used to save the Layout in a file. If the Layout has not
     * been associated with a file (that is, it was created with "new"), then it
     * is associated with the selected file. If the user cancels out, then the
     * layout is not saved and the association is not made.
     *
     * @param e
     *            is the event triggering this method.
     */
    void jMenuFileSave_actionPerformed(ActionEvent e) {
        saveLayout();
    }
    
    /**
     * File | New menu item handler.
     * <p>
     * This method is used to create a new layout description. If the current
     * layout description has not been saved, it first presents a Dialog box to
     * save the current layout.
     *
     * @param e
     *            is the event triggering this method.
     */
    void jMenuFileNew_actionPerformed(ActionEvent e) {
        Layout layout = Ctc.getLayout();
        if (checkSave(layout)) {
            System.out.println("Creating new Layout");
//            JmriName.reset();
//            IOSpecChainManager.IOSpecChainKeeper.init();
//            FastClock.TheClock.setFlagValue(false);
//            Ctc.RootCTC.newLayout();
            FileName = null;
            Ctc.RootCTC.reNew();
        }
    }
    
    /**
     * File | Save As menu item handler.
     * <p>
     * This method is used to save the Layout in a different file than it is
     * associated with. If it is not associated with a file, then it will be
     * associated with the selected file.
     *
     * @param e
     *            is the event triggering this method.
     */
    void jMenuFileSaveAs_actionPerformed(ActionEvent e) {
        PrintStream saveAsStream = saveDialog("Select file to save Layout as:");
        if (null != saveAsStream) {
//          String errorReport = Ctc.RootCTC.putXML(saveAsStream, "");
            String errorReport = Ctc.RootCTC.putXML(saveAsStream);
            if (errorReport != null) {
                JOptionPane.showMessageDialog( (Component)null, errorReport,
                        "Save Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * File | Import menu item handler.
     * <p>
     * This method is used to merge an existing layout file into the current
     * layout.
     *
     * @param e
     *            is the event triggering this method.
     */
    void jMenuFileImport_actionPerformed(ActionEvent e) {
        int result;
        String errReport;
        XmlFileFilter fFilter = new XmlFileFilter(XML_EXTENSION, "XML file");
        JFileChooser chooser = new JFileChooser(new File(FilePath));
        chooser.addChoosableFileFilter(fFilter);
        chooser.setFileFilter(fFilter);
        chooser.setDialogTitle("Select file to import:");
        result = chooser.showOpenDialog(null);
        File fileobj = chooser.getSelectedFile();
        if (result == JFileChooser.APPROVE_OPTION) {
            if (fileobj.exists() && fileobj.canRead()) {
                LoadFilter.instance().setFilter(LoadFilter.LIBRARY);
                FilePath = fileobj.getAbsolutePath();
                errReport = XMLReader.parseDocument(fileobj);
                if (errReport != null) {
                    JOptionPane.showMessageDialog( (Component)null,
                            errReport, "Open Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    /*******************************
                     * This is where to merge it in!!!!
                     *******************************/
                    ImportCommand.getHandler().actionPerformed(e);
                }
            }
            else {
                JOptionPane.showMessageDialog( (Component)null, fileobj
                        + " does not exist", "Open Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        else if (result == JFileChooser.CANCEL_OPTION) {
            System.out.println("Not opening file for reading");
        }
    }
    
    /**
     * handles saving the Configuration to a file or not. If it has been saved
     * previously, then the same file name is used; otherwise, a Dialog is
     * presented to request a file name.
     *
     * @return true if the user wants to continue or false to cancel the
     *         operation.
     */
    private boolean saveLayout() {
        String errorReport;
        PrintStream outStream = null;
        if (FileName == null) {
            outStream = saveDialog("Select file to save layout in:");
            if (outStream == null) {
                return false;
            }
        }
        else {
            if (backupLayout(new File(FileName))) {
                try {
                    outStream = new PrintStream(new FileOutputStream(FileName));
                }
                catch (FileNotFoundException except) {
                    JOptionPane.showMessageDialog( (Component)null,
                            "Error creating " + FileName + " check the backup",
                            "File Error",
                            JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
            else {
                return false;
            }
        }
        
        if ( (errorReport = Ctc.RootCTC.putXML(outStream)) != null) {
            JOptionPane.showMessageDialog( (Component)null, errorReport,
                    "Save Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        outStream.close();
        if (new File(FileName).length() == 0) {
            JOptionPane.showMessageDialog( (Component)null, 
                    "Something in the trackplan has caused nothing to be written!!",
                    "Save Error", JOptionPane.ERROR_MESSAGE);
            
        }
        return true;
    }
    
    /**
     * takes an existing file and attempts to change its name
     * to the same name with ".bak" appended.  If that file
     * also exists, it is deleted.
     * 
     * @param existing is the file to be backed up.
     * @return true if the operation succeeded and false if it
     * failed.
     */
    private boolean backupLayout(File existing) {
        String newName = new String(existing.getAbsolutePath() + ".bak");
        File duplicate = new File(newName);
        if (existing.length() != 0) {
            if (duplicate.exists()) {
                if (!duplicate.isFile() || !duplicate.delete()) {
                    JOptionPane
                    .showMessageDialog( (Component)null,
                            newName
                            + " could not be deleted",
                            "Deletion Error",
                            JOptionPane.ERROR_MESSAGE);
                    return false;              
                }
            }
            // At this point, there is no old backup, so rename the
            // file.
            existing.renameTo(duplicate);
        }
        return true;
    }
    
    /**
     * handles the user interface for selecting a file and checking it.
     *
     * @param title1
     *            is the title for the file selection Dialog
     *
     * @return the PrintStream to write to (if it can be opened) or null
     */
    private PrintStream saveDialog(String title1) {
        PrintStream outStream = null;
        int result;
        File fileobj = null;
        boolean approved = false;
        XmlFileFilter fFilter = new XmlFileFilter(".xml", "XML file");
        
        JFileChooser chooser = new JFileChooser(new File(FilePath));
        chooser.addChoosableFileFilter(fFilter);
        chooser.setFileFilter(fFilter);
        chooser.setDialogTitle(title1);
        result = chooser.showSaveDialog(null);
        fileobj = chooser.getSelectedFile();
        approved = (result == JFileChooser.APPROVE_OPTION);
        if (approved) {
            if (fileobj.exists()) {
                if (fileobj.isFile()) {
                    if (fileobj.length() != 0) {
                        result = JOptionPane.showConfirmDialog( (Component)null,
                                "Overwrite " + fileobj.getName() +
                                "?",
                                "Overwrite File?",
                                JOptionPane.YES_NO_OPTION);
                        if (result == JOptionPane.YES_OPTION) {
                            FilePath = fileobj.getAbsolutePath();
                            if (!backupLayout(fileobj)) {
                                return null;
                            }
                        }
                        else {
                            return null;
                        }
                    }
                    else {
                        if (!fileobj.delete()) {
                            JOptionPane
                            .showMessageDialog( (Component)null,
                                    "0 length file " + fileobj.getName()
                                    + " could not be deleted",
                                    "Deletion Error",
                                    JOptionPane.ERROR_MESSAGE);
                            return null;
                        }
                    }
                }
                else {
                    JOptionPane.showMessageDialog( (Component)null, fileobj
                            .getName()
                            + " is not a file", "File Error",
                            JOptionPane.ERROR_MESSAGE);
                    return null;
                }
            }
        }
        else {
            return null;
        }
        try {
            if (!fileobj.createNewFile()) {
                JOptionPane.showMessageDialog( (Component)null, fileobj
                        .getName()
                        + " could not be created.", "File Error",
                        JOptionPane.ERROR_MESSAGE);
                return null;
            }
        }
        catch (IOException except) {
            JOptionPane.showMessageDialog( (Component)null, "Error creating "
                    + fileobj.getName(), "File Error",
                    JOptionPane.ERROR_MESSAGE);
            return null;
        }
        try {
            outStream = new PrintStream(new FileOutputStream(fileobj));
        }
        catch (FileNotFoundException except) {
            JOptionPane.showMessageDialog( (Component)null, "Error creating "
                    + fileobj.getName(), "File Error",
                    JOptionPane.ERROR_MESSAGE);
            
        }
        if (FileName == null) {
            FileName = new String(fileobj.getAbsolutePath());
        }
        return outStream;
    }
    
    /**
     * handles changing the Layout for either a new one or a stored one.
     * <p>
     * If the Layout has been changed, but not saved, this method prompts for
     * saving it.
     *
     * @param layout
     *            is the current Layout.
     *
     * @return true to continue or false to stop the sequence of operations.
     */
    private boolean checkSave(Layout layout) {
        int result;
        if (!Ctc.RootCTC.isSaved()) {
            result = JOptionPane.showConfirmDialog( (Component)null,
                    "Save existing layout?",
                    "Save existing layout?",
                    JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                return saveLayout();
            }
            else {
                // Its OK for the user to decide not to save his work.
            }
        }
        return true;
    }
    
    /**
     * queries the Java properties for the name of the a folder to
     * initially store data files.  The data folder can bechanged
     * from the file selector.  This is a 3 step algorithm:
     * 1. see if a property has been defined on the command line
     * 2. see if a user "home" folder has been defined
     * 3. use the folder in which designer resides
     * 
     * @return the first of the above that is defined
     */
    private final String getDefaultDataFolder() {
    	String folder = ".";
    	if (((folder = System.getProperty(DEFAULT_FOLDER_TAG)) != null) && !folder.isEmpty()) {
    		return folder;
    	}
    	if (((folder = System.getProperty(HOME_FOLDER_TAG)) != null) && !folder.isEmpty()) {
    		return folder;
    	}
    	return ".";
    }
    
    /**
     * handles presenting the JDialog when a user requests changing the Grid
     * size.
     * @param e is the ActioneEvent
     */
    public void jMenuAppSize_actionPerformed(ActionEvent e) {
        int oldWidth = Tile.getGridSize().width;
        int oldHeight = Tile.getGridSize().height;
        Gresult = false;
        GDialog = new JDialog( (Frame)null, "Specify Grid size", true);
        Container dialogContentPane = GDialog.getContentPane();
        dialogContentPane.setLayout(new FlowLayout());
        dialogContentPane.add(GPanel1);
        dialogContentPane.add(GPanel2);
        dialogContentPane.add(GPanel3);
        GDialog.setBounds(150, 150, 150, 150);
        GDialog.setVisible(true);
        if (Gresult) {
            try {
                int w = Integer.parseInt(GwidthText.getText());
                int h = Integer.parseInt(GheightText.getText());
                if ( (w > 15) && (w < 300) && (h > 15) && (h < 300)) {
                    Tile.setGridSize(w, h);
                    Ctc.getLayout().showMe();
                    oldWidth = w;
                    oldHeight = h;
                }
                else {
                    JOptionPane.showMessageDialog( (Component)null,
                            "Sizes are out of range.", "Size Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog( (Component)null,
                        "Sizes must be integers.", "Size Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        GwidthText.setText(String.valueOf(oldWidth));
        GheightText.setText(String.valueOf(oldHeight));
    }
    
    /**
     * performs actions when Devices | Add Signal Template is selected
     * @param e is the ActionEvent
     */
    public void jMenuDevAddTemp_actionPerformed(ActionEvent e) {
        boolean finished = false;
        SignalTemplate st = null;
        while (!finished) {
            st = TemplateDialog.newTemplate(st);
            if (st == null) {
                finished = true;
            }
            else {
                if (! (finished = TemplateStore.SignalKeeper.add(st))) {
                    finished = (JOptionPane
                            .showConfirmDialog( (Component)null,
                                    "Rename Signal Template?",
                                    "Signal Template Exists",
                                    JOptionPane.YES_NO_OPTION) !=
                                        JOptionPane.YES_NO_OPTION);
                    
                }
            }
        }
    }
    
    /**
     * performs actions when Devices | Delete Signal Template is selected
     * @param e is the ActionEvent
     */
    public void jMenuDevDelTemp_actionPerformed(ActionEvent e) {
        JList templates;
        if (!TemplateStore.SignalKeeper.isEmpty()) {
            templates = TemplateStore.SignalKeeper.getTemplateList();
            templates.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            if (JScrollDialog.select(templates,
            "Select Signal Template to Delete")
            && (templates.getSelectedIndex() != -1)) {
                TemplateStore.SignalKeeper.remove( (String) templates
                        .getSelectedValue());
            }
        }
    }
    
    /**
     * performs actions when Devices | Edit Signal Template is selected
     * @param e is the ActionEvent
     */
    public void jMenuDevEditTemp_actionPerformed(ActionEvent e) {
        JList templates;
        String selection;
        SignalTemplate st;
        if (!TemplateStore.SignalKeeper.isEmpty()) {
            templates = TemplateStore.SignalKeeper.getTemplateList();
            templates.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            if (JScrollDialog.select(templates,
            "Select Signal Template to Edit")
            && (templates.getSelectedIndex() != -1)) {
                selection = (String) templates.getSelectedValue();
                st = TemplateStore.SignalKeeper.find(selection);
                if ( (st = TemplateDialog.newTemplate(st)) != null) {
                    selection = st.getName();
                    if (selection != null) {
                        if (TemplateStore.SignalKeeper.find(selection) != null) {
                            TemplateStore.SignalKeeper.remove(selection);
                        }
                        TemplateStore.SignalKeeper.add(st);
                    }
                }
            }
        }
    }
    
    /**
     * performs actions when Devices | Decoder Chain is selected
     * @param e is the ActionEvent
     */
    public void jMenuDevIOSpecChain_actionPerformed(ActionEvent e) {
        if (ManagerEditPane.editList(IOSpecChainManager.IOSpecChainKeeper.createModel())) {
            IOSpecChainManager.IOSpecChainKeeper.commit();
        }
    }
    
    /**
     * performs actions when Devices | Jmri Devices is selected
     * @param e is the ActionEvent
     */
    public void jMenuDevJmriNames_actionPerformed(ActionEvent e) {
        if (ManagerEditPane.editList(JmriNameList.instance().createModel())) {
            JmriNameList.instance().commit();
        }
    }
    
    /**
     * performs actions when the Detail | Name MenuItem is selected
     * @param e is the ActionEvent
     */
    public void jMenuDetName_actionPerformed(ActionEvent e) {
        Section sec = Ctc.getLayout().getCursorSection();
        SecNameDialog.newSecName(sec);
    }
    
    /**
     * performs actions when the Detail | Track Ends MenuItem is selected
     * @param e is the ActionEvent
     */
    public void jMenuDetEnds_actionPerformed(ActionEvent e) {
        Section sec = Ctc.getLayout().getCursorSection();
        SecEdgeDialog.newSecEdge(sec);
    }
    
    /**
     * performs actions when the Detail | Tracks MenuItem is selected
     * @param e is the ActionEvent
     */
    public void jMenuDetTracks_actionPerformed(ActionEvent e) {
        Section sec = Ctc.getLayout().getCursorSection();
        //    TrackDialog.newTrack(sec);
        TrackGroupDialog.newTrackGroup(sec);
    }
    
    /**
     * performs actions when the Detail | Station MenuItem is selected
     * @param e is the ActionEvent
     */
    public void jMenuDetDepot_actionPerformed(ActionEvent e) {
        Section sec = Ctc.getLayout().getCursorSection();
        DepotDialog.newDepotIcon(sec);
    }
    
    /**
     * performs actions when the Detail | Picture MenuItem is selected
     * @param e is the ActionEvent.
     */
    public void jMenuDetImage_actionPerformed(ActionEvent e) {
        Section sec = Ctc.getLayout().getCursorSection();
        ImageItem oldImage = sec.getImage();
        ImageItem newerImage = ImageItemDialog.newImage(oldImage);
        if (oldImage != newerImage) {
            sec.hideSec();
            sec.replaceImage(newerImage);
            sec.showSec();
        }
    }
    
    /**
     * performs actions when the Detail | Button MenuItem is selected
     * @param e is the ActionEvent.
     */
    public void jMenuDetButton_actionPerformed(ActionEvent e) {
        Section sec = Ctc.getLayout().getCursorSection();
        ButtonItem oldButton = sec.getButton();
        ButtonItem newerButton = ButtonItemDialog.newButton(oldButton);
        if (oldButton != newerButton) {
            sec.hideSec();
            sec.replaceButton(newerButton);
            sec.showSec();
        }
    }
    
    /**
     * performs actions when the Network | Operations MenuItem is selected
     * @param e is the ActionEvent
     */
    public void jMenuNetworkOps_actionPerformed(ActionEvent e) {
//    	NetworkAddressDialog.select(OperationsClient.OperationsNetwork, "Operations IP Address");
    	NetworkAddressDialog.select(ServiceClient.OperationsNetwork, "Operations");
    }    
    
    /**
     * performs actions when the Network | TrainStsServer MenuItem is selected
     * @param e is the ActionEvent
     */
    public void jMenuNetworkTSServer_actionPerformed(ActionEvent e) {
    	NetworkAddressDialog.select(ServiceClient.TrainStatClient, "TrainStat Server");
    }    
}

/**
 * is an ActionAdapter for the Edit | Undo MenuItem
 */

class DispPanel_jMenuEditUndo_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuEditUndo_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        CommandFactory.undoLast();
    }
}

/**
 * is an ActionAdapter for the Edit | Redo MenuItem
 */

class DispPanel_jMenuEditRedo_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuEditRedo_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        CommandFactory.redoLast();
    }
}

/**
 * Is an ActionAdapter for the Edit | Exit MenuItem.
 */

class DispPanel_jMenuFileExit_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuFileExit_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuFileExit_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Help | About MenuItem.
 */

class DispPanel_jMenuHelpAbout_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuHelpAbout_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuHelpAbout_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the File | Open MenuItem.
 */

class DispPanel_jMenuFileOpen_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuFileOpen_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuFileOpen_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the File | Save MenuItem.
 */

class DispPanel_jMenuFileSave_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuFileSave_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuFileSave_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the File | New MenuItem.
 */

class DispPanel_jMenuFileNew_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuFileNew_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuFileNew_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the File | Save As MenuItem.
 */

class DispPanel_jMenuFileSaveAs_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuFileSaveAs_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuFileSaveAs_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the File | Import MenuItem.
 */

class DispPanel_jMenuFileImport_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuFileImport_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuFileImport_actionPerformed(e);
    }
}

/**
 * is an ActionAdaptor for the Devices | Add Signal Template MenuItem
 */

class DispPanel_jMenuAddTemp_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuAddTemp_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDevAddTemp_actionPerformed(e);
    }
}

/**
 * is an ActionAdaptor for the Devices | Edit Signal Template MenuItem
 */

class DispPanel_jMenuEditTemp_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuEditTemp_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDevEditTemp_actionPerformed(e);
    }
}

/**
 * is an ActionAdaptor for the Devices | Delete Signal Template MenuItem
 */

class DispPanel_jMenuDelTemp_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuDelTemp_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDevDelTemp_actionPerformed(e);
    }
}

/**
 * is an ActionAdaptor for the Devices |  Decoder Chain MenuItem
 */

class DispPanel_jMenuIOSpecChain_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuIOSpecChain_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDevIOSpecChain_actionPerformed(e);
    }
}

/**
 * is an ActionAdaptor for the Devices |  Jmri Devices MenuItem
 */

class DispPanel_jMenuJmriNames_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuJmriNames_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDevJmriNames_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Trains | Load MenuItem.
 */
//class DispPanel_jMenuTrainsLoad_ActionAdapter
//implements ActionListener {
//    
//    public void actionPerformed(ActionEvent e) {
//        int result;
//        String errReport;
//        XmlFileFilter fFilter = new XmlFileFilter(DispPanel.XML_EXTENSION, "XML file");
//        JFileChooser chooser = new JFileChooser(new File("."));
//        chooser.addChoosableFileFilter(fFilter);
//        chooser.setFileFilter(fFilter);
//        chooser.setDialogTitle("Select file to read:");
//        result = chooser.showOpenDialog(null);
//        File fileobj = chooser.getSelectedFile();
//        if (result == JFileChooser.APPROVE_OPTION) {
//            if (fileobj.exists() && fileobj.canRead()) {
//                if (TrainStore.TrainKeeper == null) {
//                    TrainStore.TrainKeeper = new TrainStore();
//                }
//                errReport = XMLReader.parseDocument(fileobj);
//                if (errReport != null) {
//                    JOptionPane.showMessageDialog( (Component)null,
//                            errReport, "Open Error",
//                            JOptionPane.ERROR_MESSAGE);
//                }
//            }
//            else {
//                JOptionPane.showMessageDialog( (Component)null, fileobj
//                        + " does not exist", "Open Error",
//                        JOptionPane.ERROR_MESSAGE);
//            }
//        }
//        else if (result == JFileChooser.CANCEL_OPTION) {
//            System.out.println("Not opening file for reading");
//        }
//    }
//}

/**
 * is an ActionAdapter for the Trains | Edit MenuItem.
 */

//class DispPanel_jMenuTrainsEdit_ActionAdapter
//implements ActionListener {
//    
//    public void actionPerformed(ActionEvent e) {
//        TrainStore.TrainKeeper.editData();
//    }
//}
//
/**
 * is an ActionAdapter for the Trains | Save MenuItem
 */
//class DispPanel_jMenuTrainsSave_ActionAdapter
//implements ActionListener {
//    
//    public void actionPerformed(ActionEvent e) {
//        PrintStream outStream = null;
//        int result;
//        File fileobj = null;
//        boolean approved = false;
//        XmlFileFilter fFilter = new XmlFileFilter(DispPanel.XML_EXTENSION, "XML file");
//        JFileChooser chooser = new JFileChooser(new File("."));
//        chooser.addChoosableFileFilter(fFilter);
//        chooser.setFileFilter(fFilter);
//        chooser.setDialogTitle("Select file to save lineup in:");
//        result = chooser.showSaveDialog(null);
//        fileobj = chooser.getSelectedFile();
//        approved = (result == JFileChooser.APPROVE_OPTION);
//        if (approved) {
//            if (fileobj.exists()) {
//                if (fileobj.isFile()) {
//                    result = JOptionPane.showConfirmDialog( (Component)null,
//                            "Overwrite " + fileobj.getName() +
//                            "?",
//                            "Overwrite File?",
//                            JOptionPane.YES_NO_OPTION);
//                    if (result == JOptionPane.YES_OPTION) {
//                        if (!fileobj.delete()) {
//                            JOptionPane
//                            .showMessageDialog( (Component)null,
//                                    fileobj.getName()
//                                    + " could not be deleted",
//                                    "Deletion Error",
//                                    JOptionPane.ERROR_MESSAGE);
//                            fileobj = null;
//                        }
//                    }
//                    else {
//                        fileobj = null;
//                    }
//                }
//                else {
//                    JOptionPane.showMessageDialog( (Component)null, fileobj
//                            .getName()
//                            + " is not a file", "File Error",
//                            JOptionPane.ERROR_MESSAGE);
//                }
//            }
//            try {
//                if ( (fileobj != null) && !fileobj.createNewFile()) {
//                    JOptionPane.showMessageDialog( (Component)null, fileobj
//                            .getName()
//                            + " could not be created.",
//                            "File Error",
//                            JOptionPane.ERROR_MESSAGE);
//                    fileobj = null;
//                }
//            }
//            catch (IOException except) {
//                JOptionPane.showMessageDialog( (Component)null, "Error creating "
//                        + fileobj.getName(), "File Error",
//                        JOptionPane.ERROR_MESSAGE);
//            }
//            try {
//                outStream = new PrintStream(new FileOutputStream(fileobj));
//            }
//            catch (FileNotFoundException except) {
//                JOptionPane.showMessageDialog( (Component)null, "Error creating "
//                        + fileobj.getName(), "File Error",
//                        JOptionPane.ERROR_MESSAGE);
//                
//            }
//            if (outStream != null) {
//                String errorReport = null;
//                Element root = new Element(Ctc.DocumentTag);
//                root.setAttribute(Ctc.VersionTag, DispPanel_AboutBox.getVersion());
//                Document doc = new Document(root);
//                XMLOutputter fmt = new XMLOutputter();
//                fmt.setFormat(org.jdom.output.Format.getPrettyFormat());
//                if ( (errorReport = TrainStore.TrainKeeper.putXML(root)) == null) {
//                    try {
//                        fmt.output(doc, outStream);
//                    }
//                    catch (java.io.FileNotFoundException nfExcept) {
//                        errorReport = new String("FileNotFound error writing file " +
//                                nfExcept.getLocalizedMessage());
//                    }
//                    catch (java.io.IOException ioExcept) {
//                        errorReport = new String("IO exception writing file " +
//                                ioExcept.getLocalizedMessage());
//                    }
//                }
//                if (errorReport != null) {
//                    System.out.println("Error in writing train file:" + errorReport);
//                }
//            }
//        }
//    }
//}


/**
 * is an ActionAdapter for the Appearance | Size MenuItem.
 */

class DispPanel_jMenuAppSize_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuAppSize_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuAppSize_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Details | Name MenuItem.
 */

class DispPanel_jMenuDetName_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuDetName_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDetName_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Details | Track Ends MenuItem.
 */

class DispPanel_jMenuDetEnds_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuDetEnds_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDetEnds_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Details | Tracks MenuItem.
 */

class DispPanel_jMenuDetTracks_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuDetTracks_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDetTracks_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Details | Station MenuItem.
 */

class DispPanel_jMenuDetDepot_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuDetDepot_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDetDepot_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Details | Picture MenuItem.
 */

class DispPanel_jMenuDetImage_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuDetImage_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDetImage_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Details | Button MenuItem.
 */

class DispPanel_jMenuDetButton_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuDetButton_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuDetButton_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Network | Operations MenuItem.
 */

class DispPanel_jMenuNetworkOps_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuNetworkOps_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuNetworkOps_actionPerformed(e);
    }
}

/**
 * is an ActionAdapter for the Network | TrainStat Server MenuItem.
 */

class DispPanel_jMenuNetworkTSServer_ActionAdapter
implements ActionListener {
    DispPanel adaptee;
    
    DispPanel_jMenuNetworkTSServer_ActionAdapter(DispPanel adaptee) {
        this.adaptee = adaptee;
    }
    
    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuNetworkTSServer_actionPerformed(e);
    }
}

/* @(#)DispPanel.java */
