/*
 * Name: SignalTable
 * 
 * What:
 * A derived JTable for displaying the configuration of all physical signals.  A
 * physical signal is one in which the "Layout Signal Type" box is checked and a
 * template selected.  This table presents the decoder
 * "details" for all physical signals.
 * <p>
 * Signals cannot be added or removed.  The "panel signal" characteristics are not
 * shown.  The template cannot be changed.
 *
 */
package designer.gui.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.DefaultCellEditor;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.JmriDeviceSelector;
import designer.gui.jCustom.PrefixSelector;
import designer.layout.AspectMap;
import designer.layout.JmriDevice;
import designer.layout.SignalTemplate;
import designer.layout.TemplateStore;
import designer.layout.items.AspectCommand;
import designer.layout.items.AspectTable;
import designer.layout.items.HeadStates;
import designer.layout.items.IOSpec;
import designer.layout.items.PhysicalSignal;
import designer.layout.items.SecEdge;

/**
 * A derived JTable for displaying the configuration of all physical signals.  A
 * physical signal is one in which the "Layout Signal Type" box is checked and a
 * template selected.  This table presents the decoder
 * "details" for all physical signals.
 * <p>
 * Signals cannot be added or removed.  The "panel signal" characteristics are not
 * shown.  The template cannot be changed.
 *
 * <p>Copyright: Copyright (c) 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class SignalTable extends JPanel {
 
    /**
     * the column heading for the JMRI System name prefix
     */
    private static final String PREFIX = "Prefix";
    
    /**
     * the column heading for the JMRI device type
     */
    private static final String DEVICE = "Device";
    
    /**
     * the template value for an unknown signal template
     */
    private static  final String UNKNOWN = "unknown";
    
    /**
     * the column header names
     */
    private static final String ColumnNames[] = {
        "Name",
        "Template",
        "Head",
        "Flash",
        "SignalHead",
        "Color",
        PREFIX,
        "Address",
        DEVICE,
        "Throw",
        "User",
        "Off"
    };

    /**
     * table model (see below)
     */
    SignalTableModel Model = new SignalTableModel();
    
    /**
     * memory for dimensions
     */
    private static int Height = 200;
    private static int Width = 820;

    private JScrollPane ScrollPane;
    private STable MyTable;

    public SignalTable() {
        MyTable = new STable(Model);
        ScrollPane = new JScrollPane(MyTable);
        ScrollPane.setPreferredSize(new Dimension(Width, Height));
        ScrollPane.getViewport().setBackground(MyTable.getBackground());
        ScrollPane.addComponentListener(new ComponentAdapter() {			
			@Override
			public void componentResized(ComponentEvent e) {
				Component comp = e.getComponent();
				Height = comp.getHeight();
				Width = comp.getWidth();
			}
		});
        setLayout(new BorderLayout());
        add(ScrollPane, BorderLayout.CENTER);
        Model.findErrors();
    }

    /**************************************************************************
     * The JTable derivative    
     **************************************************************************/
    private class STable extends JTable {
        public STable(SignalTableModel model) {
            super(model);
            TableColumnModel tcm;
            int col;
            setAutoCreateColumnsFromModel(false);
            setAutoResizeMode(AUTO_RESIZE_OFF);
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tcm = getColumnModel();
            col = tcm.getColumnIndex(PREFIX);
            tcm.getColumn(col).setCellEditor(PrefixSelector.getEditor());
            col = tcm.getColumnIndex(DEVICE);
            tcm.getColumn(col).setCellEditor(new DefaultCellEditor(new JmriDeviceSelector()));
            setDefaultRenderer(String.class, new ColorCellRenderer());
        }
    }

    public static boolean showSignals() {
        SignalTable table = new SignalTable();
        if (AcceptDialog.select(table, "Signals")) {
            table.Model.updateSignals();
            return true;
        }
        return false;
    }

    /**************************************************************************
     * The TableModel derivative
     **************************************************************************/
    private class SignalTableModel extends AbstractTableModel {

        /**
         * is the foreground color for indicating a problem
         */
        private final Color Problem = Color.red;

        /**
         * copies of the signal descriptions.  This will manipulate the copies until
         * the Accept button is pushed; then all changes will be written back.
         */
        private ArrayList<Aspect> Copies = new ArrayList<Aspect>();
        
        /**
         * a bit vector of Blocks that have been modified.  true means
         * the corresponding copy has been changed.
         */
        private boolean Dirty[];
        
        /**
         * a shadow of Copies, containing the Color of each value
         */
        private Color CellColor[][];
        
        /**
         * the default foreground color
         */
        private final Color DefaultColor = UIManager.getColor("Table.foreground");
        
        public SignalTableModel() {
            ArrayList<SecEdge> edges = SecEdge.getSignals();
            PhysicalSignal sig;
            SignalTemplate template;
            AspectMap templateAspects;
            for (SecEdge e : edges) {
                sig = e.getSignal().getSigDescription();
                template = TemplateStore.SignalKeeper.find(sig.getTemplateName());
                if (template != null) {
                    templateAspects = template.getAspectMap();
                    if (templateAspects != null) {
                        for (int heads = 0; heads < templateAspects.getHeadCount(); ++heads) {
                            copyHead(e, sig, heads);
                        }
                    }
                    else {
                        copyHead(e, sig, 0);
                    }
                }
                else {
                    copyHead(e, sig, 0);
                }
            }
            Dirty = new boolean[Copies.size()];
            CellColor = new Color[Copies.size()][ColumnNames.length];
            for (int edge = 0; edge < Copies.size(); ++edge) {
                for (int i = 0; i < CellColor[edge].length; i++) {
                    CellColor[edge][i] = DefaultColor;
                }
            }
        }

        @Override
        public int getColumnCount() {
            return ColumnNames.length;
        }

        @Override
        public int getRowCount() {
            return Copies.size();
        }

        @Override
        public Object getValueAt(int row, int column) {
            switch (column) {
            case 0: // Signal Name
                if (Copies.get(row).Name != null) {
                    return Copies.get(row).Name;
                }
                break;
            case 1: // template name
                if (Copies.get(row).Name != null) {
                    return Copies.get(row).Template;
                }
                break;
            case 2: // head number
                if (Copies.get(row).HeadNo != null) {
                    return Copies.get(row).HeadNo;
                }
                break;
            case 3: // software flashing
                if (Copies.get(row).HeadNo != null) {
                    return Copies.get(row).Flash;
                }
                return false;
            case 4: // SignalHead User name
                if (Copies.get(row).HeadUser != null) {
                    return Copies.get(row).HeadUser;
                }
                break;
            case 5: // Aspect color
                    return Copies.get(row).Aspect;
            case 6: // JMRI prefix
                if (Copies.get(row).Control != null) {
                    return Copies.get(row).Control.getPrefix();
                }
                break;
            case 7: // JMRI address
                if (Copies.get(row).Control != null) {
                    return Copies.get(row).Control.getAddress();
                }
                break;
            case 8: // JMRI device
                if (Copies.get(row).Control != null) {
                    return Copies.get(row).Control.getDeviceType();
                }
                break;
            case 9: // Throw/Close
                if (Copies.get(row).Control != null) {
                    return Copies.get(row).Control.getCommand();
                }
                return false;
            case 10: // Decoder Username
                if (Copies.get(row).Control != null) {
                    return Copies.get(row).Control.getUserName();
                }
                break;
            case 11: // Off
                if (Copies.get(row).Control != null) {
                    return Copies.get(row).Control.getExitcommand();
                }
                return false;
           }
            return new String("");
        }

        @Override
        public String getColumnName(int i) {
            return ColumnNames[i];
        }
        
        @Override
        public boolean isCellEditable(int row, int column) {
        	IOSpec io = Copies.get(row).Control;
        	switch (column) {
        	case 0: // Signal Name
        		return (Copies.get(row).Name != null);

        	case 1: // template name
        		return false;

        	case 2: // head number
        		return false;

        	case 3: // software flashing
        		return (Copies.get(row).HeadNo != null);

        	case 4: // SignalHead User name
        		return (Copies.get(row).HeadNo != null);

        	case 5: // Aspect color
        		return false;

        	case 6: // JMRI Prefix
        	case 7: // JMRI address
        		if (io != null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
        		}
        		break;

        	case 8: // JMRI Device
        		if (io != null) {
        			return (io.getPrefix() == null) || io.getPrefix().equals("none");
        		}
        		break;

        	default:
        	}
        	return true;
        }
        
        @Override
        public Class<?> getColumnClass(int column) {
            switch (column) {
            case 3: // software flashing
            case 9: // Throw/Close
            case 11: // Off
                return Boolean.class;
            }
            return String.class;
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
            IOSpec io;
            switch (column) {
            case 0: // Signal Name
                Copies.get(row).Name = (String) value;
                break;
            case 1: // template name
                break;
            case 2: // head number
                break;
            case 3: // software flashing
                Copies.get(row).Flash = (Boolean) value;
                break;
            case 4: // SignalHead User name
                Copies.get(row).HeadUser = (String) value;
                break;
            case 5: // Aspect color
                break;
            case 6: // JMRI prefix
                io = Copies.get(row).Control;
                if (io == null) {
                    io = new IOSpec();
                    Copies.get(row).Control = io;
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoder(row);
                break;
            case 7: // JMRI address
                io = Copies.get(row).Control;
                if (io == null) {
                    io = new IOSpec();
                    Copies.get(row).Control = io;
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoder(row);
                break;
            case 8: // JMRI Device
                io = Copies.get(row).Control;
                if (io == null) {
                    io = new IOSpec();
                    Copies.get(row).Control = io;
                    io.setCommand(false);
                }
                io.setDeviceType((JmriDevice) value);
                checkDecoder(row);
                break;
            case 9: // Throw/Close
                io = Copies.get(row).Control;
                if (io == null) {
                    io = new IOSpec();
                    Copies.get(row).Control = io;
                }
                io.setCommand((Boolean) value);
                checkDecoder(row);
                break;
            case 10: // Decoder Username
                io = Copies.get(row).Control;
                if (io == null) {
                    io = new IOSpec();
                    Copies.get(row).Control = io;
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoder(row);
                break;
            case 11: // Off
                if (Copies.get(row).Control == null) {
                    io = new IOSpec();
                    Copies.get(row).Control = io;
                }
                else {
                    io =  Copies.get(row).Control;
                }
                io.setExitCommand((Boolean) value);
                break;
            default:
           }
           Dirty[row] = true;
        }
        
        /**
         * this method updates the signals that have been modified
         */
        public void updateSignals() {
            Aspect aspect;
            PhysicalSignal sig;
            AspectTable aTable;
            IOSpec old;
            IOSpec newSpec;
            AspectCommand command;
            HeadStates states;
            for (int decoder = 0; decoder < Copies.size(); ++decoder) {
            	if (Dirty[decoder]) {
            		aspect = Copies.get(decoder);
            		sig = aspect.MySignal;
            		aTable = sig.getCommandList();
            		if (aTable == null) {
            			aTable = new AspectTable();
            			sig.setCommandTable(aTable);
            		}
            		states = aTable.getList(aspect.Head);
            		if (states == null) {
            			states = new HeadStates();
            			aTable.addHead(states);
            		}
            		if (aspect.Name != null) {
            			aspect.Edge.getSignal().setSigName(aspect.Name);
            			sig.setTemplateName(aspect.Template);
            		}
            		if (aspect.HeadNo != null) {
            			states.setAssist(aspect.Flash);
            			states.setName(aspect.HeadUser);            			
            		}
            		newSpec = aspect.Control;
            		if ((newSpec != null) && newSpec.isComplete()) {
            			//            			aTable = sig.getCommandList();
            			//            			if (aTable == null) {
            			//            				aTable = new AspectTable();
            			//            				sig.setCommandTable(aTable);                            
            			//            			}
            			//            			states = aTable.getList(aspect.Head);
            			//            			if (states == null) {
            			//            				states = new HeadStates();
            			//            				aTable.addHead(states);
            			//            			}
            			old = states.findDecoders(aspect.Aspect);
            			if (old == null) {
            				command = new AspectCommand(aspect.Aspect);
            				command.setCommand(aspect.Control);
            				states.addElement(command);
            			}
            			else {
            				old.setPrefix(newSpec.getPrefix());
            				old.setAddress(newSpec.getAddress());
            				old.setDeviceType(newSpec.getDeviceType());
            				old.setCommand(newSpec.getCommand());
            				old.setUserName(newSpec.getUserName());
            				old.setExitCommand(newSpec.getExitcommand());
            			}
            		}
            		sig.setSaved(false);
            	}
            }
        }
        
        /**
         * this method sets the foreground color on cells in the JTable
         * @param row is the row the cells are on
         * @param start is the first cell to set the color
         * @param stop is one after the last cell to color
         * @param color is the color of the foreground
         */
        private void showError(int row, int start, int stop, Color color) {
            for (; start < stop; start++) {
                CellColor[row][start] = color;
            }            
        }
        
        /**
         * checks a decoder definition for being complete.  The foreground color
         * of an incomplete decoder is set to red
         * @param row is the index of the aspect containing the decoder
         */
        private void checkDecoder(int row) {
            Color color = Problem;
            if (Copies.get(row).Control != null) {
                if (Copies.get(row).Control.isComplete()) {
                    color = DefaultColor;
                }
                showError(row, 6, 11, color);
            }
        }
        
        /**
         * scans the Signal data and flags any problems
         */
        public void findErrors() { 
            for (int row = 0; row < Copies.size(); ++row) {
                if (UNKNOWN.equals(Copies.get(row).Aspect)) {
                    showError(row, 0, ColumnNames.length, Problem);
                }
                checkDecoder(row);
            }
        }
        
        /**
         * determines what color to use for the foreground on a table cell
         * @param row is the row index of the cell
         * @param column is the column index of the cell
         * @return the COlor
         */
        public Color getCellColor(int row, int column) {
            return CellColor[row][column];
        }
 
        /**
         * copies the decoder information for a SignalHead
         * @param e is the SecEdge
         * @param sig is the PhysicalSignal
         * @param head is the head number of the SignalHead
         */
        private void copyHead(SecEdge e, PhysicalSignal sig, int head) {
            SignalTemplate template = TemplateStore.SignalKeeper.find(sig.getTemplateName());
            AspectMap templateAspects;
            String aspectName;
            boolean first = (head == 0);
            boolean h = true;
            if (template != null){
                templateAspects = template.getAspectMap();
                if (templateAspects != null){
                    for (Enumeration<String> eAspect = templateAspects.getState(head); eAspect.hasMoreElements(); ) {
                        aspectName = eAspect.nextElement();
                        if (first) {
                            Copies.add(new Aspect(e, e.getSignal().getSigName(), Integer.toString(head), head, aspectName));
                            first = false;
                            h = false;
                        }
                        else if (h) {
                            Copies.add(new Aspect(e, null, Integer.toString(head), head, aspectName));
                            h = false;
                        }
                        else {
                            Copies.add(new Aspect(e, null, null, head, aspectName));
                        }
                    }
                }
                else {
                    Copies.add(new Aspect(e, e.getSignal().getSigName(), Integer.toString(head), head, UNKNOWN));
                }
            }
            else {
                Copies.add(new Aspect(e, e.getSignal().getSigName(), Integer.toString(head), head, UNKNOWN));
            }
        }
    }
    /**************************************************************************
     * a data structure for holding the information about a signal aspect.  This
     * is a database entry expanded.
     *************************************************************************/
    private class Aspect {
        public SecEdge Edge;
        public PhysicalSignal MySignal;
        public String Name;
        public String Template;
        public String HeadNo;
        public int Head;
        public boolean Flash;
        public String HeadUser;
        public String Aspect;
        public IOSpec Control;
        
        public Aspect(SecEdge edge, String name, String headNo, int head, String aspect) {
            IOSpec decoder;
            AspectTable aTable;
            HeadStates states = null;
            Edge = edge;
            MySignal = edge.getSignal().getSigDescription();
            aTable = MySignal.getCommandList();
            Name = name;
            if (name != null) {
                Template = MySignal.getTemplateName();
            }
            if (aTable != null) {
            	states = aTable.getList(head);
            }
            if ((headNo != null) && (states != null)) {
            	HeadUser = states.getName();
            	Flash = states.getAssist();
            }
            HeadNo = headNo;
            Head = head;
            Aspect = aspect;
            if (states != null) {
                decoder = states.findDecoders(aspect);
                if (decoder != null) {
                    Control = (IOSpec) decoder.copy();
                }
            }
        }
    }
    
    /**************************************************************************
     * A cell renderer associated with setting the foreground color
     **************************************************************************/
    private class ColorCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            setForeground(Model.getCellColor(row, column));
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}
/* @(#)SignalTable.java */