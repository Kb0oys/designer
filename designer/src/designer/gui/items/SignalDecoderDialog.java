/* Name: SignalDecoderDialog.java
 *
 * What:
 *  This class constructs the JDialog for entering or editing the information
 *  needed for identifying a decoder that controls a signal.
 */
package designer.gui.items;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.HeadPanel;
import designer.gui.jCustom.IOPanel;
import designer.layout.AspectMap;
import designer.layout.items.AspectTable;
import designer.layout.items.HeadStates;
import designer.layout.items.IOSpec;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.BevelBorder;

import java.util.Enumeration;
import java.util.Vector;

/**
 * is a JDialog for collecting information for or editing information for
 * identifying a decoder controlling a signal.  The goal is to associate
 * one (or more) decoder positions for each presentation for each head.  It
 * uses a JTabbedPane for each head.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2015</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class SignalDecoderDialog extends JPanel {
    
    /**
     *  The JPanels for each head.
     */
    private Vector<HeadPanel> HPanel = new Vector<HeadPanel>();
    
    /**
     * a JCheckBox for selecting the approach block as the approach
     * light trigger (checked) or a user defined trigger (unchecked)
     */
	private JCheckBox AppBlock;
	
	/**
	 * an IOPanel which is used to specify a custom trigger for
	 * approach lighting
	 */
	private IOPanel AppTrigger;

	/**
     * is the constructor.
     *
     * @param map is the AspectMap used with the the Signal.  This JDialog
     * will gather the information for each state of each head.  Null is
     * not legal.
     *
     * @param cmds is the list of decoder commands to be edited or null to
     * be created.
     *
     * @see designer.layout.AspectMap
     * @see designer.layout.items.AspectTable
     */
    private SignalDecoderDialog(AspectMap map, AspectTable cmds) {
    	JTabbedPane tabPane = new JTabbedPane();
    	JPanel app;
    	IOSpec trigger;
    	if (map == null) {
    		JOptionPane.showMessageDialog( (Component)null,
    				"Aspects must be defined before decoders can be defined.",
    				"Definition Error",
    				JOptionPane.ERROR_MESSAGE
    				);
    	}
    	else {
    		int heads = map.getHeadCount();
    		HeadPanel hPanel;

    		for (int i = 0; i < heads; ++i) {
    			// construct a JPanel for each head.
    			if (cmds == null) {
    				hPanel = new HeadPanel(map.getState(i), null);
    			}
    			else {
    				hPanel = new HeadPanel(map.getState(i), cmds.getList(i));
    			}
    			HPanel.add(hPanel);
    			tabPane.add("Head " + i, hPanel);
    		}
    		if (map.getApproach()) {
    			trigger = cmds == null ? null : cmds.getApproachTrigger();
    			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    			add(tabPane); 
    			app = new JPanel();
    			app.setLayout(new BoxLayout(app, BoxLayout.X_AXIS));
    			app.setBorder(new BevelBorder(BevelBorder.LOWERED));
    			AppBlock = new JCheckBox("Approach Block", trigger == null);
    			AppBlock.addActionListener(new ActionListener() {
    				public void actionPerformed(ActionEvent ce) {
    					if (AppBlock.getModel().isSelected()) {
    						AppTrigger.setEnabled(false);
    					}
    					else {
    						AppTrigger.setEnabled(true);
    					}
    				}
    			});
    			app.add(AppBlock);
    			AppTrigger = new IOPanel(trigger, "Approach Trigger", false);
    			AppTrigger.setEnabled(trigger != null);
    			app.add(AppTrigger);
    			add(app);
    		}
    		else {
    			add(tabPane);
    		}
    	}
    }
    
    /**
     * creates a JDialog for entering information about the signal decoders.
     *
     * @param map is the AspectMap containing the aspects for each indication.
     *
     * @param cmds is the decoder commands for the signal.
     *
     * @return a new AspectTable containing the information filled into the
     * JDialog.
     *
     * @see designer.layout.AspectMap
     * @see designer.layout.items.AspectTable
     */
    static public AspectTable getDecoders(AspectMap map, AspectTable cmds) {
        SignalDecoderDialog dialog = new SignalDecoderDialog(map, cmds);
        AspectTable newTable;
        HeadStates hs;
        
        if (AcceptDialog.select(dialog, "Signal Head Definitions")) {
            newTable = new AspectTable();
            for (Enumeration<HeadPanel> e = dialog.HPanel.elements(); e.hasMoreElements(); ) {
                hs = e.nextElement().getCommands();
                newTable.addHead(hs);
            }
            if (dialog.AppBlock != null) {
            	newTable.setApproachTrigger((dialog.AppBlock.isSelected()) ? null : dialog.AppTrigger.getSpec());
            }
            return newTable;
        }
        return cmds;
    }
}
/* @(#)SignalDecoderDialog.java */