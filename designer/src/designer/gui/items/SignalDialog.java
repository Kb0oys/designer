/*
 * Name: SignalDialog
 *
 * What: This file contains a Singleton, Factory object for constructing
 *  the JDialog for setting the properties of Signals.
 */
package designer.gui.items;

import designer.gui.CtcFont;
import designer.gui.frills.FrillLoc;
import designer.layout.items.AspectTable;
import designer.layout.items.Edge;
import designer.layout.items.SecSignal;
import designer.layout.items.PanelSignal;
import designer.layout.items.PhysicalSignal;
import designer.layout.SignalTemplate;
import designer.layout.TemplateStore;
import designer.gui.jCustom.JScrollList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *  This file contains a Singleton, Factory object for constructing
 *  the JDialog for setting the properties of Signals.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class SignalDialog {
  private static final String Title = "Signal definition";

  /**
   * the JLabel for the Name field.
   */
  private JLabel NameLab = new JLabel("Name:");

  /**
   * a placeholder for the Signal information.
   */
  private JTextField NameField;

  /**
   * the "Lamp" button.
   */
  private JRadioButton Lamp = new JRadioButton("Lamp");

  /**
   * the value of the Lamp/Semaphore selection.
   */
  private boolean IsLamp;

  /**
   * the "Semaphore" button.
   */
  JRadioButton Semaphore = new JRadioButton("Semaphore");

  /**
   * the selection list of number of heads.
   */
  private String[] HeadTags = {
      "None",
      "1",
      "2",
      "3"
  };

  /**
   * the tags, as a list.
   */
  private JList<?> HeadList = new JList(HeadTags);

  /**
   * the JPanel with the Icon information.
   */
  private SignalPanel IconInfo;

  /**
   * the JPanel with the actual signal information.
   */
  private PhysicalPanel PhysicalInfo;

  /**
   * the JPanel with the Icon and Physical Panels
   */
  private JPanel InnerPanel = new JPanel();
  
// The following are common to all Dialogs.

  // Result button from JDialog box
  private boolean Result;

  // The button to push to change the line width
  private JButton Accept = new JButton("Accept");

  // The button to Cancel the change
  private JButton Cancel = new JButton("Cancel");

  // The Buttons, as a group
  private JPanel Buttons = new JPanel();

  // The JDialog used for setting the name values.
  JDialog Dialog = new JDialog( (Frame)null, Title, true);

  /**
   * is the class constructor.
   *
   * @param sig contains the initial values of the SecSignal.
   *
   * @see designer.layout.items.SecSignal
   */
  public SignalDialog(SecSignal sig) {
    JPanel panel1 = new JPanel();
    JPanel common = new JPanel();
    ButtonGroup sigType = new ButtonGroup();
    JPanel heads = new JPanel();

    Container dialogContentPane = Dialog.getContentPane();

    if (sig == null) {
      NameField = new JTextField(10);
      IconInfo = new SignalPanel(null);
      PhysicalInfo = new PhysicalPanel(null);
    }
    else {
      if (sig.getSigName() == null) {
        NameField = new JTextField(10);
      }
      else {
        NameField = new JTextField(sig.getSigName());
        if (sig.getSigName().length() < 10) {
            NameField.setColumns(10);
        }
      }
      IconInfo = new SignalPanel(sig.getSigIcon());
      PhysicalInfo = new PhysicalPanel(sig.getSigDescription());
      if ( (TemplateStore.SignalKeeper.find(sig.getSigName()) == null) &&
          (sig.getSigIcon() != null)) {
        if (sig.getSigIcon().isLampType()) {
          Lamp.setSelected(true);
          IsLamp = true;
        }
        else {
          Semaphore.setSelected(true);
          IsLamp = false;
        }
        HeadList.setSelectedIndex(sig.getSigIcon().getHeadCount());
      }
    }

    panel1.add(NameLab);
    panel1.add(NameField);

    Lamp.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent ie) {
        if (ie.getStateChange() == ItemEvent.DESELECTED) {
          IsLamp = false;
        }
        else {
          IsLamp = true;
        }
      }
    });
    Semaphore.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent ie) {
        if (ie.getStateChange() == ItemEvent.DESELECTED) {
          IsLamp = true;
        }
        else {
          IsLamp = false;
        }
      }
    });
    sigType.add(Lamp);
    sigType.add(Semaphore);
    heads.add(new JLabel("Number of signal heads:"));
    heads.add(HeadList);

    common.setLayout(new FlowLayout());
    common.add(panel1);
    common.add(Lamp);
    common.add(Semaphore);
    common.add(heads);

    Buttons.add(Accept);
    Buttons.add(Cancel);
    Accept.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        Result = true;
        Dialog.dispose();
      }
    });
    Cancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        Result = false;
        Dialog.dispose();
      }
    });
    dialogContentPane.setLayout(new BoxLayout(dialogContentPane,
                                              BoxLayout.Y_AXIS));
    InnerPanel.setLayout(new BoxLayout(InnerPanel, BoxLayout.Y_AXIS));
    InnerPanel.add(common);
    InnerPanel.add(IconInfo);
    InnerPanel.add(PhysicalInfo);
    dialogContentPane.add(InnerPanel);
    dialogContentPane.add(Buttons);   
    Dialog.pack();
    
    // adding the Lock screen after the Pack() makes the JScrollPane smaller:  the
    // size of the JTabPane is defined by InnerPanel and the JScrollPane is sized to
    // conform to the JTabPane
    Dialog.setVisible(true);
  }

  /**
   * creates a JDialog for entering information about a SecSignal.
   *
   * @param sig is the initial value of the SecSignal.
   *
   * @return a new SecSignal or null (if no changes were made).
   *
   * @see designer.layout.items.SecSignal
   */
  static public SecSignal newSignal(SecSignal sig) {
    SecSignal newSig;
    PanelSignal newIcon;

    SignalDialog dialog = new SignalDialog(sig);
    if (dialog.Result) {
      newSig = new SecSignal();
      newIcon = dialog.IconInfo.getIconInfo();
      if (newIcon != null) {
        newIcon.setParms(dialog.IsLamp, dialog.HeadList.getSelectedIndex());
      }
      newSig.setSigIcon(newIcon);
      newSig.setSigDescription(dialog.PhysicalInfo.getPhysicalInfo());
      newSig.setSigName(dialog.NameField.getText());
      return newSig;
    }
    return null;
  }

  /**
   * A JPanel for showing and accepting changes to the Signal Icon (PanelSignal).
   *
   */
  class SignalPanel
      extends JPanel {
    /**
     * checkbox for the CTC Panel Options.
     */
    private JCheckBox CtcPanel;

    /**
     * selection box for placement in GridTile.
     */
    private JScrollList Location;

    /**
     * the JList containing the Names of the Edges
     */
    private JList<?> Direction = new JList(Edge.EDGENAME);

    /**
     * selection box for the orientation of the Icon.
     */
    private JPanel Orientation = new JPanel();

    /**
     * the constructor.
     *
     * @param icon is the signal icon being described.  It can be null.
     */
    public SignalPanel(PanelSignal icon) {
      FrillLoc location = null;
      int orientation = CtcFont.NOT_FOUND;
      boolean haveCtc = true;
      JPanel locPanel = new JPanel();
      JPanel orPanel = new JPanel();

      setLayout(new FlowLayout());

      if (icon != null) {
        location = icon.getLocation();
        orientation = icon.getSigOrient();
      }
      Location = FrillLoc.newFrillLocPanel(location);
      Direction.setSelectedIndex(orientation);
      if ( (location == null) || (orientation == CtcFont.NOT_FOUND)) {
        haveCtc = false;
        Direction.setEnabled(false);
        Location.setEnabled(false);
      }
      Orientation.add(Direction);
      CtcPanel = new JCheckBox("Panel", haveCtc);
      CtcPanel.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ce) {
          if (CtcPanel.getModel().isSelected()) {
            Direction.setEnabled(true);
            Location.setEnabled(true);
          }
          else {
            Direction.setEnabled(false);
            Location.setEnabled(false);
          }
        }
      });

      locPanel.add(new JLabel("Placement in Tile"));
      locPanel.add(Location);
      orPanel.add(new JLabel("Facing Direction"));
      orPanel.add(Orientation);
      add(CtcPanel);
      add(locPanel);
      add(orPanel);
    }

    /**
     * Construct a PanelSignal from the information contained.
     *
     * @return a new PanelSignal, if the checkbox was checked; otherwise,
     * return null.
     */
    public PanelSignal getIconInfo() {
      PanelSignal icon = null;
      if (CtcPanel.getModel().isSelected()) {
        icon = new PanelSignal();
        icon.setLocation(FrillLoc.newFrillLoc(Location.getSelection()));
        icon.setOrient(Direction.getSelectedIndex());
      }
      if ((icon != null) &&
              ((icon.getLocation() == null) || (icon.getSigOrient() < 0))) {
          return null;
      }
      return icon;
    }
  }

  /**
   * A JPanel for showing and accepting changes to the actual Signal
   * (PhysicalSignal).
   */
  class PhysicalPanel
      extends JPanel {
    /**
     * checkbox for the CTC Panel Options.
     */
    private JCheckBox EnablePhys;

    /**
     * JButton to pull up the detailed information panel.
     */
    private JButton Details = new JButton("Details");

    /**
     * The list of Signal types.
     */
    private JList<?> TypeList = TemplateStore.SignalKeeper.getTemplateList();

    /**
     * the index of the selected SignalTemplate.
     */
    private int TemplateIndex;

    /**
     * Something to scroll the list in.
     */
    private JScrollPane TypePane = new JScrollPane(TypeList);

    /**
     * The decoder commands.
     */
    private AspectTable DecoderCmds = null;

    /**
     * constructs information about the signal on the layout.
     *
     * @param sig has the initial values.  sig may be null.
     */
    public PhysicalPanel(PhysicalSignal sig) {
      SignalTemplate template = null;
      boolean havePhys = false;

      if (sig != null) {
        TemplateIndex = TemplateStore.SignalKeeper.indexOf(sig.getTemplateName());
        HeadList.setSelectedIndex(countHeads(sig.getTemplateName()));
        havePhys = true;
        TypeList.setEnabled(true);
        Details.setEnabled(true);
        DecoderCmds = sig.getCommandList();
      }
      else {
        TemplateIndex = -1;
        HeadList.setSelectedIndex( -1);
        TypeList.setEnabled(false);
        Details.setEnabled(false);
      }
      EnablePhys = new JCheckBox("Layout Signal Type:", havePhys);
      EnablePhys.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ce) {
          if (EnablePhys.getModel().isSelected()) {
            TypeList.setEnabled(true);
            if (TemplateIndex > -1) {
              Details.setEnabled(true);
            }
          }
          else {
            TypeList.setEnabled(false);
            Details.setEnabled(false);
          }
        }
      });
      TypeList.setSelectedIndex(TemplateIndex);

      if (TemplateIndex >= 0) {
        template = TemplateStore.SignalKeeper.elementAt(TemplateIndex);
        IsLamp = template.isLights();
      }
      setWidgets(HeadList.getSelectedIndex());
      Details.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) {
          String template = (String) TypeList.getSelectedValue();
          DecoderCmds = SignalDecoderDialog.getDecoders(
              TemplateStore.SignalKeeper.find(template).getAspectMap(),
              DecoderCmds);
        }
      });

      TypeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      TypeList.addListSelectionListener(new ListSelectionListener() {
        public void valueChanged(ListSelectionEvent le) {
          TemplateIndex = TypeList.getSelectedIndex();
          if (TemplateIndex < 0) {
            Lamp.setSelected(false);
            Semaphore.setSelected(false);
            setWidgets(0);
          }
          else {
            if (TemplateStore.SignalKeeper.elementAt(TemplateIndex).isLights()) {
              Lamp.setSelected(true);
            }
            else {
              Semaphore.setSelected(true);
            }
            setWidgets(TemplateStore.SignalKeeper.
                       elementAt(TemplateIndex).getNumHeads());
          }
        }
      }
      );
      add(EnablePhys);
      add(TypePane);
      add(Details);
    }

    /**
     * returns the number of signal heads in a SignalTemplate.
     *
     * @param name is the name of the SignalTemplate.  null is allowed.
     *
     * @return the number of heads (0-3).  If name does not identify a
     * SignalTemplate, then 0 is returned.  If it identifies a SignalTemplate
     * but the template has 0 heads, 0 is returned; otherwise, the number
     * of heads is returned.
     */
    int countHeads(String name) {
      SignalTemplate template = TemplateStore.SignalKeeper.find(name);
      int numHeads = 0;
      if (template != null) {
        numHeads = template.getNumHeads();
      }
      return numHeads;
    }

    /**
     * enables and disables widgets, based on the number of heads.  If
     * the signal has any heads, then the Details button is enabled and
     * the type buttons and head selection are disabled because the
     * SignalTemplate determines those values.
     *
     * @param heads is the number of heads.
     */
    void setWidgets(int heads) {
      if (heads > 0) {
        Details.setEnabled(true);
        Lamp.setEnabled(false);
        Semaphore.setEnabled(false);
        HeadList.setSelectedIndex(heads);
        HeadList.setEnabled(false);

      }
      else {
        Details.setEnabled(false);
        Lamp.setSelected(false);
        Lamp.setEnabled(true);
        Semaphore.setSelected(false);
        Semaphore.setEnabled(true);
        HeadList.setSelectedIndex( -1);
        HeadList.setEnabled(true);
      }
    }

    /**
     * Construct a PhysicalSignal from the information contained.
     *
     * @return a new PhysicalSignal, if the checkbox was checked; otherwise,
     * return null.
     */
    public PhysicalSignal getPhysicalInfo() {
      PhysicalSignal sig = null;
      if (EnablePhys.getModel().isSelected()) {
        String selected = (String) TypeList.getSelectedValue();
        if (countHeads(selected) > 0) {
          sig = new PhysicalSignal();
          sig.setTemplateName(selected);
          sig.setCommandTable(DecoderCmds);
        }
      }
      return sig;
    }
  }
  
}
/* @(#)SignalDialog.java */