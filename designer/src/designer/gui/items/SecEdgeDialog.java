/*
 * Name: SecEdgeDialog
 *
 * What: This file contains a Singleton, Factory object for constructing
 *  the JDialog for setting the values of Section Edges.
 */
package designer.gui.items;

import java.awt.GridLayout;
import javax.swing.JPanel;

import designer.gui.jCustom.AcceptDialog;
import designer.layout.items.Edge;
import designer.layout.items.Section;

/**
 *  This file contains a Singleton, Factory object for constructing
 *  the JDialog for setting the values of Section Edges.
 *  <p>
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class SecEdgeDialog extends JPanel {
  static private final String Title = "Specify the Edges:";

  /**
   * The panels holding the SecEdge contents.
   */
  private EdgePanel Edges[] = new EdgePanel[Edge.EDGENAME.length];

  /**
   * is the class constructor.
   *
   * @param sec is the Section containing the SecEdge.
   *
   * @see Section
   */
  public SecEdgeDialog(Section sec) {
    setLayout(new GridLayout(0, 2));
    for (int index = (Edges.length - 1); index >= 0; --index) {
      if (sec.countTracks(index) > 0) {
        Edges[index] = new EdgePanel(sec.getEdge(index));
        add(Edges[index]);
      }
    }
  }

  /**
   * creates a JDialog for all SecEdges and fills in the fields of the
   * SecEdges.  This method assumes that the Section is not null.
   *
   * @param sec is the Section containing the SecEdges.
   *
   * @return true if the operation succeeds or false if it fails.
   *
   * @see Section
   */
  static public boolean newSecEdge(Section sec) {
    SecEdgeDialog dialog = new SecEdgeDialog(sec);
    if (AcceptDialog.select(dialog, Title)) {
      sec.hideSec();
      for (int index = 0; index < Edge.EDGENAME.length; ++index) {
        if (dialog.Edges[index] != null) {
          dialog.Edges[index].create();
        }
      }
      sec.showSec();
      return true;
    }
    return false;
  }
}
/* @(#)SecEdgeDialog.java */