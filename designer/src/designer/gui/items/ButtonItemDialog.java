/* Name: ButtonItemDialog.java
 *
 * What:
 *  ButtonItemDialog defines a JPanel (with Accept and Cancel buttons) for
 *  creating or editing a Button to be painted in a GridTile.  A Button
 *  contains 4 pieces of information:
 *  <ol>
 *  <li>an IOSpec to be thrown/closed when clicked on</li>
 *  <li>an image to be displayed when the primary IOSpec is selected</li>
 *  <li>an image to be displayed when the alternate IOSpec is selected</li>
 *  <li>a timer to flip the IOSpec from Closed to Thrown, simulating
 *  a momentary button push.</li>
 *  </ol>
 */
package designer.gui.items;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.FileNamePanel;
import designer.gui.jCustom.IOPanel;
import designer.layout.items.ButtonItem;
import designer.layout.items.IOSpec;

/**
 * ButtonItemDialog defines a JPanel (with Accept and Cancel buttons) for
 * creating or editing a Button to be painted in a GridTile. A Button contains 4
 * pieces of information:
 * <ol>
 * <li>an IOSpec to be thrown/closed when clicked on</li>
 * <li>an image to be displayed when the primary IOSpec is selected</li>
 * <li>an image to be displayed when the alternate IOSpec is selected</li>
 * <li>a timer to flip the IOSpec from Closed to Thrown, simulating a momentary
 * button push.</li>
 * </ol>
 *
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2021, 2022
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */

public class ButtonItemDialog extends JPanel {

	/**
	 * the StatusOnly flag
	 */
	private JCheckBox StatusCheckBox;

	/**
	 * the FitToGrid flag
	 */
	private JCheckBox FitCheckBox;

	/**
	 * the name of the image identifying the primary state
	 */
	private JTextField PrimaryName;

	/**
	 * the name of the image identifying the alternate state
	 */
	private JTextField AlternateName;

	/**
	 * the IOSpec which changes state
	 */
	private IOSpec Trigger;

	/**
	 * the delay timer for momentary pushbuttons
	 */
	private int MomentaryDelay;

	/**
	 * the text field for entering the momentary delay value
	 */
	private JTextField DelayValue = new JTextField(6);;
	
	/**
	 * the JPanel for the Momentary Delay presentation
	 */
	private JPanel MomentaryDelayPanel;

	/**
	 * the GUI element for selecting the primary image
	 */
	private FileNamePanel PrimaryPanel;

	/**
	 * the GUI element for selecting the alternate image
	 */
	private FileNamePanel AlternatePanel;

	/**
	 * the GUI element for capturing the trigger definition
	 */
	private IOPanel TriggerPanel;

	/**
	 * a panel for holding the tuning parameters
	 */
	private JPanel TuningPanel;

	/**
	 * the JPanel containing the top row of JPanels
	 */
	private JPanel TopRowPanel;

	/**
	 * the JPanel containing the bottom row of JPanels
	 */
	private JPanel BottomRowPanel;

	/**
	 * the ctor
	 * 
	 * @param button is the initial contents of the dialog, It cannot be null.
	 */

	private ButtonItemDialog(ButtonItem button) {
		String str;

		str = button.getPrimaryImage();
		if (str == null) {
			PrimaryName = new JTextField(20);
		} else {
			PrimaryName = new JTextField(str);
			if (str.length() < 20) {
				PrimaryName.setColumns(20);
			}
		}
		PrimaryPanel = new FileNamePanel("Primary Image", PrimaryName);

		str = button.getAlternateImage();
		if (str == null) {
			AlternateName = new JTextField(20);
		} else {
			AlternateName = new JTextField(str);
			if (str.length() < 20) {
				AlternateName.setColumns(20);
			}
		}
		AlternatePanel = new FileNamePanel("Alternate Image", AlternateName);

		Trigger = button.getTrigger();
		MomentaryDelay = button.getMomentaryDelay();

		if ((MomentaryDelay < 0) || (MomentaryDelay > Integer.MAX_VALUE)) {
			MomentaryDelay = 0;
		}
		if (MomentaryDelay != 0) {
			DelayValue.setText(((Integer)MomentaryDelay).toString());;
		}
		MomentaryDelayPanel = new JPanel();
		MomentaryDelayPanel.add(DelayValue);
		MomentaryDelayPanel.add(new JLabel("Button Delay (seconds)"));

		TuningPanel = new JPanel();
		TuningPanel.setLayout(new BoxLayout(TuningPanel, BoxLayout.Y_AXIS));
		StatusCheckBox = new JCheckBox("Status Only", button.getStatusOnly());
		FitCheckBox = new JCheckBox("Fit Image to Grid", button.getFitToGrid());
		TuningPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray)));
		TuningPanel.add(StatusCheckBox);
		TuningPanel.add(FitCheckBox);
		TuningPanel.add(MomentaryDelayPanel);

		TriggerPanel = new IOPanel(Trigger, "Button Trigger", false);
		TriggerPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray)));

//		setLayout(new GridLayout(2,2));
//		add(TriggerPanel);
//		add(TuningPanel);
//		add(PrimaryPanel);
//		add(AlternatePanel);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		TopRowPanel = new JPanel(new GridLayout(1, 2));
		BottomRowPanel = new JPanel(new GridLayout(1, 2));
		TopRowPanel.add(TriggerPanel);
		TopRowPanel.add(TuningPanel);
		add(TopRowPanel);
		BottomRowPanel.add(PrimaryPanel);
		BottomRowPanel.add(AlternatePanel);
		add(BottomRowPanel);
	}

	/**
	 * the GUI It takes a ButtonItem as input. If null, a fresh one will be created.
	 * If not null, an existing one will be edited. It will be changed only if
	 * Accept is selected.
	 * 
	 * @param oldButton is a ButtonItem. If null, one will be created.
	 * @return the results of the user filling in the GUI fields.
	 */
	public static ButtonItem newButton(ButtonItem oldButton) {
		ButtonItemDialog panel;
		String delay;
		if (oldButton == null) {
			oldButton = new ButtonItem();
		}
		panel = new ButtonItemDialog(oldButton);
		if (AcceptDialog.select(panel, "Button Definition")) {
			oldButton = new ButtonItem();
			delay = panel.DelayValue.getText();
			if ((delay == null) || ("".equals(delay.trim()))) {
				oldButton.setMomentaryDelay(0);
			}
			else {
				try {
					oldButton.setMomentaryDelay(Integer.valueOf(delay));
				}
				catch (java.lang.NumberFormatException nfe) {
		    		JOptionPane.showMessageDialog( (Component)null,
		    				"Delay must be an integer.",
		    				"Number Format Error",
		    				JOptionPane.ERROR_MESSAGE
		    				);
				}
			}
			oldButton.setStatusOnly(panel.StatusCheckBox.isSelected());
			oldButton.setFitToGrid(panel.FitCheckBox.isSelected());
			oldButton.setTrigger(panel.TriggerPanel.getSpec());
			oldButton.setPrimaryImage(panel.PrimaryName.getText());
			oldButton.setAlternateImage(panel.AlternateName.getText());
		}
		return oldButton;
	}
}
/* @(#)ButtonItemDialog.java */
