/*
 * Name: DeviceTable
 * 
 * What:
 * A derived JTable for displaying the uses of all decoders.
 * <p>
 * Decoders cannot be added or removed.  Nothing can be changed.
 *
 */
package designer.gui.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import designer.gui.jCustom.AcceptDialog;
import designer.layout.AspectMap;
import designer.layout.EnumerationLocks;
import designer.layout.SignalTemplate;
import designer.layout.TemplateStore;
import designer.layout.items.AspectTable;
import designer.layout.items.Block;
import designer.layout.items.Edge;
import designer.layout.items.IOSpec;
import designer.layout.items.PhysicalSignal;
import designer.layout.items.RouteInfo;
import designer.layout.items.SecEdge;
import designer.layout.items.SwitchPoints;

/**
 * A derived JTable for displaying the uses of all decoders.
 * <p>
 * Decoders cannot be added or removed.  Nothing can be changed.
 *
 * <p>Copyright: Copyright (c) 2013 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class DeviceTable extends JPanel {
    /**
     * the use labels
     */
    private static final String BLOCK = "Block";
    private static final String LOCK = "Lock";
    private static final String SIGNAL = "Signal";
    private static final String POINTS = "Points";
    private static final String ROUTES = "Routes";
 
    /**
     * the column header names
     */
    private static final String ColumnNames[] = {
        "Use",
        "Identity",
        "Prefix",
        "Address",
        "Device",
        "Throw",
        "User Name"
     };

    /**
     * dimensions of the scroll pane
     */
    private static int Height = 400;
    private static int Width = 640;
    
    /**
     * table model (see below)
     */
    DeviceTableModel Model = new DeviceTableModel();

    protected JScrollPane ScrollPane;
    protected DTable MyTable;

    public DeviceTable() {
        MyTable = new DTable(Model);
        ScrollPane = new JScrollPane(MyTable);
        ScrollPane.setPreferredSize(new Dimension(Width, Height));
        ScrollPane.getViewport().setBackground(MyTable.getBackground());
        ScrollPane.addComponentListener(new ComponentAdapter() {			
			@Override
			public void componentResized(ComponentEvent e) {
				Component comp = e.getComponent();
				Height = comp.getHeight();
				Width = comp.getWidth();
			}
		});
        setLayout(new BorderLayout());
        add(ScrollPane, BorderLayout.CENTER);
        Model.findErrors();
    }

    /**************************************************************************
     * The JTable derivative    
     **************************************************************************/
    private class DTable extends JTable {
        public DTable(DeviceTableModel model) {
            super(model);
            setAutoCreateColumnsFromModel(false);
            setAutoResizeMode(AUTO_RESIZE_OFF);
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            setDefaultRenderer(String.class, new ColorCellRenderer());
            setAutoCreateRowSorter(true);
        }
    }

    public static boolean showDevices() {
        AcceptDialog.select(new DeviceTable(), "Devices");
        return false;
    }

    /**************************************************************************
     * The TableModel derivative
     **************************************************************************/
    private class DeviceTableModel extends AbstractTableModel {

        /**
         * is the foreground color for indicating a problem
         */
        private final Color Problem = Color.red;

        /**
         * copies of the signal descriptions.  This will manipulate the copies until
         * the Accept button is pushed; then all changes will be written back.
         */
        private ArrayList<Device> Copies = new ArrayList<Device>();
        
        /**
         * a shadow of Copies, containing the Color of each value
         */
        private Color CellColor[][];
        
        /**
         * the default foreground color
         */
        private final Color DefaultColor = UIManager.getColor("Table.foreground");
        
        public DeviceTableModel() {
            PhysicalSignal sig;
            SignalTemplate template;
            AspectMap templateAspects;
            AspectTable aTable;
            IOSpec decoder;
            EnumMap<EnumerationLocks, IOSpec> lockMap;
            EnumerationLocks[] locks = EnumerationLocks.values();
            EnumerationLocks lock;
            SwitchPoints sw;
            int which;
            RouteInfo route;
            
            // pull in the Block data
            for (Block b : Block.getBlocks()) {
                if (b.getOccupied() != null){
                    Copies.add(new Device(BLOCK, b.getBlockName(), b.getOccupied()));
                }
                if (b.getUnoccupied() != null){
                    Copies.add(new Device(BLOCK, b.getBlockName(), b.getUnoccupied()));
                }
                if (b.getReporter() != null){
                    Copies.add(new Device(BLOCK, b.getBlockName(), b.getReporter()));
                }
            }

            // pull in the Lock data
            for (SecEdge e : SecEdge.getBlkEdges()) {
            	lockMap = e.getLocks();
            	if (e != null ) {
            		for (int l = 0; l < locks.length; ++l) {
            			lock = locks[l];
            			if (lockMap.containsKey(lock)) {
            				Copies.add(new Device(LOCK, e.getBlock().getBlockName(),lockMap.get(lock)));
            			}
            		}
            	}
            }

            // pull in the Signal data
            for (SecEdge e : SecEdge.getSignals()) {
                sig = e.getSignal().getSigDescription();
                template = TemplateStore.SignalKeeper.find(sig.getTemplateName());
                if (template != null) {
                    templateAspects = template.getAspectMap();
                    if (templateAspects != null) {
                        aTable = sig.getCommandList();
                        if (aTable != null) {
                            for (int heads = 0; heads < templateAspects.getHeadCount(); ++heads) {
                                for (Enumeration<String> eAspect = templateAspects.getState(heads); eAspect.hasMoreElements(); ) {
                                     decoder = aTable.getList(heads).findDecoders(eAspect.nextElement());
                                     Copies.add(new Device(SIGNAL, e.getSignal().getSigName(), decoder));
                                }
                            }
                        }
                    }
                }
            }
            
            // pull in the Points data
            for (SecEdge e: SecEdge.getPtsEdges()) {
                if ((sw = e.getPoints()) != null) {
                    which = sw.findSpecByName("LOCKONCMD");
                    if ((decoder = sw.getIOSpec(which)) != null) {
                        Copies.add(new Device(POINTS, e.toEdge().toString(), decoder));
                    }
                    which = sw.findSpecByName("UNLOCKONCMD");
                    if ((decoder = sw.getIOSpec(which)) != null) {
                        Copies.add(new Device(POINTS, e.toEdge().toString(), decoder));
                    }
                    which = sw.findSpecByName("LOCKREPORT");
                    if ((decoder = sw.getIOSpec(which)) != null) {
                        Copies.add(new Device(POINTS, e.toEdge().toString(), decoder));
                    }
                    which = sw.findSpecByName("UNLOCKREPORT");
                    if ((decoder = sw.getIOSpec(which)) != null) {
                        Copies.add(new Device(POINTS, e.toEdge().toString(), decoder));
                    }
                }
            }

            // Pull in the Routes data
            for (SecEdge e : SecEdge.getPtsEdges()) {
                if ((sw = e.getPoints()) != null) {
                    for (int s = 0; s < Edge.EDGENAME.length; ++s) {
                        if ((route = sw.getRoute(s)) != null) {
                            if ((decoder = route.getRouteCmd()) != null) {
                                Copies.add(new Device(ROUTES, e.toEdge().toString(), decoder));
                            }
                            if ((decoder = route.getRouteReport(0)) != null) {
                                Copies.add(new Device(ROUTES, e.toEdge().toString(), decoder));
                            }
                            if ((decoder = route.getRouteReport(1)) != null) {
                                Copies.add(new Device(ROUTES, e.toEdge().toString(), decoder));
                            }
                            if ((decoder = route.getRouteReport(2)) != null) {
                                Copies.add(new Device(ROUTES, e.toEdge().toString(), decoder));
                            }
                        }
                    }
                }
            }

            CellColor = new Color[Copies.size()][ColumnNames.length];
            for (int edge = 0; edge < Copies.size(); ++edge) {
                for (int i = 0; i < CellColor[edge].length; i++) {
                    CellColor[edge][i] = DefaultColor;
                }
            }
        }

        @Override
        public int getColumnCount() {
            return ColumnNames.length;
        }

        @Override
        public int getRowCount() {
            return Copies.size();
        }

        @Override
        public Object getValueAt(int row, int column) {
            switch (column) {
            case 0:
                return Copies.get(row).Use;
            case 1:
                if (Copies.get(row).ID != null) {
                    return Copies.get(row).ID;
                }
                break;
            case 2:
                if ((Copies.get(row).Control != null) && (Copies.get(row).Control.getPrefix() != null)) {
                    return Copies.get(row).Control.getPrefix();
                }
               break;
            case 3:
                if ((Copies.get(row).Control != null) && (Copies.get(row).Control.getAddress() != null)) {
                    return Copies.get(row).Control.getAddress();
                }
               break;
            case 4:
                if ((Copies.get(row).Control != null) && (Copies.get(row).Control.getDeviceType() != null)) {
                    return Copies.get(row).Control.getDeviceType();
                }
            	break;
            case 5:
                if (Copies.get(row).Control != null) {
                    return Copies.get(row).Control.getCommand();
                }
                return false;
            case 6:
                if ((Copies.get(row).Control != null) && (Copies.get(row).Control.getUserName() != null)) {
                    return Copies.get(row).Control.getUserName();
                }
                break;
            default:
           }
            return new String("");
        }

        @Override
        public String getColumnName(int i) {
            return ColumnNames[i];
        }
        
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
        
        @Override
        public Class<?> getColumnClass(int column) {
            return (column == 5) ? Boolean.class : String.class;
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
        }
        
        
        /**
         * this method sets the foreground color on cells in the JTable
         * @param row is the row the cells are on
         * @param start is the first cell to set the color
         * @param stop is one after the last cell to color
         * @param color is the color of the foreground
         */
        private void showError(int row, int start, int stop, Color color) {
            for (; start < stop; start++) {
                CellColor[row][start] = color;
            }            
        }
        
        /**
         * checks a decoder definition for being complete.  The foreground color
         * of an incomplete decoder is set to red
         * @param row is the index of the aspect containing the decoder
         */
        private void checkDecoder(int row) {
            Color color = Problem;
            if (Copies.get(row).Control != null) {
                if (Copies.get(row).Control.isComplete()) {
                    color = DefaultColor;
                }
                showError(row, 2, 6, color);
            }
        }
        
        /**
         * scans the Signal data and flags any problems
         */
        public void findErrors() { 
            for (int row = 0; row < Copies.size(); ++row) {
                checkDecoder(row);
            }
        }
        
        /**
         * determines what color to use for the foreground on a table cell
         * @param row is the row index of the cell
         * @param column is the column index of the cell
         * @return the COlor
         */
        public Color getCellColor(int row, int column) {
            return CellColor[row][column];
        }
 
    }
    
    /**************************************************************************
     * a data structure for holding the information about a Device.  This
     * is a database entry expanded.
     *************************************************************************/
    private class Device {
        public String Use;
        public String ID;
        public IOSpec Control;
        
        public Device(String use, String id, IOSpec device) {
            Use = use;;
            ID = id;
            Control = device;
        }
    }
    
    /**************************************************************************
     * A cell renderer associated with setting the foreground color
     **************************************************************************/
    private class ColorCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            setForeground(Model.getCellColor(row, column));
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}
/* @(#)DeviceTable.java */