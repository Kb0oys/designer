/*
 * Name: RouteTable
 * 
 * What:
 * A derived JTable for displaying the decoders on the SwitchPoints routes.
 * For each Route,
 * <ul>
 * <li>location of the points (immutable text)</li>
 * <li>the label on the end of the route (top, left, etc) (immutable text)</li>
 * <li>a checkbox showing the normal route</li>
 * <li>decoder address for commanding the decoder to the route</li>
 * <li>decoder address for reporting when the points are lined to the route</li>
 * <li>decoder address for reporting when the points are not lined to the route</li>
 * <li>decoder address for requesting that the points be lined for the route</li>
 * </ul> 
 * Points cannot be added nor removed via this table.  Neither the label nor the
 * location can be changed.
 *
 */
package designer.gui.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.PrefixSelector;
import designer.layout.items.Edge;
import designer.layout.items.IOSpec;
import designer.layout.items.RouteInfo;
import designer.layout.items.SecEdge;
import designer.layout.items.SwitchPoints;

/**
 * A derived JTable for displaying the decoders on the SwitchPoints routes.
 * For each Route,
 * <ul>
 * <li>location of the points (immutable text)</li>
 * <li>the label on the end of the route (top, left, etc) (immutable text)</li>
 * <li>a checkbox showing the normal route</li>
 * <li>decoder address for commanding the decoder to the route</li>
 * <li>decoder address for reporting when the points are lined to the route</li>
 * <li>decoder address for reporting when the points are not lined to the route</li>
 * <li>decoder address for requesting that the points be lined for the route</li>
 * </ul> 
 * Points cannot be added nor removed via this table.  Neither the label nor the
 * location can be changed.
 *
 * <p>Copyright: Copyright (c) 2013</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RouteTable extends JPanel {
    /**
     * constants for indexing the RouteInfo input decoders
     */
    static private final int SELECT_ROUTE_REQUEST = 0;
    static private final int ROUTE_SELECTED_REPORT = 1;
    static private final int ROUTE_UNSELECTED_REPORT = 2;
    
    /**
     * the column header names
     */
    private static final String[] ColumnNames = {
        "Location",
        "Route",
        "Normal",
        "Command",
        "Address",
        "Throw",
        "User",
        "Request",
        "Address",
        "Throw",
        "User",
        "Lined",
        "Address",
        "Throw",
        "User",
        "Not Lined",
        "Address",
        "Throw",
        "User"
    };

    /**
     * memory for dimensions
     */
    private static int Height = 200;
    private static int Width = 820;
    
   /**
     * table model (see below)
     */
    RouteTableModel Model = new RouteTableModel();

    protected JScrollPane ScrollPane;
    protected RTable MyTable;

    public RouteTable() {
        MyTable = new RTable(Model);
        ScrollPane = new JScrollPane(MyTable);
        //ScrollPane.getViewport().add(MyTable);
        ScrollPane.setPreferredSize(new Dimension(Width, Height));
        ScrollPane.getViewport().setBackground(MyTable.getBackground());
        ScrollPane.addComponentListener(new ComponentAdapter() {			
			@Override
			public void componentResized(ComponentEvent e) {
				Component comp = e.getComponent();
				Height = comp.getHeight();
				Width = comp.getWidth();
			}
		});
       setLayout(new BorderLayout());
        add(ScrollPane, BorderLayout.CENTER);
        Model.findErrors();
    }

    /**************************************************************************
     * The JTable derivative    
     **************************************************************************/
    private class RTable extends JTable {
        public RTable(RouteTableModel model) {
            super(model);
            TableColumnModel tcm;
            setAutoCreateColumnsFromModel(false);
            setAutoResizeMode(AUTO_RESIZE_OFF);
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tcm = getColumnModel();
            tcm.getColumn(3).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(7).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(11).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(15).setCellEditor(PrefixSelector.getEditor());
            setDefaultRenderer(String.class, new ColorCellRenderer());
            setAutoCreateRowSorter(true);
        }
    }

    public static boolean showRoutes() {
        RouteTable table = new RouteTable();
        if (AcceptDialog.select(table, "Point Routes")) {
            table.Model.updateRoutes();
            return true;
        }
        return false;
    }

    /**************************************************************************
     * The TableModel derivative
     **************************************************************************/
    private class RouteTableModel extends AbstractTableModel {

        /**
         * is the foreground color for indicating a problem
         */
        private final Color Problem = Color.red;
        
        /**
         * copies of the defined Routes.  This will manipulate the copies until
         * the Accept button is pushed; then all changes will be written back.
         */
        private Points Copies[];
        
        /**
         * a bit vector of Routes that have been modified.  true means
         * the corresponding copy has been changed.
         */
        private boolean Dirty[];
        
        /**
         * a shadow of Copies, containing the Color of each value
         */
        private Color CellColor[][];
        
        /**
         * the default foreground color
         */
        private final Color DefaultColor = UIManager.getColor("Table.foreground");
        
        public RouteTableModel() {
            int count = 0;
            SwitchPoints sw;
            Vector<SecEdge> points = SecEdge.getPtsEdges();
            RouteInfo decoder;
            boolean first;
            
            // count the number of routes
            for (SecEdge e : points) {
                if ((sw = e.getPoints()) != null) {
                    for (int s = 0; s < Edge.EDGENAME.length; ++s) {
                        if (sw.getRoute(s) != null) {
                            ++count;
                        }
                    }
                }
            }
            Copies = new Points[count];
            Dirty = new boolean[count];
            CellColor = new Color[count][ColumnNames.length];
            count = 0;
            // make copies of the Switch Points
            for (SecEdge e: points) {
                if ((sw = e.getPoints()) != null) {
                    first = true;
                    for (int s = 0; s < Edge.EDGENAME.length; ++s) {
                        if ((decoder = sw.getRoute(s)) != null) {
                            if (first) {
                                first= false;
                                Copies[count] = new Points(e, e.toEdge().toString(), s, decoder.copy());
                            }
                            else {
                                Copies[count] = new Points(e, null, s, decoder.copy());
                            }
                            Dirty[count] = false;
                            for (int i = 0; i < CellColor[count].length; i++) {
                                CellColor[count][i] = DefaultColor;
                            }
                            ++count;
                        }
                    }
                }
            }
        }

        @Override
        public int getColumnCount() {
            return ColumnNames.length;
        }

        @Override
        public int getRowCount() {
            return Copies.length;
        }

        @Override
        public Object getValueAt(int row, int column) {
            IOSpec io;
            switch (column) {
            case 0: // location
                if (Copies[row].Name != null) {
                    return Copies[row].Name;
                }
                break;
            case 1: // Route label
                return Edge.fromEdge(Copies[row].Destination);
            case 2: // route is the Normal route
                return Copies[row].Route.getNormal();
            case 3: // Select route command prefix
                if ((io = Copies[row].Route.getRouteCmd()) != null) {
                    return io.getPrefix();
                }
                break;
            case 4: // Select route command address
                if ((io = Copies[row].Route.getRouteCmd()) != null) {
                    return io.getAddress();
                }
                break;
            case 5: // Select route command polarity
                if ((io = Copies[row].Route.getRouteCmd()) != null) {
                    return io.getCommand();
                }
                return false;
            case 6: // Select route command username
                if ((io = Copies[row].Route.getRouteCmd()) != null) {
                    return io.getUserName();
                }
                break;
            case 7: // Select route request prefix
                if ((io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST)) != null) {
                    return io.getPrefix();
                }
                break;
            case 8: // Select route request address
                if ((io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST)) != null) {
                    return io.getAddress();
                }
                break;
            case 9: // Select route request polarity
                if ((io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST)) != null) {
                    return io.getCommand();
                }
                return false;
            case 10: // Select route command username
                if ((io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST)) != null) {
                    return io.getUserName();
                }
                break;
            case 11: // Route selected report prefix
                if ((io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT)) != null) {
                    return io.getPrefix();
                }
                break;
            case 12: // Route selected report address
                if ((io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT)) != null) {
                    return io.getAddress();
                }
                break;
            case 13: // Route selected report polarity
                if ((io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT)) != null) {
                    return io.getCommand();
                }
                return false;
            case 14: // Route selected report username
                if ((io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT)) != null) {
                    return io.getUserName();
                }
                break;
            case 15: // Route unselected report prefix
                if ((io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT)) != null) {
                    return io.getPrefix();
                }
                break;
            case 16: // Route unselected report address
                if ((io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT)) != null) {
                    return io.getAddress();
                }
                break;
            case 17: // Route unselected report polarity
                if ((io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT)) != null) {
                    return io.getCommand();
                }
                return false;
            case 18: // Route unselected report username
                if ((io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT)) != null) {
                    return io.getUserName();
                }
                break;
            default:
            }
            return new String("");
        }

        @Override
        public String getColumnName(int i) {
            return ColumnNames[i];
        }
        
        @Override
        public boolean isCellEditable(int row, int column) {
            return column > 1;
        }
        
        @Override
        public Class<?> getColumnClass(int column) {
            switch (column) {
            case 2:  //Normal button
            case 5:  //polarity for select route command
            case 9: // polarity for select route request
            case 13: // polarity for route selected report
            case 17: // polarity for route unselected report
                return Boolean.class;
            default:
            }
            return String.class;
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
            IOSpec io;
            switch (column) {
            case 0: // location - immutable
            case 1: // Route label - immutable
                break;
            case 2:  // route is the Normal route
                Copies[row].Route.setNormal((Boolean) value);
                verifyNormal(row);
                break;
            case 3: // Select route command prefix
                if ((io = Copies[row].Route.getRouteCmd()) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteCmd(io);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(io, row, 3);
                break;
            case 4: // Select route command address
                if ((io = Copies[row].Route.getRouteCmd()) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteCmd(io);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(io, row, 3);
                break;
            case 5: // Select route command polarity
                if ((io = Copies[row].Route.getRouteCmd()) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteCmd(io);
                }
                io.setCommand((Boolean) value);
                checkDecoders(io, row, 3);
                break;
            case 6: // Select route command username
                if ((io = Copies[row].Route.getRouteCmd()) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteCmd(io);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(io, row, 3);
                break;
            case 7: // Select route request prefix
                if ((io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, SELECT_ROUTE_REQUEST);
                    io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(io, row, 7);
                break;
            case 8: // Select route request address
                if ((io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, SELECT_ROUTE_REQUEST);
                    io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(io, row, 7);
                break;
            case 9: // Select route request polarity
                if ((io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, SELECT_ROUTE_REQUEST);
                    io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST);
                }
                io.setCommand((Boolean) value);
                checkDecoders(io, row, 7);
                break;
            case 10: // Select route command username
                if ((io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, SELECT_ROUTE_REQUEST);
                    io = Copies[row].Route.getRouteReport(SELECT_ROUTE_REQUEST);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(io, row, 7);
                break;
            case 11: //Route selected report prefix
                if ((io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, ROUTE_SELECTED_REPORT);
                    io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(io, row, 11);
                break;
            case 12: // Route selected report address
                if ((io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, ROUTE_SELECTED_REPORT);
                    io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(io, row, 11);
                break;
            case 13: // Route selected report polarity
                if ((io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, ROUTE_SELECTED_REPORT);
                    io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT);
                }
                io.setCommand((Boolean) value);
                checkDecoders(io, row, 11);
                break;
            case 14: // Route selected report username
                if ((io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, ROUTE_SELECTED_REPORT);
                    io = Copies[row].Route.getRouteReport(ROUTE_SELECTED_REPORT);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(io, row, 11);
                break;
            case 15: // Route unselected report prefix
                if ((io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, ROUTE_UNSELECTED_REPORT);
                    io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(io, row, 15);
                break;
            case 16: // Route unselected report address
                if ((io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, ROUTE_UNSELECTED_REPORT);
                    io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(io, row, 15);
                break;
            case 17: // Route unselected report polarity
                if ((io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, ROUTE_UNSELECTED_REPORT);
                    io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT);
                }
                io.setCommand((Boolean) value);
                checkDecoders(io, row, 15);
                break;
            case 18: // Route unselected report username
                if ((io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT)) == null) {
                    io = new IOSpec();
                    Copies[row].Route.setRouteReport(io, ROUTE_UNSELECTED_REPORT);
                    io = Copies[row].Route.getRouteReport(ROUTE_UNSELECTED_REPORT);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(io, row, 15);
                break;
            default:
            }
           Dirty[row] = true;
        }
        
        /**
         * this method updates the Points that have been modified
         */
        public void updateRoutes() {
            Points points;
            for (int count = 0; count < Copies.length; ++count) {
                if (Dirty[count]) {
                    points = Copies[count];
                    if ((points.Route.getRouteCmd() == null) ||
                            !points.Route.getRouteCmd().isComplete()) {
                        points.Route.setRouteCmd(null);
                    }
                    for (int i = 0; i < 3; ++i) {
                        if ((points.Route.getRouteReport(i) == null) ||
                                !points.Route.getRouteReport(i).isComplete()) {
                            points.Route.setRouteReport(null, i);
                        }
                    }
                    points.Edge.getPoints().setRoute(points.Destination, points.Route);
                    points.Edge.refreshColors();
                }
            }
        }
        
        /**
         * this method sets the foreground color on cells in the JTable
         * @param row is the row the cells are on
         * @param start is the first cell to set the color
         * @param stop is one after the last cell to color
         * @param color is the color of the foreground
         */
        private void showError(int row, int start, int stop, Color color) {
            for (; start < stop; start++) {
                CellColor[row][start] = color;
            }            
        }
        
        /**
         * this method checks a Points definition to determine if there is an error.
         * The only errors are in the decoder definitions
         * @param index is the index into Copies of the Points being examined.
         */
        private void isPointsError(int index) {
            RouteInfo route = Copies[index].Route;
            checkDecoders(route.getRouteCmd(), index, 3);
            checkDecoders(route.getRouteReport(SELECT_ROUTE_REQUEST), index, 5);
            checkDecoders(route.getRouteReport(ROUTE_SELECTED_REPORT), index, 9);
            checkDecoders(route.getRouteReport(ROUTE_UNSELECTED_REPORT), index, 13);
        }
        
        /**
         * checks a decoder definitions for being complete.  The foreground color
         * of incomplete ones are set to red.
         * @param decoder is the decoder being checked
         * @param row is the row in the Table displaying the decoder's information
         * @param col is the first column in the table for the decoder's information
         */
        private void checkDecoders(IOSpec decoder, int row, int col) {
            if ((decoder != null) && !decoder.isComplete()) {
                showError(row, col, col + 4, Problem);
            }
            else {
                showError(row, col, col + 4, DefaultColor);
            }
        }
        
        /**
         * scans the Block data and flags any problems
         */
        public void findErrors() { 
            for (int row = 0; row < Copies.length; row++) {
                isPointsError(row);
                verifyNormal(row);
            }
        }

        /**
         * checks all the routes in a SwithPoints structure to ensure that one and 
         * only one route has the Normal box checked.  It finds the SwitchPoints by
         * looking at earlier Copies and later Copies for a non-null SecEdge (see how
         * SwichPoints are use to create Points in Copies).
         * <p>
         * If the parameter's Normal is set, all the others are cleared.  If the
         * parameter's Normal is not set and none of the others are, the location and
         * route fields are painted red.
         * @param route is a route which could be the Normal route.
         */
        private void verifyNormal(int route) {
            int first;
            int last;
            int normalCount = 0;
            Color color = Problem;
            for (first = route; first > 0; --first) {
                if (Copies[first].Name != null) {
                    break;
                }
            }
            for (last = route + 1; last < Copies.length; ++last) {
                if (Copies[last].Name != null) {
                    break;
                }
            }
            if (Copies[route].Route.getNormal()) {
                for (int i = first; i < last; ++i) {
                    Copies[i].Route.setNormal(i == route);
                    showError(i, 0, 3, DefaultColor);
                }
            }
            else {
                for (int i = first; i < last; ++i) {
                    if (Copies[i].Route.getNormal()) {
                        ++normalCount;
                    }
                }
                if (normalCount == 1) {
                    color = DefaultColor;
                }
                for (int i = first; i < last; ++i) {
                    showError(i, 0, 3, color);
                }
            }
            fireTableRowsUpdated(first, last - 1);
        }
        
        /**
         * determines what color to use for the foreground on a table cell
         * @param row is the row index of the cell
         * @param column is the column index of the cell
         * @return the COlor
         */
        public Color getCellColor(int row, int column) {
            return CellColor[row][column];
        }
    }
    /**************************************************************************
     * a data structure for holding the information about the Routes.  This
     * is a database entry expanded.
     *************************************************************************/
    private class Points {
        public SecEdge Edge;
        public String Name;
        public int Destination;
        public RouteInfo Route;
        
        public Points(SecEdge edge, String name, int destination, RouteInfo route) {
            Edge = edge;
            Name = name;
            Destination = destination;
            Route = route;
        }
    }
    
   
    /**************************************************************************
     * A cell renderer associated with setting the foreground color
     **************************************************************************/
    private class ColorCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            setForeground(Model.getCellColor(row, column));
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}
/* @(#)RouteTable.java */