/* Name: BlockDialog.java
 *
 * Block.java is the JDialog for entering and editing Block information.
 */
package designer.gui.items;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.IOPanel;
import designer.gui.jCustom.ReporterPanel;
import designer.layout.items.Block;

/**
 * is a class for formatting a JDialog for entering and editing detection Block
 * information.
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2012, 2013
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */

public class BlockDialog
    extends JPanel {
  /**
   * the minimum number of characters in a JTextField
   */
  private static final int FIELD_LEN = 10;
  
  /**
   * the Block's name.
   */
  JTextField BlkName;

  /**
   * the Station at the Block.
   */
  JTextField StName;
  
  /**
   * JRadioButtons - for selecting one signaling discipline.
   */
  JRadioButton DisciplineChoice[] = new JRadioButton[Block.DisciplineName.
      length];

  /**
   * a checkbox for visible or hidden.
   */
  JCheckBox VisibleBox;

  /**
   * an IOPanel for the Occupancy detection.
   */
  IOPanel Occupancy;

  /**
   * an IOPanel for unoccupancy detection.
   */
  IOPanel Unoccupancy;

  /**
   * an IOPanel for the train symbol Reporter.
   */
  ReporterPanel Reporter;

  /**
   * constructs the JPanel for editing the Detection Block information.
   *
   * @param block
   *            is the Block to be edited. Null is not allowed.
   */
  public BlockDialog(Block block) {
    JPanel namePanel = new JPanel();
    JPanel discipline = new JPanel();
    JPanel middle = new JPanel();
    ButtonGroup choice = new ButtonGroup();
    JPanel detector = new JPanel(new GridLayout(0, 1));
    JPanel reporterPanel = new JPanel(new GridLayout(0, 1));
    JPanel ioPanel = new JPanel(new GridLayout(1, 0));

    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    discipline.setLayout(new BoxLayout(discipline, BoxLayout.X_AXIS));
    detector.setLayout(new BoxLayout(detector, BoxLayout.Y_AXIS));

    // First, the Block's name field.
    if (block.getBlockName() == null) {
      BlkName = new JTextField(FIELD_LEN);
    }
    else {
      BlkName = new JTextField(block.getBlockName());
      if (block.getBlockName().length() < FIELD_LEN) {
          BlkName.setColumns(FIELD_LEN);
      }
    }
    if (block.getStationName() == null) {
        StName = new JTextField(FIELD_LEN);
    }
    else {
        StName = new JTextField(block.getStationName(),
                (block.getStationName().length() < FIELD_LEN) ? FIELD_LEN :
                    block.getStationName().length());
    }
    namePanel.add(new JLabel("Block Name:"));
    namePanel.add(BlkName);
    namePanel.add(new JLabel("Station:"));
    namePanel.add(StName);
    VisibleBox = new JCheckBox("Show Block", block.getVisible());
    namePanel.add(VisibleBox);
    add(namePanel);

    discipline.add(new JLabel("Signal Discipline:"));
    // A list of possible signaling disciplines.
    for (int disc = 0; disc < DisciplineChoice.length; ++disc) {
      DisciplineChoice[disc] = new JRadioButton(
          Block.DisciplineName[disc]);
      DisciplineChoice[disc].setSelected(false);
      discipline.add(DisciplineChoice[disc]);
      choice.add(DisciplineChoice[disc]);
    }
    DisciplineChoice[block.getDiscipline()].setSelected(true);
    middle.add(discipline);
    add(middle);

    detector.add(new JLabel("Track Occupancy Detection"));
    // The occupied IOSpec.
    Occupancy = new IOPanel(block.getOccupied(), "Occupied Report:", false);
    Occupancy.setBorder(BorderFactory.createLineBorder(Color.gray));
    detector.add(Occupancy);

    // The unoccupied IOSpec.
    Unoccupancy = new IOPanel(block.getUnoccupied(), "Unoccupied Report:", false);
    Unoccupancy.setBorder(BorderFactory.createLineBorder(Color.gray));
    detector.add(Unoccupancy);
    detector.setBorder(BorderFactory.createEtchedBorder());
    ioPanel.add(detector);

    // The Reporter IOSpec.
    Reporter = new ReporterPanel(block.getReporter(), "Train Symbol Reporter:");
    Reporter.setBorder(BorderFactory.createLineBorder(Color.gray));
    reporterPanel.add(Reporter);

    ioPanel.add(reporterPanel);
    add(ioPanel);

  }

  /**
   * creates a JDialog for a Block and fills in the fields. If the a Block is
   * passed in as a parameter, then its values are used for initial values;
   * otherwise, a default Block is created and it is edited.
   *
   * @param block
   *            is the Block being edited. Null is not allowed.
   *
   * @return a new Block is the user accepts the JDialog or null if the user
   *         cancels.
   *
   * @see designer.layout.items.Section
   * @see designer.layout.items.Block
   */
  static public Block newBlock(Block block) {
    BlockDialog panel = new BlockDialog(block);
    Block replace = null;
    String name;
    int d;

    if (AcceptDialog.select(panel, "Describe Detection Block:")) {
      name = panel.BlkName.getText().trim();
      if (name.length() > 0) {
        replace = new Block(name);
        for (d = 0; d < panel.DisciplineChoice.length; ++d) {
          if (panel.DisciplineChoice[d].isSelected()) {
            break;
          }
        }
        name = panel.StName.getText().trim();
        if (name.length() > 0) {
            replace.setStationName(name);
        }
        else {
            replace.setStationName(null);
        }
        replace.setDiscipline(d);
        replace.setVisible(panel.VisibleBox.isSelected());
        replace.setOccupied(panel.Occupancy.getSpec());
        replace.setUnoccupied(panel.Unoccupancy.getSpec());
        replace.setReporter(panel.Reporter.getSpec());
      }
      else {
        System.out.println("A block name is required to define a Block");
      }
    }
    return replace;
  }

}
/* @(#)BlockDialog.java */
