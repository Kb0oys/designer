/* Name: TrackGroupDialog.java
 *
 * What:
 *  This class decorates AcceptDialog by providing a JPanel for editing
 *  the Track contents.
 */
package designer.gui.items;

import designer.gui.Ctc;
import designer.gui.frills.RailFrill;
import designer.gui.jCustom.AcceptDialog;
import designer.gui.Tile;
import designer.layout.items.Section;
import designer.layout.items.Track;
import designer.layout.items.TrackGroup;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * is a class for editing the Track configuration in a Section.  Its single
 * parameter is a section to be edited.  If the parameter is null, then
 * it creates a blank Track Group.
 * <p>
 * The JPanel is set up the first time someone edits the TrackGroup and
 * persists for the life of the program.  Every time newTrackGroup is called,
 * the checkboxes are set to reflect the group passed in as a parameter.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TrackGroupDialog {

  /**
   * the JPanel containing the track selection radio buttons.
   */
  private static JPanel GroupPanel = new JPanel();

  /**
   * the boxes for selecting which Tracks are included.
   */
  private static JCheckBox[] ChkBox = new JCheckBox[TrackGroup.TrackName.length];

  /**
   * the drawings of the possible track configurations.
   */
  private static JButton[] TrackIcon = new JButton[TrackGroup.TrackName.length];

  /**
   * The speed selection boxes.
   */
  private static JComboBox[] Speeds = new JComboBox[TrackGroup.TrackName.length];

  /**
   * constructs the JPanel for editing the Tracks in a Section.
   *
   * @param group is the TrackGroup to be edited.  If null, then default
   * values are used.
   *
   * @return - a JPanel with the entries filled in from group.
   */
  private static JPanel createGroupPanel(TrackGroup group) {
    boolean checkMark = false;

    if (TrackIcon[0] == null) {
      createIcons();
    }
    for (int trk = 0; trk < ChkBox.length; ++trk) {
      Speeds[trk].setSelectedIndex(0);
      if (group != null) {
        checkMark = group.isIncluded(trk);
        if (checkMark) {
          Speeds[trk].setSelectedIndex(group.getTrkSpeed(trk));
        }
      }
      ChkBox[trk].setSelected(checkMark);
      TrackIcon[trk].setSelected(checkMark);
    }
    return GroupPanel;
  }

  /**
   * creates a JDialog for a set of Tracks and fills in the fields of the
   * Tracks.  If the Section does not have a TrackGroup, then a default
   * one is created and it is edited.
   *
   * @param sec is the Section that will contain the new TrackGroup.
   *
   * @return true if the user accepted the editing or false if it was
   * cancelled.
   *
   * @see designer.layout.items.Section
   * @see designer.layout.items.TrackGroup
   */
  public static boolean newTrackGroup(Section sec) {
    JPanel panel = createGroupPanel(sec.getTrackGroup());
    TrackGroup newGroup;

    if (AcceptDialog.select(panel, "Select Tracks:")) {
      sec.hideSec();
      newGroup = new TrackGroup();
      for (int trk = 0; trk < TrackGroup.TrackName.length; ++trk) {
        if (ChkBox[trk].isSelected()) {
          newGroup.set(trk);
          newGroup.setTrkSpeed(trk, Speeds[trk].getSelectedIndex());
        }
      }
//      sec.replaceTrackGroup(newGroup);
      sec.modifyTrackGroup( newGroup);
      sec.showSec();
      return true;
    }
    return false;
  }

  /**
   * creates the Icons for each of the possible track patterns.
   */
  private static void createIcons() {
    Image i;
    Graphics g;
    int edge1;
    int edge2;
    RailFrill rails;
    Dimension perim = Tile.getGridSize();
    GroupPanel.setLayout(new BoxLayout(GroupPanel, BoxLayout.Y_AXIS));
    Box anEntry;
    int w = perim.width;
    int h = perim.height;

    for (int trk = 0; trk < TrackIcon.length; ++trk) {
      i = Ctc.RootCTC.getDispPanel().createImage(w, h);
      g = i.getGraphics();
      g.setClip(0, 0, w, h);
      g.setColor(Color.BLACK);
      g.fillRect(0, 0, w, h);
      g.setColor(Color.WHITE);
      edge1 = TrackGroup.Termination[trk].nextSetBit(0);
      edge2 = TrackGroup.Termination[trk].nextSetBit(edge1 + 1);
      anEntry = new Box(BoxLayout.X_AXIS);
      rails = new RailFrill(edge1, edge2);
      rails.decorate(g);
      TrackIcon[trk] = new JButton("", new ImageIcon(i));

      ChkBox[trk] = new JCheckBox(TrackGroup.TrackName[trk]);
      Speeds[trk] = new JComboBox(Track.TrackSpeed);
      Speeds[trk].setSelectedIndex(0);
      Speeds[trk].setAlignmentX(Component.RIGHT_ALIGNMENT);
      Speeds[trk].setMaximumSize(new Dimension(78, 24));
      Speeds[trk].setEnabled(false);

      anEntry.add(TrackIcon[trk]);
      anEntry.add(ChkBox[trk]);
      anEntry.add(Box.createHorizontalGlue());
      anEntry.add(Speeds[trk]);
      GroupPanel.add(anEntry);
      TrackIcon[trk].addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) {
          int trk;
          for (trk = 0; trk < TrackIcon.length; ++trk) {
            if (TrackIcon[trk] == ae.getSource()) {
              break;
            }
          }
          ChkBox[trk].setSelected(!ChkBox[trk].isSelected());
          Speeds[trk].setEnabled(ChkBox[trk].isSelected());
        }
      });
      ChkBox[trk].addItemListener(new ItemListener() {
        public void itemStateChanged(ItemEvent ie) {
          int trk;
          for (trk = 0; trk < TrackIcon.length; ++trk) {
            if (ChkBox[trk] == ie.getSource()) {
              break;
            }
          }
          Speeds[trk].setEnabled(ChkBox[trk].isSelected());
        }
      });
    }
  }
}
/* @(#)TrackGroupDialog.java */
