/* Name: XEdgeDialog.java
 *
 * XEdge.java is the JDialog for entering and editing ross over.
 */
package designer.gui.items;

import java.util.BitSet;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import designer.gui.jCustom.AcceptDialog;
import designer.layout.items.Edge;
import designer.layout.items.XEdge;

/**
 * XEdge.java is the JDialog for entering and editing ross over.
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */
public class XEdgeDialog extends JPanel {
    
    /**
     * the Tracks that form half the crossing
     */
    private int[] Tracks = new int[Edge.EDGENAME.length];
    
    /**
     * checkboxes for each of the Tracks.  Only the ones that are NO_BLOCK
     * and BLOCK will be shown.
     */
    private JCheckBox[] Checks = new JCheckBox[Edge.EDGENAME.length];
    
    /**
     * constructs the JPanel for editing the cross over information.
     *
     * @param xEdge is the cross over to be edited.  If null, one is created.
     * @param ends indicate which tracks terminate on the edge
     */
    public XEdgeDialog(XEdge xEdge, BitSet ends) {
        int state;
        if (xEdge == null) {
            xEdge = new XEdge();
            for (int i = 0; i < Tracks.length; ++i) {
                state = (ends.get(i)) ? XEdge.NO_BLOCK : XEdge.NO_TRACK;
                xEdge.setTrackEnd(i, state);
            }
        }
        for (int i = 0; i < Tracks.length; ++i) {
            Tracks[i] = xEdge.getTrackEnd(i);
            Checks[i] = new JCheckBox(Edge.fromEdge(i) + " Block?");
            Checks[i].setSelected(Tracks[i] == XEdge.BLOCK);
            if (Tracks[i] != XEdge.NO_TRACK) {
                add(Checks[i]);
            }
        }
    }

    /**
     * creates a JDialog for a Crossover and fills in the fields. If an XEdge in
     * the SecEdge already exists, then its values are used for initial values;
     * otherwise, a default XEdge is created and it is edited.
     *
     * @param xEdge is the XEdge being edited. Null is allowed
     * @param ends are the other ends of the tracks
     *
     * @return a new XEdge if the user accepts the JDialog or null if the user
     *         cancels.
     *
     * @see designer.layout.items.SecEdge
     * @see designer.layout.items.XEdge
     */
    static public XEdge getXEdge(XEdge xEdge, BitSet ends) {
        XEdgeDialog dialog = new XEdgeDialog(xEdge, ends);
        XEdge newXEdge = null;
        if (AcceptDialog.select(dialog, "Describe Cross Over:")) {
            newXEdge = new XEdge();
            for (int i = 0; i < dialog.Tracks.length; ++i) {
                if (dialog.Tracks[i] != XEdge.NO_TRACK) {
                    dialog.Tracks[i] = (dialog.Checks[i].isSelected()) ? XEdge.BLOCK :
                        XEdge.NO_BLOCK;
                    newXEdge.setTrackEnd(i, dialog.Tracks[i]);
                }
            }
        }
        return newXEdge;
    }
}
/* @(#)XedgeDialog.java */
