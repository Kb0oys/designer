/*
 * Name: PointsTable
 * 
 * What:
 * A derived JTable for displaying the configuration common to a set of
 * Switch Points:
 * <ul>
 * <li>location of the points (immutable text)</li>
 * <li>decoder address for turning the "locked" light on</li>
 * <li>decoder address for turning the "unlocked" light on</li>
 * <li>decoder address for receiving the "locked" report</li>
 * <li>decoder address for receiving the "unlocked" report</li>
 * </ul> 
 * Points cannot be added nor removed via this table.
 *
 */
package designer.gui.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.JmriDeviceSelector;
import designer.gui.jCustom.PrefixSelector;
import designer.layout.JmriDevice;
import designer.layout.items.IOSpec;
import designer.layout.items.SecEdge;
import designer.layout.items.SwitchPoints;

/**
 * A derived JTable for displaying the configuration common to a set of
 * Switch Points:
 * <ul>
 * <li>location of the points (immutable text)</li>
 * <li>decoder address for turning the "locked" light on</li>
 * <li>decoder address for turning the "unlocked" light on</li>
 * <li>decoder address for receiving the "locked" report</li>
 * <li>decoder address for receiving the "unlocked" report</li>
 * </ul> 
 * Points cannot be added nor removed via this table.
 *
 * <p>Copyright: Copyright (c) 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class PointsTable extends JPanel {
    /** 
     * Points fields identifiers.  These correspond to the order of
     * IOSpec in SwithPoints.MessageNAmes.
     * @see designer.layout.items.SwitchPoints
     */
    private static final int LOCKON = 0;
    private static final int UNLOCKON = 1;
    private static final int UNLOCKED = 2;
    private static final int LOCKED = 3;
    
    /**
     * the column header names
     */
    private static final String[] ColumnNames = {
        "Location",
        "Spur",
        "Lock Light",
        "Address",
        "Device",
        "Throw",
        "User",
        "Unlock Light",
        "Address",
        "Device",
        "Throw",
        "User",
        "Locked",
        "Address",
        "Device",
        "Throw",
        "User",
        "Unlocked",
        "Address",
        "Device",
        "Throw",
        "User"
    };

    /**
     * memory for panel size
     */
    private static int Height = 200;
    private static int Width = 800;
    /**
     * table model (see below)
     */
    PointsTableModel Model = new PointsTableModel();

    protected JScrollPane ScrollPane;
    protected PTable MyTable;

    public PointsTable() {
        MyTable = new PTable(Model);
        ScrollPane = new JScrollPane(MyTable);
        //ScrollPane.getViewport().add(MyTable);
        ScrollPane.setPreferredSize(new Dimension(Width, Height));
        ScrollPane.getViewport().setBackground(MyTable.getBackground());
        ScrollPane.addComponentListener(new ComponentAdapter() {			
			@Override
			public void componentResized(ComponentEvent e) {
				Component comp = e.getComponent();
				Height = comp.getHeight();
				Width = comp.getWidth();
			}
		});
        setLayout(new BorderLayout());
        add(ScrollPane, BorderLayout.CENTER);
        Model.findErrors();
    }

    /**************************************************************************
     * The JTable derivative    
     **************************************************************************/
    private class PTable extends JTable {
        public PTable(PointsTableModel model) {
            super(model);
            TableColumnModel tcm;
            setAutoCreateColumnsFromModel(false);
            setAutoResizeMode(AUTO_RESIZE_OFF);
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tcm = getColumnModel();
            tcm.getColumn(2).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(4).setCellEditor(new DefaultCellEditor(new JmriDeviceSelector()));
            tcm.getColumn(7).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(9).setCellEditor(new DefaultCellEditor(new JmriDeviceSelector()));
            tcm.getColumn(12).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(14).setCellEditor(new DefaultCellEditor(new JmriDeviceSelector()));
            tcm.getColumn(17).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(19).setCellEditor(new DefaultCellEditor(new JmriDeviceSelector()));
            setDefaultRenderer(String.class, new ColorCellRenderer());
            setAutoCreateRowSorter(true);
        }
    }

    public static boolean showPoints() {
        PointsTable table = new PointsTable();
        if (AcceptDialog.select(table, "Point Configurations")) {
            table.Model.updatePoints();
            return true;
        }
        return false;
    }

    /**************************************************************************
     * The TableModel derivative
     **************************************************************************/
    private class PointsTableModel extends AbstractTableModel {

        /**
         * is the foreground color for indicating a problem
         */
        private final Color Problem = Color.red;
        
        /**
         * copies of the defined Decoders.  This will manipulate the copies until
         * the Accept button is pushed; then all changes will be written back.
         */
        private Points Copies[];
        
        /**
         * a bit vector of SwitchPoints that have been modified.  true means
         * the corresponding copy has been changed.
         */
        private boolean Dirty[];
        
        /**
         * a shadow of Copies, containing the Color of each value
         */
        private Color CellColor[][];
        
        /**
         * the default foreground color
         */
        private final Color DefaultColor = UIManager.getColor("Table.foreground");
        
        public PointsTableModel() {
            int count = 0;
            SwitchPoints sw;
            Vector<SecEdge> points = SecEdge.getPtsEdges();
            Copies = new Points[points.size()];
            Dirty = new boolean[Copies.length];
            CellColor = new Color[Copies.length][ColumnNames.length];
            
            // make copies of the Switch Points
            for (SecEdge e: points) {
                if ((sw = e.getPoints()) != null) {
                    Copies[count] = new Points(e, sw.copy());
                    Dirty[count] = false;
                    for (int i = 0; i < CellColor[count].length; i++) {
                        CellColor[count][i] = DefaultColor;
                    }
                    count++;
                }
            }
        }

        @Override
        public int getColumnCount() {
            return ColumnNames.length;
        }

        @Override
        public int getRowCount() {
            return Copies.length;
        }

        @Override
        public Object getValueAt(int row, int column) {
            IOSpec io;
            switch (column) {
            case 0:  // location
                return Copies[row].Edge.toEdge().toString();
            case 1:
                return Copies[row].Pts.getSpur();
            case 2:  // prefix for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io != null) {
                    return io.getPrefix();
                }
                break;
            case 3:  // address for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io != null) {
                    return io.getAddress();
                }
                break;
            case 4:  // device for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io != null) {
                    return io.getDeviceType();
                }
            	break;
            case 5:  //polarity for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io != null) {
                    return io.getCommand();
                }
                return false;
            case 6:  // username for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io != null) {
                    return io.getUserName();
                }
                break;
            case 7:  // prefix for unlock light
                io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io != null) {
                    return io.getPrefix();
                }
                break;
            case 8:  // address for unlock light
                io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io != null) {
                    return io.getAddress();
                }
               break;
            case 9:  // device for unlock light
                io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io != null) {
                    return io.getDeviceType();
                }
            	break;
            case 10:  //polarity for unlock light
                io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io != null) {
                    return io.getCommand();
                }
                return false;
            case 11:  // username for unlock light
                io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io != null) {
                    return io.getUserName();
                }
                break;
            case 12:  // prefix for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io != null) {
                    return io.getPrefix();
                }
                break;
            case 13: // address for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io != null) {
                    return io.getAddress();
                }
                break;
            case 14:  // device for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io != null) {
                    return io.getDeviceType();
                }
            	break;
            case 15: // polarity for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io != null) {
                    return io.getCommand();
                }
               return false;
            case 16: //username for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io != null) {
                    return io.getUserName();
                }
                break;
            case 17: //prefix for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                 if (io != null) {
                    return io.getPrefix();
                }
                break;
            case 18: // address for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                if (io != null) {
                    return io.getAddress();
                }
               break;
            case 19:  // device for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                if (io != null) {
                    return io.getDeviceType();
                }
            	break;
            case 20: // polarity for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                if (io != null) {
                    return io.getCommand();
                }
               return false;
            case 21: // username for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                if (io != null) {
                    return io.getUserName();
                }
                break;
            default:
            }
            return new String("");
        }

        @Override
        public String getColumnName(int i) {
            return ColumnNames[i];
        }
        
        @Override
        public boolean isCellEditable(int row, int column) {
        	IOSpec io;
            switch (column) {
            case 2:
            	io = Copies[row].Pts.getIOSpec(LOCKON);
            	if (io!= null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
            	}
            	break;
            case 3:
            	io = Copies[row].Pts.getIOSpec(LOCKON);
            	if (io!= null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
            	}
            	break;
            case 4:
            	io = Copies[row].Pts.getIOSpec(LOCKON);
            	if (io!= null) {
        			return (io.getPrefix() == null) || (io.getPrefix().equals("none"));
            	}
            	break;
            case 7:
            	io = Copies[row].Pts.getIOSpec(UNLOCKON);
            	if (io!= null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
            	}
            	break;
            case 8:
            	io = Copies[row].Pts.getIOSpec(UNLOCKON);
            	if (io!= null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
            	}
            	break;
            case 9:
            	io = Copies[row].Pts.getIOSpec(UNLOCKON);
            	if (io!= null) {
        			return (io.getPrefix() == null) || (io.getPrefix().equals("none"));
            	}
            	break;
            case 12:
            	io = Copies[row].Pts.getIOSpec(UNLOCKED);
            	if (io!= null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
            	}
            	break;
            case 13:
            	io = Copies[row].Pts.getIOSpec(UNLOCKED);
            	if (io!= null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
            	}
            	break;
            case 14:
            	io = Copies[row].Pts.getIOSpec(UNLOCKED);
            	if (io!= null) {
        			return (io.getPrefix() == null) || (io.getPrefix().equals("none"));
               	}
            	break;
            case 17:
            	io = Copies[row].Pts.getIOSpec(LOCKED);
            	if (io!= null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
            	}
            	break;
            case 18:
            	io = Copies[row].Pts.getIOSpec(LOCKED);
            	if (io!= null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
            	}
            	break;
            case 19:
            	io = Copies[row].Pts.getIOSpec(LOCKED);
            	if (io!= null) {
        			return (io.getPrefix() == null) || (io.getPrefix().equals("none"));
               	}
            	break;           	
            default:
            }
            return true;
        }
        
        @Override
        public Class<?> getColumnClass(int column) {
            switch (column) {
            case 1:  //spur
            case 5:  //polarity for lock light
            case 10:  //polarity for unlock light
            case 15: // polarity for locked report
            case 20: // polarity for unlocked report
                return Boolean.class;
            default:
            }
            return String.class;
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
            IOSpec io;
            switch (column) {
            case 0:  // location - not editable
                break;
            case 1:
                Copies[row].Pts.setSpur((Boolean)value);
                break;
            case 2:  // prefix for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(LOCKON, io);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(io, row, 1);
                break;
            case 3:  // address for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(LOCKON, io);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(io, row, 1);
               break;
            case 4:	 //device for lock light
            	io = Copies[row].Pts.getIOSpec(LOCKON);
            	if (io == null) {
            		io = new IOSpec();
            		Copies[row].Pts.setIOSpec(LOCKON, io);
            		io.setCommand(false);
            	}
            	io.setDeviceType((JmriDevice) value);
            	checkDecoders(io, row, 1);
            	break;
            case 5:  //polarity for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(LOCKON, io);
                }
                io.setCommand((Boolean) value);
                checkDecoders(io, row, 1);
                break;
            case 6:  // username for lock light
                io = Copies[row].Pts.getIOSpec(LOCKON);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(LOCKON, io);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(io, row, 1);
                break;
            case 7:  // prefix for unlock light
                io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(UNLOCKON, io);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(io, row, 5);
                break;
            case 8:  // address for unlock light
                io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(UNLOCKON, io);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(io, row, 5);
                break;
            case 9:	 //device for unlock light
            	io = Copies[row].Pts.getIOSpec(UNLOCKON);
            	if (io == null) {
            		io = new IOSpec();
            		Copies[row].Pts.setIOSpec(UNLOCKON, io);
            		io.setCommand(false);
            	}
            	io.setDeviceType((JmriDevice) value);
            	checkDecoders(io, row, 1);
            	break;
            case 10:  //polarity for unlock light
                 io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(UNLOCKON, io);
                }
                io.setCommand((Boolean) value);
                checkDecoders(io, row, 5);
               break;
            case 11:  // username for unlock light
                io = Copies[row].Pts.getIOSpec(UNLOCKON);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(UNLOCKON, io);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(io, row, 5);
                break;
            case 12:  // prefix for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(LOCKED, io);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(io, row, 9);
                break;
            case 13: // address for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(LOCKED, io);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(io, row, 9);
                break;
            case 14:	 //device for locked report
            	io = Copies[row].Pts.getIOSpec(LOCKED);
            	if (io == null) {
            		io = new IOSpec();
            		Copies[row].Pts.setIOSpec(LOCKED, io);
            		io.setCommand(false);
            	}
            	io.setDeviceType((JmriDevice) value);
            	checkDecoders(io, row, 1);
            	break;
            case 15: // polarity for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(LOCKED, io);
                }
                io.setCommand((Boolean) value);
                checkDecoders(io, row, 9);
                break;
            case 16: //username for locked report
                io = Copies[row].Pts.getIOSpec(LOCKED);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(LOCKED, io);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(io, row, 9);
                break;
            case 17: //prefix for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(UNLOCKED, io);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(io, row, 13);
                break;
            case 18: // address for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(UNLOCKED, io);
                    io.setCommand(false);
               }
                io.setAddress((String) value);
                checkDecoders(io, row, 13);
                break;
            case 19:	 //device for unlocked report
            	io = Copies[row].Pts.getIOSpec(UNLOCKED);
            	if (io == null) {
            		io = new IOSpec();
            		Copies[row].Pts.setIOSpec(UNLOCKED, io);
            		io.setCommand(false);
            	}
            	io.setDeviceType((JmriDevice) value);
            	checkDecoders(io, row, 1);
            	break;
            case 20: // polarity for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(UNLOCKED, io);
                }
                io.setCommand((Boolean) value);
                checkDecoders(io, row, 13);
                break;
            case 21: // username for unlocked report
                io = Copies[row].Pts.getIOSpec(UNLOCKED);
                if (io == null) {
                    io = new IOSpec();
                    Copies[row].Pts.setIOSpec(UNLOCKED, io);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(io, row, 13);
                break;
            default:
            }
           Dirty[row] = true;
        }
        
        /**
         * this method updates the Points that have been modified
         */
        public void updatePoints() {
            IOSpec decoder;
            for (int count = 0; count < Copies.length; ++count) {
                if (Dirty[count]) {
                    Copies[count].Edge.getPoints().setSpur(Copies[count].Pts.getSpur());
                    if (((decoder = Copies[count].Pts.getIOSpec(LOCKON))!= null) &&
                        decoder.isComplete()) {
                        Copies[count].Edge.getPoints().setIOSpec(LOCKON, decoder);
                    }
                     if (((decoder = Copies[count].Pts.getIOSpec(UNLOCKON))!= null) &&
                        decoder.isComplete()) {
                        Copies[count].Edge.getPoints().setIOSpec(UNLOCKON, decoder);
                    }
                    if (((decoder = Copies[count].Pts.getIOSpec(LOCKED))!= null) &&
                        decoder.isComplete()) {
                        Copies[count].Edge.getPoints().setIOSpec(LOCKED, decoder);
                    }
                     if (((decoder = Copies[count].Pts.getIOSpec(UNLOCKED))!= null) &&
                        decoder.isComplete()) {
                        Copies[count].Edge.getPoints().setIOSpec(UNLOCKED, decoder);
                    }
                }
            }
        }
        
        /**
         * this method sets the foreground color on cells in the JTable
         * @param row is the row the cells are on
         * @param start is the first cell to set the color
         * @param stop is one after the last cell to color
         * @param color is the color of the foreground
         */
        private void showError(int row, int start, int stop, Color color) {
            for (; start < stop; start++) {
                CellColor[row][start] = color;
            }            
        }
        
        /**
         * this method checks a Points definition to determine if there is an error.
         * The only errors are in the decoder definitions
         * @param index is the index into Copies of the Points being examined.
         */
        private void isPointsError(int index) {
            checkDecoders(Copies[index].Pts.getIOSpec(LOCKON), index, 2);
            checkDecoders(Copies[index].Pts.getIOSpec(UNLOCKON), index, 7);
            checkDecoders(Copies[index].Pts.getIOSpec(LOCKED), index, 12);
            checkDecoders(Copies[index].Pts.getIOSpec(UNLOCKED), index, 17);
        }
        
        /**
         * checks a decoder definitions for being complete.  The foreground color
         * of incomplete ones are set to red.
         * @param decoder is the decoder being checked
         * @param row is the row in the Table displaying the decoder's information
         * @param col is the first column in the table for the decoder's information
         */
        private void checkDecoders(IOSpec decoder, int row, int col) {
            if ((decoder != null) && !decoder.isComplete()) {
                showError(row, col, col + 5, Problem);
            }
            else {
                showError(row, col, col + 5, DefaultColor);
            }
        }
        
        /**
         * scans the Block data and flags any problems
         */
        public void findErrors() { 
            for (int row = 0; row < Copies.length; row++) {
                isPointsError(row);
            }
        }
        
        /**
         * determines what color to use for the foreground on a table cell
         * @param row is the row index of the cell
         * @param column is the column index of the cell
         * @return the Color
         */
        public Color getCellColor(int row, int column) {
            return CellColor[row][column];
        }
    }
    /**************************************************************************
     * a data structure for holding the information about the Points.  This
     * is a database entry expanded.
     *************************************************************************/
    private class Points {
        public SecEdge Edge;
        public SwitchPoints Pts;
        
        public Points(SecEdge edge, SwitchPoints pts) {
            Edge = edge;
            Pts = pts;
        }
    }
    
   
    /**************************************************************************
     * A cell renderer associated with setting the foreground color
     **************************************************************************/
    private class ColorCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            setForeground(Model.getCellColor(row, column));
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}
/* @(#)PointsTable.java */