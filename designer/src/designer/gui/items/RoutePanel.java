/* Name: RoutePanel.java
 *
 * What:
 *  A class for presenting the fields needed for describing how to
 *  throw the points on a SecEdge for a particular route.
 */
package designer.gui.items;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import designer.gui.jCustom.IOPanel;
import designer.layout.items.RouteInfo;

/**
 * is a class for presenting the fields needed for describing how to
 * throw the points on a SecEdge for a particular route.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2012</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class RoutePanel
extends JPanel {
    
    /**
     * the IOSpec information for each thing associated with a decoder.
     */
    IOPanel Decoders[] = new IOPanel[RouteInfo.RouteName.length];
    
    /**
     * the IOSpec list for moving the points.
     */
    IOPanel MoveCommand;
    
    /**
     * the Normal Route radio button.
     */
    JRadioButton NormButton = new JRadioButton("Normal Route");
    
    /**
     * the other SecEdge of the route.
     */
    private int OtherEnd;
    
    /**
     * the constructor.
     *
     * @param route is the initial value of the RouteInfo.
     * @param term is the SecEdge that the route terminates on.
     * @param norm is the ButtonGroup containing all the Normal Route buttons.
     */
    public RoutePanel(RouteInfo route, int term, ButtonGroup norm) {
        JPanel dec = new JPanel(new GridLayout(0, 2));
        OtherEnd = term;
        norm.add(NormButton);
        add(NormButton);
        if (route == null) {
            for (int msg = 0; msg < Decoders.length; ++msg) {
                Decoders[msg] = new IOPanel(null,
                        RouteInfo.RouteName[msg][RouteInfo.
                                                 LAB_INDEX], false);
                Decoders[msg].setBorder(BorderFactory.createLineBorder(Color.gray));
            }
            MoveCommand = new IOPanel(null,
                    RouteInfo.RouteCommand[RouteInfo.LAB_INDEX],
                    false);
        }
        else {
            if (route.getNormal()) {
                NormButton.setSelected(true);
            }
            for (int msg = 0; msg < Decoders.length; ++msg) {
                Decoders[msg] = new IOPanel(route.getRouteReport(msg),
                        RouteInfo.RouteName[msg][RouteInfo.
                                                 LAB_INDEX], false);
                Decoders[msg].setBorder(BorderFactory.createLineBorder(Color.gray));
            }
            MoveCommand = new IOPanel(route.getRouteCmd(),
                    RouteInfo.RouteCommand[RouteInfo.LAB_INDEX],
                    false);
        }
        MoveCommand.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        dec.add(Decoders[1]);
        dec.add(Decoders[2]);
        dec.add(Decoders[0]);
        dec.add(MoveCommand);
        add(dec);
    }
    
    /**
     * is the method for retrieving the edited fields of the RoutePanel.
     *
     * @return the panel contents translated to data.  The ComponentTag field
     * will be null, but that is fine because it is used only when reading
     * an XML document.
     */
    public RouteInfo getPanel() {
        RouteInfo newInfo = new RouteInfo(OtherEnd);
        boolean noEntries = true;
        newInfo.setNormal(NormButton.isSelected());
        for (int msg = 0; msg < Decoders.length; ++msg) {
            if (Decoders[msg].getSpec() != null) {
                newInfo.setRouteReport(Decoders[msg].getSpec(), msg);
                noEntries = false;
            }
        }
        if (MoveCommand.getSpec() != null) {
            newInfo.setRouteCmd(MoveCommand.getSpec());
            noEntries = false;
        }
        
        if (noEntries && (!NormButton.isSelected())) {
            return null;
        }
        return newInfo;
    }
}
/* @(#)RoutePanel.java */
