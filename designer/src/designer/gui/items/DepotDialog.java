/* Name: DepotDialog.java
 *
 * What:
 *  This Dialog is for adding a depot to a section.
 */
package designer.gui.items;

import designer.gui.frills.FrillLoc;
import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.JScrollList;
import designer.layout.items.Depot;
import designer.layout.items.Section;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * a JDialog for adding or removing a depot Icon from a Section.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2011, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class DepotDialog extends JPanel{

  /**
   * a checkbox for adding a Station.
   */
  JCheckBox Station;

  /**
   * a JScrollPane for selecting where to place the station.
   */
  JScrollList DepotList;

  /**
   * a JScrollPane for selecting the color of the icon
   */
  JComboBox<?> DepotColor;
  
  /**
   * constructs the JPanel for editing the Depot in a Section.
   *
   * @param depot is the depot to be edited.  If null, then default
   * values are used.
   *
   */
  private DepotDialog(Depot depot) {
    FrillLoc loc = null;

    // Set up the Station information.
    if (depot == null) {
    	return;
    }
    loc = depot.getLocation();
    Station = new JCheckBox("Station: ", loc != null);
    DepotList = FrillLoc.newFrillLocPanel(loc);
    DepotList.setEnabled(loc != null);
    DepotColor = depot.getColorFinder().createFinder();
    DepotColor.setEnabled(loc != null);
    Station.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ce) {
        if (Station.getModel().isSelected()) {
          DepotList.setEnabled(true);
          DepotColor.setEnabled(true);
        }
        else {
          DepotList.setEnabled(false);
          DepotColor.setEnabled(false);
        }
      }
    });
    setLayout(new FlowLayout());
    add(Station);
    add(DepotList);
    add(DepotColor);
  }

  /**
   * creates a JDialog for adding or removing a Depot Icon and for
   * positioning it in the Section.  If the Section does not have a
   * Depot Icon, then a default one is created and it is edited.
   *
   * @param sec is the Section being edited.
   *
   * @return true if the user accepted the editing or false if it was
   * cancelled.
   *
   * @see designer.layout.items.Section
   * @see designer.gui.frills.FrillLoc
   */
  static public boolean newDepotIcon(Section sec) {
      Depot d = sec.getStation();
      if (d == null) {
          d = new Depot();
      }
      DepotDialog panel = new DepotDialog(d);
      
      if (AcceptDialog.select(panel, "Position Station Icon:")) {
          sec.hideSec();
          if (panel.Station.getModel().isSelected()) {
              d.getColorFinder().commitSelection((String) panel.DepotColor.getSelectedItem());
              d.setLocation(FrillLoc.newFrillLoc(panel.DepotList.
                      getSelection()));
              d.addSelf(sec);
          }
          else {
              sec.replaceStation(null);
          }
          
          sec.showSec();
          return true;
      }
      return false;
  }
}
/* @(#)DepotDialog.java */
