/*
 * Name: BlockTable
 * 
 * What:
 * A derived JTable for displaying the configuration of all defined Blocks:
 * <ul>
 * <li>Block Name (editable text)</li>
 * <li>Station Name (editable text)</li>
 * <li>Discipline (editable selection list)</li>
 * <li>Occupancy Prefix (editable selection list)</li>
 * <li>Occupancy Address (editable text)</li>
 * <li>Occupancy polarity (editable checkBox)</li>
 * <li>Occupancy User Name (editable text)</li>
 * <li>Unoccupancy Prefix (editable selection list)</li>
 * <li>Unoccupancy Address (editable text)</li>
 * <li>Unoccupancy Polarity (editable checkBox)</li>
 * <li>Unoccupancy User Name (editable text)</li>
 * <li>Train reporter prefix (editable drop down)</li>
 * <li>Train reporter Address (editable text)</li>
 * <li>Train reporter User Name (editable text)</li>
 * </ul> 
 * Blocks cannot be added nor removed via this table.
 *
 */
package designer.gui.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.JmriDeviceSelector;
import designer.gui.jCustom.JmriTypeSelector;
import designer.gui.jCustom.PrefixSelector;
import designer.layout.JmriDevice;
import designer.layout.JmriName;
import designer.layout.items.Block;
import designer.layout.items.IOSpec;
import designer.layout.items.ReporterSpec;

/**
 * A derived JTable for displaying the configuration of all defined Blocks:
 * <ul>
 * <li>Block Name (editable text)</li>
 * <li>Station Name (editable text)</li>
 * <li>Discipline (editable selection list)</li>
 * <li>Occupancy Prefix (editable selection list)</li>
 * <li>Occupancy Address (editable text)</li>
 * <li>Occupancy Device type (editable selection list)</li>
 * <li>Occupancy polarity (editable checkBox)</li>
 * <li>Occupancy User Name (editable text)</li>
 * <li>Unoccupancy Prefix (editable selection list)</li>
 * <li>Unoccupancy Address (editable text)</li>
 * <li>Unoccupancy Device type (editable selection list)</li>
 * <li>Unoccupancy Polarity (editable checkBox)</li>
 * <li>Unoccupancy User Name (editable text)</li>
 * <li>Train reporter prefix (editable drop down)</li>
 * <li>Train reporter Address (editable text)</li>
 * <li>Train reporter User Name (editable text)</li>
 * </ul> 
 * Blocks cannot be added nor removed via this table.
 *
 * <p>Copyright: Copyright (c) 2013</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class BlockTable extends JPanel {
    /**
     * the column header names
     */
    private static final String[] ColumnNames = {
            "Block",
            "Station",
            "Discipline",
            "Occ Pre",
            "Occ Addr",
            "Device",
            "Throw",
            "User Name",
            "Unocc Pre",
            "Unocc Addr",
            "Device",
            "Throw",
            "User Name",
            "Rep Prefix",
            "Rep Addr",
            "User Name"
    };

    /**
     * the size and position of the window
     */
    private static int Height = 200;
    private static int Width = 800;
    /**
     * table model (see below)
     */
    BlockTableModel Model = new BlockTableModel();

    protected JScrollPane ScrollPane;
    protected BTable MyTable;

    public BlockTable() {
        MyTable = new BTable(Model);
        ScrollPane = new JScrollPane(MyTable);
        //ScrollPane.getViewport().add(MyTable);
        ScrollPane.setPreferredSize(new Dimension(Width, Height));
        ScrollPane.getViewport().setBackground(MyTable.getBackground());
        setLayout(new BorderLayout());
        add(ScrollPane, BorderLayout.CENTER);
        ScrollPane.addComponentListener(new ComponentAdapter() {			
			@Override
			public void componentResized(ComponentEvent e) {
				Component comp = e.getComponent();
				Height = comp.getHeight();
				Width = comp.getWidth();
			}
		});
        Model.findErrors();
    }

    /**************************************************************************
     * The JTable derivative    
     **************************************************************************/
    private class BTable extends JTable {
        public BTable(BlockTableModel model) {
            super(model);
            TableColumnModel tcm;
            setAutoCreateColumnsFromModel(false);
            setAutoResizeMode(AUTO_RESIZE_OFF);
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tcm = getColumnModel();
            tcm.getColumn(2).setCellEditor(new DefaultCellEditor(new JComboBox(Block.DisciplineName)));
            tcm.getColumn(3).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(5).setCellEditor(new DefaultCellEditor(new JmriDeviceSelector()));
            tcm.getColumn(8).setCellEditor(PrefixSelector.getEditor());
            tcm.getColumn(10).setCellEditor(new DefaultCellEditor(new JmriDeviceSelector()));
            tcm.getColumn(13).setCellEditor(new DefaultCellEditor(
                    new JComboBox(JmriName.getPrefixesForType(JmriTypeSelector.REPORTER))));
            setDefaultRenderer(String.class, new ColorCellRenderer());
            setAutoCreateRowSorter(true);
        }
    }

    public static boolean showBlocks() {
        BlockTable table = new BlockTable();
        if (AcceptDialog.select(table, "Track Blocks")) {
            table.Model.updateBlocks();
            return true;
        }
        return false;
    }

    /**************************************************************************
     * The TableModel derivative
     **************************************************************************/
    private class BlockTableModel extends AbstractTableModel {

        /**
         * is the foreground color for indicating a problem
         */
        private final Color Problem = Color.red;
        
        /**
         * copies of the defined Blocks.  This will manipulate the copies until
         * the Accept button is pushed; then all changes will be written back.
         */
        private Block Copies[];
        
        /**
         * a bit vector of Blocks that have been modified.  true means
         * the corresponding copy has been changed.
         */
        private boolean Dirty[];
        
        /**
         * a shadow of Copies, containing the Color of each value
         */
        private Color CellColor[][];
        
        /**
         * the default foreground color
         */
        private final Color DefaultColor = UIManager.getColor("Table.foreground");
        
        public BlockTableModel() {
            int count = 0;
            Vector<Block> blocks = Block.getBlocks();
            Copies = new Block[blocks.size()];
            Dirty = new boolean[Copies.length];
            CellColor = new Color[Copies.length][ColumnNames.length];
            
            // make copies of the Blocks
            for (Block b : blocks) {
                Copies[count] = new Block(null);
                b.deepCopy(Copies[count]);
                Dirty[count] = false;
                for (int i = 0; i < CellColor[count].length; i++) {
                    CellColor[count][i] = DefaultColor;
                }
                count++;
            }
        }

        @Override
        public int getColumnCount() {
            return ColumnNames.length;
        }

        @Override
        public int getRowCount() {
            return Copies.length;
        }

        @Override
        public Object getValueAt(int row, int column) {
            Block b = Copies[row];
            switch (column) {
            case 0:
                if (b.getBlockName() != null) {
                    return b.getBlockName();
                }
                break;                
            case 1:
                if (b.getStationName() != null) {
                    return b.getStationName();
                }
                break;
            case 2:
                return Block.DisciplineName[b.getDiscipline()];
            case 3:
                if (b.getOccupied() != null) {
                    return b.getOccupied().getPrefix();
                }
                break;
            case 4:
                if (b.getOccupied() != null) {
                    return b.getOccupied().getAddress();
                }
                break;
            case 5:
            	if (b.getOccupied() != null) {
            		return b.getOccupied().getDeviceType();
            	}
            	break;
            case 6:
                if (b.getOccupied() != null) {
                    return b.getOccupied().getCommand();
                }
                return new Boolean(false);
            case 7:
                if (b.getOccupied() != null) {
                    return b.getOccupied().getUserName();
                }
                break;
            case 8:
                if (b.getUnoccupied() != null) {
                    return b.getUnoccupied().getPrefix();
                }
                break;
            case 9:
                if (b.getUnoccupied() != null) {
                    return b.getUnoccupied().getAddress();
                }
                break;
            case 10:
            	if (b.getUnoccupied() != null) {
            		return b.getUnoccupied().getDeviceType();
            	}
            	break;
            case 11:
                if (b.getUnoccupied() != null) {
                    return b.getUnoccupied().getCommand();
                }
                return new Boolean(false);
            case 12:
                if (b.getUnoccupied() != null) {
                    return b.getUnoccupied().getUserName();
                }
                break;
            case 13:
                if (b.getReporter() != null) {
                    return b.getReporter().getPrefix();
                }
                break;
            case 14:
                if (b.getReporter() != null) {
                    return b.getReporter().getAddress();
                }
                break;
            case 15:
                if (b.getReporter() != null) {
                    return b.getReporter().getUserName();
                }
                break;
           default:
            }
            return new String("");
        }

        @Override
        public String getColumnName(int i) {
            return ColumnNames[i];
        }
        
        @Override
        public boolean isCellEditable(int row, int column) {
            Block b = Copies[row]; 
            IOSpec io;
        	switch (column) {
        	case 3: // Occupied prefix
        	case 4: // Occupied address
        		io = b.getOccupied();
        		if (io != null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
        		}
        		break;
        	case 5: // Occupied device
        		io = b.getOccupied();
        		if (io != null) {
        			return ((io.getPrefix() == null) || io.getPrefix().equals("none"));
        		}
        		break;
        	case 8: // Unoccupied prefix
        	case 9: // Unoccupied address
        		io = b.getUnoccupied();
        		if (io != null) {
        			return (io.getDeviceType() == null) || (io.getDeviceType() == JmriDevice.none);
        		}
        		break;
        	case 10: // Unoccupied device
        		io = b.getUnoccupied();
        		if (io != null) {
        			return ((io.getPrefix() == null) || io.getPrefix().equals("none"));
        		}
        		break;
        	case 14: // reporter address
        		io = b.getReporter();
        		if (io != null) {
        			return ((io.getPrefix() != null) && !io.getPrefix().equals("none"));
        		}
        		break;
        		
        	default:
        	}
            return true;
        }
        
        @Override
        public Class<?> getColumnClass(int column) {
            switch (column) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return Integer.class;
            case 3:
                return String.class;
            case 4:
                return String.class;
            case 5:
            	return String.class;
            case 6:
                return Boolean.class;
            case 7:
                return String.class;
            case 8:
                return String.class;
            case 9:
                return String.class;
            case 10:
            	return String.class;
            case 11:
                return Boolean.class;
            case 12:
                return String.class;
            case 13:
                return String.class;
            case 14:
                return String.class;
            case 15:
                return String.class;
             default:   
            }
            return String.class;
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
            Block b = Copies[row];
            IOSpec io;
            ReporterSpec rep;
            String svalue = value.toString();
            switch (column) {
            case 0:  //Block name
                b.setBlockName((String) value);
                isBlockError(row);
                break;                
            case 1:  //Station name
                b.setStationName((String) value);
                break;
            case 2:  //Discipline
                for (int i = 0; i < Block.DisciplineName.length; i++) {
                    if (svalue.equals(Block.DisciplineName[i])) {
                        b.setDiscipline(i);
                        break;
                    }
                }
                
                break;
            case 3:  //Occupied prefix
                io = b.getOccupied();
                if (io == null) {
                    io = new IOSpec();
                    b.setOccupied(io);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(row);
                break;
            case 4: // Occupied address
                io = b.getOccupied();
                if (io == null) {
                    io = new IOSpec();
                    b.setOccupied(io);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(row);
                break;
            case 5: // Occupied device type
            	io = b.getOccupied();
            	if (io == null) {
            		io = new IOSpec();
            		b.setOccupied(io);
            		io.setCommand(false);
            	}
            	io.setDeviceType((JmriDevice) value);
            	checkDecoders(row);
            	break;
            case 6: //Occupied polarity
                io = b.getOccupied();
                if (io == null) {
                    io = new IOSpec();
                    b.setOccupied(io);
                    io.setCommand(false);
                }
                io.setCommand((Boolean) value);
                checkDecoders(row);
                break;
            case 7:  //Occupied username
                io = b.getOccupied();
                if (io == null) {
                    io = new IOSpec();
                    b.setOccupied(io);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(row);
                break;
            case 8:  //Unoccupied prefix
                io = b.getUnoccupied();
                if (io == null) {
                    io = new IOSpec();
                    b.setUnoccupied(io);
                    io.setCommand(false);
                }
                io.setPrefix((String) value);
                checkDecoders(row);
                break;
            case 9:  //Unoccupied address
                io = b.getUnoccupied();
                if (io == null) {
                    io = new IOSpec();
                    b.setUnoccupied(io);
                    io.setCommand(false);
                }
                io.setAddress((String) value);
                checkDecoders(row);
                break;
            case 10: //Unoccupied device type
            	io = b.getUnoccupied();
            	if (io == null) {
            		io = new IOSpec();
            		b.setUnoccupied(io);
            		io.setCommand(false);
            	}
            	io.setDeviceType((JmriDevice) value);
            	checkDecoders(row);
            	break;
            case 11:  //Unoccupied polarity
                io = b.getUnoccupied();
                if (io == null) {
                    io = new IOSpec();
                    b.setUnoccupied(io);
                    io.setCommand(false);
                }
                io.setCommand((Boolean) value);
                checkDecoders(row);
                break;
            case 12:  //Unoccupied username
                io = b.getUnoccupied();
                if (io == null) {
                    io = new IOSpec();
                    b.setUnoccupied(io);
                    io.setCommand(false);
                }
                io.setUserName((String) value);
                checkDecoders(row);
                break;
            case 13:  // Reporter prefix
                rep = b.getReporter();
                if (rep == null) {
                    rep = new ReporterSpec(null);
                    b.setReporter(rep);
                }
                rep.setPrefix((String) value);
                checkDecoders(row);
                break;
            case 14:  //Reporter address
                rep = b.getReporter();
                if (rep == null) {
                    rep = new ReporterSpec(null);
                    b.setReporter(rep);
                }
                rep.setAddress((String) value);
                checkDecoders(row);
                break;
            case 15:  // Reporter username
                rep = b.getReporter();
                if (rep == null) {
                    rep = new ReporterSpec(null);
                    b.setReporter(rep);
                }
                rep.setUserName((String) value);
                checkDecoders(row);
                break;
           default:
            }
           Dirty[row] = true;
        }
        
        /**
         * this method updates the Blocks that have been modified
         */
        public void updateBlocks() {
            Vector<Block> originals = Block.getBlocks();
            for (int count = 0; count < Copies.length; ++count) {
                if (Dirty[count]) {
                    originals.elementAt(count).replaceBlock(Copies[count]);
                }
            }
        }
        
        /**
         * this method sets the foreground color on cells in the JTable
         * @param row is the row the cells are on
         * @param start is the first cell to set the color
         * @param stop is one after the last cell to color
         * @param color is the color of the foreground
         */
        private void showError(int row, int start, int stop, Color color) {
            for (; start < stop; start++) {
                CellColor[row][start] = color;
            }            
        }
        
        /**
         * this method checks a Block definition to determine if there is an error.
         * An error is when 
         * <ul>
         * <li>the block name is null</li>
         * <li>the block name is the empty string</li>
         * <li>another block has the same name<li>
         * <li>a decoder is defined and it is not complete<\li>
         * </ul>
         * Except for the last, an error is handled by changing the foreground
         * color of the row to red.  For the last, only the decoder fields
         * are changed to red.
         * @param index is the index into Copies of the Block being examined.
         */
        private void isBlockError(int index) {
            String name = Copies[index].getBlockName();
            showError(index, 0, ColumnNames.length, DefaultColor);
            if ((name == null) || name.trim().isEmpty()) {
                showError(index, 0, ColumnNames.length, Problem);
            }
            else {
                for (int row = 0; row < Copies.length; row++) {
                    if ((row != index) && (Copies[row].getBlockName() != null) &&
                            name.equals(Copies[row].getBlockName().trim())) {
                        showError(index, 0, ColumnNames.length, Problem);
                        break;
                    }
                }
            }
            checkDecoders(index);
        }
        
        /**
         * checks the decoder definitions for being complete.  The foreground color
         * of incomplete ones are set to red.
         * @param index is the index of the Block being checked
         */
        private void checkDecoders(int index) {
            IOSpec spec;
            if (CellColor[index][0] != Problem) {
                spec = Copies[index].getOccupied();
                if ((spec != null) && !spec.isComplete()) {
                    showError(index, 3, 8, Problem);
                }
                else {
                    showError(index, 3, 8, DefaultColor);
                }
                spec = Copies[index].getUnoccupied();
                if ((spec != null) && !spec.isComplete()) {
                    showError(index, 8, 13, Problem);
                }
                else {
                    showError(index, 8, 13, DefaultColor);
                }
                spec = Copies[index].getReporter();
                if ((spec != null) && !spec.isComplete()) {
                    showError(index, 13, 16, Problem);
                }                
                else {
                    showError(index, 13, 16, DefaultColor);
                }
            }
        }
        
        /**
         * scans the Block data and flags any problems
         */
        public void findErrors() { 
            for (int row = 0; row < Copies.length; row++) {
                isBlockError(row);
            }
        }
        
        /**
         * determines what color to use for the foreground on a table cell
         * @param row is the row index of the cell
         * @param column is the column index of the cell
         * @return the COlor
         */
        public Color getCellColor(int row, int column) {
            return CellColor[row][column];
        }
    }
    
    /**************************************************************************
     * A cell renderer associated with setting the foreground color
     **************************************************************************/
    private class ColorCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            setForeground(Model.getCellColor(row, column));
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}
/* @(#)BlockTable.java */