/*
 * Name: EdgePanel.java
 *
 * What: This file contains a Singleton, Factory object for constructing
 *  the JPanel for setting the values of a single Section Edge.
 */
package designer.gui.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.BitSet;
import java.util.EnumMap;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.IOPanel;
import designer.layout.EnumerationLocks;
import designer.layout.items.Block;
import designer.layout.items.Edge;
import designer.layout.items.IOSpec;
import designer.layout.items.IntermediateSignal;
import designer.layout.items.SecEdge;
import designer.layout.items.SecSignal;
import designer.layout.items.Section;
import designer.layout.items.SwitchPoints;
import designer.layout.items.XEdge;

/**
 *  This file contains a Singleton, Factory object for constructing
 *  the JPanel for setting the values of a single Section Edge.  It has
 * two pieces:
 * <ul>
 * <li>
 * the top piece is information for either the Points or a Signal (implying
 * the SecEdge is on a Block boundary).
 * <li>
 * the bottom piece is information for specifying the SecEdge connected
 * to.  It can either be adjacent, or somewhere else.
 * </ul>
 *  <p>
 *  To make the changes permanent, the caller should invoke
 *  the commit method.
 *  <p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2020</p>
 * @author Rodney Black
 * @version $Revision$
 */

public class EdgePanel
extends JPanel {

    /**
     * the Button for pulling up the Signal definition dialog.
     */
    private JButton SigButton;

    /**
     * is the information for placing the Signal Icon on the dispatcher's
     * panel and the instructions for controlling the actual Signals.  If
     * the SecEdge does not have switch points and its connecting SecEdge
     * does not have switch points, then the Signal definition button is
     * enabled.
     * <p>
     * If the SecEdge being edited has a Signal, then it is a Block boundary.
     * so the signal's contents are used as the initial value for the
     * SignalDialog.  However, if the SecEdge does not have a Signal, then
     * it is not a Block boundary, so the SignalDialog presents a blank.
     */
    SecSignal SigDef = null;

    /**
     * the JCheckBox for adding or editing Block information.
     */
    private JCheckBox BlockBox;

    /**
     * the Button for pulling up the Block definition dialog.
     */
    private JButton BlockButton;

    /**
     * the Block information.  This is propagated to all Tracks that connect
     * to this SecEdge so that the other Block boundaries can share the
     * same information.
     * <p>
     * It has the same considerations as the Signal.
     */
    Block BlockDef = null;

    /**
     * the Button for pulling up the Locks dialog
     */
    private JButton LocksButton;

    /**
     * The Locks information
     */
    private EnumMap<EnumerationLocks, IOSpec> LocksDef = null;

    /**
     * are the instructions for controlling the switch points that are on
     * the SecEdge.  It is picked up from the SecEdge being edited and
     * may not have any contents.  It must exist if there are switch points.
     */
    SwitchPoints PtsDef = null;

    /**
     * are the definitions of the track ends forming a crossover
     */
    XEdge Xdef = null;

    /**
     * is the JButton pushed to pop up the Switch Points definition panel
     */
    JButton PtsButton;

    /**
     * is the JButton pushed to pop up the Crossing definition panel
     */
    JButton XButton;

    /**
     * is true when the Points option is selected and false when the
     * crossing option is selected.
     */
    boolean SelectPts = true;

    /**
     * describes the routes by identifying the other end of each route.
     */
    private BitSet OtherEnds;

    /**
     * the toggle button on the Adjacent field
     */
    private JCheckBox Adjacent;

    /**
     * the JTextField for the X coordinates.
     */
    private JTextField XField = new JTextField(3);

    /**
     * the JTextField for the Y coordinates.
     */
    private JTextField YField = new JTextField(3);

    /**
     * the JList containing the Names of the Edges
     */
    private JList<?> EdgeList = new JList(Edge.EDGENAME);

    /**
     * the Edge Pane
     */
    private JScrollPane EdgePane = new JScrollPane(EdgeList);

    /**
     * the JPanel that holds the Shared SecEdge information.
     */
    private JPanel EdgeInfoPanel = new JPanel();

    /**
     * the SecEdge being edited.
     */
    private SecEdge OldEdge;

    /**
     * the enclosing Section.
     */
    private Section Enclosure;

    /**
     * is the class constructor.
     *
     * @param edge is the SecEdge being edited.  It can not be null because
     * a SecEdge cannot be created through the GUI.  SecEdges are created
     * only through Track definitions.
     *
     * @see designer.layout.items.Section
     */
    public EdgePanel(SecEdge edge) {
        String id = new String(edge.getX() + "," + edge.getY() + " : " +
                Edge.fromEdge(edge.getEdge()));

        JRadioButton choosePts;
        JRadioButton chooseXing;

        JPanel comboPanel = null; // Panel for holding the Pts and Crossing buttons
        boolean adjacent;
        JPanel xPanel = new JPanel(); // Panel for accepting link's X coordinates
        JPanel yPanel = new JPanel(); // Panel for accepting link's Y coordinates
        JPanel ePanel = new JPanel(); // Panel for accepting link's edge
        JPanel sPanel = null; // Panel for constructing Signal selection
        ButtonGroup edgeType = null;

        OldEdge = edge;
        OtherEnds = edge.getSection().getTrackGroup().getEnds(edge.getEdge());
        setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.gray), id));
        Enclosure = edge.getSection();
        SelectPts = true;

        if (Enclosure.countTracks(edge.getEdge()) > 1) {
            PtsDef = edge.getPoints();
            PtsButton = new JButton("Define Switch Points");
            PtsButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    SwitchPoints newPts = PointsDialog.getPoints(PtsDef, OtherEnds);
                    if (newPts != null) {
                        PtsDef = newPts;
                    }
                }
            });
            if (XEdge.isPossibleXEdge(edge, edge.getShared())) {
                Xdef = edge.getCrossing();
                XButton = new JButton("Define Crossing");
                XButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        XEdge newX = XEdgeDialog.getXEdge(Xdef, OtherEnds);
                        if (newX != null) {
                            Xdef = newX;
                        }
                    }
                });
                comboPanel = new JPanel(new GridLayout(2, 2));
                edgeType = new ButtonGroup();
                choosePts = new JRadioButton("Tracks are turnout", true);
                choosePts.addItemListener(new ItemListener() {
                    public void itemStateChanged(ItemEvent arg0) {
                        XButton.setEnabled(false);
                        PtsButton.setEnabled(true);
                        SelectPts = true;
                    }});
                edgeType.add(choosePts);
                chooseXing = new JRadioButton("Tracks are crossing", true);
                chooseXing.addItemListener(new ItemListener() {
                    public void itemStateChanged(ItemEvent arg0) {
                        if (arg0.getStateChange() == ItemEvent.SELECTED) {
                            XButton.setEnabled(true);
                            PtsButton.setEnabled(false);
                            // This is commented out so XEdgeDialog will be forced to create
                            // the Xdef from OtherEnds
                            //                      if (Xdef == null) {
                            //                          Xdef = new XEdge();
                            //                      }
                            SelectPts = false;
                        }
                    }});
                edgeType.add(chooseXing);
                if ((Xdef == null) || edge.getPreference()) {
                    choosePts.setSelected(true);
                    XButton.setEnabled(false);
                    PtsButton.setEnabled(true);
                }
                else {
                    chooseXing.setSelected(true);
                    XButton.setEnabled(true);
                    PtsButton.setEnabled(false);
                    SelectPts = false;
                }
                comboPanel.add(choosePts);
                comboPanel.add(PtsButton);
                comboPanel.add(chooseXing);
                comboPanel.add(XButton);
            }
        }
        else if (edge.canBeBlock()) {
            BlockDef = edge.getBlock();
            BlockButton = new JButton("Define Block");
            BlockButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    Block newBlk = BlockDialog.newBlock(BlockDef);
                    if (newBlk != null) {
                        BlockDef = newBlk;
                    }
                }
            });

            // Now do the Signal
            SigDef = edge.getSignal();
            SigButton = new JButton("Define Signal");
            SigButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    SecSignal newSig = SignalDialog.newSignal(SigDef);
                    if (newSig != null) {
                        SigDef = newSig;
                    }
                }
            });

            // Now do the Locks
            LocksDef = edge.getLocks();
            LocksButton = new JButton("Define Locks");
            LocksButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    LocksDef = new LockPanel(LocksDef).editLocks();
                }
            });
            BlockBox = new JCheckBox("Block Boundary", BlockDef != null);
            BlockBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ce) {
                    if (BlockBox.getModel().isSelected()) {
                        BlockButton.setEnabled(true);
                        SigButton.setEnabled(true);
                        LocksButton.setEnabled(true);
                        if (BlockDef == null) {
                            BlockDef = OldEdge.getBlockInfo();
                        }
                    }
                    else {
                        BlockButton.setEnabled(false);
                        SigButton.setEnabled(false);
                        LocksButton.setEnabled(false);
                    }
                }
            });
            BlockButton.setEnabled(BlockDef != null);
            SigButton.setEnabled(BlockDef != null);
            LocksButton.setEnabled(BlockDef != null);
            sPanel = new JPanel();
            sPanel.add(BlockBox);
            sPanel.add(BlockButton);
            sPanel.add(SigButton);
            /*****************************************************
             ************************* FUTURE ADDITION ***********
            sPanel.add(LocksButton);
            ******************************************************
            ******************************************************/
        }

        adjacent = edge.isAdjacentPeer();
        Adjacent = new JCheckBox("Joins to adjacent track", adjacent);
        Adjacent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ce) {
                if (Adjacent.getModel().isSelected()) {
                    XField.setEnabled(false);
                    YField.setEnabled(false);
                    EdgeList.setEnabled(false);
                }
                else {
                    XField.setEnabled(true);
                    YField.setEnabled(true);
                    EdgeList.setEnabled(true);
                }
            }
        });

        if (adjacent) {
            XField = new JTextField("   ");
            YField = new JTextField("   ");
            EdgeList.setSelectedIndex( -1);
        }
        else {
            SecEdge other = edge.getShared();
            XField = new JTextField(String.valueOf(other.getX()));
            YField = new JTextField(String.valueOf(other.getY()));
            EdgeList.setSelectedIndex(other.getEdge());
        }
        XField.setEnabled(!adjacent);
        YField.setEnabled(!adjacent);
        EdgeList.setEnabled(!adjacent);
        xPanel.add(new JLabel("Column number:"));
        xPanel.add(XField);
        yPanel.add(new JLabel("Row number:"));
        yPanel.add(YField);
        ePanel.add(new JLabel("Edge:"));
        EdgeList.setVisibleRowCount(Edge.EDGENAME.length);
        ePanel.add(EdgePane);
        EdgeInfoPanel.setLayout(new BorderLayout());
        EdgeInfoPanel.add("North", Adjacent);
        EdgeInfoPanel.add("West", xPanel);
        EdgeInfoPanel.add("Center", yPanel);
        EdgeInfoPanel.add("East", ePanel);
        EdgeInfoPanel.setBorder(BorderFactory.createEtchedBorder());

        setLayout(new BorderLayout());
        if (sPanel != null) {
            add("Center", sPanel);
        }
        else if (comboPanel != null) {
            add("Center", comboPanel);
        }
        else if (PtsButton != null) {
            add("Center", PtsButton);
        }
        add("South", EdgeInfoPanel);
    }

    /**
     * creates a SecEdge based on the values the user entered.
     *
     * @return a new SecEdge, as indicated.
     *
     * @see designer.layout.items.SecEdge
     */
    public SecEdge create() {
        Edge newDescript = null;
        boolean adjacent = Adjacent.getModel().isSelected();
        if (!adjacent) {
            int x;
            int y;
            int edge;
            try {
                x = Integer.parseInt(XField.getText().trim());
                y = Integer.parseInt(YField.getText().trim());
                if ( (edge = EdgeList.getSelectedIndex()) != -1) {
                    newDescript = new Edge(x, y, edge);
                }
            }
            catch (NumberFormatException nfe) {
                if ( (XField.getText().length() != 0) ||
                        (YField.getText().length() != 0)) {
                    System.out.println("No update - Edge must be integer coordinates.");
                }
            }
        }
        if ( (BlockBox != null) && BlockBox.getModel().isSelected()) {
            if (SigDef != null) {
                /*
                 * There are four cases:
                 * 1. no physical and no panel => nullify the SecSignal
                 * 2. physical and no panel    => intermediate signal
                 * 3. no physical and panel    => dummy signal for dispatcher control
                 * 4. physical and panel       => control point
                 */
                if (SigDef.getSigIcon() == null) {
                    if (SigDef.getSigDescription() == null) {  // case 1 - destroy SecSignal
                        SigDef = null;
                    }
                    else {  // case 2 - intermediate.  Create an icon showing the
                        // intermediate signal
                        SigDef.setSigIcon(new IntermediateSignal(OldEdge.getEdge()));
                    }
                }
            }
            OldEdge.setSignal(SigDef);
            OldEdge.setBlock(BlockDef);
            if (OldEdge.getLocks() != LocksDef) {
                OldEdge.setLocks(LocksDef);
            }
        }
        else {
            OldEdge.setSignal(null);
            OldEdge.setBlock(null);
            OldEdge.setLocks(null);
        }
        if ((PtsDef != null) || (Xdef != null)) {
            OldEdge.setPoints(PtsDef);
            OldEdge.setCrossing(Xdef);
            OldEdge.setPreference(SelectPts);
        }
        OldEdge.rebindToEdge(newDescript);
        return OldEdge;
    }
    /**
     * A JPanel for presenting the IOSPec for the external Locks.
     */
    private class LockPanel extends JPanel {

        /**
         * The GUI form of the map being edited
         */
        private EnumMap<EnumerationLocks, IOPanel> LockMap = new EnumMap<EnumerationLocks, IOPanel>(EnumerationLocks.class);
        
        /**
         * The IOSpec form of the map being edited
         */
        private EnumMap<EnumerationLocks, IOSpec> MyMap;

        /**
         * the ctor.  From the map parameter, it creates IOPanels (the editable, GUI form of an IOSPec) and
         * places them in itself.  A call to editLocks() displays the panel.
         * 
         * map is the map of existing EnumerationLocks to IOspecs
         */
        public LockPanel(EnumMap<EnumerationLocks, IOSpec> map) {
            MyMap = map;
            EnumerationLocks lock;
            EnumerationLocks[] locks = EnumerationLocks.values();
            IOPanel spec;
            setLayout(new GridLayout(0, 3));
            for (int i = 0; i < locks.length; ++i) {
                lock = locks[i];
                if ((map != null) && map.containsKey(lock)) {
                    spec = new IOPanel((IOSpec) map.get(lock).copy(), lock.name(), false);
                }
                else {
                    spec = new IOPanel(null, lock.name(), false);
                }
                spec.setBorder(BorderFactory.createTitledBorder(
                        BorderFactory.createLineBorder(Color.gray)));
                LockMap.put(lock, spec);
                add(spec);
            }
        }

        /**
         * displays the JPanel in an Accept/Cancel window.  If it is accepted, then a new map is generated
         * and the IOSpecs are placed in that map.  The new map is returned.  If it is cancelled, the
         * original map is returned.
         * @param map
         * @return
         */
        public EnumMap<EnumerationLocks, IOSpec> editLocks() {
            EnumMap<EnumerationLocks, IOSpec> newMap = new EnumMap<EnumerationLocks, IOSpec>(EnumerationLocks.class);
            IOSpec spec;
            if (AcceptDialog.select(this, "Define External Locks:")) {
                for (EnumerationLocks lock : EnumerationLocks.values()) {
                    spec = LockMap.get(lock).getSpec();
                    if (spec != null) {
                        newMap.put(lock, spec);
                    }
                }
                return newMap;
            }
            return MyMap;
        }
    }
}
/* @(#)EdgePanel.java */
