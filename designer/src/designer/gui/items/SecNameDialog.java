/*
 * Name: SecNameDialog
 *
 * What: This file contains a Singleton, Factory object for constructing
 *  the JDialog for setting the values of Section Names.
 */
package designer.gui.items;

import designer.gui.frills.FrillLoc;
import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.JScrollList;
import designer.layout.items.Section;
import designer.layout.items.SecName;
import java.awt.FlowLayout;
import javax.swing.*;

/**
 *  This file contains a Singleton, Factory object for constructing
 *  the JDialog for setting the values of SectionNames.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2011, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class SecNameDialog extends JPanel{
    static private final String Title = "Specify a label:";
    
    JScrollList Location;
    JTextField Name;
    JComboBox<?> FontSelector;
    
    /**
     * is the class constructor.
     *
     * @param name is the name description being edited or, if null, created.
     *
     * @see designer.layout.items.SecName
     */
    public SecNameDialog(SecName name) {
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        
        if ( (name != null) && (name.getName() != null)) {
            Name = new JTextField(name.getName());
            if (name.getName().length() < 8) {
                Name.setColumns(8);
            }
        }
        else {
        	Name = new JTextField(15);
        	name = new SecName(FrillLoc.newFrillLoc(0));
        }
        Location = FrillLoc.newFrillLocPanel(name.getLocation());
        FontSelector = name.getFontFinder().createFinder();
        panel1.add(new JLabel("Position in Tile"));
        panel1.add(Location);
        panel2.add(new JLabel("Name of Tile"));
        panel2.add(Name);
        panel3.add(new JLabel("Font"));
        panel3.add(FontSelector);
        setLayout(new FlowLayout());
        add(panel1);
        add(panel2);
        add(panel3);
    }
    
    /**
     * creates a JDialog for a SecName and fills in the fields of the
     * SecName.  This method assumes that the SecName is not null.
     *
     * @param sec is the Section containing the Name.
     *
     * @return true if the operation succeeds or false if it fails.
     *
     * @see designer.layout.items.SecName
     */
    static public boolean newSecName(Section sec) {
        SecName secName = sec.getName();
        if (secName == null) {
            secName = new SecName(null);
        }
        SecNameDialog dialog = new SecNameDialog(secName);
        if (AcceptDialog.select(dialog, Title)) {
            sec.hideSec();
            if ((dialog.Name != null) &&(dialog.Name.getText().trim().length() != 0) 
                    && (dialog.Location.getSelection() != -1)) {
                secName.getFontFinder().commitSelection((String) dialog.FontSelector.getSelectedItem());
                secName.setAll(dialog.Name.getText().trim(),
                        FrillLoc.newFrillLoc(dialog.Location.getSelection()),
                        secName.getFontFinder().getFontTag());
                secName.addSelf(sec);                
            }
            else {
                sec.replaceSecName(null);
            }
            sec.showSec();
            return true;
        }
        return false;
    }
}
/* @(#)SecNameDialog.java */
