/*
 * Name: LockTable
 * 
 * What:
 * A derived JTable for displaying the configuration of Locks on all Block edges.
 * Because of the number of Locks and because a Lock is specific to a Block Edge, the
 * table can get quite large.  To avoid showing the full table, only the Locks which
 * have a definition and the Block edges that have a Lock are displayed.
 * @see designer.layout.EnumerationLocks.java
 * <p>
 * Block edges cannot be added nor removed via this table.  Any Lock class that does
 * not have an instance is not shown.  Any Block edge without a Lock is not shown.
 *
 */
package designer.gui.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.PrefixSelector;
import designer.layout.EnumerationLocks;
import designer.layout.items.IOSpec;
import designer.layout.items.SecEdge;

/**
 * A derived JTable for displaying the configuration of Locks on all Block edges.
 * Because of the number of Locks and because a Lock is specific to a Block Edge, the
 * table can get quite large.  To avoid showing the full table, only the Locks which
 * have a definition and the Block edges that have a Lock are displayed.
 * @see designer.layout.EnumerationLocks
 * <p>
 * Block edges cannot be added nor removed via this table.  Any Lock class that does
 * not have an instance is not shown.  Any Block edge without a Lock is not shown.
 *
 * <p>Copyright: Copyright (c) 2013, 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class LockTable extends JPanel {
 
    /**
     * the offset of the first Lock column.  Column 0 has the location
     * and Column 1 has the Block name
     */
    private static final int BASE = 2;
    
    /**
     * the number of columns used by a decoder definition
     */
    private static final int DEC_WIDTH = 4;
    
    /**
     * the number of locks that a Block can have
     */
    private static final int MAX_LOCKS = EnumerationLocks.values().length;
    
    /**
     * the number of columns in the table - 1 for the grid location, 1 for the Block name, and 4 for each
     * Lock (prefix, address, polarity, user name)
     */
    private static final int MAX_COLUMNS = BASE + (MAX_LOCKS * DEC_WIDTH); 
    
    /**
     * the column header names
     */
    private static String ColumnNames[] = new String[MAX_COLUMNS];

    /**
     * table model (see below)
     */
    LockTableModel Model = new LockTableModel();

    protected JScrollPane ScrollPane;
    protected LTable MyTable;

    public LockTable() {
        int col = BASE;
        ColumnNames[0] = "Where";
        ColumnNames[1] = "Block";
        for (EnumerationLocks e : EnumerationLocks.values()) {
            ColumnNames[col++] = e.toString();
            ColumnNames[col++] = "Address";
            ColumnNames[col++] = "Throw";
            ColumnNames[col++] = "User Name";
        }
        MyTable = new LTable(Model);
        ScrollPane = new JScrollPane(MyTable);
        ScrollPane.setPreferredSize(new Dimension(800, 200));
        ScrollPane.getViewport().setBackground(MyTable.getBackground());
        setLayout(new BorderLayout());
        add(ScrollPane, BorderLayout.CENTER);
        Model.findErrors();
    }

    /**************************************************************************
     * The JTable derivative    
     **************************************************************************/
    private class LTable extends JTable {
        public LTable(LockTableModel model) {
            super(model);
            TableColumnModel tcm;
            int col;
            setAutoCreateColumnsFromModel(false);
            setAutoResizeMode(AUTO_RESIZE_OFF);
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            setAutoCreateRowSorter(true);
            tcm = getColumnModel();
            for (col = BASE; col < MAX_COLUMNS; col += DEC_WIDTH) {
                tcm.getColumn(col).setCellEditor(PrefixSelector.getEditor());
            }
            setDefaultRenderer(String.class, new ColorCellRenderer());
            for (int column : model.findHiddenColumns()) {
                tcm.removeColumn(tcm.getColumn(column));
            }
        }
    }

    public static boolean showLocks() {
        LockTable table = new LockTable();
        if (AcceptDialog.select(table, "Block Locks")) {
            table.Model.updateLocks();
            return true;
        }
        return false;
    }

    /**************************************************************************
     * The TableModel derivative
     **************************************************************************/
    private class LockTableModel extends AbstractTableModel {

        /**
         * is the foreground color for indicating a problem
         */
        private final Color Problem = Color.red;

        /**
         * a list of all Block edges with one or more Locks
         */
        private ArrayList<SecEdge> EdgeList= new ArrayList<SecEdge>();
        
        /**
         * copies of the defined locks.  This will manipulate the copies until
         * the Accept button is pushed; then all changes will be written back.
         */
        private IOSpec Copies[][];
        
        /**
         * a bit vector of Blocks that have been modified.  true means
         * the corresponding copy has been changed.
         */
        private boolean Dirty[];
        
        /**
         * a shadow of Copies, containing the Color of each value
         */
        private Color CellColor[][];
        
        /**
         * the default foreground color
         */
        private final Color DefaultColor = UIManager.getColor("Table.foreground");
        
        public LockTableModel() {
            int edge = 0;
            Vector<SecEdge> edges = SecEdge.getBlkEdges();
            EnumMap<EnumerationLocks, IOSpec> lockMap;
            EnumerationLocks[] locks = EnumerationLocks.values();
            EnumerationLocks lock;

            for (SecEdge e : edges) {
                if ((e.getLocks() != null) && !e.getLocks().isEmpty()) {
                    EdgeList.add(e);
                }
            }
            Copies = new IOSpec[EdgeList.size()][MAX_LOCKS];
            Dirty = new boolean[EdgeList.size()];
            CellColor = new Color[EdgeList.size()][MAX_COLUMNS];
            
            // make copies of the Locks on each Block edge
            for (SecEdge e : EdgeList) {
                Copies[edge] = new IOSpec[ColumnNames.length];
                Dirty[edge] = false;
                if ((lockMap = e.getLocks()) != null ) {
                	for (int l = 0; l < locks.length; ++l) {
                		lock = locks[l];
                		if (lockMap.containsKey(lock)) {
                			Copies[edge][l]= (IOSpec) lockMap.get(lock).copy();
                		}
                	}
                }
                for (int i = 0; i < CellColor[edge].length; i++) {
                    CellColor[edge][i] = DefaultColor;
                }
                edge++;
            }
        }

        @Override
        public int getColumnCount() {
            return MAX_COLUMNS;
        }

        @Override
        public int getRowCount() {
            return Copies.length;
        }

        @Override
        public Object getValueAt(int row, int column) {
            int lock = (column - BASE) / DEC_WIDTH;
            switch (column) {
            case 0:
                return EdgeList.get(row).toEdge().toString();
            case 1:
                return EdgeList.get(row).getBlock().getBlockName();
           default:
               column -= BASE;
               switch (column % DEC_WIDTH) {
               case 0:
                   // the JMRI prefix
                   if (Copies[row][lock] != null) {
                       return Copies[row][lock].getPrefix();
                   }
                   break;
                   
               case 1:
                   // The decoder address
                   if (Copies[row][lock] != null) {
                       return Copies[row][lock].getAddress();
                   }
                   break;
                   
               case 2:
                   // the throw/close polarity
                   if (Copies[row][lock] != null) {
                       return Copies[row][lock].getCommand();
                   }
                   return false;
               default:
                   // the user name
                   if (Copies[row][lock] != null) {
                       return Copies[row][lock].getUserName();
                   }
              }
            }
            return new String("");
        }

        @Override
        public String getColumnName(int i) {
            return ColumnNames[i];
        }
        
        @Override
        public boolean isCellEditable(int row, int column) {
            return (column >= BASE);
        }
        
        @Override
        public Class<?> getColumnClass(int column) {
            if (((column - BASE) % DEC_WIDTH) == 2) {
                return Boolean.class;
            }
            return String.class;
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
            int lock = (column - BASE) / DEC_WIDTH;
            IOSpec io;
            switch (column) {
            case 0:  // location cannot be changed
            case 1:  // Block name cannot be changed
                break;
           default:
               switch ((column - BASE) % DEC_WIDTH) {
               case 0:
                   // the JMRI prefix
                   io = Copies[row][lock];
                   if (io == null) {
                       io = new IOSpec();
                       Copies[row][lock] = io;
                       io.setCommand(false);
                   }
                   io.setPrefix((String) value);
                   checkDecoder(row, lock);
                   break;
                   
               case 1:
                   // The decoder address
                   io = Copies[row][lock];
                   if (io == null) {
                       io = new IOSpec();
                       Copies[row][lock] = io;
                       io.setCommand(false);
                   }
                   io.setAddress((String) value);
                   checkDecoder(row, lock);
                   break;
                   
               case 2:
                   // the throw/close polarity
                   io = Copies[row][lock];
                   if (io == null) {
                       io = new IOSpec();
                       Copies[row][lock] = io;
                   }
                   io.setCommand((Boolean) value);
                   checkDecoder(row, lock);
                   break;
                   
               default:
                   // the user name
                   io = Copies[row][lock];
                   if (io == null) {
                       io = new IOSpec();
                       Copies[row][lock] = io;
                       io.setCommand(false);
                   }
                   io.setUserName((String) value);
                   checkDecoder(row, lock);
               }
            }
           Dirty[row] = true;
        }
        
        /**
         * this method updates the Locks that have been modified
         */
        public void updateLocks() {
            EnumMap<EnumerationLocks, IOSpec> lockMap;
            EnumerationLocks[] locks = EnumerationLocks.values();
            EnumerationLocks lock;
            for (int edge = 0; edge < EdgeList.size(); ++edge) {
                if (Dirty[edge]) {
                	if ((lockMap = EdgeList.get(edge).getLocks()) != null) {
                		for (int l = 0; l < locks.length; ++l) {
                			lock = locks[l];
                			if ((Copies[edge][l] == null) || !Copies[edge][l].isComplete()) {
                				lockMap.remove(lock);
                			}
                			else {
                				lockMap.put(lock, Copies[edge][l]);
                			}
                		}
                	}
                    EdgeList.get(edge).locksChanged();
                }
            }
        }

        /**
         * this method sets the foreground color on cells in the JTable
         * @param row is the row the cells are on
         * @param start is the first cell to set the color
         * @param stop is one after the last cell to color
         * @param color is the color of the foreground
         */
        private void showError(int row, int start, int stop, Color color) {
            for (; start < stop; start++) {
                CellColor[row][start] = color;
            }            
        }
        
        /**
         * checks a decoder definition for being complete.  The foreground color
         * of an incomplete decoder is set to red
         * @param edge is the index of the SecEdge containing the decoder
         * @param lock is the index of the lock
         */
        private void checkDecoder(int edge, int lock) {
            Color color = Problem;
            if (Copies[edge][lock] != null) {
                if (Copies[edge][lock].isComplete()) {
                    color = DefaultColor;
                }
                showError(edge, BASE + (lock * DEC_WIDTH), BASE + ((lock + 1) * DEC_WIDTH), color);
            }
        }
        
        /**
         * scans the Block data and flags any problems
         */
        public void findErrors() { 
            for (int edge = 0; edge < EdgeList.size(); ++edge) {
                for (int lock = 0; lock < Copies[edge].length; ++lock) {
                    checkDecoder(edge, lock);
                }
            }
        }
        
        /**
         * determines what color to use for the foreground on a table cell
         * @param row is the row inde xof the cell
         * @param column is the column index of the cell
         * @return the COlor
         */
        public Color getCellColor(int row, int column) {
            return CellColor[row][column];
        }
        
        /**
         * this method scans locks and determines which ones are not being
         * used.  For each Lock not in use, its columns are added to the list
         * returned.
         * @return a list of all columns that should be hidden
         */
        public ArrayList<Integer> findHiddenColumns() {
            ArrayList<Integer> hide = new ArrayList<Integer>();
            boolean used;
            for (int lock = 0; lock < MAX_LOCKS; ++lock) {
                used = false;
                for (int edge = 0; edge < Copies.length; ++edge) {
                    if (Copies[edge][lock] != null) {
                        used = true;
                        break;
                    }
                }
                if (!used) {
                    hide.add(0, BASE + (lock * DEC_WIDTH));
                    hide.add(0, BASE + 1 + (lock * DEC_WIDTH));
                    hide.add(0, BASE + 2 + (lock * DEC_WIDTH));
                    hide.add(0, BASE + 3 + (lock * DEC_WIDTH));
                }
            }
            return hide;
        }
    }
    
    /**************************************************************************
     * A cell renderer associated with setting the foreground color
     **************************************************************************/
    private class ColorCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            setForeground(Model.getCellColor(row, column));
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}
/* @(#)LockTable.java */