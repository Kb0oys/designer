/* Name: PointsDialog.java
 *
 * What:
 *   This class creates Objects which are used to create and edit the
 *   information about the switch points.
 */
package designer.gui.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.BitSet;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import designer.gui.jCustom.AcceptDialog;
import designer.gui.jCustom.IOPanel;
import designer.layout.items.Edge;
import designer.layout.items.RouteInfo;
import designer.layout.items.SwitchPoints;

/**
 * is a JDialog for creating and editing information for controlling switch
 * points.
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2011
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */

public class PointsDialog extends JPanel {
    
    /**
     * the IOPanels for each of the Points parameters.
     */
    private IOPanel[] Specs = new IOPanel[SwitchPoints.MessageName.length];
    
    /**
     * The TabbedPane for all routes.
     */
    private JTabbedPane TabPane = new JTabbedPane();
    
    /**
     * The RoutePanels for each Route.
     */
    private RoutePanel[] RoutePanes = new RoutePanel[Edge.EDGENAME.length];
    
    /**
     * The Spur selection.
     */
    private JCheckBox SpurBox = new JCheckBox("Spur");
    
    /**
     * the constructor.
     *
     * @param points
     *            is the SwitchPoints being edited.
     * @param ends
     *            specifies which routes are included.
     */
    public PointsDialog(SwitchPoints points, BitSet ends) {
        JPanel ControlPanel = new JPanel();
        JPanel TogglePanel = new JPanel(new GridLayout(0, 2));
        ButtonGroup normRoute = new ButtonGroup();
        
        if (points != null) {
            for (int i = 0; i < SwitchPoints.MessageName.length; ++i) {
                Specs[i] = new IOPanel(points.getIOSpec(i),
                        SwitchPoints.MessageName[i][SwitchPoints.MSGLABEL],
                        (i < 2));
                Specs[i].setBorder(BorderFactory.createLineBorder(Color.gray));
                TogglePanel.add(Specs[i]);
            }
            SpurBox.setSelected(points.getSpur());
        } else {
            for (int i = 0; i < SwitchPoints.MessageName.length; ++i) {
                Specs[i] = new IOPanel(null,
                        SwitchPoints.MessageName[i][SwitchPoints.MSGLABEL],
                        (i < 2));
                Specs[i].setBorder(BorderFactory.createLineBorder(Color.gray));
                TogglePanel.add(Specs[i]);
            }
            SpurBox.setSelected(false);
        }
        
        // Set up the enclosing JPanel
        ControlPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        ControlPanel.add(SpurBox);
        ControlPanel.add(TogglePanel);
        
        // Set up the RoutePanels for each route.
        for (int route = 0; route < RoutePanes.length; ++route) {
            if (ends.get(route)) {
                if ((points == null) || (points.getRoute(route) == null)) {
                    RoutePanes[route] = new RoutePanel(null, route, normRoute);
                } else {
                    RoutePanes[route] = new RoutePanel(points.getRoute(route),
                            route, normRoute);
                }
                TabPane.add(Edge.fromEdge(route), RoutePanes[route]);
            }
        }
        
        // Finally, put it all together.
        setLayout(new BorderLayout());
        add("North", ControlPanel);
        add("South", TabPane);
    }
    
    /**
     * is the mechanism to create the JDialog and return its results.
     *
     * @param points
     *            are the SwitchPoints being edited
     * @param ends
     *            are the other ends of the tracks
     *
     * @return the contents of the JPanel in a SwitchPoints data structure (if
     *         Accept is pushed) or null (if Cancel is pushed).
     *
     * @see designer.layout.items.SwitchPoints
     */
    static SwitchPoints getPoints(SwitchPoints points, BitSet ends) {
        PointsDialog dialog = new PointsDialog(points, ends);
        SwitchPoints newPoints = null;
        RouteInfo info;
        if (AcceptDialog.select(dialog, "Definition of Switch Points")) {
            newPoints = new SwitchPoints();
            for (int i = 0; i < SwitchPoints.MessageName.length; ++i) {
                newPoints.setIOSpec(i, dialog.Specs[i].getSpec());
            }
            // Pick up the changes for each route.
            for (int route = 0; route < dialog.RoutePanes.length; ++route) {
                if ((dialog.RoutePanes[route] != null)
                        && (info = dialog.RoutePanes[route].getPanel()) != null) {
//                        newPoints = new SwitchPoints();
                    newPoints.setRoute(route, info);
                }
            }
            newPoints.setSpur(dialog.SpurBox.isSelected());
        }
        return newPoints;
    }
}
/* @(#)PointsDialog.java */
