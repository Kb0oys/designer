/* Name: ImageItemDialog.java
 *
 * What:
 *  ImageItemDialog defines a JPanel (with Accept and Cancel buttons) for
 *  creating or editing an Image file name to be painted in a GridTile.
 */
package designer.gui.items;

import designer.layout.items.ImageItem;
import designer.gui.jCustom.AcceptDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * is a JPanel for creating or editing an Image file name to be painted
 * in a GridTile.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2011, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class ImageItemDialog
extends JPanel {
    
    /**
     * the enable/disable file name selection chackbox.
     */
    private JCheckBox ImageBox = new JCheckBox("Select File:");
    
    /**
     * The file name.
     */
    private JTextField ImageName;
    
    /**
     * The "browse" JButton.
     */
    private JButton Browse = new JButton("browse");
    
    /**
     * constructs the ImageItemDialog.
     *
     * @param fileName is the initial value to use for the file name.
     */
    public ImageItemDialog(ImageItem fileName) {
        if ((fileName == null) || (fileName.getImageName() == null)) {
            ImageBox.setSelected(false);
            ImageName = new JTextField(20);
            ImageName.setEnabled(false);
            Browse.setEnabled(false);
        }
        else {
            ImageBox.setSelected(true);
            if (fileName.getImageName() == null) {
                ImageName = new JTextField(20);
            }
            else {
                ImageName = new JTextField(fileName.getImageName());
                if (fileName.getImageName().length() < 20) {
                    ImageName.setColumns(20);
                }
            }
        }
        ImageBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ce) {
                if (ImageBox.getModel().isSelected()) {
                    ImageName.setEnabled(true);
                    Browse.setEnabled(true);
                }
                else {
                    ImageName.setEnabled(false);
                    Browse.setEnabled(false);
                }
            }
        });
        Browse.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent be) {
                int result;
                JFileChooser chooser = new JFileChooser(new File(designer.gui.DispPanel.FilePath));
                chooser.setDialogTitle("Select image file:");
                result = chooser.showOpenDialog(null);
                File fileobj = chooser.getSelectedFile();
                if (result == JFileChooser.APPROVE_OPTION) {
                    if (fileobj.exists() && fileobj.canRead()) {
                        designer.gui.DispPanel.FilePath = fileobj.getAbsolutePath();
                        ImageName.setText(fileobj.getAbsolutePath());
                    }
                }
            }
        });
        add(ImageBox);
        add(ImageName);
        add(Browse);
    }
    
    /**
     * creates a Dialog for editing a file name (assumed to name an Image
     * file).  If a value is passed in, the file name takes on that value.
     * Otherwise, or if the user elects to edit the filename, a file selector
     * is created.
     *
     * @param fileName is the initial file name.
     *
     * @return the file name selected, or null if the "enable" JCheckBox
     * is turned off.
     *
     * @see designer.layout.items.ImageItem
     * @see java.awt.Image
     */
    public static ImageItem newImage(ImageItem fileName) {
        ImageItemDialog panel = new ImageItemDialog(fileName);
        String name;
        if (AcceptDialog.select(panel, "Image File Name:")) {
            if (panel.ImageBox.getModel().isSelected()) {
                name = panel.ImageName.getText().trim();
            }
            else {
                name = null;
            }
            if (name == null) {
                fileName = null;
            }
            else if ((fileName == null) || !name.equals(fileName.getImageName())) {
                fileName = new ImageItem();
                fileName.setImageName(name);
            }
        }
        return fileName;
    }
}
/* @(#)ImageItemDialog.java */
