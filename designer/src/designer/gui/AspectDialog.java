/* Name: AspectDialog.java
 *
 * What:
 * constructs a Dialog which are two Grid Layouts, an Accept button, and a Cancel
 * button. The Contents of the Grids are the minimum of the protected track speeds in the
 * left column and a column for next track speed. The signal head entries are a
 * pull down of possible signal presentations, based on semaphore or light, for each head.
 * <p>
 * The second Grid Layout is similar, but smaller.  It contains cells for Stop, Stop and
 * Go, and Approach Restricting.  Restricting is in the upper grid, with a next speed of None.
 */
package designer.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import designer.gui.jCustom.AcceptDialog;
import designer.layout.AspectMap;

/**
 * constructs a Dialog which are two Grid Layouts, an Accept button, and a Cancel
 * button. The Contents of the Grids are the minimum of the protected track speeds in the
 * left column and a column for next track speed. The signal head entries are a
 * pull down of possible signal presentations, based on semaphore or light, for each head.
 * <p>
 * The second Grid Layout is similar, but smaller.  It contains cells for Stop, Stop and
 * Go, and Approach Restricting.  Restricting is in the upper grid, with a next speed of None.
 *
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A model railroad dispatching program
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2006, 2010, 2011, 2012, 2013, 2014
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */

public class AspectDialog extends JPanel {

	/**
	 * is a constant used to substring into presentation
	 * tags for identifying a flashing aspect.  This constant
	 * is also used in SWSignalHead in CATS.
	 */
	public static final String FLASHING = "flashing ";

	/**
	 * the Strings identifying all possible light presentations.
	 * There is one important dependency.  The HeadEnumeration
	 * in AspectMap depends upon "off" being the first element.
	 * Furthermore, HeadStates in CATS depends on the off presentation
	 * being called "off".  So, if "off" moves, or the String is
	 * changed, be sure to make those other changes.
	 */
	public static final String[] LightAspect = { "off", "green", "yellow",
		"red", FLASHING + "green", FLASHING + "yellow", FLASHING + "red",
		"lunar", FLASHING + "lunar"};

	/**
	 * the Strings identifying all possible semaphore presentations.
	 */
	public static final String[] SemaphoreAspect = { "horizontal", "diagonal",
	"vertical" };

	/**
	 * the column labels, which correspond to speeds
	 */
	public static final String[] SpeedName = {
		"", "Normal", "Limited", "Medium", "Slow", "None", "Approach", "Halt"};

	/**
	 * the number of Columns (including headings).
	 */
	public static final int COLUMNS = SpeedName.length;

	/**
	 * the number of rows (including headings).  None, Approach, and Halt are not
	 * speeds.
	 */
	public static final int ROWS = SpeedName.length - 3;

	/**
	 * a place to put the presentation of each head for each indication.
	 */
	JComboBox NewMap[][] = new JComboBox[AspectMap.IndicationNames.length][];

	/**
	 * the table of aspects for each speed
	 */
	private AspectPanel[][] AspectSelection;

	/**
	 * the selection box for Stop aspect
	 */
	private AspectPanel HaltSelection;

	/**
	 * the selection box for Stop and Proceed
	 */
	private AspectPanel ProceedSelection;

	/**
	 * is the scratchpad of aspect name
	 */
	private JTextField[] AspectLabels;

	/**
	 * is the registry of speeds used.
	 */
	private boolean[] SpeedRegistry;

	/**
	 * is am array of flags for showing which aspects
	 * have a problem.  True means there is a problem, so the field
	 * initializes to all false - no problems.
	 */
	private boolean[] AspectError;

	/**
	 * is the list of the aspect names on entry
	 */
	private static String[] AspectNames;

	/**
	 * a checkbox for approach lighting.
	 */
	JCheckBox ApproachBox = new JCheckBox("Approach Lighting", false);

	/**
	 * a checkbox for advance approach (distant signal).
	 */
	JCheckBox AdvanceBox = new JCheckBox("Advance", false);

	/**
	 * a checkbox for hold - CATS will only hold/release the aspect,
	 * so the SignalMast determines the aspect.
	 */
	JCheckBox HoldBox = new JCheckBox("Hold only", false);

	/**
	 * The JDialog used for setting the name values.
	 */
	JDialog Dialog = new JDialog((Frame) null, "Define a Signal Template", true);

	/**
	 * constructs the Dialog, using the modal model.
	 *
	 * @param map
	 *            is an existing AspectMap being edited or null.
	 * @param heads
	 *            is the number of signal heads. heads should be > 0.
	 * @param lamp
	 *            is true if the signal is composed of lights and false if it is
	 *            composed of semaphores.
	 *
	 * @see designer.layout.AspectMap
	 */
	private AspectDialog(AspectMap map, int heads, boolean lamp) {
		JPanel specialAspects = new JPanel();
		JPanel tempPanel;
		//        JPanel speedAspects = new JPanel(new GridLayout(Track.TrackSpeed.length,
		//                SpeedName.length + 1));  // account for headings, no row for Stop
		JPanel speedAspects = new JPanel(new GridLayout(ROWS, COLUMNS));  // account for headings
		String[] items = (lamp) ? LightAspect : SemaphoreAspect;
		String[][] defaultAspects = (lamp) ? AspectMap.DefaultLights[heads - 1] :
			AspectMap.DefaultSemaphores[heads - 1];
		int indication;

		AspectLabels = new JTextField[AspectNames.length];

		// this collects the speeds being used.  The elements are:
		// 0 - Default
		// 1 - Normal
		// 2 - Limited
		// 3 - Medium
		// 4 - Slow
		// 5 - None
		// 6 - Approach
		// 7 - Halt
		SpeedRegistry = new boolean[SpeedName.length];
		Ctc.getLayout().determineSpeeds(SpeedRegistry);
		SpeedRegistry[5] = SpeedRegistry[7] = true;
		SpeedRegistry[6] = (map != null) && map.getAdvance();
		AspectError = new boolean[AspectNames.length];
		setLayout(new BorderLayout());
		add(new JLabel("Protected"), BorderLayout.WEST);
		add(new JLabel("Next Speed", SwingConstants.CENTER), BorderLayout.NORTH);

		// this creates the panels for Approach Restricting, Halt, Stop & Proceed
		tempPanel = new JPanel();
		tempPanel.add(new JLabel("Halt", SwingConstants.RIGHT));
		indication = AspectMap.findRule("Halt", "");
		HaltSelection = new AspectPanel(indication,
				buildIndices(map, defaultAspects[indication], items, indication),
				items);
		tempPanel.add(HaltSelection);
		//        tempPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		specialAspects.add(tempPanel);

		tempPanel = new JPanel();
		tempPanel.add(new JLabel("Stop & Proceed", SwingConstants.RIGHT));
		indication = AspectMap.findRule("Proceed", "");
		ProceedSelection = new AspectPanel(indication,
				buildIndices(map, defaultAspects[indication], items, indication),
				items);
		tempPanel.add(ProceedSelection);
		//        tempPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		specialAspects.add(tempPanel);
		if (lamp) {
			if (map != null) {
				ApproachBox.setSelected(map.getApproach());
			}
			ApproachBox.setHorizontalAlignment(SwingConstants.CENTER);
			specialAspects.add(ApproachBox);
		}
		if (map != null) {
			HoldBox.setSelected(map.getHoldOnly());
		}
		HoldBox.setHorizontalAlignment(SwingConstants.RIGHT);
		specialAspects.add(HoldBox);
		add(specialAspects, BorderLayout.SOUTH);

		// The next chunk of code creates the column headings
		//        speedAspects.add(new JLabel(""));
		for (int heading = 0; heading < COLUMNS; ++heading) {
			if (SpeedName[heading].equals("Approach")) {
				speedAspects.add(AdvanceBox);
			}
			else {
				speedAspects.add(new JLabel(SpeedName[heading], SwingConstants.CENTER));
			}
		}

		// the next chunk creates the rows of the speed table. The "Protected" dimension
		// (rows) have three fewer speeds than the "next" dimension because "None"
		// "Approach", and "Stop" are not speeds.
		AspectSelection = new AspectPanel[ROWS][];
		for (int pspeed = 1; pspeed < ROWS; ++pspeed) {
			AspectSelection[pspeed] = new AspectPanel[SpeedName.length];
			speedAspects.add(new JLabel(SpeedName[pspeed], SwingConstants.RIGHT));
			for (int nspeed = 1; nspeed < SpeedName.length; ++nspeed) {
				indication = AspectMap.findRule(SpeedName[pspeed], SpeedName[nspeed]);
				if (indication >= 0) {
					AspectSelection[pspeed][nspeed] = new AspectPanel(indication,
							buildIndices(map, defaultAspects[indication], items, indication),
							items);
					//                    AspectSelection[pspeed][nspeed].setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
					speedAspects.add(AspectSelection[pspeed][nspeed]);
					if (!SpeedRegistry[pspeed] || !SpeedRegistry[nspeed]) {
						AspectSelection[pspeed][nspeed].enableSelection(false);
					}
				}
			}
		}
		//        AspectSelection = new AspectPanel[ROWS][];
		//        for (int pspeed = 0; pspeed < ROWS; ++pspeed) {
		//            if (SpeedName[pspeed].equals("Approach")) {
		//                for (int nspeed = 0; nspeed < (COLUMNS + 1); ++nspeed) {
		//                    speedAspects.add(new JLabel(""));
		//                }
		//            }
		//            else {
		//                AspectSelection[pspeed] = new AspectPanel[COLUMNS];
		//                speedAspects.add(new JLabel(SpeedName[pspeed], SwingConstants.RIGHT));
		//                for (int nspeed = 0; nspeed < COLUMNS; ++nspeed) {
		//                    indication = AspectMap.findRule(SpeedName[pspeed], SpeedName[nspeed]);
		//                    if (indication >= 0) {
		//                        AspectSelection[pspeed][nspeed] = new AspectPanel(indication,
		//                                buildIndices(map, defaultAspects[indication], items, indication),
		//                                items);
		//                        AspectSelection[pspeed][nspeed].setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		//                        speedAspects.add(AspectSelection[pspeed][nspeed]);
		//                        AspectSelection[pspeed][nspeed].enableSelection(speedRegistry[nspeed + 1] & speedRegistry[pspeed + 1]);
		//                    }
		//                    else {
		//                        AspectSelection[pspeed][nspeed] = null;
		//                        speedAspects.add(new JLabel(""));
		//                    }
		//                }
		//            }
		//        }
		add(speedAspects, BorderLayout.CENTER);

		AdvanceBox.setHorizontalAlignment(SwingConstants.CENTER);
		if ((map != null) && map.getAdvance()) {
			AdvanceBox.setSelected(true);
		}
		else {
			enableApproach(false);
		}
		AdvanceBox.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0) {
				enableApproach(AdvanceBox.isSelected());
			}            
		});
	}

	/**
	 * searches a list of aspect names for one that matches a String.  If found, it
	 * returns the index (selection) of the String.
	 * @param aspectName is the list of aspect names (either LightAspect or
	 * SemaphoreAspect)
	 * @param candidate is the String being matched
	 * @return the index of candidate in aspectName, if found; otherwise -1.
	 */
	private int aspectToIndex(String aspectName[], String candidate) {
		for (int test = 0; test < aspectName.length; ++test) {
			if (aspectName[test].equals(candidate)) {
				return test;
			}
		}
		return -1;
	}

	/**
	 * builds the array of indices for the current selection for each
	 * head for an aspect.
	 * @param map contains the current selections, as Strings
	 * @param defaults is an array containing the default String for the
	 * indication
	 * @param aspects is the array of aspects for the signal type
	 * @param ind is the indication
	 * @return and array containing the selections
	 */
	private int[] buildIndices(AspectMap map, String defaults[],
			String[] aspects, int ind) {
		int[] selections = new int[defaults.length];
		String current;
		for (int head = 0; head < defaults.length; ++head) {
			if ((map == null)
					|| ((current = map.getPresentation(ind, head)) == null)) {
				current = defaults[head];
			}
			selections[head] = aspectToIndex(aspects, current);
		}
		return selections;        
	}

	/**
	 * this method searches the indication table for a particular indication
	 * by index.
	 * @param indication  indication is the index into the array of indications
	 * @return the AspectPanel that contains the selected aspects for the indication
	 */
	private AspectPanel findAspectPanel(int indication) {
		//        if (RestrictedSelection.getIndex() == indication) {
		//            return RestrictedSelection;
		//        }
		if (HaltSelection.getIndex() == indication) {
			return HaltSelection;
		}
		if (ProceedSelection.getIndex() == indication) {
			return ProceedSelection;
		}
		for (int pspeed = 1; pspeed < (SpeedName.length); ++pspeed){
			for (int nspeed = 1; nspeed < SpeedName.length; ++nspeed) {
				if (AspectSelection[pspeed][nspeed].getIndex() == indication) {
					return AspectSelection[pspeed][nspeed];
				}
			}
		}
		//        for (int pspeed = 1; pspeed < ROWS; ++pspeed){
			//            for (int nspeed = 1; nspeed < COLUMNS; ++nspeed) {
		//                if ((AspectSelection[pspeed] != null) && (AspectSelection[pspeed][nspeed] != null)
		//                        && AspectSelection[pspeed][nspeed].getIndex() == indication) {
		//                    return AspectSelection[pspeed][nspeed];
		//                }
		//            }
		//        }
		return null;
	}

	/**
	 * enables (or disables) the Aspects for next == Approach.  This
	 * method cannot be invoked until the Approach aspects have
	 * been instantiated.
	 *
	 * @param setting is true to enable the Aspect selection box and
	 * false to disable it.
	 */
	public void enableApproach(boolean setting) {
		boolean enabled;
		for (int pspeed = 1; pspeed < ROWS; ++pspeed) {
			enabled = (SpeedRegistry[pspeed]) ? setting : false;
			AspectSelection[pspeed][6].enableSelection(enabled);
		}       
	}

	/**
	 * runs a check on the aspect names and indications.  A name must
	 * have at least one character in it.  If multiple aspects are the same,
	 * then the indications must also be the same.  This is because
	 * the name is used as a hashtag tag in JMRI AppearanceMaps and
	 * tags must be unique.
	 * <p>
	 * Invalid aspects are flagged on the screen with a red border.
	 * @param map is an ApsectMap whose contents are being examined
	 * @return true if no problems are detected
	 */
	public boolean verifyAspects(AspectMap map) {
		boolean valid = true;
		String trimmed;
		int heads;
		for (int i = 0; i < AspectError.length; i++) {
			AspectError[i] = false;
		}
		for (int i = 0; i < AspectError.length; i++) {
			trimmed = AspectNames[i].trim();
			AspectNames[i] = trimmed;
			if (trimmed.isEmpty()) {
				valid = false;
				AspectError[i] = true;
			}
		}
		if (map != null) {
			heads = map.getHeadCount();
			for (int i = 0; i < (AspectError.length - 1); i++) {
				for (int j = (i + 1); j < AspectError.length; j++) {
					if (AspectNames[i].equals(AspectNames[j])) {
						for (int h = 0; h < heads; h++) {
							if ((map.getPresentation(i, h) != null) &&
									!map.getPresentation(i, h).equals(map.getPresentation(j, h))) {
								valid = false;
								AspectError[i] = AspectError[j] = true;
							}
							else {
								break;
							}
						}
					}
				}
			}
		}
		return valid;
	}
	/**
	 * creates the AspectMap Dialog and waits for the Accept or Cancel button to
	 * be pushed.
	 *
	 * @param map
	 *            is an existing AspectMap being edited or null.
	 * @param heads
	 *            is the number of signal heads. heads must be > 0.
	 * @param lamp
	 *            is true if the signal is composed of lights and false if it is
	 *            composed of semaphores.
	 * @param names
	 *            is the list of aspect names (aliases) for identifying aspects
	 *
	 * @return a new AspectMap containing the values entered or null (if the
	 *         Cancel button was selected)
	 *
	 * @see designer.layout.AspectMap
	 */
	static public AspectMap createAspectMap(AspectMap map, int heads,
			boolean lamp, String[] names) {
		AspectDialog ad;
		AspectMap am;
		AspectPanel ap;
		String s[];
		AspectNames = names;
		ad = new AspectDialog(map, heads, lamp);
		ad.verifyAspects(map);
		while (AcceptDialog.select(ad, "Signal Aspect Template")) {

			am = new AspectMap();
			// Copy the signal presentations from the Dialog to the AspectMap
			//            for (int ind = 0; ind < AspectMap.IndicationNames.length; ++ind) {
			//                s = new String[heads];
			//                ap = ad.findAspectPanel(ind);
			//                if (ap == null) {
			//                    System.out.println("Something is not working right.  " +
			//                            "Could not find the aspects for indication " + ind + ".");
			//                }
			//                else {
			//                    s = ap.getResults();
			//                }
			//                am.addAspect(ind, s);
			//            }
			for (int ind = 0; ind < AspectNames.length; ++ind) {
				s = new String[heads];
				ap = ad.findAspectPanel(ind);
				if (ap != null) {
					s = ap.getResults();
					am.addAspect(ind, s);
				}
				AspectNames[ind] = ad.AspectLabels[ind].getText();
			}
			if (lamp) {
				am.setApproach(ad.ApproachBox.isSelected());
			}
			else {
				am.setApproach(false);
			}
			am.setAdvance(ad.AdvanceBox.isSelected());
			am.setHoldOnly(ad.HoldBox.isSelected());
			if (ad.verifyAspects(am)) {
				return am;
			}
			for (int a = 0; a < AspectNames.length; a++) {
				ap = ad.findAspectPanel(a);
				if (ad.AspectError[a]) {
					ap.setBorder(BorderFactory.createLineBorder(Color.red, 4));
				}
				else {
					ap.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
				}
			}
		}
		return null;
	}

	/**
	 * an internal class for constructing the JComboBoxes for each
	 * indication.
	 */
	private class AspectPanel extends JPanel {

		/**
		 * the JComboBoxes - a variable is needed so the selected
		 * indexes can be retrieved.
		 */
		private JComboBox boxes[];

		/**
		 * the index of the aspect being configured in the aspect list
		 */
		int listIndex = -1;
		
		/**
		 * the index of the ApectMapping
		 */
		final int MyMapping;

		/**
		 * constructs a JPanel with a title and a column of JComboBoxes
		 * @param aspectIndex is the index of the aspect in the aspect list
		 * @param aspects is an array of indexes containing the initial selections
		 * @param items is the list in each JComboBox
		 */     
		public AspectPanel(int aspectIndex, int aspects[],
				String items[]) {
			super();
			MyMapping = aspectIndex;
			listIndex = aspectIndex;
			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			boxes = new JComboBox[aspects.length];
			AspectLabels[aspectIndex] = new JTextField(AspectNames[aspectIndex]);
			add(AspectLabels[aspectIndex]);
			for (int i = 0; i < aspects.length; ++i) {
				boxes[i] = new JComboBox(items);
				boxes[i].setSelectedIndex(aspects[i]);
				add(boxes[i]);
			}
			if (AspectError[aspectIndex]) {
				setBorder(BorderFactory.createLineBorder(Color.red, 4));
			}
			else {
				setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			}
		}

		/**
		 * constructs an array of selection.  Each element is the String
		 * that denotes the selection for a head.
		 * @return the array
		 */
		public String[] getResults() {
			String[] results = new String[boxes.length];
			if (!boxes[0].isEnabled()) {
				return null;
			}
			for (int i = 0; i < results.length; ++i) {
				results[i] = (String) boxes[i].getSelectedItem();
			}
			return results;
		}

		/**
		 * returns the index of the aspect being configured
		 * @return the index in the aspect list of the aspect
		 */
		public int getIndex() {
			return listIndex;
		}

		/**
		 * enables (or disables) the selection boxes
		 * 
		 * @param setting is true to enable the selection boxes and
		 * false to disable them.
		 */
		public void enableSelection(boolean setting){
			AspectLabels[MyMapping].setEnabled(setting);
			for (int i = 0; i < boxes.length; ++i) {
				boxes[i].setEnabled(setting);
			}            
		}
	}
}
/* @(#)AspectDialog.java */
