/*
 * Name: LineFactory
 *
 * What: This file contains a Singleton, Factory object for constructing
 *  CtcLines - line width descriptions whose values can be changed while
 *  the dispatcher program is running (so that the user can adjust the look).
 *  In addition to creating and returning a CtcLine object, it creates a
 *  MenuItem for editing the CtcLine object and adds the MenuItem to the
 *  Lines pulldown.  It also adds the mnemonic for the object to the XML
 *  parser.
 */
package designer.gui;

import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JMenuItem;

import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 * This file contains a Singleton, Factory object for constructing CtcLines -
 * line descriptions whose values can be changed while the dispatcher program is
 * running (so that the user can adjust the look). In addition to creating and
 * returning a CtcLine object, it creates a MenuItem for editing the CtcLine
 * object and adds the MenuItem to the Lines pulldown. It also adds the mnemonic
 * for the object to the XML parser so that the user can set the line width in
 * the configuration file.
 * <p>
 * To add a new CtcLine Object, add a creation line (see createLine).
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2010, 2020
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */

public class LineFactory implements XMLEleFactory, XMLEleObject, Savable {
    private static final String LINE = "LINE";
    
    /**
     * is the internal name of the diagonal line width
     */
    public static final String DIAGONAL = "DIAGONAL";
    
    /**
     * is the internal name of the horizontal and vertical line widths.
     */
    public static final String LEVEL = "LEVEL";
    
    private DispPanel Parent; // where to show the Dialog
    
    private Vector<MnenomicEntry> Mnenomic;
    
    /**
     * is the class constructor.
     *
     * @param parent
     *            is the root of the GUI classes.
     */
    public LineFactory(DispPanel parent) {
        Parent = parent;
        MnenomicEntry entry;
        Enumeration<MnenomicEntry> e;
        Mnenomic = new Vector<MnenomicEntry>();
        
        createLine("Diagonal", DIAGONAL);
        createLine("non-Diagonal", LEVEL);
        // Register with XML
        XMLReader.registerFactory(LINE, this);
        e = Mnenomic.elements();
        while (e.hasMoreElements()) {
            entry = (e.nextElement());
            XMLReader.registerFactory(entry.MnenomicName, entry.LineObject);
        }
    }
    
    /**
     * creates a new CtcLine object.
     *
     * @param label
     *            is the label of the entry in the Lines pulldown menu
     * @param mnenomic
     *            is the name to identify the object in a configuration file
     * @return a line factory with the requested name.
     *
     * @see designer.gui.CtcLine
     */
    public CtcLine createLine(String label, String mnenomic) {
        CtcLine l = new CtcLine(label, mnenomic, Parent);
        JMenuItem item = new JMenuItem();
        item.setText(label);
        item.addActionListener(l);
        Parent.addLineItem(item);
        if (findLine(mnenomic) == null) {
            Mnenomic.addElement(new MnenomicEntry(mnenomic, l));
        } else {
            System.out.println("Duplicate Line mnenomic: " + mnenomic);
        }
        return l;
    }
    
    /**
     * searches the table of mnenomics for an entry.
     *
     * @param mnenomic
     *            is the mnenomic which may be in the table
     *
     * @return null if an entry doesn't exist in the table or the associated
     *         CtcLine, if it does.
     *
     * @see designer.gui.CtcLine
     */
    public CtcLine findLine(String mnenomic) {
        MnenomicEntry entry;
        Enumeration<MnenomicEntry> e = Mnenomic.elements();
        while (e.hasMoreElements()) {
            entry = (e.nextElement());
            if (entry.MnenomicName.equals(mnenomic)) {
                return entry.LineObject;
            }
        }
        return null;
    }

    /**
     * resets all lines to the default width
     */
    public void reNew() {
        MnenomicEntry entry;
        Enumeration<MnenomicEntry> e = Mnenomic.elements();
        while (e.hasMoreElements()) {
            entry = (e.nextElement());
            entry.LineObject.setValue(CtcLine.DEF_WIDTH);
        }        
    }
    
    /*
     * tells the factory that an XMLEleObject is to be created. Thus, its
     * contents can be set from the information in an XML Element description.
     */
    public void newElement() {
    }
    
    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute. @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error string.
     */
    public String addAttribute(String tag, String value) {
        return new String("Attributes are not accepted in " + LINE
                + " elements");
    }
    
    /*
     * tells the factory that the attributes have been seen; therefore, return
     * the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem in
     * creating it).
     */
    public XMLEleObject getObject() {
        return this;
    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("Text fields are not accepted in " + LINE
                + " elements");
    }
    
    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String if it is not.
     */
    public String setObject(String objName, Object objValue) {
        MnenomicEntry entry;
        Enumeration<MnenomicEntry> e = Mnenomic.elements();
        while (e.hasMoreElements()) {
            entry = (e.nextElement());
            if (entry.MnenomicName.equals(objName)) {
                return null;
            }
        }
        return new String(objName + " is not a valid XML Element of " + LINE);
    }
    
    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the Element
     * tag).
     */
    public String getTag() {
        return LINE;
    }
    
    /*
     * tells the XMLEleObject that no more setValue or setObject calls will be
     * made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error string
     * if something isn't correct.
     */
    public String doneXML() {
        return null;
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Enumeration<MnenomicEntry> e = Mnenomic.elements();
        MnenomicEntry entry;
        Element thisObject = new Element(LINE);
        while (e.hasMoreElements()) {
            entry = (e.nextElement());
            entry.LineObject.putXML(thisObject);
        }
        parent.addContent(thisObject);
        return null;
    }
    
    /*
     * tells the caller if the state has been saved or not.
     *
     * @return true if the state has not been changed since the last save
     */
    public boolean isSaved() {
        Enumeration<MnenomicEntry> e = Mnenomic.elements();
        MnenomicEntry entry;
        while (e.hasMoreElements()) {
            entry = (e.nextElement());
            if (!entry.LineObject.isSaved()) {
                return false;
            }
        }
        return true;
    }
    
    /*
     * Name: MnenomicEntry
     *
     * What: A simple data structure for associating a mnenomic with its CtcFont
     * Object.
     */
    class MnenomicEntry {
        String MnenomicName;
        
        CtcLine LineObject;
        
        MnenomicEntry(String name, CtcLine object) {
            MnenomicName = name;
            LineObject = object;
        }
    }
}
/* @(#)LineFactory.java */