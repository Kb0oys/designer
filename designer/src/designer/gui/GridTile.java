/* Name: Tile.java
 *
 * What:
 *   This file contains a class for constructing the objects that occupy
 *   a grid square on the screen.  Objects of this sub-class form the body
 *   of the screen.
 */
package designer.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Line2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

import designer.gui.frills.Frills;
import designer.layout.ColorList;
import designer.layout.Node;
import designer.layout.items.Edge;

/**
 * contains a specialization of Tile for holding a Section of track from the
 * Layout. Node.
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2010
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author R. W. Black
 * @version $Revision$
 */
public class GridTile extends Tile {

	/**
	 * this is the width of the Block gap, in pixels. It should be set globally,
	 * so that it could be changed.
	 */
	private final int GAP = 6;

	/**
	 * a zero-width border around the GridTile.
	 */
	private static final Border NOBORDER = BorderFactory.createEmptyBorder();

	/**
	 * a border, indicating the GridTile has been selected.
	 */
	private static final Border SELBORDER = BorderFactory.createLineBorder(Color.RED);

	/**
	 * an array of flags - one for each of the four sides of the GridTile. True
	 * means the side forms the end of a Block, so a gap exists between the side
	 * of the GridTile and visible track.
	 */
	private boolean[] Boundaries = { false, false, false, false };

    /**
     * an array of flags - one for each of the four sides of the GridTile.  True
     * means the side does not connect to the side of another GridTile.  The side
     * is the end of a track, so a "bumper" should be drawn.
     */
    private boolean[] Bumpers = { false, false, false, false };
    
	/**
	 * is where the track description is kept.
	 */
	private Node MyNode;

	/**
	 * is true if the GridTile has been selected for editing.
	 */
	private boolean Selected;

	/**
	 * is the list of things painted into the GridTile.
	 */
	Vector<Frills> MyFrills;

	/**
	 * is the Image where what is presented on the screen is kept. Double
	 * buffering is used so that the painting is intact.
	 */
	private Image MyImage;

	/**
	 * is the Graphics context for the Image.
	 */
	private Graphics MyGraphics;

	/**
	 * is the size of the Graphics context in pixels.
	 */
	private Rectangle GBounds;

	/**
	 * constructs the grid square and remembers what Section it is displaying.
	 * 
	 * @param n
	 *            is the Node holding the Section of track
	 * 
	 * @see designer.layout.Node
	 */
	public GridTile(Node n) {
		super();
		MyNode = n;
		Selected = false;
		setSize(Size);
		setBorder(NOBORDER);
		addMouseListener(new ma());
		addMouseMotionListener(new mm());
		MyFrills = new Vector<Frills>();
		GBounds = new Rectangle(0, 0, Tile.getGridSize().width, Tile
				.getGridSize().height);
	}

	/**
	 * highlights the GridTile to show if is selected or not.
	 * 
	 * @param isSet
	 *            is true if selected or false, if not.
	 */
	public void setSelect(boolean isSet) {
		if (isSet) {
			setBorder(SELBORDER);
			Selected = true;
		} else {
			setBorder(NOBORDER);
			Selected = false;
		}
		repaint();
	}

	/**
	 * tells the GridTile that something has changed, so it needs to repaint.
	 */
	public void update() {
		Frills frill;
		int tileWidth = Tile.getGridSize().width;
		int tileHeight = Tile.getGridSize().height;

		Rectangle boundaries;
		if (MyGraphics != null) {
			if ((GBounds.width != tileWidth) || (GBounds.height != tileHeight)) {
				MyGraphics.dispose();
				newImage(tileWidth, tileHeight);
			}
		} else {
			newImage(tileWidth, tileHeight);
		}
		boundaries = computeClipping(GBounds);
		MyGraphics.setClip(GBounds);
		MyGraphics.setColor(ColorList.safeGetColor(ColorList.BACKGROUND, null));
		MyGraphics.fillRect(0, 0, GBounds.width, GBounds.height);
		if (boundaries != null) {
			MyGraphics.setClip(boundaries);
		}
		for (Enumeration<Frills> e = MyFrills.elements(); e.hasMoreElements();) {
			frill = e.nextElement();
			frill.decorate(MyGraphics);
		}
        for (int side = 0; side < Bumpers.length; ++side) {
            if (Bumpers[side]) {
                drawTickMark(side);
            }
        }
		repaint();
	}

	/**
	 * creates a new place to draw the Section.
	 * 
	 * @param width
	 *            is the width of the new Image in pixels.
	 * @param height
	 *            is the new height of the Image in pixels.
	 */
	private void newImage(int width, int height) {
		GBounds = new Rectangle(0, 0, width, height);
		MyImage = Ctc.RootCTC.getDispPanel().createImage(width, height);
		MyGraphics = MyImage.getGraphics();
	}

	/**
	 * makes a copy of itself.
	 * 
	 * @param n
	 *            is the Node the GridTile is attached to.
	 * 
	 * @return the copy.
	 * 
	 * @see designer.layout.Node
	 */
	public GridTile copy(Node n) {
		GridTile theCopy = new GridTile(n);
		return theCopy;
	}

	/**
	 * adds a Frill to the list of decorators for the Tile.
	 * 
	 * @param frill
	 *            is the Frill being added.
	 * 
	 * @see designer.gui.frills.Frills
	 */
	public void addFrill(Frills frill) {
		MyFrills.add(frill);
		//    repaint();
		update();
	}

	/**
	 * deletes a Frill from the list of decorators for the Tile.
	 * 
	 * @param frill
	 *            is the Frill being deleted.
	 * 
	 * @see designer.gui.frills.Frills
	 */
	public void delFrill(Frills frill) {
		MyFrills.remove(frill);
		update();
	}

	/**
	 * clears the Frills.
	 */
	public void clearFrills() {
		MyFrills.removeAllElements();
		update();
	}

	/**
	 * sets one of the sides to be a block boundary.
	 * 
	 * @param side
	 *            is the side of the GridTile (see Edge for definitions).
	 * @param block
	 *            is true if the side is a block boundary, false if it is not
	 */
	public void setSide(int side, boolean block) {
		Boundaries[side] = block;
	}

    /**
     * sets one of the sides to be the end of track.
     * @param side is the side of the GridTile (@see designer.layout.items.Edge)
     * @param endOfTrack is true if a tick mark should be drawn, indicating the
     * track does not connect to any other track; false, if the tick mark should
     * not be drawn.
     */
    public void setBumber(int side, boolean endOfTrack) {
        Bumpers[side] = endOfTrack;
    }
    
	/**
	 * computes the Clipping Rectangle for the block boundaries.
	 * 
	 * @param old
	 *            is the current clipping area
	 * @return the clipping rectangle, after adjusting for block gaps
	 */
	public Rectangle computeClipping(Rectangle old) {
		int side;
		Rectangle clipper = null;
		for (side = 0; side < Edge.EDGENAME.length; ++side) {
			if (Boundaries[side]) {
				clipper = new Rectangle(old);
				if (Boundaries[Edge.RIGHT]) {
					clipper.width -= GAP;
				}
				if (Boundaries[Edge.LEFT]) {
					clipper.width -= GAP;
					clipper.x += GAP;
				}
				if (Boundaries[Edge.TOP]) {
					clipper.height -= GAP;
					clipper.y += GAP;
				}
				if (Boundaries[Edge.BOTTOM]) {
					clipper.height -= GAP;
				}
			}
		}
		return clipper;
	}

      /**
       * draws the end of track "bumper" (tick mark).  The tick mark is drawn with
       * the same width of line as the track.  It will be twice as long as the width,
       * centered on the middle of the Rectangle side.
       * 
       * @param edge is the edge to draw it on
       * @param bounds is the bounding Rectangle from which the place to draw it
       * is computed
       */
      private void drawTickMark(int edge) {
          double lWidth = Ctc.RootCTC.getLineFactory().findLine(LineFactory.LEVEL).grabLine();
          Graphics2D g2d = (Graphics2D) MyGraphics;
          // The coordinates for the ends of the tick mark
          double x1 = 0.0;
          double y1 = 0.0;
          double x2 = 0.0;
          double y2 = 0.0;
          double center;
          Color oldInk = MyGraphics.getColor();
          Stroke oldWidth = g2d.getStroke();
          
          switch (edge) {
          case Edge.RIGHT:
              x1 = x2 = GBounds.x + GBounds.width - lWidth;
              center = GBounds.y + (GBounds.height / 2.0);
              y1 = center - lWidth;
              y2 = center + lWidth;
              break;
              
          case Edge.BOTTOM:
              y1 = y2 = GBounds.y + GBounds.height - lWidth;
              center = GBounds.x + (GBounds.width / 2.0);
              x1 = center -lWidth;
              x2 = center + lWidth;
              break;
              
          case Edge.LEFT:
              x1 = x2 = GBounds.x;
              center = GBounds.y + (GBounds.height / 2.0);
              y1 = center - lWidth;
              y2 = center + lWidth;          
              break;
              
          case Edge.TOP:
              y1 = y2 = GBounds.y;
              center = GBounds.x + (GBounds.width / 2.0);
              x1 = center -lWidth;
              x2 = center + lWidth;          
              break;
              
          default:
              System.out.println("Illegal edge for setting an end of track mark.");          
          }
          g2d.setPaint(Color.YELLOW);
          g2d.setStroke(new BasicStroke((float) lWidth, BasicStroke.CAP_BUTT,
                  BasicStroke.JOIN_BEVEL));
          g2d.draw(new Line2D.Double(x1, y1, x2, y2));     
          g2d.setStroke(oldWidth);
          g2d.setColor(oldInk);
      }

      /**
	 * fills in the contents of the Tile.
	 * 
	 * @param g
	 *            is the Graphics context where the Tile appears.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//    setBackground(Ctc.RootCTC.getPaletteFactory().
		//                  findPalette(PaletteFactory.BACKGROUND).grabColor());
		//    if (boundaries != null) {
		//      g.setClip(boundaries);
		//    }
		//    for (Enumeration e = MyFrills.elements(); e.hasMoreElements(); ) {
		//      frill = (Frills) e.nextElement();
		//      frill.decorate(g);
		//    }
		//    if (boundaries != null) {
		//      g.setClip(oldClip);
		//    }
		//    System.out.println("clipping rectangle (" + MyNode.getColumn() + ","
		//                       + MyNode.getRow() + ") = " + oldClip.toString());
		if (MyImage != null) {
			g.drawImage(MyImage, 0, 0, this);
		}
	}

	/***************************************************************************
	 * Inner classes for handling the mouse
	 **************************************************************************/

	class ma extends MouseAdapter {

		ma() {
		}

		public void mouseClicked(MouseEvent me) {
			if (me.isShiftDown()) {
				MyNode.requestShiftSel();
			} else {
				MyNode.requestSel();
			}
		}
	}

	// The following do not work as expected.
	class mm extends MouseMotionAdapter {

		mm() {
		}

		public void mouseDragged(MouseEvent me) {
			if (!Selected) {
				MyNode.requestShiftSel();
			}
		}
	}
}
/* @(#)GridTile.java */