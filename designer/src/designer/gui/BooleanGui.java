/*
 * Name: BoolenaGui.java
 * 
 * What:
 *  This file contains the BooleanGui class.  Each instantiation has
 *  a flag, a JMenuItem for changing the state of the flag, and an
 *  accessor method for retrieving the state of the flag.
 */
package designer.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBoxMenuItem;

import org.jdom2.Element;

import designer.layout.xml.SimpleSavable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;
/**
 *  This file contains the BooleanGui class.  Each instantiation has
 *  a flag, a JMenuItem for changing the state of the flag, and an
 *  accessor method for retrieving the state of the flag.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2007, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class BooleanGui extends JCheckBoxMenuItem implements ActionListener,
XMLEleObject, SimpleSavable {
    
    /**
     * is the XML tag
     */
    private String XML_TAG;
    
    /**
     * is the initial (default) polarity of the flag.
     */
    private boolean MyDefault;

    /**
     *  the polarity that has been saved.  If the current polarity
     *  is not the saved polarity, then the setting should be saved.
     */
    protected boolean Saved;
    
    /**
     * constructs the factory.
     *
     * @param label is the JMenuItem label for the factory.
     * @param tag is the XML tag.
     * @param polarity is the default polarity of the flag.
     */
    public BooleanGui(String label, String tag, boolean polarity) {
        super(label, polarity);
        addActionListener(this);
        XML_TAG = tag;
        MyDefault = polarity;
        Saved = MyDefault;
        init();
    }
    
    /**
     * is the ActionListener for setting or clearing the flag.
     */
    public void actionPerformed(ActionEvent arg0) {
    }
    
    /**
     * is the accessor for retrieving the value of the flag.
     * @return the current trace setting
     */
    public boolean getFlagValue() {
        return getState();
    }
    
    /**
     * is the setter for changing the value of the
     * flag.
     * 
     * @param newState is the new value of the 
     * flag.
     */
    public void setFlagValue(boolean newState) {
        setState(newState);
    }

    /**
     * is called when the object is read in from the XML file.
     * The logic on this is kind of obtuse.  If Saved is the
     * default value, then as long as state is also the default
     * value, nothing needs to be saved (the value did not
     * change).  Conversely, if Saved is not the default value,
     * then as long as the state is not the default value,
     * nothing needs to be saved.
     *
     */
    public void created() {
        setFlagValue(!MyDefault);
        Saved = !MyDefault;
    }

    public void reNew() {
        setFlagValue(MyDefault);
        Saved = MyDefault;
    }
    
    /**
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String(XML_TAG + " elements cannot have values.");
    }
    
    /**
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        return new String("XML objects are not accepted by " + XML_TAG +
        " elements.");
    }
    
    /**
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return XML_TAG;
    }
    
    /**
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        return null;
    }
    
    
    /**
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved == getFlagValue();
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        if (getFlagValue() != MyDefault) {
            Element thisObject = new Element(XML_TAG);
            parent.addContent(thisObject);
        }
        Saved = getFlagValue();
        return null;
    }
    /**
     * registers a BooleanGui with the XMLReader.
     */
    public void init() {
        XMLReader.registerFactory(XML_TAG, new BooleanFactory(XML_TAG,
                this));
        Ctc.RootCTC.registerSave(this);
    }
}

/**
 * is a Class known only to the BooleanGui class for creating a false
 * value from an XML file.
 */
class BooleanFactory
implements XMLEleFactory {
    
    /**
     * is the XML tag
     */
    private final String GuiTag;
    
    /**
     * is the BooleanGui being created.
     */
    private final BooleanGui MyGui;
    
    /**
     * the ctor
     * @param tag is the XML tag for the BooleanGui
     * @param bg is the Boolean Gui
     */
    public BooleanFactory(String tag, BooleanGui bg){
        GuiTag = tag;
        MyGui = bg;
    }
    
    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        MyGui.created();
    }
    
    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        return new String("A " + GuiTag +
                " XML Element cannot have a " + tag +
        " attribute.");
    }
    
    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        return MyGui;
    }
}
/* @(#)BooleanGui.java */