/* Name: ImageFrill.java
 *
 * What:
 *  ImageFrill is a class of objects for painting inages on Tiles.
 */
package designer.gui.frills;

import designer.gui.Ctc;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * a class for painting images on Tiles.  These images can be anything
 * of the Java AWT Image class, read through a getImage(filename)
 * method.  The image can be a logo, thumbnail, or anything else to
 * make the dispatcher panel a little more fancy.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class ImageFrill
    implements Frills {

	  /**
	   * the image.
	   */
	  protected Image MyImage;
	  
	  /**
	   * the width of the image in pixels.  0 means its native width.
	   */
	  protected int ImageWidth;
	  
	  /**
	   * the height of the image in pixels.  0 means native height.
	   */
	  protected int ImageHeight;

  /**
   * the constructor.  The upper left corner of the image is drawn in
   * the upper left corner of the GridTile.
   *
   * @param fileName is the path to the file containing the Image.  If the
   * file cannot be read then a message is logged; thus, the caller should
   * verify that the file exists before instantiating an ImageFrill.
   */
	  public ImageFrill(final String fileName) {
		  ImageWidth = 0;
		  ImageHeight = 0;
		  File f;
		  try {
			  //      MyImage = Toolkit.getDefaultToolkit().getImage(fileName);
			  f = new File(fileName);
			  MyImage = ImageIO.read(f);
		  }
		  catch (IOException err) {
			  log.warn("Failure to open Image file " + fileName);
			  System.out.println("Failure to open Image " + fileName);
		  }
	  }

  /*
   * is the method called by a GridTile, describing itself - the area on
   * the screen to be written to.
   *
   * @param g is the Graphics context on which to draw
   */
  public void decorate(Graphics g) {
    if (MyImage != null) {
        g.drawImage(MyImage, ImageWidth, ImageHeight, Ctc.RootCTC.getDispPanel());
    }
  }

  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
      ImageFrill.class.getName());
}
/* @(#)ImageFrill.java */
