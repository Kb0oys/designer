/* Name: ButtonFrill.java
 *
 * What:
 * a class for painting buttons on Tiles.  In designer, it is just a place holder for
 * the real thing in CATS.  Its function is just to show what the CATS panel will look like.
 */
package designer.gui.frills;

import java.awt.Graphics;

import designer.gui.Ctc;
import designer.gui.Tile;

/**
 * a class for painting buttons on Tiles.  In designer, it is just a place holder for
 * the real thing in CATS.  Its function is just to show what the CATS panel will look like.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ButtonFrill extends ImageFrill {
	/**
	 * true means scale the image to fit in a tile; false means use
	 * the image's native size.
	 */
	private boolean ScaleImage;

	/**
	 * the constructor.  The upper left corner of the image is drawn in
	 * the upper left corner of the GridTile.
	 *
	 * @param fileName is the path to the file containing the Image.  If the
	 * file cannot be read then a message is logged; thus, the caller should
	 * verify that the file exists before instantiating an ImageFrill.
	 */
	public ButtonFrill(final String fileName) {
		super(fileName);
		ScaleImage = false;
	}
	
//	/**
//	 * adjusts the size of the image.  The adjustment is made when the image is painted.
//	 * The size is either the native size of the image (height = 0, width = 0) or the size
//	 * of the gridtile.
//	 * @param height of the image in pixels.  0 is valid, but negative numbers are not.
//	 * @param width of the image in pixels.  0 is valid, but negative numbers are not.
//	 */
//	public void setSize(final int width, final int height) {
//		ImageWidth = width;
//		ImageHeight = height;
//	}
	
	public void setFitSize(final boolean fit) {
		ScaleImage = fit;
		if (fit) {
			ImageWidth = Tile.getGridSize().width;
			ImageHeight = Tile.getGridSize().height;
		}
		else {
			ImageWidth = 0;
			ImageHeight = 0;
		}
	}
	
	@Override
	public void decorate(Graphics g) {
		if (ScaleImage) {
			g.drawImage(MyImage, 0, 0, ImageWidth, ImageHeight, Ctc.RootCTC.getDispPanel());
		}
		else {
			super.decorate(g);
		}
	}
}
/* @(#)ButtonFrill.java */
