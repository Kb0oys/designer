/* Name: DepotFrill.java
 *
 * What:
 *   This file defines a class for placing a Depot Icon on the GridTile.
 */
package designer.gui.frills;

import designer.gui.jCustom.ColorFinder;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

/**
 *   DepotFrill defines a class for placing a Depot Icon on the GridTile.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class DepotFrill
    implements Frills {

static final int Side = 9;  // pixels - this should be changeable.

  /**
   * is where to place the Depot Icon (see FrillLoc).
   */
  private FrillLoc Where;

  /**
   * is the color of the Depot.  It may be null.
   */
  private ColorFinder DepotColor;
  
  /**
   * constructs the DepotFrill.
   *
   * @param position is where to write the String.
   * @param color is the color of the icon.  It may be null.
   */
  public DepotFrill(FrillLoc position, ColorFinder color) {
    Where = position;
    DepotColor = color;
  }

  /*
   * is the method called by a GridTile, describing itself - the area on
   * the screen to be written to.
   *
   * @param g is the Graphics context on which to draw
   */
  public void decorate(Graphics g) {
    if (Where != null ) {
      Rectangle limits = g.getClipBounds();
      Color oldColor = g.getColor();
      Point anchor = Where.locateCorner(limits, new Dimension(Side, Side));
//      g.setColor(Ctc.RootCTC.getPaletteFactory().
//                 findPalette(PaletteFactory.DEPOT).grabColor());
      g.setColor(DepotColor.getColor());
      g.fillRect(anchor.x, anchor.y, Side, Side);
      g.setColor(oldColor);
    }
  }

  /**
   * retrieves the FrillLoc describing where the Depot is placed.
   *
   * @return the description.
   *
   * @see FrillLoc
   */
  public FrillLoc getLocation() {
    return Where;
  }

  /**
   * changes the Location on the GridTile of where the Depot Icon is placed.
   *
   * @param where is the FrillLoc describing where to place the Depot Icon.
   * It may be null, in which case the Icon will not be shown.
   *
   * @see FrillLoc
   */
  public void setLocation(FrillLoc where) {
    Where = where;
  }
}
/* @(#)DepotFrill.java */