/* Name: frills.java
 *
 * What:
 *   This file contains an interface used by all Objects which draw
 *   on a GridTile.  The concept is that the Objects register with the
 *   GridTile and when the GridTile is told to paint itself, it invokes
 *   a method defined in this interface for each registered Object
 *   (Observer pattern).  The parameters passed to the method describe
 *   where the Object can draw.
 *
 *   This name, frills, comes from the movie Office Space, where the
 *   character played by Jennifer Aniston is a waitress who can't keep
 *   a job, partially because she refuses to conform to management
 *   dictates that she wear little badges and emblems (called "frills")
 *   indicating that she is a team player and has achieved management
 *   goals.  Thus, these Objects apply "frills" to the GridTiles.
 */
package designer.gui.frills;

import java.awt.Graphics;

/**
 * This file contains an interface used by all Objects which draw on a GridTile.
 * The concept is that the Objects register with the GridTile and when the
 * GridTile is told to paint itself, it invokes a method defined in this
 * interface for each registered Object (Observer pattern). The parameters
 * passed to the method describe where the Object can draw.
 * 
 * This name, frills, comes from the movie Office Space, where the character
 * played by Jennifer Aniston is a waitress who can't keep a job, partially
 * because she refuses to conform to management dictates that she wear little
 * badges and emblems (called "frills") indicating that she is a team player and
 * has achieved management goals. Thus, these Objects apply "frills" to the
 * GridTiles.
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public interface Frills {
	/**
	 * is the method called by a GridTile, describing itself - the area on the
	 * screen to be written to.
	 * 
	 * @param g
	 *            is the Graphics context on which to draw
	 */
	public void decorate(Graphics g);
}
/* @(#)Frills.java */