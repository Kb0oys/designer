/* Name: SignalFrill.java
 *
 * What:
 *   This file defines a class for placing a Signal Icon on the GridTile.
 */
package designer.gui.frills;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import designer.gui.jCustom.ColorFinder;
import designer.layout.ColorList;
import designer.layout.items.Edge;

/**
 * defines a class for placing a Signal Icon on the GridTile.
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2009
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */

public abstract class SignalFrill implements Frills {

	/**
	 * defines the X offset of the upperleft corner of the signal head for each
	 * orientation. true means there is an offset of Base pixels.
	 */
	protected static final boolean HEADXOFFSET[] = { true, // RIGHT - base is to
														   // the left
			false, // BOTTOM - base is above
			true, // LEFT - base is to the right
			false // TOP - base is below
	};

	/**
	 * defines the Y offset of the upperleft corner of the signal head for each
	 * orientation. true means there is an offset of Base pixels.
	 */
	protected static final boolean HEADYOFFSET[] = { false, // RIGHT - base is
															// to the left
			true, // BOTTOM - base is above
			false, // LEFT - base is to the right
			true // TOP - base is below
	};

	/**
	 * defines the X offset of one corner of the base of the "mast". true means
	 * it is Radius + Base pixels from the anchor.
	 */
	protected static final boolean BASEXOFFSET[] = { false, // RIGHT - head is
															// to the right
			false, // BOTTOM - head is below
			true, // LEFT - head is to the left
			false // TOP - head is above
	};

	/**
	 * defines the Y offset of one corner of the base of the "mast". true means
	 * it is Radius + Base fromthe anchor.
	 */
	protected static final boolean BASEYOFFSET[] = { false, // RIGHT - head is
															// to the right
			false, // BOTTOM - head is below
			false, // LEFT - head is to the left
			true // TOP - head is above
	};

	/**
	 * defines the number of pixels in the radius of the signal head. This
	 * should be changeable.
	 */
	static final int Radius = 8;

	/**
	 * defines the height of the base of the signal mast in pixels.
	 */
	static final int Base = Radius;

	/**
	 * defines the orientation of the head, relative to the base:
	 * <ul>
	 * <li>RIGHT means the head is to the right of the base
	 * <li>BOTTOM means the head is below the base
	 * <li>LEFT means the head is to the left os the base
	 * <li>TOP means the head is above the base
	 * </ul>
	 */
	protected int Orient;

	/**
	 * defines the dimensions of the Bounding Box containing the signal Icon.
	 */
	protected Dimension BBox;

	/**
	 * is where to place the Signal Icon (see FrillLoc).
	 */
	protected FrillLoc Where;

	/**
	 * is the number of heads to draw.
	 */
	protected int Heads;

    /**
     * is the color used for the "no color" aspect
     */
    protected ColorFinder MyColor;
    
	/**
	 * constructs the SignalFrill.
	 * 
	 * @param position
	 *            is where to place the upper left corner of the Icon.
	 * @param orientation
	 *            is the relationship of the head to the mast.
	 *            <ul>
	 *            <li>RIGHT means the head is on the right (protects right hand
	 *            movement)
	 *            <li>BOTTOM means the head is on the bottom
	 *            <li>LEFT means the head is on the left
	 *            <li>TOP means the head is on the top
	 *            </ul>
	 * @param heads
	 *            is the number of heads to be shown (1-3 heads are allowed)
	 * @see FrillLoc
	 */
	public SignalFrill(FrillLoc position, int orientation, int heads) {
		Where = position;
		if ((orientation == Edge.LEFT) || (orientation == Edge.RIGHT)) {
			BBox = new Dimension((heads * Radius) + Base, Radius);
		} else {
			BBox = new Dimension(Radius, (heads * Radius) + Base);
		}
		Orient = orientation;
		Heads = heads;
        MyColor = new ColorFinder(ColorList.EMPTY);
	}

	/*
	 * is the method called by a GridTile, describing itself - the area on the
	 * screen to be written to.
	 * 
	 * @param g is the Graphics context on which to draw
	 */
	public void decorate(Graphics g) {
		int x;
		int y;
		//    GeneralPath outline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 3);
		//    Polygon points = new Polygon();
		if (Where != null) {
			Rectangle limits = g.getClipBounds();
			Color oldColor = g.getColor();
			Point anchor = Where.locateCorner(limits, new Dimension(BBox));
			g.setColor(MyColor.getColor());
			x = anchor.x;
			y = anchor.y;
			if (Orient == Edge.RIGHT) {
				x += Radius;
			} else if (Orient == Edge.BOTTOM) {
				y += Radius;
			}
			for (int head = 0; head < Heads; ++head) {
				drawHead(x, y, g);
				if (HEADXOFFSET[Orient]) {
					x += Radius;
				}
				if (HEADYOFFSET[Orient]) {
					y += Radius;
				}
			}
			//      g.fillOval(x, y, l, l);
			//      ((Graphics2D) g).fill(new Ellipse2D.Double(x, y, l, l));

			if (BASEXOFFSET[Orient]) {
				x += Base;
			} else {
				x = anchor.x;
			}
			if (BASEYOFFSET[Orient]) {
				y += Base;
			} else {
				y = anchor.y;
			}
			//      outline.moveTo(x, y);

			// Plant one corner of the side on the ground.
			//      points.addPoint(x, y);

			// Plant the other corner of the side on the ground.
			//      if ( (Orient == Edge.LEFT) || (Orient == Edge.RIGHT)) {
			//        y += Base;
			//      }
			//      else {
			//        x += Base;
			//      }
			//      points.addPoint(x, y);

			// Determine the apex, the point on the head.
			//      switch (Orient) {
			//        case Edge.RIGHT:
			//          x += Base;
			//          y -= Base / 2;
			//          break;
			//
			//        case Edge.BOTTOM:
			//          x -= Base / 2;
			//          y += Base;
			//          break;
			//
			//        case Edge.LEFT:
			//          x -= Base;
			//          y -= Base / 2;
			//          break;
			//
			//        default:
			//          x -= Base / 2;
			//          y -= Base;
			//      }
			//      points.addPoint(x, y);
			//      g.fillPolygon(points);
			drawMast(x, y, g);
			g.setColor(oldColor);
		}
	}

	/**
	 * retrieves the FrillLoc describing where the Depot is placed.
	 * 
	 * @return the description.
	 * 
	 * @see FrillLoc
	 */
	public FrillLoc getLocation() {
		return Where;
	}

	/**
	 * changes the Location on the GridTile of where the Depot Icon is placed.
	 * 
	 * @param where
	 *            is the FrillLoc describing where to place the Depot Icon. It
	 *            may be null, in which case the Icon will not be shown.
	 * 
	 * @see FrillLoc
	 */
	public void setLocation(FrillLoc where) {
		Where = where;
	}

	/*
	 * the following are what make this class abstract.
	 */
	/**
	 * paint a single head.
	 * <p>
	 * 
	 * @param x
	 *            is the x coordinate of the upper left corner of the head.
	 * 
	 * @param y
	 *            is the y coordinate of the upper left corner of the head.
	 * 
	 * @param g
	 *            is the Graphics to draw the head on.
	 */
	public abstract void drawHead(int x, int y, Graphics g);

	/**
	 * paint the signal mast.
	 * <p>
	 * 
	 * @param x
	 *            is the x coordinate of the upper left corner of the mast.
	 * 
	 * @param y
	 *            is the y coordinate of the upper left corner of the mast.
	 * 
	 * @param g
	 *            is the Graphics to draw the head on.
	 */
	public abstract void drawMast(int x, int y, Graphics g);

}
/* @(#)SignalFrill.java */