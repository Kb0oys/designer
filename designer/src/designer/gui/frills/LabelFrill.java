/* Name: LabelFrill.java
 *
 * What:
 *   This file defines a class for writing Strings on the GridTile.
 */
package designer.gui.frills;

import designer.gui.Ctc;
import designer.gui.jCustom.FontFinder;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.Rectangle;

/**
 *   LabelFrill defines a class for writing Strings on the GridTile.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class LabelFrill
    implements Frills {

  private String Lab;
  private FrillLoc Where;
  private FontFinder MyFont;

  /**
   * constructs the LabelFrill.
   *
   * @param label is the String to be written.
   * @param position is where to write the String.
   * @param font is the FontFinder associated with the LabelFrill
   *
   * @see FrillLoc
   */
  public LabelFrill(String label, FrillLoc position, FontFinder font) {
    Lab = new String(label);
    Where = position;
    MyFont = font;
  }

  /*
   * is the method called by a GridTile, describing itself - the area on
   * the screen to be written to.
   *
   * @param g is the Graphics context on which to draw
   */
  public void decorate(Graphics g) {
    Rectangle limits = g.getClipBounds();
    Color oldColor = g.getColor();
    Font myFont = MyFont.getFont();
    g.setFont(myFont);
    FontMetrics currentMetrics = Ctc.RootCTC.getDispPanel().getFontMetrics(
        myFont);
    Point anchor = Where.locateCorner(limits, new Dimension(currentMetrics.stringWidth(Lab) + 4,
                         currentMetrics.getHeight()));
    g.setColor(MyFont.getColor());
    g.drawString(Lab, anchor.x, anchor.y + currentMetrics.getAscent());
    g.setColor(oldColor);
    g.setFont(myFont);
  }
}
/* @(#)LabelFrill.java */