/* Name: IntermediateFrill.java
 *
 * What:
 *  A concrete class for representing an intermediate signal (a physical signal
 *  without a panel signal) on the designer CTC display.
 */
package designer.gui.frills;

import java.awt.Dimension;
import java.awt.Graphics;

/**
 *  A concrete class for representing an intermediate signal (a physical signal
 *  without a panel signal) on the designer CTC display.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class IntermediateFrill extends SignalFrill {

    /**
     * constructs the IntermediateFrill.
     *
     * @param position is where to draw the Frill.
     * @param orient describes where in the Grid to place the Frill
     * @param heads is the number of heads on the signal mast
     * @see FrillLoc
     */
    public IntermediateFrill(FrillLoc position, int orient, int heads) {
        super(position, orient, heads);
        BBox = new Dimension(Radius, Radius);
      }

    /*
     * paint a single head.
     * <p>
     * @param x is the x coordinate of the upper left corner of the head.
     *
     * @param y is the y coordinate of the upper left corner of the head.
     *
     * @param g is the Graphics to draw the head on.
     */
    public void drawHead(int x, int y, Graphics g) {
      g.fillOval(x, y, Radius, Radius);
    }

    /*
     * paint the signal mast.  There is no mast, so nothing to paint.
     *
     * @param x is the x coordinate of the upper left corner of the mast.
     *
     * @param y is the y coordinate of the upper left corner of the mast.
     *
     * @param g is the Graphics to draw the head on.
     */
    public void drawMast(int x, int y, Graphics g) {
    }

}
/* @(#)IntermediateFrill.java */
