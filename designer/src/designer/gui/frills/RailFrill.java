/* Name: RailFrill.java
 *
 * What:
 *   This file defines a class for drawing tracks on a GridTile.  A track
 *   is a straight line connecting the mid-point of one edge of the GridTile
 *   to the mid-point of another edge.  However, the clipping Rectangle may
 *   be set to provide some blank space, representing a block boundary or
 *   a turnout thrown the other direction.
 */
package designer.gui.frills;

import designer.gui.Ctc;
import designer.gui.LineFactory;
import designer.gui.Tile;
import designer.gui.jCustom.ColorFinder;
import designer.layout.ColorList;
import designer.layout.items.Edge;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Line2D;

/**
 *   RailFrill defines a class for drawing tracks on a GridTile.  A track
 *   is a straight line connecting the mid-point of one edge of the GridTile
 *   to the mid-point of another edge.  However, the clipping Rectangle may
 *   be set to provide some blank space, representing a block boundary or
 *   a turnout thrown the other direction.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class RailFrill
    implements Frills {
  private int Edge1; // the GridTile edge of one end
  private int Edge2; // the GridTile edge of the other end
  private ColorFinder MyColor; // the Color to paint the track in
  private Insets Gaps; // space around the borders

  /**
   * constructs the LineFrill.
   *
   * @param edge1 indicates one end
   * @param edge2 indicates the other
   */
  public RailFrill(int edge1, int edge2) {
    Edge1 = edge1;
    Edge2 = edge2;
    Gaps = new Insets(0, 0, 0, 0);
    MyColor = new ColorFinder(ColorList.EMPTY);
  }

  /*
   * is the method called by a GridTile, describing itself - the area on
   * the screen to be written to.
   *
   * @param g is the Graphics context on which to draw
   */
  public void decorate(Graphics g) {
    Graphics2D g2 = (Graphics2D) g;
    Rectangle bounds = new Rectangle(Tile.getGridSize());
    Color ink = MyColor.getColor();
    Color oldInk = g2.getColor();
    BasicStroke lineWidth;
    Stroke oldWidth = g2.getStroke();
    float lWidth;

    if ((Edge1 & 1) == (Edge2 & 1)) {
      lWidth = Ctc.RootCTC.getLineFactory().findLine(LineFactory.LEVEL).grabLine();
    }
    else {
      lWidth = Ctc.RootCTC.getLineFactory().findLine(LineFactory.DIAGONAL).grabLine();
    }
    
    lineWidth = new BasicStroke(lWidth, BasicStroke.CAP_BUTT,
                                            BasicStroke.JOIN_BEVEL);
    g2.setPaint(ink);
    g2.setStroke(lineWidth);
    g2.draw(new Line2D.Double(xEdge(Edge1, bounds), yEdge(Edge1, bounds),
                              xEdge(Edge2, bounds), yEdge(Edge2, bounds)));
    g2.setStroke(oldWidth);
    g2.setColor(oldInk);
  }

  /**
   * calculates the X offset of an edge as a double, for use in
   * Line2D.Double.
   *
   * @param edge denotes which edge of the rectangle the line terminates on
   * @param bounds describes the rectangle
   *
   * @return the X coordinate of the edge.
   */
  private double xEdge(int edge, Rectangle bounds) {
    double result;
    switch (edge) {
      case Edge.RIGHT:
        result = (bounds.x + bounds.width);
        break;

      case Edge.BOTTOM:
      case Edge.TOP:
        result = (bounds.x + (bounds.width / 2.0));
        break;

      case Edge.LEFT:
        result = bounds.x;
        break;

      default:
        System.out.println("Illegal edge passed to xEdge");
        result = 0.0;
    }
    return result;
  }

  /**
   * calculates the Y offset of an edge as a double, for use in
   * Line2D.Double.
   *
   * @param edge denotes which edge of the rectangle the line terminates on
   * @param bounds describes the rectangle
   *
   * @return the Y coordinate of the edge.
   */
  private double yEdge(int edge, Rectangle bounds) {
    double result;
    switch (edge) {
      case Edge.RIGHT:
      case Edge.LEFT:
        result = (bounds.y + (bounds.height / 2.0));
        break;

      case Edge.BOTTOM:
        result = (bounds.y + bounds.height);
        break;

      case Edge.TOP:
        result = (bounds.y);
        break;

      default:
        System.out.println("Illegal edge passed to yEdge");
        result = 0.0;
    }
    return result;
  }

//    /**
//     * calculates the X offset of an edge as an int, for use in
//     * drawLine.
//     *
//     * @param edge denotes which edge of the rectangle the line terminates on
//     * @param bounds describes the rectangle
//     *
//     * @return the X coordinate of the edge.
//     */
//    private int xedge(int edge, Rectangle bounds) {
//      int result;
//      switch (edge) {
//        case Edge.RIGHT:
//          result = (bounds.x + bounds.width);
//          break;
//
//        case Edge.BOTTOM:
//        case Edge.TOP:
//          result = (bounds.x + (bounds.width / 2));
//          break;
//
//        case Edge.LEFT:
//          result = bounds.x;
//          break;
//
//        default:
//          System.out.println("Illegal edge passed to xEdge");
//          result = 0;
//      }
//      return result;
//    }
//
//    /**
//     * calculates the Y offset of an edge as an int, for use in
//     * drawLine.
//     *
//     * @param edge denotes which edge of the rectangle the line terminates on
//     * @param bounds describes the rectangle
//     *
//     * @return the Y coordinate of the edge.
//     */
//    private int yedge(int edge, Rectangle bounds) {
//      int result;
//      switch (edge) {
//        case Edge.RIGHT:
//        case Edge.LEFT:
//          result = (bounds.y + (bounds.height / 2));
//          break;
//
//        case Edge.BOTTOM:
//          result = (bounds.y + bounds.height);
//          break;
//
//        case Edge.TOP:
//          result = (bounds.y);
//          break;
//
//        default:
//          System.out.println("Illegal edge passed to yEdge");
//          result = 0;
//      }
//      return result;
//    }

  /**
   * sets one of the gaps around the edge of the line.
   *
   * @param edge is the edge that the gap is applied to (RIGHT, etc.).
   * @param width is the size of the gap.  It should be >= 0
   */
  public void adjustGap(int edge, int width) {
    switch (edge) {
      case Edge.RIGHT:
        Gaps.right = width;
        break;

      case Edge.BOTTOM:
        Gaps.bottom = width;
        break;

      case Edge.LEFT:
        Gaps.left = width;
        break;

      case Edge.TOP:
        Gaps.top = width;
        break;

      default:
        System.out.println("Illegal edge for setting a rail border");
    }
  }

  /**
   * changes the color used to paint the tracks
   * @param newColor is the ColorList key that references the Color to use
   */
  public void setColorFinder(String newColor) {
      MyColor = new ColorFinder(newColor);
  }
 
  /**
   * is called to retrieve the name of the Color (from ColorList), that the
   * rail will be painted in.
   * 
   * @return the name of the Color
   */
  public String getColorName() {
      return MyColor.getColorTag();
  }
}
/* @(#)RailFrill.java */