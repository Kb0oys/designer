/* What: FormattedCellRenderer.java
 *
 * What:
 *  This class creates an object that is a wrapper around a DefaultTableCellRenderer.
 *  Its purpose is to create one of the Swing standard Table Cell Renderers.
 *  <p>
 *  This code is based on example 18.6 from "Swing" by Robinson and Vorobiev. 
 */
package designer.gui.store;

import java.awt.Component;
import java.text.Format;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *  This class creates an object that is a wrapper around a DefaultTableCellRenderer.
 *  Its purpose is to create one of the Swing standard Table Cell Renderers.
 *  <p>
 *  This code is based on example 18.6 from "Swing" by Robinson and Vorobiev. 
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class FormattedCellRenderer extends DefaultTableCellRenderer {
    /**
     * the specific type of renderer being instantiated
     */
    protected Format m_format;
    
    /**
     * the ctor.
     * 
     * @param f is the desired format
     */
    public FormattedCellRenderer(Format f) {
        m_format = f;
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int nRow, int nCol) {
        return super.getTableCellRendererComponent(table, value == null ? null : m_format.format(value),
                isSelected, hasFocus, nRow, nCol);
    }
}
/* @(#)FormattedCellRenderer.java */
