/* What: TimeSpec.java
 *
 * What:
 *  This class creates an object that is a time specification.
 *  <p>
 *  A time specification looks like hh:mm, where hh (00-23) specifies an
 *  hour and mm (00-59) specifies a minute.  Without a '+' or '-' prefix, the
 *  time is interpreted as a clock value, using the fast clock (if fast
 *  clock is selected in Appearance) or the time of day clock (if fast clock
 *  is not selected)
 *  <p>
 *  A '+' is interpreted as meaning the time is relative to the clock, when
 *  the operating session begins.  The base clock depends on the fast clock
 *  selection; thus, if a time specification is +00:30, a fast clock is not
 *  being used and the operating session starts at 1:00 pm, then the time
 *  specification refers to 1:30 pm.
 *  <p>
 *  A '-' is interpreted as meaning the time is relative to the clock, before
 *  the operating session begins.  The base clock depends upon the fast clock
 *  selection; thus, if a time specification is -02:00 and a fast clock is
 *  selected, and the fast clock always starts at 12:00 am, then the time
 *  specification refers to 10:00 pm on the fast clock, the previous day.
 *  <p>
 *  A value of all spaces is valid and interpreted as meaning the time
 *  specification is to be ignored.
 *  <p>
 *  There are three critical functions of this class:
 *  <ul>
 *  <li>a parser for identifying the blank string, + relative, - relative,
 *      and the hour and minute fields.
 *  <li>a method for formatting the value in a JTable.
 *  <li>a method for editing the value in a JTable.
 *  </ul>
 */
package designer.gui.store;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import javax.swing.DefaultCellEditor;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.table.TableCellRenderer;

/**
 *  This class creates an object that is a time specification.
 *  <p>
 *  A time specification looks like hh:mm, where hh (00-23) specifies an
 *  hour and mm (00-59) specifies a minute.  Without a '+' or '-' prefix, the
 *  time is interpreted as a clock value, using the fast clock (if fast
 *  clock is selected in Appearance) or the time of day clock (if fast clock
 *  is not selected)
 *  <p>
 *  A '+' is interpreted as meaning the time is relative to the clock, when
 *  the operating session begins.  The base clock depends on the fast clock
 *  selection; thus, if a time specification is +00:30, a fast clock is not
 *  being used and the operating session starts at 1:00 pm, then the time
 *  specification refers to 1:30 pm.
 *  <p>
 *  A '-' is interpreted as meaning the time is relative to the clock, before
 *  the operating session begins.  The base clock depends upon the fast clock
 *  selection; thus, if a time specification is -02:00 and a fast clock is
 *  selected, and the fast clock always starts at 12:00 am, then the time
 *  specification refers to 10:00 pm on the fast clock, the previous day.
 *  <p>
 *  A value of all spaces is valid and interpreted as meaning the time
 *  specification is to be ignored.
 *  <p>
 *  There are three critical functions of this class:
 *  <ul>
 *  <li>a parser for identifying the blank string, + relative, - relative,
 *      and the hour and minute fields.
 *  <li>a method for formatting the value in a JTable.
 *  <li>a method for editing the value in a JTable.
 *  </ul>
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

/**
 * is a filter for selecting certain file suffixes.
 */
public class TimeSpec extends SpecialType {

  /**
   * is the help message for the Verifier.
   */
  private static final String Usage = "Time format is [+|-]hh:mm";

  /**
   * is the specification.
   */
  private String Spec;

  /**
   * is the no argument constructor.
   */
  public TimeSpec() {
    this("");
  }

  /**
   * is the single argument constructor.
   *
   * @param spec is the TimeSpec in String form.
   */
  public TimeSpec(String spec) {
    Spec = new String(spec);
  }

  /*
   * is used for printing it's value in a JTextTextField, as well as the
   * common usage.
   *
   * @return the value of the TimeSpec, as a String.
   */
  public String toString() {
    return Spec;
  }

  /**
   * returns a JTableCellEditor for editing the time specification.
   *
   * @return the editor.
   */
  public DefaultCellEditor getCellEditor() {
    JFormattedTextField ftf = new JFormattedTextField(new TimeFormat());
    ftf.setColumns(10);
    ftf.setInputVerifier(new TimeVerifier(Usage));
    ftf.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
    return new FormattedCellEditor(ftf);
  }

  /**
   * returns a JTableCellRenderer for rendering the time specification.
   *
   * @return the renderer.
   */
  public TableCellRenderer getCellRenderer() {
    return new FormattedCellRenderer(new TimeFormat());
  }

  /**
   * returns the TimeSpec as a JFormattedTextField.
   */
//  public JComponent getComponent() {
//    JFormattedTextField tField = new JFormattedTextField(new TimeFormat());
//    tField.setColumns(10);
//    tField.setInputVerifier(new TimeVerifier(Usage));
//    return tField;
//  }
}

/**
 * is an internal class for editing a TimeSpec.
 */
//class FormattedCellEditor
//    extends DefaultCellEditor {
//  /**
//   * is the constructor.
//   * @param timeField is the field being formatted.
//   */
//  public FormattedCellEditor(final JFormattedTextField timeField) {
//    super(timeField);
//    timeField.removeActionListener(delegate);
//    delegate = new EditorDelegate() {
//      public void setValue(Object value) {
//        timeField.setValue(value);
//      }
//
//      public Object getCellEditorValue() {
//        return timeField.getValue();
//      }
//    };
//    timeField.addActionListener(delegate);
//    timeField.setBorder(null);
//  }
//}

/**
 * is an internal class for rendering a TimeSpec.
 */
//class FormattedCellRenderer
//    extends DefaultTableCellRenderer {
//  protected Format tFormat;
//
//  /**
//   * is the constructor
// * @param format is the format to apply.
// */
//  public FormattedCellRenderer(Format format) {
//    tFormat = format;
//  }
//
//  public Component getTableCellRendererComponent(JTable table, Object value,
//                                                 boolean isSelected,
//                                                 boolean hasFocus, int nRow,
//                                                 int nCol) {
//    return super.getTableCellRendererComponent(table, value == null ? null :
//                                               tFormat.format(value),
//                                               isSelected,
//                                               hasFocus, nRow, nCol);
//  }
//}

/**
 * is a class that handles parsing the TimeSpec input.
 */
class TimeFormat
    extends Format {
  /**
   * converts an Object to a String.
   */
  public StringBuffer format(Object obj, StringBuffer toAppendTo,
                             FieldPosition fieldPosition) {
    String str = obj.toString().trim();
    fieldPosition.setBeginIndex(toAppendTo.length());
    toAppendTo.append(str);
    fieldPosition.setEndIndex(toAppendTo.length());
    return toAppendTo;
  }

  /**
   * parses the input for one or two digits.
   *
   * @param input is the input String being parsed.
   * @param index is the next character being examined.
   *
   * @return the number of characters that are digits: 0 for
   * none, 1 for one, and 2 for two.
   */
  private int parse_1_2(String input, int index) {
    int result = 0;
    if ( (input.length() > index) && Character.isDigit(input.charAt(index))) {
      result = 1;
      if ( (input.length() > (index + 1)) &&
          Character.isDigit(input.charAt(index + 1))) {
        result = 2;
      }
    }
    return result;
  }

  /**
   * parses Text to produce an Object.
   */
  public Object parseObject(String text, ParsePosition parsePos) {
    char ch;
    int pos = 0;
    int matched;
    StringBuffer result = new StringBuffer(text.length());
    String units;
    if (text.length() == 0) {
      return text;
    }
    if (text.length() > 2) {
      ch = text.charAt(pos);
      if ( (ch == '+') || (ch == '-')) {
        result.append(ch);
        ++pos;
      }
      if ( (text.length() > (pos + 2)) &&
          ( (matched = parse_1_2(text, pos)) != 0)) {
        units = text.substring(pos, pos + matched);
        if (Integer.parseInt(units) < 24) {
          result.append(units);
          pos += matched;
          ch = text.charAt(pos);
          if ((ch == ':') && (text.length() > (pos + 1))) {
            result.append(":");
            ch = text.charAt(++pos);
            if ( (matched = parse_1_2(text, pos)) != 0) {
              units = text.substring(pos, pos + matched);
              if (Integer.parseInt(units) < 60) {
                result.append(units);
                pos += matched;
                parsePos.setIndex(parsePos.getIndex() + pos);
                return result.toString();
              }
            }
          }
        }
      }
    }
    parsePos.setIndex(parsePos.getIndex() + pos);
    return null;
  }
}

/**
 * is an inner class that handles verifying the input.
 */
class TimeVerifier
    extends InputVerifier {
  /**
   * is the constructor
   *
   * @param errMsg is the usage message.
   */
  public TimeVerifier(String errMsg) {
  }

  /**
   * does the actual work of verifying a JFormattedTextField.
   *
   * @param input is the thing being verified.
   */
  public boolean verify(JComponent input) {
    if (! (input instanceof JFormattedTextField)) {
      return true;
    }
    JFormattedTextField jtf = (JFormattedTextField) input;
    JFormattedTextField.AbstractFormatter formatter = jtf.getFormatter();
    if (formatter != null) {
      try {
        formatter.stringToValue(jtf.getText());
      }
      catch (ParseException pe) {
        return false;
      }
    }
    return true;
  }

  public boolean shouldYieldFocus(JComponent input) {
    return verify(input);
  }
}
/* @(#)TimeSpec.java */