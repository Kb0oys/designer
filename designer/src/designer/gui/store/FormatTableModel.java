/*
 * Name: StoreTableModel.java
 *
 * What:
 *  This class provides a JTableModel for presenting and manipulating format FieldInfos.
 *  It requires:
 *  <ol>
 *  <li> a copy of the FieldVector for an AbstractStore (converted to GenericRecords)
 *  <li> a copy of the MetaDataStore, which contains the format information for editing
 *  a FieldVector
 *  </ol>
 *  This class extends StoreTableModel by doing nothing to save column widths.
 */
package designer.gui.store;

import java.util.Iterator;
import designer.layout.store.AbstractStore;
import designer.layout.store.FieldInfo;
import designer.layout.store.FieldVector;
import designer.layout.store.GenericRecord;
import designer.layout.store.RecordVector;

/**
 *  This class provides a JTableModel for presenting and manipulating format FieldInfos.
 *  It requires:
 *  <ol>
 *  <li> a copy of the FieldVector for an AbstractStore (converted to GenericRecords)
 *  <li> a copy of the MetaDataStore, which contains the format information for editing
 *  a FieldVector
 *  </ol>
 *  This class extends StoreTableModel by doing nothing to save column widths.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class FormatTableModel extends StoreTableModel {
    
    /**
     * is the prefix character on generating the names of new fields.  XML
     * will not accept an attribute name that begins with a digit.
     */
    private static final String LEAD_IN = "f";

    /**
     * ctor
     * @param contents is a copy of the store being edited
     * @param format is the FieldVector that controls the editing
     * @param store is the AbstractStore that contains the master copies
     * of the contents and format
     */
    public FormatTableModel(RecordVector<GenericRecord> contents, FieldVector format,
            AbstractStore store) {
        super(contents, format, store);
    }

    /**
     * tells the JTable which cells can be edited.  Most cells can be edited,
     * but a few models will want to override this method.
     * 
     * @param row is the row number of a cell (list element)
     * @param col is a column number (field)
     * 
     * @return true - the table cell can be edited
     */
    public boolean isCellEditable(int row, int col) {
        FieldInfo f = Formatting.elementAt(toColumn(col));
        boolean b = f.getEdit();
        if (f.getKeyField().equals(FieldInfo.CLASS_TAG) && 
                isProtected(toRow(row))) {
            b = false;
        }
        return b;
    }

    /**
     * inserts a new Record at the specified location.  The key property in the
     * new FieldInfo is a numeric String.  Its value is one higher that the
     * largest value found in the table.
     *
     * @param where is the row number of the new row.
     */
    public void insertRecord(int where) {
        int smallest = 0;
        int fName;
        String keyName = Formatting.getKeyField();
        GenericRecord rec;
        for (Iterator<GenericRecord> iter = Contents.iterator(); iter.hasNext(); ) {
            rec = iter.next();
            if (Contents.isUnProtected(rec)) {  
                fName = Integer.parseInt(((String) rec.findValue(keyName)).substring(LEAD_IN.length()));
                smallest = Math.max(smallest, fName);
            }
        }
        Contents.insertElementAt(Formatting.createDefaultRecord(FieldInfo.EDITRECORD, (LEAD_IN +
                String.valueOf(smallest + 1))), toRow(where));
        fireTableRowsInserted(where, where + 1);
    }
    
    /**
     * updates the column widths in the field descriptions.  Because the
     * field descriptions are final, they cannot be changed; therefore,
     * this method does nothing but override the base method.
     *
     * @param widths is an array of int's, one element for each column.
     */
    public void saveWidths(int[] widths) {
    }

    /**
     * steps through all the GenericRecords and verifies that they are acceptable.
     * @return an error String if any GenericRecord is not acceptable
     */
    public String verifyResults() {
        return defaultVerifyResults(Contents, FieldInfo.KEY_TAG, FieldInfo.KEY_LABEL);
    }    
}
/* @(#)FormatTableModel.java */