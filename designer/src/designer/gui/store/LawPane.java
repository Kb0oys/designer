/* Name: LawPane.java
 *
 * What:
 *  This Dialog is for setting the "Dead on the Law" Hours.
 */
package designer.gui.store;

import designer.gui.jCustom.AcceptDialog;
import designer.layout.Hours;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.text.SimpleDateFormat;

/**
 *  This Dialog is for setting the "Dead on the Law" Hours.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class LawPane
    extends JPanel {

  /**
   * is the field for editing the hours.
   */
  private JFormattedTextField HoursField;

  /**
   * constructs the JPanel for editing the Hours field.
   *
   * @param hours is a JFormatedTextField containing the current value.
   * If no hours have been set, it is null or "".
   */
  public LawPane(String hours) {
    if (hours == null) {
      hours = new String("");;
    }
    HoursField = new JFormattedTextField(new SimpleDateFormat("HH:mm"));
    HoursField.setColumns(5);
    HoursField.setText(hours);
    add(new JLabel("Hours Allowed by Law to Work:"));
    add(HoursField);
  }

  /**
   * is the factory to construct a LawDialog.
   *
   * @param hours is the number of hours the crew may work when the method
   * is called.
   */
  public static void editHours(String hours) {
    LawPane pane = new LawPane(hours);
    if (AcceptDialog.select(pane, "Edit Work Hours")) {
      Hours.setHours(pane.HoursField.getText());
    }
  }
}
/* @(#)LawPane.java */