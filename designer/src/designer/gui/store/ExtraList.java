/* What: ExtraList.java
 *
 * What:
 *  This class is a placeholder for a Class that creates a Selection
 *  List from crew members on the extra board.
 */
package designer.gui.store;

import java.util.Vector;

import designer.layout.store.GenericRecord;

/**
 *  This class is a placeholder for a Class that creates a Selection
 *  List from crew members on the extra board.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

/**
 * is a filter for selecting certain file suffixes.
 */
public class ExtraList extends SelectionSpec {


  /**
   * is the no argument constructor.
   */
  public ExtraList() {
    this("");
  }

  /**
   * is the single argument constructor.
   *
   * @param spec is a dummy String.
   */
  public ExtraList(String spec) {
    super(spec);
  }

  /**
   * sets up the List from the specific store.
   *
   * @return the GenericRecords from the specific Store that from which a
   * selection can be made.
   */
  protected Vector<GenericRecord> initializeList() {
      return new Vector<GenericRecord>();
  }

}
/* @(#)ExtraList.java */
