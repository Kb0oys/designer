/* What: SelectionSpec.java
 *
 * What:
 *  This class creates a generic class that is dummy for a Selection
 *  List in CATS.  The intent is for members of the class to appear
 *  in JTable cells, so that the column widths can be set and so that
 *  the actual class will be filled in by CATS.  Sub-classes need only
 *  be derived from this class.
 */
package designer.gui.store;

import java.util.Vector;

import designer.layout.store.GenericRecord;

/**
 *  This class creates a generic class that is dummy for a Selection
 *  List in CATS.  The intent is for members of the class to appear
 *  in JTable cells, so that the column widths can be set and so that
 *  the actual class will be filled in by CATS.  Sub-classes need only
 *  be derived from this class.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

/**
 * is a filter for selecting certain file suffixes.
 */
public abstract class SelectionSpec extends ListSpec {
    
    /**
     * is the Tag used to select the FieldPair in each GenericRecord for
     * the label place in the Selection Dialog.
     */
    protected String SelectionTag;

    /**
     * is the no argument constructor.
     */
    public SelectionSpec() {
      this("");
    }

    /**
     * is the single argument constructor.
     *
     * @param spec is a dummy String.
     */
    public SelectionSpec(String spec) {
      super(spec);
    }

    /**
     * is the sub-class specific method for building the Vector of labels
     * in the Selection dialog.
     *
     * @return the Vector of Strings that forms the contents of the Selection
     * Dialog.
     */
    protected Vector<String> createSpecificList() {
      return createList(initializeList(), SelectionTag);
    }

    /**
     * sets up the List from the specific store.
     *
     * @return the GenericRecords from the specific Store that from which a
     * selection can be made.
     */
    protected abstract Vector<GenericRecord> initializeList();
}
/* @(#)SelectionSpec.java */
