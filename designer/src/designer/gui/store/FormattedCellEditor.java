/* What: FormattedCellEditor.java
 *
 * What:
 *  This class creates an object that is a wrapper around a DefaultTableCellEditor.
 *  Its purpose is to create one of the Swing standard Table Cell Editors.
 *  <p>
 *  This code is based on example 18.6 from "Swing" by Robinson and Vorobiev. 
 */
package designer.gui.store;

import javax.swing.DefaultCellEditor;
import javax.swing.JFormattedTextField;

/**
 *  This class creates an object that is a wrapper around a DefaultTableCellEditor.
 *  Its purpose is to create one of the Swing standard Table Cell Editors.
 *  <p>
 *  This code is based on example 18.6 from "Swing" by Robinson and Vorobiev. 
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class FormattedCellEditor extends DefaultCellEditor {
    
    /**
     * the ctor
     * @param formattedTextField is the thing being edited
     */
    public FormattedCellEditor(final JFormattedTextField
            formattedTextField) {
        super(formattedTextField);
        formattedTextField.removeActionListener(delegate);
        delegate = new EditorDelegate() {
            public void setValue(Object value) {
                formattedTextField.setValue(value);
            }
            
            public Object getCellEditorValue() {
                return formattedTextField.getValue();
            }
        };
        formattedTextField.addActionListener(delegate);
        formattedTextField.setBorder(null);
    }
}
/* @(#)FormattedCellEditor.java */
