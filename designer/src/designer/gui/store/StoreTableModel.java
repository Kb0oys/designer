/*
 * Name: StoreTableModel.java
 *
 * What:
 *  This class provides a JTableModel for presenting and manipulating an AbstractStore.
 *  It requires:
 *  <ol>
 *  <li> a copy of the store
 *  <li> a copy of the FieldVector
 *  </ol>
 *  The first is only a copy because if the user cancels any edits, the original should
 *  remain unchanged.  The latter controls the presentation of the former.  It should
 *  also be a copy because the user may adjust the column widths through the JTable.
 */
package designer.gui.store;

import java.util.Iterator;
import javax.swing.table.AbstractTableModel;

import designer.layout.store.AbstractStore;
import designer.layout.store.FieldInfo;
import designer.layout.store.FieldVector;
import designer.layout.store.GenericRecord;
import designer.layout.store.RecordVector;

/**
 *  This class provides a JTableModel for presenting and manipulating an AbstractStore.
 *  It requires:
 *  <ol>
 *  <li> a copy of the store
 *  <li> a copy of the FieldVector
 *  </ol>
 *  The first is only a copy because if the user cancels any edits, the original should
 *  remain unchanged.  The latter controls the presentation of the former.  It should
 *  also be a copy because the user may adjust the column widths through the JTable.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class StoreTableModel extends AbstractTableModel{
 
    /**
     * are the JTable contents
     */
    protected RecordVector<GenericRecord> Contents;
    
    /**
     * is the formatting information for the JTable
     */
    protected FieldVector Formatting;
 
    /**
     * is the AbstractStore containing the information being
     * edited
     */
    protected AbstractStore Store;
    
    /**
     * is the number of visible columns
     */
    private final int NumCols;
    
    /**
     * ctor
     * @param contents is a copy of the store being edited
     * @param format is the FieldVector that controls the editing
     * @param store is the AbstractStore that contains the master copies
     * of the contents and format
     */
    public StoreTableModel(RecordVector<GenericRecord> contents, FieldVector format,
            AbstractStore store) {
        Contents = contents;
        Formatting = format;
        Store = store;
        int cols = 0;
        
        /* Count the number of visible columns.  This cannot change. */
        for (Iterator<FieldInfo> iter = Formatting.iterator(); iter.hasNext(); ) {
            if (iter.next().getVisible()) {
                ++cols;
            }
        }
        NumCols = cols;
    }

    /**
     * is called to get a copy of the GenericRecords being edited.  This should
     * only be called as part of verification.
     * 
     * @return the GenericRecords
     */
    public RecordVector<GenericRecord> getContents() {
        return Contents;
    }
    
    /**
     * counts the number of columns.  The number is immutable throughout
     * the life of the TableModel; thus, it is computed when the TableModel
     * is created.
     * 
     * @return the immutable number of columns
     */
    public int getColumnCount() {
        return NumCols;
    }

    /**
     * counts the number of visible rows.  It may change, as rows are added and
     * deleted.
     * 
     * @return the number of visible rows
     */
    public int getRowCount() {
        int rows = 0;
        for (Iterator<GenericRecord> iter = Contents.iterator(); iter.hasNext(); ) {
            if (Contents.isVisible(iter.next())) {
                ++rows;
            }
        }
        return rows;
    }

    /**
     * return the value from the requested visible record at the requested
     * visible column.  The element at that place is a tag:value pair.  The value
     * half is what is of interest.
     * 
     * @param r is the row index in the JTable
     * @param c is the column index in the JTable
     * @return the value at that place.  It may have to be cast to the right
     * type, per the equivalent format field.
     */
    public Object getValueAt(int r, int c) {
        int column = toColumn(c);
        int row = toRow(r);
        FieldInfo desc = Formatting.get(column);
        Object o = Contents.elementAt(row).findValue(desc.getKeyField());
        Class<?> colClass = desc.getFieldClass();
        if (ClassSpec.class.isAssignableFrom(colClass)) {
            o = ClassSpec.normalizeClassName(o.toString());
        }
        return o;
    }

    /**
     * sets the value at the requested visible record at the requested visible
     * column.  The element at that place is a tag:value pair.  It may be
     * necessary to cast the value type.
     * 
     * @param aValue is the new value
     * @param r is the index of the requested visible row
     * @param c is the index of the requested column
     */
    public void setValueAt(Object aValue, int r, int c) {
        int column = toColumn(c);
        int row = toRow(r);
        FieldInfo desc = Formatting.get(column);
        Class<?> colClass = desc.getFieldClass();
        if (ClassSpec.class.isAssignableFrom(colClass)) {
            aValue = ClassSpec.toClass((String) aValue);
        }
        Contents.elementAt(row).findPair(desc.getKeyField()).FieldValue = aValue;
    }
    
    /**
     * converts a JTable column number to a Contents column number. If there
     * are no visible columns, this will return the index of the last column. 
     * @param c is the JTable column number
     * @return the number of the "cth" visible column
     */
    protected int toColumn(int c) {
        int cols = 0;
        int stepper = 0;
        for (Iterator<FieldInfo> iter = Formatting.iterator(); iter.hasNext(); ) {
            if (iter.next().getVisible()) {
                if (cols == c) {
                    break;
                }
                ++cols;
            }
            ++stepper;
        }
        return stepper;
    }
    
    /**
     * converts a JTable row number to a Contents row number.  The mapping is
     * controlled by the abstract method isVisible().  Any record which is not visible is
     * skipped in the count.
     * @param r is the index of the row in the JTable
     * @return the index of the equivalent record in the RecordVector
     */
    protected int toRow(int r) {
        int rows = 0;
        int stepper = 0;
        for (Iterator<GenericRecord> iter = Contents.iterator(); iter.hasNext(); ) {
            if (Contents.isVisible(iter.next())) {
                if (rows == r) {
                    break;
                }
                ++rows;
            }
            ++stepper;
        }
        return stepper;            
    }
    
    /**
     * tells the JTable what heading should be used in a column.
     * 
     * @param i is the column
     * 
     * @return the column heading for column i.
     */
    public String getColumnName(int i) {
        int col = toColumn(i);
        return Formatting.elementAt(col).getLabel();
    }
    
    /**
     * tells the JTable what class of Object is in a column.
     * 
     * @param col is the column number
     * 
     * @return the Class of objects in the column
     */
    public Class<?> getColumnClass(int col) {
        int index = toColumn(col);
        Class<?> c = Formatting.elementAt(index).getFieldClass();
        return c;
    }
    
    /**
     * tells the JTable how wide a column is.
     * 
     * @param col is the column number
     * 
     * @return the preferred width of the column.
     */
    public int getColWidth(int col) {
        int index = toColumn(col);
        return Formatting.elementAt(index).getWidth();
    }
    
    /**
     * is called to retrieve the alignment of the text fields within
     * a column.
     * 
     * @param column is the index of the column being queried.
     * 
     * @return the SwingConstant for the alignment
     */
    public int getColumnAlignment(int column) {
        int index = toColumn(column);
        String align = Formatting.elementAt(index).getAlignment();
        if (align.length() == 0) {
            align = AlignmentList.DEFAULT;
        }
        return AlignmentList.findAlignment(align);
    }

    /**
     * is called to retrieve the tag on a field at a specific
     * column.  The field order does not change while the table is
     * active.  Thus, the requested column is the index of the field
     * in the model.
     * @param index is the index into the vector of fields that describe
     * each column.  Visibility must be considered.
     * @return the tag on the field
     */
    public String getColumnIdentifier(int index) {
        return Formatting.elementAt(toColumn(index)).getKeyField();
    }
    
    /**
     * updates the column widths in the field descriptions.
     *
     * @param widths is an array of int's, one element for each column.
     */
    public void saveWidths(int[] widths) {
        int tCol = 0;
        for (int col = 0; col < Formatting.size(); ++col) {
            if (Formatting.elementAt(col).getVisible()) {
                (Formatting.elementAt(col)).setWidth(widths[tCol++]);
            }
        }
    }
    
    /**
     * tells the JTable which cells can be edited.  Most cells can be edited,
     * but a few models will want to override this method.
     * 
     * @param row is the row number of a cell (list element)
     * @param col is a column number (field)
     * 
     * @return true - the table cell can be edited
     */
    public boolean isCellEditable(int row, int col) {
        int index = toColumn(col);
        boolean b = Formatting.elementAt(index).getEdit();
        return b;
    }

    /**
     * is a query for if the row in the table can be deleted
     * or not.  The default is that it can be deleted; however,
     * derived classes may want to override this method.
     * @param row is the index of the GenericRecord in Contents, not on the
     * JTable.
     * @return true if it can be deleted and false if it cannot.
     */
    public boolean isProtected(int row) {
        return !Contents.isUnProtected(Contents.elementAt(toRow(row)));
    }
    
    /**
     * creates and inserts a new element
     * 
     * @param row is the place in the vector
     * where the element is inserted.
     */
    public void insertRecord(int row) {
        Contents.insertElementAt(Formatting.createDefaultRecord(FieldInfo.DATARECORD, null), toRow(row));
        fireTableRowsInserted(row, row + 1);
    }

    /**
     * deletes a list element.  The deletion is from highest index to lowest
     * because after an element is deleted, all the higher ones move down.
     * 
     * @param low is the lowest numbered row to delete
     * @param high is the highest numbered row to delete (it
     * can be the same as low, if only one row is being deleted).
     * @param status is a String that explains why the record should be deleted
     */
    public void delRecord(int low, int high, String status) {
        int first = toRow(low);
        int last = toRow(high);
        GenericRecord rec;
        for (int i = last; i >= first; --i) {
            rec = Contents.elementAt(i);
            if (!isProtected(i) && Contents.isVisible(rec)) {
                Contents.hide(rec, status);
            }
        }
        fireTableRowsDeleted(low, high);
    }

    /**
     * moves a block of Generic Records down in the Vector.  The table model displays
     * a Vector of GenericRecords.  Some of these GenericRecords may not be visible.  However,
     * it is desirable to retain the ordering between visible and non-visible GenericRecords.
     * When a visible GenericRecord is moved, all the invisible GenricRecords that follow it
     * (up to the next visible GenericRecord) should move with it.  Thus, the visible GenericRecord
     * acts as an anchor for itself and possibly more GenericRecords.
     * <p>
     * This algorithm moves the anchor (and attached blocks) down (to lower indexes) "n" visible
     * GenericRecords.  Movement is always down.  There are several reasons for this:
     * <ul>
     * <li>After removing an element from a Java Vector, the indexes of all elements after it
     * shift to the next smaller value.  By removing from after the insertion point, the index
     * of the insertion point does not change.
     * <li>As long as the insertion point is not a negative number (and the blocks to move exist),
     * the insertion point is guaranteed to exist.
     * <li>Moving a block up one visible GenericRecord is accomplished by moving the anchor
     * (and associated invisible GenericRecords) to in front of the block being moved.
     * </ul>
     * @param bottom is the index of bottom (lowest numbered) GenericRecord to move, in the index space
     * of visible GenericRecords (i.e. as known by the JTable)
     * @param size is the number of visible GenericRecords (i.e. anchors) to move
     * @param dest is the index of the visible GenericRecord, in the index space of visible
     * GenericRecords, of the insertion point.  All GenericRecords in the moved block will be
     * inserted immediately before the dest visible GenericRecord
     */
    private void moveRows(int bottom, int size, int dest) {
        GenericRecord rec;
        int insert = toRow(dest);   // the index into Contents of the insertion point
        int count = 0;              // a counter of visible records
        int pull = toRow(bottom) + 1;   // the index into Contents of where records are being moved from
        if ((dest >= 0) && (bottom > dest) && (size > 0)) {
            // this locates the beginning (highest index) of the block being moved
            while (pull < Contents.size()) {
                rec = Contents.elementAt(pull);
                if (Contents.isVisible(rec)) {
                    if ((++count) == size) {
                        break;
                    }
                }
                ++pull;
            }

            // At this point pull is the index of the next visible record or off the Vector,
            // so the last GenericRecord in the block is one less
            --pull;

            // The indexes do not have to be adjusted.  Pull from pull and add at insert until
            // the record corresponding to bottom has been moved
            for (count = 0; count < size;) {
                rec = Contents.remove(pull);
                Contents.add(insert, rec);
                if (Contents.isVisible(rec)) {
                    ++count;
                }
            }
        }
    }
    
    /**
     * moves a contiguous group of rows up one row in the JTable (i.e.
     * to lower indices).
     *
     * @param top is the lowest numbered row to be moved.
     * @param bottom is the highest numbered row to be moved.
     */
    public void moveUp(int top, int bottom) {
      moveRows(top, bottom - top + 1, top - 1);
    }

    /**
     * moves a contiguous group of rows down one row.
     *
     * @param top is the lowest numbered row to be moved.
     * @param bottom is the highest numbered row to be moved.
     */
    public void moveDown(int top, int bottom) {
      moveRows(bottom + 1, 1, top);
    }

    /**
     * Moves a block of columns to lower indices.  It is the same algorithm
     * as moveRows except it operates on formatting, the indexes are in
     * Formatting, and the visibility test is different.
     * @param left is the lowest index FieldInfos in the block
     * @param size is the number of visible FieldInfos to move
     * @param dest is the where left should be moved to
     */
    private void moveColumns(int left, int size, int dest) {
        FieldInfo field;
        int insert = dest;   // the index into Formatting of the insertion point
        int count = 0;              // a counter of visible FieldInfos
        int pull = left + 1;   // the index into Formatting of where records are being moved from
        if ((dest >= 0) && (left > dest) && (size > 0)) {
            // this locates the beginning (highest index) of the block being moved
            while (pull < Formatting.size()) {
                field = Formatting.elementAt(pull);
                if (field.getVisible()) {
                    if ((++count) == size) {
                        break;
                    }
                }
                ++pull;
            }

            // At this point pull is the index of the next visible FieldInfo or off the Vector,
            // so the last FieldInfo in the block is one less
            --pull;

            // The indexes do not have to be adjusted.  Pull from pull and add at insert until
            // the FieldInfo corresponding to left has been moved
            for (count = 0; count < size;) {
                field = Formatting.remove(pull);
                Formatting.add(insert, field);
                if (field.getVisible()) {
                    ++count;
                }
            }
        }        
    }

    /**
     * steps through the Vector of FieldInfos and arranges them in the order
     * specified by ids.
     * @param ids is the order of the columns (FieldInfos) when closing the JTable.
     * Each column has the tag of the FieldInfo that describes it.
     */
    public void reorderFormat(String[] ids) {
        FieldInfo field;
        int index = 0;
        int mark;
        String match;
        for (int col = 0; col < ids.length; ++col) {
            // find the next visible FieldInfo
            field = Formatting.elementAt(index);
            while (!field.getVisible()) {
                field = Formatting.elementAt(++index);
            }
            
            // if not done, see if it has been moved
            match = ids[col];
            if (!field.getKeyField().equals(match)) {
                // locate where the named FieldInfo was
                for (mark = index + 1; !Formatting.elementAt(mark).getKeyField().equals(match);
                ++mark) {
                    // do nothing
                }
                moveColumns(mark, 1, index);
                field.setSaved(false);
            }
            ++index;
        }
    }
    
    /**
     * steps through all the GenericRecords and verifies that they are acceptable.
     * @return an error String if any GenericRecord is not acceptable
     */
    public String verifyResults() {
        return Store.checkConsistency(this);
    }
    
    /**
     * steps through all the GenericRecords in a RecordVector and verifies that
     * they are acceptable.  This is the default verification scheme.  It assumes
     * that the value of the keys is a String.  It checks that each value is
     * not the empty string ("") and is unique.  It terminates on the first
     * error.
     * @param rVector is the RecordVector of GenericRecords being verified
     * @param keyTag is the tag of the key field
     * @param label is the column heading on the key field, for printing
     * an error.
     * @return an error String if any GenericRecord is not acceptable
     */
    public static String defaultVerifyResults(RecordVector<GenericRecord> rVector,
            String keyTag, String label) {
        int recordCount = rVector.size();
        String key1;
        String key2;
        GenericRecord gr;

        // The first check is to be sure all key fields are not blank.
        for (int rec = 0; rec < recordCount; ++rec) {
            gr = rVector.elementAt(rec);
            key1 = (String) gr.findValue(keyTag);
            if ((key1 == null) || key1.trim().equals("")) {
                return new String("Row " + rec + " needs a " + label);
            }
        }

        // The next check is to be sure there are no duplicates.
        for (int lower = 0; lower < recordCount; ++lower) {
            gr = rVector.elementAt(lower);
            key1 = ((String) gr.findValue(keyTag)).trim();
            if (key1 != null) {
                for (int test = lower + 1; test < recordCount; ++test) {
                    gr = rVector.elementAt(test);
                    key2 = ((String) gr.findValue(keyTag)).trim();
                    if ((key2 != null) && key1.equals(key2)) {
                        return new String("Multiple " + label + " have the value " + key1);
                    }
                }
            }
        }
        return null;        
    }
}
/* @(#)StoreTableModel.java */