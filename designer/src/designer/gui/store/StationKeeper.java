/*
 * Name: StationKeeper.java
 * 
 * What:
 *   This file contains the object that holds the train/station
 *   relations.
 *   
 * Special Considerations:  
 */
package designer.gui.store;

import java.util.Enumeration;
import java.util.Vector;
/**
 * This class is a repository for train/station relations.
 * It stores which trains stop at which stations.
 * <p>
 * Because it relates stations to trains, it is susceptible
 * to either a train or a station being deleted, which
 * would result in a dangling reference.  Some of the
 * gyrations that the code goes through is to prevent
 * dangling references.
 * 
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2006, 2011</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 *
 */
public class StationKeeper {

    /**
     * This data Structure holds the information about a
     * Station.
     */
    private class Station {
        /**
         * The name of the Station, as it appears in Blocks.
         */
        public String BlkStationName;
        
        /**
         * The Label for the Station on the Status screen.
         */
//        public String StationLabel = "";
        
        /**
         * The number of Blocks that contain the Station.
         */
        public int RefCount = 1;
        
        /**
         * a constructor.
         * @param station is the name of the station
         */
        public Station(String station) {
            BlkStationName = new String(station);
        }
    }

    /**
     * is the Station/Train relation.  An object of this
     * class is created for each train that stops at a Station.
     * Conversely, an object exists for each Station that a 
     * train stops at.
     */
/*    private class Relation {
        String S; // the Station name
        Train T;  // the Train
    }
    */
    /**
     * is the respository of Stations.
     */
    static private Vector<Station> StationStore;
    
    /**
     * is the constructor.
     */
    public StationKeeper() {
        StationStore = new Vector<Station>();
    }
    
    /**
     * is called to tell the StationKeeper that a Station
     * has been found.  Because multiple blocks may contain
     * the same Station (e.g. either the Block on the main or
     * the Block on a siding can be considered the Station),
     * the Station may already exist and it should not be blindly
     * added.  The inverse operation of deleting a Station must
     * not also blindly delete a Station.  To handle garbage
     * collection, a reference count is used.
     * 
     * @param station is the name of the Station, as it
     * appears in the Block.
     */
    public void addStation(String station) {
        String testName = "";
        Station testStation = null;
        /* search for an existing Station with the same name */
        for (Enumeration<Station> e = StationStore.elements(); e.hasMoreElements(); ) {
            testStation = e.nextElement();
            if ((testName = testStation.BlkStationName).equals(station)) {
                break;
            }
        }
        if ((testStation != null) && testName.equals(station)) {
            ++testStation.RefCount;
        }
        else {
            StationStore.add(new Station(station));
        }
    }
    
    /**
     * is called to delete a Station.  Because it could be
     * in multiple Blocks, a reference count is kept of
     * all the Blocks it is in.  When the reference count
     * goes to zero, it can be deleted.
     * 
     * @param station is the name of the Station, as it
     * appears in the Block.
     */
    public void deleteStation(String station) {
        String testName = "";
        Station testStation = null;
        /* search for an existing Station with the same name */
        for (Enumeration<Station> e = StationStore.elements(); e.hasMoreElements(); ) {
            testStation = e.nextElement();
            if ((testName = testStation.BlkStationName).equals(station)) {
                break;
            }
        }
        if (testName.equals(station) && (testStation != null)) {
            if ((--testStation.RefCount) <= 0) {
                StationStore.removeElement(testStation);
            }
        }
    }
}
/* @(#)StationKeeper.java */