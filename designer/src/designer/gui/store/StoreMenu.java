/* Name: StoreMenu.java
 *
 * What:
 *  This file contains the class definition for an AbstractStoreMenu object.  An
 *  AbstractStoreMenu constructs the menu and sub-menus for editing an
 *  AbstractStore.
 *
 */
package designer.gui.store;

import designer.gui.Ctc;
import designer.gui.DispPanel_AboutBox;
import designer.gui.XmlFileFilter;
import designer.layout.items.LoadFilter;
import designer.layout.store.AbstractStore;
import designer.layout.xml.XMLReader;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;


/**
 *  This file contains the class definition for an AbstractStoreMenu object.  An
 *  AbstractStoreMenu constructs the menu and sub-menus for editing an
 *  AbstractStore.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2011, 2020</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class StoreMenu extends JMenu {

  /**
   * is the concrete store to be edited.
   */
  private AbstractStore MyStore;

  /**
   * is the name of the data being edited.
   */
  private String DataKind;

  /**
   * the constructor.
   *
   * @param title is the kind of contents of the Store.
   * @param store is the Store being edited.
   */
  public StoreMenu(String title, AbstractStore store) {
    super(title);
    DataKind = title;
    MyStore = store;

    JMenuItem subMenu;
    subMenu = new JMenuItem("Edit " + title + "s");
    subMenu.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MyStore.editData();
      }
    });
    add(subMenu);
    subMenu = new JMenuItem("Save " + title + "s");
    subMenu.addActionListener(new Save_ActionAdapter());
    add(subMenu);
    subMenu = new JMenuItem("Load " + title + "s");
    subMenu.addActionListener(new Load_ActionAdapter());
    add(subMenu);
    subMenu = new JMenuItem("Edit " + title + " Fields");
    subMenu.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        MyStore.editControls();
    }
    });
    add(subMenu);
  }

  /**
   * Determines if the Store should be saved.  If so, it
   * invoked the dialog to save it.
   * 
   * @return true if the operation should continue or false if the
   * user cancles.
   */
  private boolean chkSaveStatus() {
      int result;
      if (!MyStore.isSaved()) {
          result = JOptionPane.showConfirmDialog( (Component)null,
                  "Save existing " + DataKind + "s?",
                  "Save existing " + DataKind + "s?",
                  JOptionPane.YES_NO_OPTION);
          if (result == JOptionPane.YES_OPTION) {
              return saveStore();
          }
          else {
              // Its OK for the user to decide not to save his work.
          }
      }
      return true;
  }
  
  /**
   * handles the dialog for saving the Store.
   * @return true if the user wants to continue or false to abort
   */
  private boolean saveStore() {
      PrintStream outStream = null;
      int result;
      File fileobj = null;
      boolean approved = false;
      String fileName;
      XmlFileFilter fFilter = new XmlFileFilter(".xml", "XML file");
      JFileChooser chooser = new JFileChooser(new File(designer.gui.DispPanel.FilePath));
      chooser.addChoosableFileFilter(fFilter);
      chooser.setFileFilter(fFilter);
      chooser.setDialogTitle("Select file to save " + DataKind + "s in:");
      result = chooser.showSaveDialog(null);
      fileobj = chooser.getSelectedFile();
      approved = (result == JFileChooser.APPROVE_OPTION);
      if (approved) {
    	fileName = fileobj.getName();
        if (fileobj.exists()) {
        	
          if (fileobj.isFile()) {
            result = JOptionPane.showConfirmDialog( (Component)null,
                                                   "Overwrite " + fileobj.getName() +
                                                   "?",
                                                   "Overwrite File?",
                                                   JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
              if (!fileobj.delete()) {
                  designer.gui.DispPanel.FilePath = fileobj.getAbsolutePath();
                JOptionPane
                    .showMessageDialog( (Component)null,
                                       fileobj.getName()
                                       + " could not be deleted",
                                       "Deletion Error",
                                       JOptionPane.ERROR_MESSAGE);
                fileobj = null;
              }
            }
            else {
              fileobj = null;
            }
          }
          else {
            JOptionPane.showMessageDialog( (Component)null, fileobj
                                          .getName()
                                          + " is not a file", "File Error",
                                          JOptionPane.ERROR_MESSAGE);
          }
        }
        try {
          if ( (fileobj != null) && !fileobj.createNewFile()) {
            JOptionPane.showMessageDialog( (Component)null, fileName + " could not be created.",
                                          "File Error",
                                          JOptionPane.ERROR_MESSAGE);
            fileobj = null;
          }
        }
        catch (IOException except) {
          JOptionPane.showMessageDialog( (Component)null, "Error creating "
                                        + fileobj.getName(), "File Error",
                                        JOptionPane.ERROR_MESSAGE);
        }
        try {
          outStream = new PrintStream(new FileOutputStream(fileobj));
        }
        catch (FileNotFoundException except) {
          JOptionPane.showMessageDialog( (Component)null, "Error creating "
                                        + fileName, "File Error",
                                        JOptionPane.ERROR_MESSAGE);

        }
        if (outStream != null) {
          String errorReport = null;
          Element root = new Element(Ctc.DocumentTag);
          root.setAttribute(Ctc.VersionTag, DispPanel_AboutBox.getVersion());
          Document doc = new Document(root);
          XMLOutputter fmt = new XMLOutputter();
          fmt.setFormat(org.jdom2.output.Format.getPrettyFormat());
          if ( (errorReport = MyStore.putReducedXML(root)) == null) {
            try {
              fmt.output(doc, outStream);
            }
            catch (java.io.FileNotFoundException nfExcept) {
              errorReport = new String("FileNotFound error writing file " +
                                       nfExcept.getLocalizedMessage());
            }
            catch (java.io.IOException ioExcept) {
              errorReport = new String("IO exception writing file " +
                                       ioExcept.getLocalizedMessage());
            }
          }
          if (errorReport != null) {
              System.out.println("Write Error: " + errorReport);
          }
        }
      }
      return approved;
  }

  /**
   * is an ActionAdapter for the Load MenuItem.
   */
  class Load_ActionAdapter
      implements ActionListener {

      public void actionPerformed(ActionEvent e) {
          int result;
          String errReport;
          XmlFileFilter fFilter = new XmlFileFilter(".xml", "XML file");
          JFileChooser chooser = new JFileChooser(new File(designer.gui.DispPanel.FilePath));
          if (chkSaveStatus()) {
              chooser.addChoosableFileFilter(fFilter);
              chooser.setFileFilter(fFilter);
              chooser.setDialogTitle("Select " + DataKind + "s file to read:");
              result = chooser.showOpenDialog(null);
              File fileobj = chooser.getSelectedFile();
              if (result == JFileChooser.APPROVE_OPTION) {
                  if (fileobj.exists() && fileobj.canRead()) {
                      LoadFilter.instance().setFilter(MyStore.getTag());
                      designer.gui.DispPanel.FilePath = fileobj.getAbsolutePath();
                      errReport = XMLReader.parseDocument(fileobj);
                      if (errReport != null) {
                          JOptionPane.showMessageDialog( (Component)null,
                                  errReport, "Open Error",
                                  JOptionPane.ERROR_MESSAGE);
                      }
                  }
                  else {
                      JOptionPane.showMessageDialog( (Component)null, fileobj
                              + " does not exist", "Open Error",
                              JOptionPane.ERROR_MESSAGE);
                  }
              }
              else if (result == JFileChooser.CANCEL_OPTION) {
                  System.out.println("Not opening file for reading");
              }
          }
      }
  }

  /**
   * is an ActionAdapter for the Save MenuItem
   */
  class Save_ActionAdapter
      implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        saveStore();
    }
  }
}
/* @(#)StoreMenu.java */