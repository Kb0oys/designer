/**
 * What: ComboRenderer.java
 *
 * What: a class for placing a ComboBox in a JTable cell.
 */
package designer.gui.store;

import java.awt.Component;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * is a class for rendering a ComboBox in a JTable cell.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ComboRenderer extends JComboBox
implements TableCellRenderer {

    /**
     * is the list composing the JComboBox.
     */
    private Vector<String> MyList;

    /**
     * is the constructor.
     *
     * @param list is a Vector list items.
     */
    public ComboRenderer(Vector<String> list) {
      super(list);
      MyList = list;
    }

    /*
     * is the method required by the TableCellRenderer interface.
     * <p>
     * The class assumes that a blank String has been added to the
     * selection list, as the last item.  So, if the value is null
     * or is the blank string, the index will be set to that item.
     */
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus, int row,
                                                   int column) {
      String svalue = (String) value;
      int index = -1;
      if (isSelected) {
        setForeground(table.getSelectionForeground());
        super.setBackground(table.getSelectionBackground());
      }
      else {
        setForeground(table.getForeground());
        setBackground(table.getBackground());
      }
      if ( (svalue == null) || (svalue.length() == 0)) {
        index = getItemCount() - 1;
      }
      else {
        index = MyList.indexOf(svalue);
      }
      setSelectedIndex(index);
      return this;
    }

}
/* @(#)ComboRenderer.java */