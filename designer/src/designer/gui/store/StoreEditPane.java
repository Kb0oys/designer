/* Name: StoreEditPane.java
 *
 * What:
 *  This file contains the class definition for a StoreEditPane object.  A
 *  StoreEditPane constructs a JTable within a ScrollPane for adding and
 *  deleting records and for editing Fields in a record.
 *
 */
package designer.gui.store;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import designer.gui.jCustom.AcceptDialog;
/**
 *  This file contains the class definition for a Store EditPane object.  A
 *  StoreEditPane constructs a JTable within a ScrollPane for adding and
 *  deleting records and for editing Fields in a record.
 * <p>
 * This class uses the strategy design pattern for controlling the editing.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2009</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class StoreEditPane
    extends JPanel {
  /**
   * is the Button for interchanging the selected row with the one above it.
   */
  private final JButton UpButton = new JButton("Move Record Up");

  /**
   * is the Button for interchanging the selected row with the one below it.
   */
  private final JButton DownButton = new JButton("Move Record Down");

  /**
   * is the Button for adding a new row below Bottom row.
   */
  private final JButton AddButton = new JButton("Add Record");

  /**
   * is the Button for removing a Record at the selection point.
   */
  private final JButton DelButton = new JButton("Delete Record(s)");

  /**
   * is the JTable that holds the fields of the Records.
   */
  private JTable Table;

  /**
   * is the TableModel that controls the editing.
   */
  private StoreTableModel EditModel;

  /**
   * is the row number of the top row of the selected range.
   */
  private int TopRow;

  /**
   * is the row number of the bottom row of the selected range.
   */
  private int BottomRow = -1;

  /**
   * constructs the AbstractEditPane.
   * @param model is the TableModel controlling the editing.
   */
  public StoreEditPane(StoreTableModel model) {
    TableColumnModel tcm;
    JPanel movers = new JPanel();
    Class<?> colClass;
    SpecialType special;
    EditModel = model;
    Table = new JTable(EditModel);
    int tableHeight;
    int tableWidth = 0;
    DefaultTableCellRenderer renderer;
    DefaultCellEditor editor;
    TableCellRenderer tcr;
    
    tcm = Table.getColumnModel();
    for (int c = 0; c < tcm.getColumnCount(); ++c) {
        tableWidth += model.getColWidth(c);
        tcm.getColumn(c).setIdentifier(model.getColumnIdentifier(c));
        tcm.getColumn(c).setPreferredWidth(model.getColWidth(c));
        colClass = EditModel.getColumnClass(c);
        if (SpecialType.class.isAssignableFrom(colClass)) {
            try {
                special = (SpecialType) colClass.newInstance();
                editor = special.getCellEditor();
                editor.setClickCountToStart(1);
                tcm.getColumn(c).setCellEditor(editor);
                tcr = special.getCellRenderer();
                if (DefaultTableCellRenderer.class.isInstance(tcr) ) {
                    ((DefaultTableCellRenderer)tcr).setHorizontalAlignment(model.getColumnAlignment(c));
                }
                tcm.getColumn(c).setCellRenderer(tcr);
            }
            catch (IllegalAccessException iae) {
                System.out.println("Illegal Access Exception for " +
                        colClass.getName());
            }
            catch (InstantiationException ie) {
                System.out.println("Instantiation Exception for " +
                        colClass.getName());
            }
        }
        else if (!Boolean.class.isAssignableFrom(colClass)){
            if (Integer.class.isAssignableFrom(colClass)) {
                editor = new FormattedCellEditor(new JFormattedTextField(NumberFormat.getIntegerInstance()));
                renderer = new FormattedCellRenderer(NumberFormat.getIntegerInstance());
            }
            else {
                renderer = new DefaultTableCellRenderer();
                editor = new DefaultCellEditor(new JTextField());
            }
            editor.setClickCountToStart(1);
            tcm.getColumn(c).setCellEditor(editor);
            renderer.setHorizontalAlignment(model.getColumnAlignment(c));
            tcm.getColumn(c).setCellRenderer(renderer);
        }
    }
    Table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    Table.getSelectionModel().addListSelectionListener(new
        ListSelectionListener() {
      public void valueChanged(ListSelectionEvent ev) {
        int selRows[] = Table.getSelectedRows();
        TopRow = EditModel.getRowCount();
        BottomRow = -1;
        if (!ev.getValueIsAdjusting()) {
          for (int r = 0; r < selRows.length; ++r) {
            TopRow = Math.min(TopRow, selRows[r]);
            BottomRow = Math.max(BottomRow, selRows[r]);
          }
          UpButton.setEnabled(TopRow > 0);
          DownButton.setEnabled(BottomRow < (EditModel.getRowCount() - 1));
          DelButton.setEnabled(true);
        }
      }
    });
    tableHeight = Math.min(20, EditModel.getRowCount());
    tableHeight = Math.max(tableHeight, 5);
    UpButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) {
            int top = TopRow - 1;
            int bottom = BottomRow - 1;
            if (top <= bottom) {
                Table.editingCanceled(null);
                EditModel.moveUp(TopRow, BottomRow);
                Table.removeRowSelectionInterval(TopRow, BottomRow);
                Table.setRowSelectionInterval(top, bottom);
                UpButton.setEnabled(TopRow > 0);
            }
        }
    });
    DownButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) {
            int top = TopRow + 1;
            int bottom = BottomRow + 1;
            if (top <= bottom) {
                Table.editingCanceled(null);
                EditModel.moveDown(TopRow, BottomRow);
                Table.removeRowSelectionInterval(TopRow, BottomRow);
                Table.setRowSelectionInterval(top, bottom);
                DownButton.setEnabled(BottomRow < (EditModel.getRowCount() - 1));
            }
        }
    });
    AddButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        EditModel.insertRecord(BottomRow + 1);
        validate();
        repaint();
      }
    });
    DelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        Table.editingCanceled(null);
        EditModel.delRecord(TopRow, BottomRow, null);
        DelButton.setEnabled(false);
        UpButton.setEnabled(false);
        DownButton.setEnabled(false);
        validate();
        repaint();
      }
    });
    movers.setLayout(new BoxLayout(movers, BoxLayout.Y_AXIS));
    UpButton.setEnabled(false);
    DownButton.setEnabled(false);
    DelButton.setEnabled(false);
    AddButton.setEnabled(true);
    movers.add(UpButton);
    movers.add(DownButton);
    movers.add(AddButton);
    movers.add(DelButton);
    JScrollPane jsp = new JScrollPane(Table);
//    System.out.println("Calculated Width = " + tableWidth + " Width = " + tcm.getTotalColumnWidth());
    jsp.setPreferredSize(new Dimension(tableWidth + 10, tableHeight * 20));
    setLayout(new BorderLayout());
    add(jsp, BorderLayout.CENTER);
    add(movers, BorderLayout.EAST);
  }

  /**
   * returns the widths of each column in a Vector.
   *
   * @return an array of int's, where each element is the current width of
   * a column.  The array is arranged in the order of columns in the JTable.
   */
  private int[] getColWidths() {
    int[] widths = new int[Table.getColumnCount()];
    TableColumnModel tcm = Table.getColumnModel();
    for (int col = 0; col < widths.length; ++col) {
      widths[col] = tcm.getColumn(col).getWidth();
    }
    return widths;
  }

  /**
   * returns the identities of the columns, in the order they appear
   * on the JTable
   * @return an array of Strings that identify the columns
   */
  private String[] getColIds() {
      String[] ids = new String[Table.getColumnCount()];
      TableColumnModel tcm = Table.getColumnModel();
      for (int col = 0; col < ids.length; ++col) {
        ids[col] = (String) tcm.getColumn(col).getIdentifier();
      }
      return ids;      
  }
  
  /**
   * is the factory to construct an edit table.
   *
   * @param model is the TableModel used to direct the editing.
   * @return true if the user asked that the Store be updated.
   */
  public static boolean editRecords(StoreTableModel model) {
      boolean notDone = true;
      String errorMsg = null;
      StoreEditPane pane = new StoreEditPane(model);
      while (notDone) {
          if (AcceptDialog.select(pane, "Edit Records")) {
              if (((errorMsg = pane.EditModel.verifyResults()) == null)) {
                  model.reorderFormat(pane.getColIds());
                  model.saveWidths(pane.getColWidths());
                  return true;
              }
              else {
                  JOptionPane.showMessageDialog( (Component)null, errorMsg, "Data Error",
                          JOptionPane.ERROR_MESSAGE);
              }
          }
          else {
              return false;
          }
      }
      return false;
  }
}
/* @(#)StoreEditPane.java */