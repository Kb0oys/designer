/* Name: Tile.java
 *
 * What:
 *   This file contains a class for constructing the objects that occupy
 *   a grid square on the screen.  This super class allows only a String
 *   in the square.
 */
package designer.gui;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * contains the base class for the viewable grid squares. It also contains the
 * methods for increasing and decreasing the size of the grid squares.
 * <p>
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public class Tile extends JPanel {
	static protected Dimension Size = new Dimension(30, 30);

	/**
	 * constructs a dummy grid square, without a title.
	 *  
	 */
	public Tile() {
		setSize(Size);
	}

	/**
	 * constructs a dummy grid square.
	 * 
	 * @param s
	 *            is a label to put on the Tile.
	 */
	public Tile(String s) {
		setSize(Size);
		add(new JLabel(s));
	}

	/**
	 * returns the current size of a grid square in pixels.
	 * 
	 * @return the current size
	 */
	static public Dimension getGridSize() {
		return Size;
	}

	/**
	 * adjusts the size of a grid square in pixels.
	 * 
	 * @param width
	 *            is the new width.
	 * @param height
	 *            is the new height.
	 */
	static public void setGridSize(int width, int height) {
		Size = new Dimension(width, height);
	}

	/**
	 * retrieves the minimum size of the Tile.
	 * 
	 * @return Size.
	 */
	public Dimension getMinimumSize() {
		return Size;
	}

	/**
	 * retrieves the preferred size of the Tile.
	 * 
	 * @return Size.
	 */
	public Dimension getPreferredSize() {
		return Size;
	}
}
/* @(#)Tile.java */
