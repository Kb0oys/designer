/**
 * Name: NetworkAddressDialog
 * 
 * What:
 * This class creates a JPanel for entering information on the network connection from
 * CATS to a JMRI instance running Operations or CATS (TrainStat server).
 *   
 * Special Considerations:
 */
package designer.gui;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import designer.gui.jCustom.AcceptDialog;
import designer.layout.ServiceClient;

/**
 * This class creates a JPanel for entering information on the network connection from
 * CATS to a JMRI instance running Operations or CATS (TrainStat server).
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2011, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class NetworkAddressDialog extends JPanel{

    /**
     * constants for this class
     */
    /**
     * the text tag for the name of the computer running Operations.
     */
    private static final String IP_HOSTNAME = " Host Name:";
    
    /**
     * the text tag for the IP address of the computer running the server.
     */
    private static final String IP_ADDRESS = " IP Address:";

    /**
     * the text tag for the IP port address on the server computer.
     */
    private static final String REMOTE_PORT = " port:";

//    /**
//     * the text tag for the local IP port.
//     */
//    private static final String CATS_PORT = "Local Port:";

    /**
     * the text tag for the connection box
     */
    private static final String CONNECTION = "Connect on Start Up:";
    
    /**
     * is the JTextField for the host name of the computer running
     * CATS.
     */
    private JTextField HostName;
    
    /**
     * is the JTextField for IP address of the computer running CATS.
     */
    private JTextField IPAddress;
    
    /**
     * is the JTextField for the telnet port on the computer running CATS.
     */
    private JTextField RemotePort;
    
//    /**
//     * is the JTextField for the telnet port on this computer.
//     */
//    private JTextField LocalPort;
    
    /**
     * is the JCheckBox for the connection state
     */
    private JCheckBox ConnectBox;
    
    /**
     * is the constructor.
     * 
     * @param service is the String identifying the service connection being defined
     * @param addr contains the network information
     */
    private NetworkAddressDialog(final ServiceClient addr, final String service) {
        JPanel name = new JPanel(new FlowLayout());
        JPanel address = new JPanel(new FlowLayout());
        JPanel rPort = new JPanel(new FlowLayout());
//        JPanel lPort = new JPanel(new FlowLayout());
        String str;
        
        name.add(new JLabel(service + IP_HOSTNAME));
        str = addr.getHostName();
        if (str == null) {
            HostName = new JTextField(15);
        }
        else {
            HostName = new JTextField(str,
                    (str.length() < 15) ? 15 : str.length());
        }
        name.add(HostName);
        
        address.add(new JLabel(service + IP_ADDRESS));
        str = addr.getIPAddress();
        if (str == null) {
            IPAddress = new JTextField(15);
        }
        else {
            IPAddress = new JTextField(str, 15);
        }
        address.add(IPAddress);
        
        rPort.add(new JLabel(service + REMOTE_PORT));
        str = addr.getServicePort();
        if (str == null) {
            RemotePort = new JTextField(5);
        }
        else
        {
            RemotePort = new JTextField(str, 5);
        }
        rPort.add(RemotePort);
    
//        lPort.add(new JLabel(CATS_PORT));
//        str = addr.getCatsPort();
//        if (str == null) {
//            LocalPort = new JTextField(5);
//        }
//        else {
//            LocalPort = new JTextField(str, 5);
//        }
//        lPort.add(LocalPort);
// 
        ConnectBox = new JCheckBox(CONNECTION);
        ConnectBox.setSelected(addr.getConnected());
        
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(name);
        add(address);
        add(rPort);
//        add(lPort);
        add(ConnectBox);
    }
    
    /**
     * creates the JDialog, displays it, and tells the user what closing
     * button was pushed.
     *
     * @param addr contains the network address information.  It must not be null.
     * @param service is the service connection being configured
     *
     * @return true if the user pushed the Accept button or false if the
     * user pushed the Cancel button.
     */
    static public boolean select(ServiceClient addr, String service) {
        NetworkAddressDialog nad = new NetworkAddressDialog(addr, service);
        while (AcceptDialog.select(nad, service + " IP Address")) {
             if ((nad.IPAddress.getText() != null) && !nad.IPAddress.getText().trim().equals("") &&
                    (addr.toInetAddress(nad.IPAddress.getText()) == null)) {
                JOptionPane.showMessageDialog(nad, "Improperly formatted IP address",
                        "", JOptionPane.ERROR_MESSAGE);
            }
            else if (addr.isValidServicePort(nad.RemotePort.getText()) == ServiceClient.INVALID_PORT) {
                JOptionPane.showMessageDialog(nad, "Illegal IP port number on " + service + " computer",
                        "", JOptionPane.ERROR_MESSAGE);
            }
//            else if (addr.isValidCATSPort(nad.LocalPort.getText()) == ServiceClient.INVALID_PORT) {
//                JOptionPane.showMessageDialog(nad, "Illegal IP port number on CATS computer",
//                        "", JOptionPane.ERROR_MESSAGE);                
//            }
            else {
                addr.setAll(nad.HostName.getText(),
                        nad.IPAddress.getText(),
                        nad.RemotePort.getText(),
//                        nad.LocalPort.getText(),
                        nad.ConnectBox.isSelected());
                return true;
            }
        }
        return false;
    }    

}
/* @(#)NetworkAddressDialog.java */