package designer.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.UIManager;

import designer.gui.store.ClassSpec;
import designer.layout.ColorList;
import designer.layout.CrewStore;
import designer.layout.DebugFlag;
import designer.layout.DecoderInterlock;
import designer.layout.FontList;
import designer.layout.JmriNameList;
import designer.layout.LocalEnforcement;
import designer.layout.DirectionArrow;
import designer.layout.Compression;
import designer.layout.RowWrap;
import designer.layout.ServiceClient;
import designer.layout.FastClock;
import designer.layout.FlashRate;
import designer.layout.Hours;
import designer.layout.IncludeFile;
import designer.layout.InitRefresh;
import designer.layout.JmriName;
import designer.layout.LightMast;
import designer.layout.SignalTemplate;
import designer.layout.SpeedColor;
import designer.layout.items.IOSpecChainManager;
import designer.layout.JobStore;
import designer.layout.Layout;
import designer.layout.store.GenericRecord;
import designer.layout.TemplateStore;
import designer.layout.TrainLabel;
import designer.layout.TrainStore;
import designer.layout.TranspondingBit;
import designer.layout.items.Block;
import designer.layout.xml.SimpleSavable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

/**
 * <p>
 * Ctc.java is the Dispatcher Panel Startup
 * </p>
 * <p>
 * Description: The file which generates the dispatcher panel. It can be
 * considered to be a factory of factories because it generates the classes
 * which generate objects.
 * <p>
 * The Dispatcher panel follows the Swing paradigm of a View and a data Model.
 * This class ties them together at the highest level.
 * <p>
 * A really good design would create a class for the data Model (see the Layout
 * class) and a class for the View (it does not exist). So, to add a
 * configurable presentation class, do the following:
 * <ol>
 * <li>instantiate a factory class in the Ctc constructor() or initScreen()
 * <li>add a isSaved() test in isSaved() for the class group
 * <li>add a doSave() call in doSave()
 * <li>instantiate the XMLEleFactory in the DocumentFactory constructor
 * <li>add a check for the element in DocumentFactory
 * </ol>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2005, 2006, 2007, 2009, 2010, 2011, 2013, 2020,2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class Ctc {

	private static final int DEFAULT_COLS = 2; // number columns in default
	// Layout

	private static final int DEFAULT_ROWS = 2; // number rows in default Layout

	/**
	 * is the tag for the XML document.
	 */
	public static final String DocumentTag = "DOCUMENT";

	/**
	 * is the XML tag for identifying the Version of designer used to
	 * create the XML file.  It identifies the format.
	 */
	public static final String VersionTag = "VERSION";

	/**
	 * is the minimal Version number.
	 */
	public static String FileVersion = "0.07"; // The last version not recorded

	/**
	 * is the Document attribute name for the screen width
	 */
	private static final String WIDTH = "WIDTH";

	/**
	 * is the Document attribute name for the screen width
	 */
	private static final String HEIGHT = "HEIGHT";

	/**
	 * is the Document attribute name for the screen X coordinate
	 */
	private static final String X = "X";

	/**
	 * is the Document attribute name for the screen Y coordinate
	 */
	private static final String Y = "Y";

	/**
	 * is the list of objects that implement Savable.  They are checked
	 * when the layout is to be saved and called when it is saved.
	 */
	private ArrayList<SimpleSavable> SaveList;

	/**
	 * is the root of the object tree.
	 */
	public static Ctc RootCTC; // The thing that knows where other Objects are

	boolean packFrame = false;

	DispPanel MyPanel;

	/**
	 * the Layout under construction
	 */
	private static Layout TheLayout;

	//  FontFactory FFactory;

	LineFactory LFactory;

	/**
	 * is the constructor.
	 */
	public Ctc() {
		XMLReader.registerFactory(DocumentTag, new DocumentFactory());
		SaveList= new ArrayList<SimpleSavable>(15);

		Layout.initLayout();
	}

	/**
	 * Construct the screen. This cannot be included in the constructor because
	 * the Objects created need a Ctc Object.
	 */
	private void screenInit() {
		ClassSpec.init();
		MyPanel = new DispPanel();
		//    FFactory = new FontFactory(MyPanel);
		FontList.instance();
		new FontFactory();
		LFactory = new LineFactory(MyPanel);
		ColorList.instance();
		new PaletteFactory();
		MyPanel.addAppearanceItem(new FastClock());
		MyPanel.addAppearanceItem(new TrainLabel());
		MyPanel.addAppearanceItem(new LightMast());
		MyPanel.addAppearanceItem(new DirectionArrow());
		MyPanel.addAppearanceItem(new Compression());
		MyPanel.addAppearanceItem(new RowWrap());
		MyPanel.addAppearanceItem(new DecoderInterlock());
		MyPanel.addAppearanceItem(new LocalEnforcement());
		MyPanel.addAppearanceItem(new InitRefresh());
		MyPanel.addAppearanceItem(new DebugFlag());
		MyPanel.addAppearanceItem(new TranspondingBit());
		/** Disabled because the UNDEFINED Discipline will be used as end of track
    MyPanel.addAppearanceItem(new RecognizeBonding());
		 **/
		MyPanel.addAppearanceItem(new SpeedColor());
		new IncludeFile(MyPanel);
		new IOSpecChainManager();
		new FlashRate(MyPanel);
		new ScreenSize(MyPanel);
		new Hours();
		GenericRecord.init(GenericRecord.DATARECORD);
		GenericRecord.init(GenericRecord.EDITRECORD);
		SignalTemplate.init();
		JmriName.init();
//		OperationsClient.init();
		ServiceClient.init();

		//Validate frames that have preset sizes
		//Pack frames that have useful preferred size info, e.g. from their
		// layout
		if (packFrame) {
			MyPanel.pack();
		}
		else {
			MyPanel.validate();
		}
		//Center the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = MyPanel.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		MyPanel.setLocation( (screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height) / 2);
		MyPanel.setVisible(true);
		newLayout();
	}

	/**
	 * returns the FontFactory.
	 *
	 * @return the Factory used for constructing Fonts.
	 *
	 * @see designer.gui.FontFactory
	 */
	//  public FontFactory getFontFactory() {
	//    return FFactory;
	//  }

	/**
	 * returns the LineFactory.
	 *
	 * @return the Factory used for constructing Line Widths.
	 *
	 * @see designer.gui.LineFactory
	 */
	public LineFactory getLineFactory() {
		return LFactory;
	}

	/**
	 * gets the DispPanel object.
	 *
	 * @return the JFrame containing the dispatcher panel.
	 *
	 * @see designer.gui.DispPanel
	 */
	public DispPanel getDispPanel() {
		return MyPanel;
	}

	/**
	 * creates a new Layout, of the default size.
	 */
	public void newLayout() {
		TheLayout = new Layout(DEFAULT_COLS, DEFAULT_ROWS);
		TheLayout.showMe();
	}

	/**
	 * replaces the current Layout with one from a file.
	 *
	 * @param lo
	 *            is the replacement.
	 *
	 * @see designer.layout.Layout
	 */
	public void replaceLayout(Layout lo) {
		TheLayout = lo;
		TheLayout.showMe();
	}

	/**
	 * gets the Layout object.
	 *
	 * @return the object that describes the Layout.
	 *
	 * @see designer.layout.Layout
	 */
	public static Layout getLayout() {
		return TheLayout;
	}

	/**
	 * adds a Savable object to the list of objects that implement
	 * Savable.
	 * @param saveObject is an Object that implements Savable.
	 */
	public void registerSave(SimpleSavable saveObject) {
		SaveList.add(saveObject);
	}

	/**
	 * walks through the list of Savable objects looking for any that
	 * have been changed and need to be saved.
	 * @return true if all the objects have been saved and false if
	 * any one has not been saved.
	 */
	private boolean checkSaveList() {
		for (Iterator<SimpleSavable> iter = SaveList.iterator(); iter.hasNext(); ) {
			if (!iter.next().isSaved()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * saves the XML file using jdom.
	 * @param root is the Element at the root of the XML document.
	 * @return null if the file was written successfully or an error
	 * message if it was not.
	 */
	private String saveList(Element root) {
		String result;
		for (Iterator<SimpleSavable> iter = SaveList.iterator(); iter.hasNext(); ) {
			result = iter.next().putXML(root);
			if (result != null) {
				return result;
			}
		}
		return null;      
	}

	/**
	 * 
	 *
	 */
	private void renewSaveList() {
		for (Iterator<SimpleSavable> iter = SaveList.iterator(); iter.hasNext(); ) {
			iter.next().reNew();
		}      
	}

	/**
	 * asks if the state of the Object has been saved to a file
	 *
	 * @return true if it has been saved; otherwise return false if it should be
	 * written.
	 */
	public boolean isSaved() {
		return (TheLayout.isSaved() && LFactory.isSaved()
				&& TemplateStore.SignalKeeper.isSaved() &&
				TrainStore.TrainKeeper.isSaved() && JobStore.JobsKeeper.isSaved()
				&& ColorList.instance().isSaved()
				&& FontList.instance().isSaved()
				&& CrewStore.CrewKeeper.isSaved()
				&& CounterFactory.CountKeeper.isSaved()
				&& Hours.HourStore.isSaved()
				&& IOSpecChainManager.IOSpecChainKeeper.isSaved()
				&& FlashRate.TheFlashRate.isSaved()
				&& ScreenSize.TheScreenSize.isSaved()
				&& IncludeFile.TheIncludeFile.isSaved()
				&& checkSaveList());
	}

	/**
	 * writes the Object to an XML file.
	 *
	 * @param out is the file that the XML file is written to
	 * @return an error message if the file write fails.
	 */
	public String putXML(PrintStream out) {
		String errorReport = null;
		Element root = new Element(DocumentTag);
		root.setAttribute(VersionTag, DispPanel_AboutBox.getVersion());
		if (!ScreenSize.TheScreenSize.isSaved()) {
			root.setAttribute(WIDTH, String.valueOf(ScreenSize.TheScreenSize.getScreenWidth()));
			root.setAttribute(HEIGHT, String.valueOf(ScreenSize.TheScreenSize.getScreenHeight()));
			root.setAttribute(X, String.valueOf(ScreenSize.TheScreenSize.getScreenX()));
			root.setAttribute(Y, String.valueOf(ScreenSize.TheScreenSize.getScreenY()));
		}
		if ((IncludeFile.TheIncludeFile.getIncludeFile() != null) && 
				(IncludeFile.TheIncludeFile.getIncludeFile().length() != 0)) {
			root.setAttribute(IncludeFile.getTag(), IncludeFile.TheIncludeFile.getIncludeFile());
		}
		Document doc = new Document(root);
		XMLOutputter fmt = new XMLOutputter();
		fmt.setFormat(org.jdom2.output.Format.getPrettyFormat());
		if ( ( (errorReport = FontList.instance().putXML(root)) == null)
				&& ( (errorReport = LFactory.putXML(root)) == null)
				&& ( (errorReport = CounterFactory.CountKeeper.putXML(root)) == null)
				&& ( (errorReport = TemplateStore.SignalKeeper.putXML(root)) == null)
				&& ( (errorReport = TrainStore.TrainKeeper.putXML(root)) == null)
				&& ( (errorReport = JobStore.JobsKeeper.putXML(root)) == null)
				&& ( (errorReport = CrewStore.CrewKeeper.putXML(root)) == null)
				&& ( (errorReport = ColorList.instance().putXML(root)) == null)
				&& ( (errorReport = saveList(root)) == null)
				&& ( (errorReport = JmriNameList.instance().putXML(root)) == null)
				&& ( (errorReport = Hours.HourStore.putXML(root)) == null)
				&& ( (errorReport = FlashRate.TheFlashRate.putXML(root)) == null)
				&& ( (errorReport = IOSpecChainManager.IOSpecChainKeeper.putXML(root))== null)
				&& ( (errorReport = TheLayout.putXML(root)) == null)) {
			try {
				fmt.output(doc, out);
				Block.clearSaved();
			}
			catch (java.io.FileNotFoundException nfExcept) {
				errorReport = new String("FileNotFound error writing file " +
						nfExcept.getLocalizedMessage());
			}
			catch (java.io.IOException ioExcept) {
				errorReport = new String("IO exception writing file " +
						ioExcept.getLocalizedMessage());
			}
		}
		return errorReport;
	}

	/**
	 * This method is called to set the panel back to its initialized state.
	 * It is called from File->New and File->Open.
	 */
	public void reNew() {
		RootCTC.newLayout();  // resets columns, row, size, etc.
		IncludeFile.TheIncludeFile.setIncludeFile("");
		FontList.instance().init();
		LFactory.reNew();
		CounterFactory.CountKeeper.reNew();
		TemplateStore.SignalKeeper.reNew();
		TrainStore.TrainKeeper.reNew();
		JobStore.JobsKeeper.reNew();
		CrewStore.CrewKeeper.reNew();
		ColorList.instance().init();
		renewSaveList();
		JmriName.reset();
		Hours.HourStore.reNew();
		FlashRate.TheFlashRate.reNew();
		IOSpecChainManager.IOSpecChainKeeper.init();
	}

	/**
	 * This is where everything starts.
	 *
	 * @param args
	 *            are optional command line arguments. Currently, this parameter
	 *            is ignored.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		initLog4J();

		RootCTC = new Ctc();
		RootCTC.screenInit();
	}

	static protected void initLog4J() {
		// initialize log4j - from logging control file (lcf) only
		// if we can find it!
		String logFile = "default.lcf";
		try {
			if (new java.io.File(logFile).canRead()) {
				org.apache.log4j.PropertyConfigurator.configure(logFile);
			}
			else {
				org.apache.log4j.BasicConfigurator.configure();
				org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.ERROR);
			}
		}
		catch (java.lang.NoSuchMethodError e) {
			System.out.println("Exception starting logging: " + e);
		}
	}

	class DocumentFactory
	implements XMLEleFactory, XMLEleObject {

		/*
		 * tells the factory that an XMLEleObject is to be created. Thus, its
		 * contents can be set from the information in an XML Element
		 * description.
		 */
		public void newElement() {
		}

		/*
		 * gives the factory an initialization value for the created
		 * XMLEleObject.
		 *
		 * @param tag is the name of the attribute. @param value is it value.
		 *
		 * @return null if the tag:value are accepted; otherwise, an error
		 * string.
		 */
		public String addAttribute(String tag, String value) {
			if (VersionTag.equals(tag)) {
				FileVersion = new String(value);
			}
			else if (WIDTH.equals(tag)) {
				int w;
				try {
					w = Integer.parseInt(value);
					ScreenSize.TheScreenSize.setScreenWidth(w);
				}
				catch (NumberFormatException nfe) {
					return new String(value + " is not a legal screen width.");
				}
			}
			else if (HEIGHT.equals(tag)) {
				int h;
				try {
					h = Integer.parseInt(value);
					ScreenSize.TheScreenSize.setScreenHeight(h);
				}
				catch (NumberFormatException nfe) {
					return new String(value + " is not a legal screen height.");
				}
			}
			else if (X.equals(tag)) {
				int x;
				try {
					x = Integer.parseInt(value);
					ScreenSize.TheScreenSize.setScreenX(x);
				}
				catch (NumberFormatException nfe) {
					return new String(value + " is not a legal screen X coordinate.");
				}
			}
			else if (Y.equals(tag)) {
				int y;
				try {
					y = Integer.parseInt(value);
					ScreenSize.TheScreenSize.setScreenY(y);
				}
				catch (NumberFormatException nfe) {
					return new String(value + " is not a legal screen Y coordinate.");
				}
			}
			else if (IncludeFile.getTag().equals(tag)) {
				IncludeFile.TheIncludeFile.setIncludeFile(value);
			}
			else {
				return new String(tag + " is not an attribute for a " + DocumentTag);
			}
			return null;
		}

		/*
		 * tells the factory that the attributes have been seen; therefore,
		 * return the XMLEleObject created.
		 *
		 * @return the newly created XMLEleObject or null (if there was a
		 * problem in creating it).
		 */
		public XMLEleObject getObject() {
			return this;
		}

		/*
		 * is the method through which the object receives the text field.
		 *
		 * @param eleValue is the Text for the Element's value.
		 *
		 * @return if the value is acceptable, then null; otherwise, an error
		 * string.
		 */
		public String setValue(String eleValue) {
			return new String(DocumentTag + " cannot have a text field ("
					+ eleValue + ").");
		}

		/*
		 * is the method through which the object receives embedded Objects.
		 *
		 * @param objName is the name of the embedded object @param objValue is
		 * the value of the embedded object
		 *
		 * @return null if the Object is acceptible or an error String if it is
		 * not.
		 */
		public String setObject(String objName, Object objValue) {
			//      if (objName.equals(Layout.LayoutTag)) {
			//        TheLayout = (Layout) objValue;
			//      }
			return null;
		}

		/**
		 * returns the XML Element tag for the XMLEleObject.
		 *
		 * @return the name by which XMLReader knows the XMLEleObject (the
		 *         Element tag).
		 */
		public String getTag() {
			return new String(DocumentTag);
		}

		/**
		 * tells the XMLEleObject that no more setValue or setObject calls will
		 * be made; thus, it can do any error checking that it needs.
		 *
		 * @return null, if it has received everything it needs or an error
		 *         string if something isn't correct.
		 */
		public String doneXML() {
			if (!Ctc.FileVersion.equals(DispPanel_AboutBox.getVersion())) {
				System.out.println("Warning: editing XML file created with version " +
						Ctc.FileVersion);
			}
			return null;
		}
	}

	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Ctc.class.getName());
}
/* @(#)Ctc.java */
