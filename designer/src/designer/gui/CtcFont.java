/* Name: CtcFont.java
 *
 * What: This file contains the CtcFont class - a wrapper around the Java
 *   Font class, providing a level of indirection, so that all uses
 *   of a particular Font will change when the referenced Font changes.
 *   It includes the Font family and Font size.
 *
 * Special Considerations:
 */
package designer.gui;

import java.awt.Font;
import designer.layout.FontDefinition;
import designer.layout.FontList;
import designer.layout.items.LoadFilter;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;

/**
 * defines a class for associating the fonts of objects on the dispatcher panel
 * with fonts. These are not constants, but variables. The reason is so that the
 * user can modify them to suit the characteristics of the display.
 *
 * It also contains a couple of class-independent, static methods for working
 * with lists of strings.
 *
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2010, 2011
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */
public class CtcFont
//    implements java.awt.event.ActionListener, XMLEleFactory,
//    XMLEleObject, Savable {
implements XMLEleFactory, XMLEleObject {
  /**
   * Is a constant indicating that an item was not found in a liat.
   */
  public static final int NOT_FOUND = -1;

  private static final String STYLE = "FONT_STYLE";

  private static final String SIZE = "FONT_SIZE";

  private static final String[] FONT_STYLE = {
      "BOLD", "ITALIC", "BOLD_ITALIC", "PLAIN"};

  private static final int[] STYLE_INDEX = {
      Font.BOLD, Font.ITALIC, Font.ITALIC + Font.BOLD,
      Font.PLAIN};

  private static String FONT_SIZE[];

  private static final int[] SIZE_INDEX = {
      8, 9, 10, 11, 12, 13, 14, 15, 16,
      17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};

  //The current font
  private Font TheFamily;

  // The current index into the arrays of Font name
  // private int TheName;

  // The current size of the font
  private String TheSize;

  // The current style of the font
  private String TheStyle;

  //The default Font
//  private Font DefFamily;

  // The default index into the arrays of Font name
  // private int DefName;

  // The default index into the arrays of Font size
//  private int DefSize;

  // The default index into the arrays of Font style
//  private int DefStyle;

  // What to place on the JColorChooser title bar
//  private String Title;

  // The tag for this Object in an XML Element
  private String XMLTag;

  // The list of available fonts
  //private final String[] FONT_NAME = getFontNames();

  // The name of the default Font.
  private String FontName;

  // The JDialog box for accepting new font specifications
//  private JDialog Dialog;

  // The "Accept" Button on the JDialog
//  private JButton Accept = new JButton("Accept");
//
//  // The "Cancel" Button on the Dialog
//  private JButton Cancel = new JButton("Cancel");

  // The JList of styles
//  private JList Style = new JList(FONT_STYLE);

  // The JList of Font names
  //private JList Names;

  // The ScrollPane for the Names JList
  //private JScrollPane NamePane;

  // The JList of supported font sizes
//  private JList Size;

  //The ScrollPane for the Size JList
//  private JScrollPane SizePane;

  // Group a label with the Font family name
  //private JPanel NameGroup = new JPanel();

  // Group a label with the Style
//  private JPanel StyleGroup = new JPanel();

  // Group a label with the Size
//  private JPanel SizeGroup = new JPanel();

  // Group the Buttons
//  private JPanel ButtonGroup = new JPanel();

  // Result button from JDialog box
//  private boolean Result;

  // a true flag meaning that all the attributes and the text field were
  // received.
  private boolean Complete;

  /**
   * construct an object that can be used for a displayable object.
   *
   * @param xmlTag
   *            is the XML tag for this Object.
   */
  public CtcFont(String xmlTag) {
    FONT_SIZE = new String[SIZE_INDEX.length];
    for (int index = 0; index < SIZE_INDEX.length; ++index) {
      FONT_SIZE[index] = new String(String.valueOf(SIZE_INDEX[index]));
    }
//    Size = new JList(FONT_SIZE);
    //Names = new JList(FONT_NAME);
    TheFamily = Ctc.RootCTC.getDispPanel().getContentPane().getFont();
//    DefFamily = TheFamily;
    //    initName(TheFamily.getFamily());
    FontName = TheFamily.getFamily();
    initStyle(TheFamily.getStyle());
    initSize(TheFamily.getSize());
//    Title = title;
    XMLTag = xmlTag;
//    Size.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//    Size.setVisibleRowCount(10);
//    SizePane = new JScrollPane(Size);
    //Names.setVisibleRowCount(10);
    //Names.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//    Style.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    //NamePane = new JScrollPane(Names);
//    Accept.addActionListener(new ActionListener() {
//      public void actionPerformed(ActionEvent ae) {
//        Result = true;
//        Dialog.dispose();
//      }
//    });
//    Cancel.addActionListener(new ActionListener() {
//      public void actionPerformed(ActionEvent ae) {
//        Result = false;
//        Dialog.dispose();
//      }
//    });

    //NameGroup.add(new JLabel("Font Family:"));
    //NameGroup.add(NamePane);
//    StyleGroup.add(new JLabel("Font Style:"));
//    StyleGroup.add(Style);
//    SizeGroup.add(new JLabel("Font Point Size: "));
//    SizeGroup.add(SizePane);
//    ButtonGroup.add(Accept);
//    ButtonGroup.add(Cancel);
  }

  /**
   * retrieve the Font.
   *
   * @return The current value of the Font.
   */
//  public Font grabFont() {
//    return TheFamily;
//  }

  /**
   * initializes the Font style. If the style is not an accepted value, then
   * it is ignored. So, this method converts the internal number to an index
   * into the style arrays.
   *
   * @param style
   *            is the numeric value of the initial style.
   */
  private void initStyle(int style) {
    int where = findInt(style, STYLE_INDEX);
    if (where == NOT_FOUND) {
//      Style.setSelectedIndex(where);
      TheStyle = "PLAIN";
//      DefStyle = where;
    }
    else {
        TheStyle = FONT_STYLE[where];
    }
  }

  /**
   * initializes the Font size. If the size is not within the accepted values,
   * it replaces the smallest of the accepted values.
   *
   * @param size
   *            is the numeric value of the initial size.
   */
  private void initSize(int size) {
    int where = findInt(size, SIZE_INDEX);
    if (where != NOT_FOUND) {
//      Size.setSelectedIndex(where);
      TheSize = String.valueOf(size);
//      DefSize = where;
    }
    else {
      // The size was not found, so replace the smallest with it
//      SIZE_INDEX[0] = size;
      FONT_SIZE[0] = String.valueOf(size);
    }
  }

  /**
   * initializes the Font name.
   *
   * @param name
   *            is the Name of the initial name.
   */
  //  private void initName(String name) {
  //    int index = findString(name, FONT_NAME);
  //    if (index != NOT_FOUND) {
  //      Names.setSelectedIndex(index);
  //      TheName = index;
  //      DefName = index;
  //    }
  //    else {
  //      System.out.println(name + " is not in the list of fonts");
  //    }
  //  }
  /**
   * locates a particular Font. If the environment supports it, then change
   * the current properties of the Font. This method assumes that the
   * parameters have been validated before being called.
   *
   * @param family
   *            is an index into the List of Font names
   * @param style
   *            is the Font style
   * @param size
   *            is the Font size
   *
   * @return null if the Font is found; otherwise, return an error String
   */
  private String locateFont(int family, String style, String size) {
	  //    Font newFont = new Font(FONT_NAME[family], STYLE_INDEX[style],
	  //                            SIZE_INDEX[size]);
	  Font newFont = new Font(FontName, STYLE_INDEX[findString(style, FONT_STYLE)], Integer.parseInt(size));
	  TheFamily = newFont;
	  //      TheName = family;
	  //      Names.setSelectedIndex(family);
	  TheStyle = style;
	  //      Style.setSelectedIndex(style);
	  TheSize = String.valueOf(size);
	  //      Size.setSelectedIndex(size);
	  return null;
  }

  /*
   * tells the factory that an XMLEleObject is to be created. Thus, its
   * contents can be set from the information in an XML Element description.
   */
  public void newElement() {
    Complete = false;
  }

  /**
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag
   *            is the name of the attribute.
   * @param attrValue
   *            is its value.
   *
   * @return null if the tag:value are accepted; otherwise, an error string.
   */
  public String addAttribute(String tag, String attrValue) {
    String resultMsg = null;
    if (tag.equals(STYLE)) {
      if ( (findString(attrValue, FONT_STYLE)) == NOT_FOUND) {
        resultMsg = new String("Unrecognized Font Style: " + attrValue);
      }
      else {
        TheStyle = attrValue;
      }
    }
    else if (tag.equals(SIZE)) {
      if ( (findString(attrValue, FONT_SIZE)) == NOT_FOUND) {
        resultMsg = new String("Unrecognized Font Size: " + attrValue);
      }
      else {
        TheSize = attrValue;
      }
    }
    else {
      resultMsg = new String("Unrecognized Font attribute: " + tag);
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore, return
   * the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem in
   * creating it).
   */
  public XMLEleObject getObject() {
    return this;
  }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    //    int index = findString(eleValue, FONT_NAME);
    //    if (index == NOT_FOUND) {
    //      return new String(eleValue + " is an unrecognized Font.");
    //    }
    //    Complete = true;
    //    return locateFont(index, TheStyle, TheSize);
    Complete = true;
    return locateFont(0, TheStyle, TheSize);
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object @param objValue is the
   * value of the embedded object
   *
   * @return null if the Object is acceptible or an error String if it is not.
   */
  public String setObject(String objName, Object objValue) {
    return new String("XML objects are not accepted by " + XMLTag
                      + " elements.");
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the Element
   * tag).
   */
  public String getTag() {
    return XMLTag;
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will be
   * made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error string
   * if something isn't correct.
   */
  public String doneXML() {
      FontDefinition fDef;
      if (Complete) {
          if (LoadFilter.instance().isEnabled(LoadFilter.XML_FONTDEFINITION)) {
              fDef = FontList.instance().findElementbyKey(XMLTag);
              if (fDef != null) {
                  fDef.setFontSize(TheSize);
                  fDef.setFontStyle(TheStyle);
              }
          }
          return null;
      }
      return new String(XMLTag + " is incomplete.");
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param outStream
   *            is where to write the contents. This method assumes that
   *            outStream exists and is open for writing.
   * @param indent is
   *            the indentation string at the time of the call.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
//  public String putXML(PrintStream outStream, String indent) {
//
//    if (!isSaved()) {
//      String myIndent = indent + XMLReader.INDENT;
//      outStream.println(myIndent + "<" + XMLTag + " " + STYLE + "=\""
//                        + FONT_STYLE[findInt(TheFamily.getStyle(), STYLE_INDEX)]
//                        + "\" " + SIZE + "=\""
//                        + FONT_SIZE[findInt(TheFamily.getSize(), SIZE_INDEX)]
//                        + "\">");
//      outStream.println(myIndent + XMLReader.INDENT
//                        + TheFamily.getFamily());
//      outStream.println(myIndent + "</" + XMLTag + ">");
//    }
//    return null;
//  }
//
  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
//  public String putXML(Element parent) {
//    if (!isSaved()) {
//      Element thisObject = new Element(XMLTag);
//      thisObject.setAttribute(STYLE,
//                              FONT_STYLE[findInt(TheFamily.getStyle(), STYLE_INDEX)]);
//      thisObject.setAttribute(SIZE,
//                              FONT_SIZE[findInt(TheFamily.getSize(), SIZE_INDEX)]);
//      thisObject.addContent(TheFamily.getFamily());
//      parent.addContent(thisObject);
//    }
//    return null;
//  }
//
  /*
   * tells the caller if the state has been saved or not.
   *
   * @return true if the state has not been changed since the last save
   */
//  public boolean isSaved() {
//    return (TheSize == DefSize) && (TheStyle == DefStyle)
//    && (TheFamily == DefFamily);
//  }

  /**
   * presents a ColorMenu and adjusts its Color by what is selected.
   */
//  public void actionPerformed(ActionEvent e) {
//    // Pop up a list of fonts, styles, and sizes to select from
//    int name = 0;
//    int style;
//    int size;
//    String errReport;
//    Dialog = new JDialog( (Frame)null, Title, true);
//    Container dialogContentPane = Dialog.getContentPane();
//    dialogContentPane.setLayout(new FlowLayout());
//    //    dialogContentPane.add(NameGroup);
//    dialogContentPane.add(StyleGroup);
//    dialogContentPane.add(SizeGroup);
//    dialogContentPane.add(ButtonGroup);
//    Dialog.pack();
//    Result = false;
//    Dialog.setVisible(true);
//    if (Result) {
//      style = Style.getSelectedIndex();
//      //      name = Names.getSelectedIndex();
//      size = Size.getSelectedIndex();
//      if ( (style >= 0) && (name >= 0) && (size >= 0)) {
//        errReport = locateFont(name, style, size);
//        if (errReport != null) {
//          JOptionPane.showMessageDialog( (Component)null, errReport,
//                                        "Font Error", JOptionPane.ERROR_MESSAGE);
//
//        }
//      }
//    }
//    else {
//      //      Names.setSelectedIndex(TheName);
//      Style.setSelectedIndex(TheStyle);
//      Size.setSelectedIndex(TheSize);
//    }
//  }
//
  /**
   * retrieves a list of Font names from the environment.
   * This method has been disabled because Windows SE doesn't support
   * looking for Font names.
   *
   * @return the list of Font names
   */
//  private static String[] getFontNames() {
//    GraphicsEnvironment localEnv = GraphicsEnvironment
//        .getLocalGraphicsEnvironment();
//    return (localEnv.getAvailableFontFamilyNames());
//  }

  /**
   * searches an array of Strings for a particular String.
   *
   * @param query
   *            is the String being looked for.
   * @param array
   *            is the array being searched.
   *
   * @return the index of the String in the array, if found, or NOT_FOUND.
   */
  public static int findString(String query, String[] array) {
    for (int index = 0; index < array.length; ++index) {
      if (query.equals(array[index])) {
        return index;
      }
    }
    return NOT_FOUND;
  }

  /**
   * searches an array of ints for a particular int.
   *
   * @param query
   *            is the int being looked for.
   * @param array
   *            is the array being searched.
   *
   * @return the index of the int in the array, if found, or NOT_FOUND.
   */
  public static int findInt(int query, int[] array) {
    for (int index = 0; index < array.length; ++index) {
      if (query == array[index]) {
        return index;
      }
    }
    return NOT_FOUND;
  }
}/* @(#)CtcFont.java */
