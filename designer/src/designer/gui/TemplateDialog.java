/* Name: TemplateDialog
 *
 * What:
 *  The root class for creating new SignalTemplates.
 */
package designer.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import designer.gui.jCustom.AcceptDialog;
import designer.layout.AspectMap;
import designer.layout.SignalTemplate;

/**
 * is the root class for generating new Signal Templates. Through this class the
 * user designates the new Signal as a Semaphore or Light. Other JDialogs allow
 * further refinement, but the eventual goal is a table mapping the Signal
 * indication (meaning) to its aspect (presentation). This class creates
 * templates, so that the user does not have to enter the information for each
 * Signal on the layout.
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2020
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */

public class TemplateDialog extends JPanel {
    
    /**
     * the name of the Signal template for identifying it.
     */
    JTextField TempName;
    
    /**
     * the "Lamp" button.
     */
    private JRadioButton Lamp = new JRadioButton("Lamp");
    
    /**
     * the value of the Lamp/Semaphore selection.
     */
    private boolean IsLamp;
    
    /**
     * the "Semaphore" button.
     */
    JRadioButton Semaphore = new JRadioButton("Semaphore");
    
    /**
     * the selection list of number of heads.
     */
    private String[] HeadTags = { "None", "1", "2", "3" };
    
    /**
     * the tags, as a list.
     */
    private JList HeadList = new JList(HeadTags);
    
    /**
     * the aspect names.  These are both labels on the aspects
     * and an alias list for use in creating SignalMasts
     */
    private String Names[];
    
    /**
     * the AspectMap created by the secondary Dialog.
     */
    private AspectMap NewMap;
    
    /**
     * the constructor.
     * 
     * @param template
     *            is a SignalTemplate being edited or null for one to be
     *            created.
     * 
     * @see designer.layout.SignalTemplate
     */
    public TemplateDialog(SignalTemplate template) {
        JPanel namePanel = new JPanel();
        JPanel heads = new JPanel();
        JPanel options = new JPanel();
        ButtonGroup sigType = new ButtonGroup();
        JButton mapButton = new JButton("Create Aspects");
        JPanel bottom = new JPanel(new GridLayout(0, 1));
        
        if (template == null) {
            TempName = new JTextField(8);
            HeadList.setSelectedIndex(0);
            Names = new String[AspectMap.IndicationNames.length];
        	for (int aspect = 0; aspect < Names.length; aspect++) {
        		Names[aspect] = new String(AspectMap.IndicationNames[aspect][AspectMap.LABEL]);
        	}
        } else {
            if (template.getName() == null) {
                TempName = new JTextField(8);
            }
            else {
                TempName = new JTextField(template.getName());
                if (template.getName().length() < 8) {
                    TempName.setColumns(8);
                }
            }
            HeadList.setSelectedIndex(template.getNumHeads());
            IsLamp = template.isLights();
            Lamp.setSelected(IsLamp);
            Semaphore.setSelected(!IsLamp);
            NewMap = template.getAspectMap();
            Names = template.copyAspectNames();
        }
        namePanel.add(new JLabel("Template Name:"));
        namePanel.add(TempName);
        heads.add(new JLabel("Number of signal heads:"));
        heads.add(HeadList);
        
        Lamp.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent ie) {
                if (ie.getStateChange() == ItemEvent.DESELECTED) {
                    IsLamp = false;
                } else {
                    IsLamp = true;
                }
            }
        });
        Semaphore.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent ie) {
                if (ie.getStateChange() == ItemEvent.DESELECTED) {
                    IsLamp = true;
                } else {
                    IsLamp = false;
                }
            }
        });
        sigType.add(Lamp);
        sigType.add(Semaphore);
        options.add(Lamp);
        options.add(Semaphore);
        bottom.add(options);
        mapButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (HeadList.getSelectedIndex() > 0) {
                    NewMap = AspectDialog.createAspectMap(NewMap, HeadList
                            .getSelectedIndex(), IsLamp, Names);
                }
            }
        });
        bottom.add(mapButton);
        setLayout(new GridLayout(0,1));
        add(namePanel);
        add(heads);
        add(bottom);
    }
    
    /**
     * creates a JDialog for entering information about a SignalTemplate.
     * 
     * @param template
     *            is the initial value of the SignalTemplate.
     * 
     * @return a new SignalTemplate or null (if no changes were made).
     * 
     * @see designer.layout.SignalTemplate
     */
    static public SignalTemplate newTemplate(SignalTemplate template) {
        TemplateDialog dialog = new TemplateDialog(template);
        SignalTemplate st;
        if (AcceptDialog.select(dialog, "Edit SignalTemplate")) {
            if (dialog.TempName.getText().trim().length() != 0) {
            	// it would be a good idea to ensure that all names are not empty
            	// and that duplicate names have the same aspects.
            	// Check for changes
                st = new SignalTemplate(dialog.HeadList.getSelectedIndex(),
                        dialog.IsLamp, dialog.TempName.getText().trim(), dialog.Names);
                st.setAspectMap(dialog.NewMap);
                return st;
            }
        }
        return null;
    }
}
/* @(#)TemplateDialog.java */