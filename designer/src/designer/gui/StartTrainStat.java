/**
 * Name: StartTrainStat
 * 
 * What:
 *   This file contains a boolean for controlling if the networking
 *   for a TrainStat server should be launched when CATS starts up or
 *   not.  "true" means CATS should start the network server and
 *   false means it should not.
 *   
 * Special Considerations:
 *   The checkbox is in the "Network" menu pulldown, unlike most of the
 *   other booleans.
 */
package designer.gui;

/**
 *   This file contains a boolean for controlling if the networking
 *   for a TrainStat server should be launched when CATS starts up or
 *   not.  "true" means CATS should start the network server and
 *   false means it should not.
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

public class StartTrainStat extends BooleanGui {
    /**
     * is the tag for identifying a StartTrainStat Object in the XMl file.
     */
    static final String XMLTag = "TRAINSTATLABEL";
    
    /**
     * is the label on the JCheckBoxMenuItem
     */
    static final String Label = "Start TrainStat Server";
    

    /**
     * constructs the factory.
     */
    public StartTrainStat() {
      super(Label, XMLTag, false);
    }

}
/* @(#)StartTrainStat.java */