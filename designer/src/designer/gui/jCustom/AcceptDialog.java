/* Name: AcceptDialog.java
 *
 * What:
 *  This class is an extension to JDialog which wraps a JPanel with
 *  an Accept Button and a Cancel Button.  The caller (or sub-class)
 *  supplies the JPanel.  Thus, this class forms a uniform look and feel
 *  for Dialogs.
 */
package designer.gui.jCustom;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Container;
import java.awt.Frame;
import java.awt.BorderLayout;

/**
 * is an extension to JDialog, wrapping a JPanel with an Accept JButton
 * and a Cancel JButton.  The class supports a static routine (factory)
 * for constructing the JDIalog, displaying the JDialog (in modal model),
 * and returning which JButton was pushed to exit the JDialog.  Thus,
 * this class provides a uniform look and feel for Dialogs.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class AcceptDialog {

  /**
   *   Result button from JDialog box
   */
  private boolean Result;

  /**
   *   The button to push to change the line width
   */
  private JButton Accept = new JButton("Accept");

  /**
   *   The button to Cancel the change
   */
  private JButton Cancel = new JButton("Cancel");

  /**
   *   The Buttons, as a group
   */
  private JPanel Buttons = new JPanel();

  /**
   *   The JDialog used for setting the name values.  It is outside
   *   of the constructor so that the anonymous classes can find it.
   */
  private JDialog Dialog;

  /**
   * is the constructor.  Since the caller provides the JPanel, the
   * caller has control over the contents of the JDialog.
   *
   * @param contents is the JPanel forming the body of the JDialog
   * @param title is a title for the JDialog.
   */
  public AcceptDialog(JPanel contents, String title) {
    Dialog = new JDialog( (Frame)null, title, true);
    Container dialogContentPane = Dialog.getContentPane();
    Buttons.add(Accept);
    Buttons.add(Cancel);
    Accept.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        Result = true;
        Dialog.dispose();
      }
    });
    Cancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        Result = false;
        Dialog.dispose();
      }
    });
    dialogContentPane.setLayout(new BorderLayout());
//    dialogContentPane.add("North", contents);
    dialogContentPane.add("Center", contents);
    dialogContentPane.add("South", Buttons);
    Dialog.pack();
    Dialog.setVisible(true);
  }

  /**
   * creates the JDialog, displays it, and tells the user what closing
   * button was pushed.
   *
   * @param contents is the JPanel forming the body of the JDialog
   * @param title is the title of the JDialog.
   *
   * @return true if the user pushed the Accept button or false if the
   * user pushed the Cancel button.
   */
  static public boolean select(JPanel contents, String title) {
    AcceptDialog dialog = new AcceptDialog(contents, title);
    return dialog.Result;
  }

}
/* @(#)AcceptDialog.java */
