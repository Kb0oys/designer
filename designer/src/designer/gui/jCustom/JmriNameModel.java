/**
 * Name: JmriNameModel.java
 *
 * What:
 *  This file contains the class definition for a JmriNameModel object.  A
 *  JmriNameModel controls the editing within a StoreEditPane of an
 *  the definitions of the JMRI names.
 */
package designer.gui.jCustom;

import java.util.Vector;

import javax.swing.JLabel;

import designer.layout.JmriName;

/**
 *  This file contains the class definition for a JmriNameModel object.  A
 *  JmriNameModel controls the editing within a StoreEditPane of
 *  the definitions of the JMRI names.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2013, 2020, 2023</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class JmriNameModel extends AbstractManagerTableModel {

    /**
     * are the column headings
     */
    static final private String Headings[] = {
        "Prefix",
        "Commment",
        "Type",
        "Select"
    };
    
    /**
     * are the column descriptions
     */
    static final private ColumnData ColInfo[] = {
        new ColumnData(Headings[0], 50, JLabel.RIGHT, String.class),
        new ColumnData(Headings[1], 300, JLabel.RIGHT, String.class),
        new ColumnData(Headings[2], 75, JLabel.RIGHT, String.class),
        new ColumnData(Headings[3], 75, JLabel.RIGHT, Boolean.class)
    };

    /**
     * is the constructor.
     *
     * @param records are the list of names being edited.  Because this object
     * manipulates the Vector, the caller should be certain to pass a copy
     * to this object.
     */
    public JmriNameModel(Vector<Object> records) {
        super(records);
        ColumnInfo = ColInfo;
    }
    
    /**
     * tells the JTable what value to show in a column.
     * 
     * @param row is the row (JmriName)
     * @param col is the field in the JmriName
     * 
     * @return a value from a JmriName
     */
    public Object getValueAt(int row, int col) {
        Object ret = null;
        
        switch (col) {
        case 0: // JMRI Prefix
            ret = (((JmriName) Contents.elementAt(row)).getJmriID());
            if (ret == null) {
                ret = new String("");
            }
            break;
            
        case 1: // class of the JMRI manager for this Prefix
            ret = (((JmriName)Contents.elementAt(row)).getJmriClass());
            if (ret == null) {
                ret = new String("");
            }
            break;
            
        case 2: // JMRI device type
            ret = (((JmriName) Contents.elementAt(row)).getJmriType());
            if (ret == null) {
                ret = new String("");
            }            
            break;
            
        case 3: // Selection button
            ret = new Boolean(((JmriName) Contents.elementAt(row)).getSelected());
            break;

        default:
        }
        return ret;
    }
    
    /**
     * changes a field in a JmriName
     * 
     * @param val is the new value
     * @param row is the row from the JTable (JmriName)
     * @param col is the JTable column (field)
     */
    public void setValueAt(Object val, int row, int col) {
        switch (col) {
        case 0: // JMRI Prefix
            if (((String)val).length() == 0) {
                val = null;
            }
            else {
                ((String) val).trim();
            }
            ((JmriName) Contents.elementAt(row)).setJmriID(
                    (String) val);
            break;
            
        case 1: // Class of manager for this Prefix
            if (((String)val).length() == 0) {
                val = null;
            }
            else {
                ((String)val).trim();
            }
            ((JmriName) Contents.elementAt(row)).setJmriClass(
                    (String) val);
            break;
 
        case 2: // read only - set by the last character in the Prefix
//            if (((String)val).length() == 0) {
//                val = null;
//            }
//            ((JmriName) Contents.elementAt(row)).setJmriType(
//                    (String) val);
            break;

        case 3: // Selection button
            ((JmriName) Contents.elementAt(row)).setSelected(
                    (Boolean) val);           
            break;
            
        default:
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
    	// the type field is not editable
        return col != 2;
    }
    
    /**
     * Looks at the entries and checks that all selected
     * names are unique.
     * 
     * @return null if things look fine or an error string
     * if duplicates are found.
     */
    public String verifyResults() {
        JmriName lower;
        JmriName upper;
        String result = null;
        for (int index = 0; index < Contents.size(); ++index) {
            lower = (JmriName) Contents.elementAt(index);
            if ((lower.getJmriID() == null) || 
                    (lower.getJmriID().length() == 0)) {
                result = new String("No name can be blank.");
                
            }
        }
        for (int start = 0; (start < Contents.size()) && (result == null); ++start) {
            lower = (JmriName) Contents.elementAt(start);
            if (lower.getSelected()) {
                for (int i = start + 1; i < Contents.size(); ++i) {
                    upper = (JmriName) Contents.elementAt(i);
                    if (upper.getSelected() && lower.getJmriID().equals(upper.getJmriID())) {
                        result = new String("JMRI names must be unique.");
                    }
                }
            }
        }
        return result;
    }

    public Object createElement() {
        return new JmriName("", "");
    }
}
/* @(#)JmriNameModel.java */
