/**
 * What: PrefixSelector.java
 *
 * What:
 *  This is a concrete class for generating a list of JMRI
 *  prefixes.
 */
package designer.gui.jCustom;

import java.util.Vector;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import designer.gui.store.ListSpec;
import designer.layout.JmriName;

/**
 *  This is a concrete class for generating a list of JMRI
 *  prefixes.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class PrefixSelector extends ListSpec {

    /**
     * is the sub-class specific method for building the Vector of labels
     * in the Selection dialog.
     *
     * @return the Vector of Strings that forms the contents of the Selection
     * Dialog.
     */
    protected Vector<String> createSpecificList() {
        return ListSpec.arrayToVector(JmriName.getPrefixes());
    }

    /**
     * creates a TableCellRenderer for a list of JMRI prefixes
     * @return the TableCellRenderer
     */
    static public TableCellRenderer getRenderer() {
        return new PrefixSelector().getCellRenderer();
    }

    /**
     * creates a TableCellEditor for a list of JMRI prefixes
     * 
     * @return the TableCellEditor
     */
    static public TableCellEditor getEditor() {
        return new PrefixSelector().getCellEditor();
    }
}
/* @(#)PrefixSelector.java */