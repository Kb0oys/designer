/**
 * Name: JmriDeviceSelector.java
 *
 * What:
 *  This file contains the class definition for a JmriDeviceSelector - 
 *  derived from the JComboBox class for picking a JMRI device type used
 *  by CATS.  Rather than embed the operations piecemeal in another class,
 *  it extends JComboBox by encapsulating the device names and adding
 *  specific accessor methods.
 */
package designer.gui.jCustom;

import javax.swing.JComboBox;

import designer.layout.JmriDevice;

/**
 *  This file contains the class definition for a JmriDeviceSelector - 
 *  derived from the JComboBox class for picking a JMRI device type used
 *  by CATS.  Rather than embed the operations piecemeal in another class,
 *  it extends JComboBox by encapsulating the device names and adding
 *  specific accessor methods.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class JmriDeviceSelector extends JComboBox {

	/**
	 * the constructor.  Make the JComboBox select
	 * from the JMRI device names
	 */
	public JmriDeviceSelector() {
		super(JmriDevice.values());
	}

	/**
	 * a constructor.
	 * @param initial is the JMRI device letter for the
	 * initial selection
	 */
	public JmriDeviceSelector(final char initial) {
		this();
		selectDevice(initial);
	}
	
	/**
	 * sets the selected index of the JComboBox to the JMRI
	 * mnemonic.  If the letter is not recognized as a device
	 * CATS uses, the selection is set to None.
	 * @param d is a 1 letter JMRI device type designator.
	 */
	public void selectDevice(final char d) {
//		JmriDevice[] device = JmriDevice.values();
//		for (int i = 0; i < device.length; ++i) {
//			if (device[i].getNmemonic() == d) {
//				setSelectedIndex(i);
//				return;
//			}
//		}
//		setSelectedIndex(device.length - 1);
		setSelectedItem(JmriDevice.mnemonicToDevice(d));
	}
}
/* @(#)JmriDeviceSelector.java */
