/**
 * Name: ChainModel.java
 *
 * What:
 *  This file contains the class definition for a ChainModel object.  A
 *  ChainModel controls the editing within a StoreEditPane of an
 *  IOSpecChain.
 */
package designer.gui.jCustom;

import java.util.Vector;

import javax.swing.JLabel;

import designer.layout.items.IOSpec;

/**
 *  This file contains the class definition for a ChainModel object.  A
 *  ChainModel directs the editing within a StoreEditPane of an
 *  IOSpecChain.
 *  @see designer.gui.store.StoreEditPane
 *  @see designer.layout.items.IOSpecChain
 * <p>
 * IOSpecs and IOSpecChains are not derived from AbstractStore,
 * so the methods in this class override the DesignerTableModel
 * methods.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2006, 2009, 2020, 2022</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ChainModel extends AbstractManagerTableModel {

    /**
     * are the column descriptions
     */
    static final private ColumnData ColInfo[] = {
        new ColumnData("Prefix", 75, JLabel.RIGHT, PrefixSelector.class),
        new ColumnData("Address", 50, JLabel.RIGHT, String.class),
        new ColumnData("User Name", 150, JLabel.RIGHT, String.class),
        new ColumnData("Throw", 75, JLabel.RIGHT, Boolean.class),
        new ColumnData("Undo", 75, JLabel.RIGHT, Boolean.class),
        new ColumnData("Delay", 75, JLabel.RIGHT, Integer.class)
    };

    /**
     * is the constructor.
     *
     * @param records are the IOSpecs being edited.  Because this object
     * manipulates the Vector, the caller should be certain to pass a copy
     * to this object.
     */
    public ChainModel(Vector<Object> records) {
        super(records);
        ColumnInfo = ColInfo;
    }
    
    /**
     * tells the JTable what value to show in a column.
     * 
     * @param row is the row (IOSPec)
     * @param col is the field in the IOSpec
     * 
     * @return a value from an IOSPec
     */
    public Object getValueAt(int row, int col) {
        Object ret = null;
        
        switch (col) {
        case 0: // JMRI Prefix
            ret = (((IOSpec) Contents.elementAt(row)).getPrefix());
            if (ret == null) {
                ret = new String("");
            }
            break;
            
        case 1:
            ret = (((IOSpec)Contents.elementAt(row)).getAddress());
            break;
            
        case 2:
            ret = (((IOSpec) Contents.elementAt(row)).getUserName());
            if (ret == null) {
                ret = new String("");
            }            
            break;
            
        case 3:
            ret = new Boolean(((IOSpec) Contents.elementAt(row)).getCommand());
            break;

        case 4:
            ret = new Boolean(((IOSpec) Contents.elementAt(row)).getExitcommand());            
            break;
            
        case 5:
            ret = new Integer(((IOSpec)Contents.elementAt(row)).getDelay());            
            break;
                
        default:
        }
        return ret;
    }
    
    /**
     * changes a field in an IOSpec
     * 
     * @param val is the new value
     * @param row is the row from the JTable (IOSpec)
     * @param col is the JTable column (field)
     */
    public void setValueAt(Object val, int row, int col) {
        switch (col) {
        case 0:
            if (((String)val).length() == 0) {
                val = null;
            }
            else {
                ((IOSpec) Contents.elementAt(row)).setPrefix(
                        (String) val);
            }
            break;
            
        case 1:
            ((IOSpec) Contents.elementAt(row)).setAddress(
                    (String) val);
            break;
 
        case 2:
            if (((String)val).length() == 0) {
                val = null;
            }
//            else {
                ((IOSpec) Contents.elementAt(row)).setUserName(
                        (String) val);
//            }
            break;

        case 3:
            ((IOSpec) Contents.elementAt(row)).setCommand(
                    ((Boolean) val).booleanValue());           
            break;
            
        case 4:
            ((IOSpec) Contents.elementAt(row)).setExitCommand(
                    ((Boolean) val).booleanValue()); 
            break;
            
        case 5:
            ((IOSpec) Contents.elementAt(row)).setDelay(
                    ((Integer) val).intValue());
            break;
            
        default:
        }
    }
    
    /**
     * creates a new IOSpec.
     * @return the new IOSpec
     */
   public Object createElement() {
        return new IOSpec();
    }

    /**
     * Looks at the entries and checks that all numbers and 
     * user names are unique.
     * 
     * @return null if things look fine or an error string
     * if duplicates are found.
     */
    public String verifyResults() {
        String result = null;
        String addr;
        String jmri;
        
        for (int spec = 0; spec < getRowCount(); ++spec) {
            addr = ((IOSpec) Contents.elementAt(spec)).getAddress();
            if ((addr == null) || (addr.equals(""))) {
                result = new String("A decoder address is required.");
            }
            jmri = ((IOSpec) Contents.elementAt(spec)).getPrefix();
            if ((jmri == null) || jmri.equals("") || jmri.equals("none")) {
                result = new String("A JMRI prefix is required.");
            }
        }
        return result;
    }
}
/* @(#)ChainModel.java */
