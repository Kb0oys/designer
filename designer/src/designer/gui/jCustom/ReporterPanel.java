/* Name: ReporterPanel.java
 *
 * What:
 *  This class generates a JPanel containing the information for describing
 *  a JMRI Internal Reporter
 */
package designer.gui.jCustom;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import designer.layout.JmriName;
import designer.layout.items.ReporterSpec;

/**
 * is a class for creating and editing the parameters describing a
 * JMRI Internal Reporter.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2012, 2020, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ReporterPanel extends JPanel {
    /**
     * This is a ComboBox for selecting the JMRI prefix (layout connection).
     */
    private JComboBox PrefixBox;

    /**
     * is the label describing the ReporterSpec.
     */
    private final JLabel SpecLabel;

    /**
     * is the Address label.
     */
    private final JLabel AddrLabel = new JLabel("Address:");

    /**
     * is the decoder address.
     */
    private JTextField AddrField = new JTextField(10);

    /**
     * is the User Name label
     */
    private final JLabel UsrNameLabel = new JLabel("User Name:");
    
    /**
     * is the User Name.
     */
    private JTextField UsrNameField = new JTextField(20);
    
    /**
     * is the device label
     */
    private final JLabel DeviceLabel = new JLabel("JMRI Device");
    
    /**
     * is the JMRI Device type
     */
    private final JmriDeviceSelector DeviceType = new JmriDeviceSelector('R');
    
    /**
     * is the constructor.
     *
     * @param spec is an IO specification for a Reporter.  If ReporterSpec is null, then a new
     * ReporterSpec is being created; otherwise,
     * if ReporterSpec is not null, then it is being edited.
     *
     * @param label is added to the JPanel to identify the ReporterSpec.
     *
     * to the layout; false, if it is looking for messages from the layout.
     */
    public ReporterPanel(ReporterSpec spec, String label) {
      JPanel cmdGroup = new JPanel();
      JPanel addrGroup = new JPanel();
      JPanel nameGroup = new JPanel();
      JPanel combo = new JPanel();
      JPanel deviceGroup = new JPanel();
      boolean enabled = false;
      String prefix;

      SpecLabel = new JLabel(label);
      PrefixBox = new JComboBox(JmriName.getPrefixesForType(JmriTypeSelector.REPORTER));
      PrefixBox.setSelectedIndex(PrefixBox.getItemCount() - 1);
      if (spec == null) {
        AddrField.setText("");
        UsrNameField.setText("");
      }
      else {
        enabled = true;
        AddrField.setText(spec.getAddress());
        prefix = spec.getPrefix();
        if (prefix != null) {
          for (int index = 0; index < PrefixBox.getItemCount(); ++index) {
            if (((String)PrefixBox.getItemAt(index)).equals(prefix)) {
              PrefixBox.setSelectedIndex(index);
              break;
            }
          }
        }
        if ((spec.getUserName() == null) || (spec.getUserName().length() == 0)) {
            UsrNameField.setText("");
        }
        else {
            UsrNameField.setText(spec.getUserName());
        }
      }
      AddrLabel.setEnabled(enabled);
      AddrField.setEnabled(enabled);
      PrefixBox.setEnabled(true);
      PrefixBox.addItemListener(new ItemListener() {
        public void itemStateChanged(ItemEvent ie) {
          boolean enable;
          if (ie.getStateChange() == ItemEvent.SELECTED) {
            enable = (ie.getItem() != 
                PrefixBox.getItemAt(PrefixBox.getItemCount() - 1));
            AddrLabel.setEnabled(enable);
            AddrField.setEnabled(enable);
          }
        }
      }
      );

      setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
      combo.add(SpecLabel);
      combo.add(PrefixBox);
      cmdGroup.add(combo);
      addrGroup.add(AddrLabel);
      AddrField.setMaximumSize(new Dimension(30, 20));
      AddrField.setAlignmentX(Component.RIGHT_ALIGNMENT);
      addrGroup.add(AddrField);
      cmdGroup.add(addrGroup);
      
      nameGroup.add(UsrNameLabel);
      nameGroup.add(UsrNameField);
      
      deviceGroup.add(DeviceLabel);
      deviceGroup.add(DeviceType);
      DeviceLabel.setEnabled(false);
      DeviceType.setEnabled(false);
      
      add(cmdGroup);
      add(nameGroup);
      add(deviceGroup);
    }

    /**
     * constructs an ReporterSpec for a Reporter from the information in the Jpanel.
     *
     * @return an ReporterSpec if either the address or UserName is filled in.
     *
     * @see designer.layout.items.ReporterSpec
     */
    public ReporterSpec getSpec() {
        ReporterSpec spec;
        String userName;
        String addr = AddrField.getText();
        userName = UsrNameField.getText();
        if (((addr == null) || addr.equals("")) &&
                ((userName == null) || userName.equals(""))) {
            return null;
        }
        spec = new ReporterSpec(addr);
        spec.setPrefix((String) PrefixBox.getSelectedItem());
        if ((userName == null) || (userName.length() == 0)) {
            spec.setUserName(null);
        }
        else {
            spec.setUserName(userName);
        }
        return spec;
    }
}
/* @(#)ReporterPanel.java */
