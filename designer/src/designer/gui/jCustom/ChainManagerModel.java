/*
 * Name: ChainManagerModel.java
 *
 * What:
 *  This class provides an AbstractTableModel for editing
 *  the list of IOSpecChains.
 */
package designer.gui.jCustom;

import java.util.Vector;

import javax.swing.JLabel;
import designer.layout.items.IOSpecChain;
/**
 * Provides an AbstractTableModel for adding, deleting, and
 * selecting IOSpecChains for editing.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2006, 2009, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

/**
 * is a class that constructs a JTableModel for creating,
 * editing, and deleting IOSPecChains.
 */
public class ChainManagerModel extends AbstractManagerTableModel {
    /**
     * are the column descriptions
     */
    protected ColumnData ColInfo[] = {
        new ColumnData("Number", 50, JLabel.RIGHT, Integer.class),
        new ColumnData("User Name", 150, JLabel.RIGHT, String.class),
        new ColumnData("IOSpec Chain", 100, JLabel.RIGHT, IOSpecChain.class)
    };
    
    /**
     * is the ctor.  It creates a copy of the manager so that
     * nothing is changed if the user cancels the operation.
     * @param myCopy is what is being copied
     */        
    public ChainManagerModel(Vector<Object> myCopy) {
        super(myCopy);
        ColumnInfo = ColInfo;
    }
    
    /**
     * tells the JTable what value to show in a column.
     * 
     * @param row is the row (IOSPecChain)
     * @param col is the field in the IOSpecChain
     * 
     * @return a value from an IOSPecChain
     */
    public Object getValueAt(int row, int col) {
        Object ret = null;
        
        switch (col) {
        case 0: // JMRI System number
            ret = new Integer(Integer.parseInt(((IOSpecChain) Contents.elementAt(row)).getAddress()));
            break;
            
        case 1:
            ret = ((IOSpecChain) Contents.elementAt(row)).getUserName();
            if (ret == null) {
                ret = new String("");
            }
            break;
            
        case 2:
            ret = ((IOSpecChain) Contents.elementAt(row)).getSelf();
            break;
            
        default:
        }
        return ret;
    }
    
    /**
     * changes a field in an IOSpecChain
     * 
     * @param val is the new value
     * @param row is the row from the JTable (IOSpecChain)
     * @param col is the JTable column (field)
     */
    public void setValueAt(Object val, int row, int col) {
        switch (col) {
        case 0:
            ((IOSpecChain) Contents.elementAt(row)).setAddress(
                    String.valueOf(val));
            break;
            
        case 1:
            if (((String)val).length() == 0) {
                val = null;
            }
//            else {
                ((IOSpecChain) Contents.elementAt(row)).setUserName(
                        (String) val);
//            }
            break;
            
        case 2:
            break;
            
        default:
        }
    }
    
    /**
     * tells the JTable which cells can be edited.  Most cells can be edited,
     * but a few models will want to override this method.
     * 
     * @param row is the row number of a cell (list element)
     * @param col is a column number (field)
     * 
     * @return true - the table cell can be edited
     */
    public boolean isCellEditable(int row, int col) {
        return true;
    }
    
    /**
     * creates a new IOSpecChain.
     * @return the new IOSpecChain
     */
    public Object createElement() {
        return new IOSpecChain();
    }
    
    /**
     * Looks at the entries and checks that all addresses and 
     * user names are unique.
     * 
     * @return null if things look fine or an error string
     * if duplicates are found.
     */
    public String verifyResults() {
        String result = null;
        String name;
        IOSpecChain isc;
        String addr1;
        
        // scan the addresses
        for (int chain = 0; chain < getRowCount(); ++chain){
            isc = (IOSpecChain)Contents.elementAt(chain);
            addr1 = isc.getAddress();
            name = isc.getUserName();
            if ("0".equals(addr1)) {
                result = new String("0 is not an acceptable chain number.");
            }
            else {
                for (int c2 = chain + 1; c2 < getRowCount(); ++ c2) {
                    if (((IOSpecChain)Contents.elementAt(c2)).getAddress().equals(addr1)) {
                        result = new String("Chain numbers must be unique.");
                    }
                }
            }
            if (name != null) {
                for (int c2 = chain + 1; c2 < getRowCount(); ++c2) {
                    if (name.equals(((IOSpecChain)Contents.elementAt(c2)).getUserName())) {
                        result = new String("Chains cannot have the same user name.");
                    }
                }
            }
        }
        return result;
    }
}
/* @(#)ChainManagerModel.java */
