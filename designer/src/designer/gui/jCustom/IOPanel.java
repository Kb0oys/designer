/* Name: IOPanel.java
 *
 * What:
 *  This class generates a JPanel containing the information for describing
 *  a DCC input or output on the railroad.
 */
package designer.gui.jCustom;

import designer.layout.JmriDevice;
import designer.layout.JmriName;
import designer.layout.items.IOSpec;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 * is a class for creating and editing the parameters describing a
 * decoder.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2020, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class IOPanel
extends JPanel {

	/**
	 * This is a ComboBox for selecting one of the above.
	 */
	private JComboBox<String> PrefixBox;

	/**
	 * is the label describing the IOSpec.
	 */
	private JLabel SpecLabel;

	/**
	 * the "Throw" button.
	 */
	private JRadioButton ThrowButton = new JRadioButton("Throw");

	/**
	 * the "Close" button.
	 */
	private JRadioButton CloseButton = new JRadioButton("Close");

	/**
	 * is true if the IOSpec is for a throw command.
	 */
	private boolean ThrowCmd;

	/**
	 * is the Address label.
	 */
	private final JLabel AddrLabel = new JLabel("Address:");

	/**
	 * is the decoder address.
	 */
	private JTextField AddrField = new JTextField(10);

	/**
	 * is a checkbox for enabling or disabling the Off Command option.
	 */
	private JCheckBox OffCmdBox = new JCheckBox("Off Command?");

	/**
	 * is the User Name label
	 */
	private final JLabel UsrNameLabel = new JLabel("User Name:");

	/**
	 * is the User Name.
	 */
	private JTextField UsrNameField = new JTextField(20);

	/**
	 * is the label on the JMRI device selector
	 */
	private final JLabel DeviceLabel = new JLabel("JMRI Device:");

	/**
	 * is the JComboBox for selecting a JMRI device type
	 */
	private JmriDeviceSelector DeviceSelector = new JmriDeviceSelector();

	/**
	 * is the constructor.
	 *
	 * @param spec is an IO specification.  If IOSpec is null, then a new
	 * IOSpec is being created; otherwise,
	 * if IOSPec is not null, then it is being edited.
	 *
	 * @param label is added to the JPanel to identify the IOSpec.
	 *
	 * @param output is true if the IOSpec is for sending commands
	 * to the layout; false, if it is looking for messages from the layout.
	 */
	public IOPanel(IOSpec spec, String label, boolean output) {
		ButtonGroup cmd = new ButtonGroup();
		JPanel senseGroup = new JPanel();
		JPanel cmdGroup = new JPanel();
		JPanel addrGroup = new JPanel();
		JPanel nameGroup = new JPanel();
		JPanel deviceGroup = new JPanel();
		JPanel combo = new JPanel();
		String prefix;

		SpecLabel = new JLabel(label);
		PrefixBox = new JComboBox(JmriName.getPrefixes());
		PrefixBox.setSelectedIndex(PrefixBox.getItemCount() - 1);
		DeviceSelector.selectDevice(' ');
		if (spec == null) {
			AddrField.setText("");
			OffCmdBox.setSelected(false);
			UsrNameField.setText("");
		}
		else {
			ThrowCmd = spec.getCommand();
			ThrowButton.setSelected(ThrowCmd);
			CloseButton.setSelected(!ThrowCmd);
			AddrField.setText(spec.getAddress());
			prefix = spec.getPrefix();
			OffCmdBox.setSelected(spec.getExitcommand());
			if (prefix != null) {
				for (int index = 0; index < PrefixBox.getItemCount(); ++index) {
					if (PrefixBox.getItemAt(index).equals(prefix)) {
						PrefixBox.setSelectedIndex(index);
						break;
					}
				}
			}
			if ((spec.getUserName() == null) || (spec.getUserName().length() == 0)) {
				UsrNameField.setText("");
			}
			else {
				UsrNameField.setText(spec.getUserName());
			}
			if (spec.getDeviceType() != null) {
				DeviceSelector.setSelectedItem(spec.getDeviceType());
			}
		}
		PrefixBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent ie) {
				if (ie.getStateChange() == ItemEvent.SELECTED) {
					setEnabled(isEnabled());
				}
			}
		}
				);
		DeviceSelector.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent ie) {
				if (ie.getStateChange() == ItemEvent.SELECTED) {
					setEnabled(isEnabled());
				}
			}
		});
		ThrowButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent ie) {
				ThrowCmd = (ie.getStateChange() != ItemEvent.DESELECTED);
			}
		});
		CloseButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent ie) {
				ThrowCmd = (ie.getStateChange() == ItemEvent.DESELECTED);
			}
		});

		setEnabled(true);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		combo.add(SpecLabel);
		combo.add(PrefixBox);
		cmdGroup.add(combo);
		addrGroup.add(AddrLabel);
		AddrField.setMaximumSize(new Dimension(30, 20));
		AddrField.setAlignmentX(Component.RIGHT_ALIGNMENT);
		addrGroup.add(AddrField);
		cmdGroup.add(addrGroup);

		nameGroup.add(UsrNameLabel);
		nameGroup.add(UsrNameField);

		deviceGroup.add(DeviceLabel);
		deviceGroup.add(DeviceSelector);

		cmd.add(ThrowButton);
		cmd.add(CloseButton);
		senseGroup.add(ThrowButton);
		senseGroup.add(CloseButton);
		if (output) {
			senseGroup.add(OffCmdBox);
		}

		add(cmdGroup);
		add(nameGroup);
		add(deviceGroup);
		add(senseGroup);
	}

	/**
	 * constructs an IOSPec from the information in the Jpanel.
	 *
	 * @return an IOSpec if the checkbox is checked and the fields
	 * are filled in.
	 *
	 * @see designer.layout.items.IOSpec
	 */
	public IOSpec getSpec() {
		if (isPrefixSelected() || isDeviceSelected()) {
			if ((ThrowButton.isSelected() || CloseButton.isSelected())) {
				String addr = AddrField.getText();
				String userName;
				IOSpec spec = new IOSpec();
				userName = UsrNameField.getText();
				if (isPrefixSelected()) {
					if (((addr == null) || (addr.equals(""))) && ((userName == null) || (userName.length() == 0))) {
						System.out.println("Missing decoder address");
						return null;
					}
					spec.setPrefix((String) PrefixBox.getSelectedItem());
					if ((addr == null) || addr.equals("")) {
						spec.setAddress(null);
					}
					else {
						spec.setAddress(addr);
					}
					if ((userName == null) || (userName.length() == 0)) {
						spec.setUserName(null);
					}
					else {
						spec.setUserName(userName);
					}
				}
				if (isDeviceSelected()) {
					if ((userName == null) || (userName.length() == 0)) {
						System.out.println("Missing decoder user name");
						return null;					
					}
					spec.setDeviceType((JmriDevice) DeviceSelector.getSelectedItem());
					spec.setUserName(userName);
				}
				spec.setCommand(ThrowCmd);
				spec.setExitCommand(OffCmdBox.isSelected());
				return spec;
			}
			else {
				System.out.println("Need to select either thrown or closed");
			}
		}
		return null;
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (isPrefixSelected()) {
			// if a JMRI System prefix has been selected, the JMRI device type
			// should be disabled - the prefix will select the proper index -
			// and the System Name information (prefix+address) should be
			// enabled/disabled as requested
			DeviceLabel.setEnabled(false);
			DeviceSelector.setEnabled(false);
			PrefixBox.setEnabled(enabled);
			SpecLabel.setEnabled(enabled);
			AddrLabel.setEnabled(enabled);
			AddrField.setEnabled(enabled);
		}
		else if (isDeviceSelected()) {
			// no JMRI prefix has been selected.  If a JMRI device type has been selected,
			// the System name fields should be disabled.  The JMRI Device should be as
			// requested
			DeviceLabel.setEnabled(enabled);
			DeviceSelector.setEnabled(enabled);
			PrefixBox.setEnabled(false);
			SpecLabel.setEnabled(false);
			AddrLabel.setEnabled(false);
			AddrField.setEnabled(false);
		}
		else {
			// no JMRI prefix or JMRI device has been selected, so the prefix
			// and device should both be as requested.  The address should
			// disabled.
			DeviceLabel.setEnabled(enabled);
			DeviceSelector.setEnabled(enabled);
			PrefixBox.setEnabled(enabled);
			SpecLabel.setEnabled(enabled);
			AddrLabel.setEnabled(false);
			AddrField.setEnabled(false);			
		}
		// the user field should be set as requested
		UsrNameLabel.setEnabled(enabled);
		UsrNameField.setEnabled(enabled);
		enableButtons(enabled);
	}

	/**
	 * a shared method to set the buttons and labels to a consistent state.
	 * The buttons are unconditionally disabled if neither a JMRI prefix
	 * is selected or JMRI device is selected.
	 * @param enabled true to enable them and false to disable them
	 */
	private void enableButtons(boolean enabled) {
		enabled &= (isPrefixSelected() || isDeviceSelected());
		ThrowButton.setEnabled(enabled);
		CloseButton.setEnabled(enabled);
		OffCmdBox.setEnabled(enabled);		
	}

	/**
	 * a predicate for determining if a JMRI prefix has been selected
	 * @return rue if it has and false if not
	 */
	private boolean isPrefixSelected() {
		return (PrefixBox.getSelectedIndex() != (PrefixBox.getItemCount() - 1));
	}


	/**
	 * a predicate for determining if a JMRI device has been selected
	 * @return rue if it has and false if not
	 */
	private boolean isDeviceSelected() {
		return (DeviceSelector.getSelectedIndex() != (DeviceSelector.getItemCount() - 1));
	}
}
/* @(#)IOPanel.java */
