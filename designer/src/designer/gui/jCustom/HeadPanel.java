/*Name: HeadPanel.java
 *
 * What:
 *  This Panel constructs a JPanel containing the signal presentation
 *  labels for a signal and IOSpecs for that presentation, if they exist.
 */
package designer.gui.jCustom;

import designer.gui.AspectDialog;
import designer.layout.items.AspectCommand;
import designer.layout.items.HeadStates;
import designer.layout.items.IOSpec;
import java.util.Enumeration;
import java.util.Vector;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * is a class for creating the JPanel for creating or editing the HeadStates
 * data structure for a signal head.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class HeadPanel
extends JPanel {
    
    /**
     * the string to label the Decoder information with.
     */
    static final String DECLABEL = "Decoder:";
    
    /**
     * The commands for each state.
     */
    private Vector<IOPanel> States = new Vector<IOPanel>();
    
    /**
     * The labels for each state.
     */
    private Vector<String> Labels = new Vector<String>();
 
    /**
     * The checkbox for software assist.
     */
    private JCheckBox Assist = new JCheckBox("Software Flashing", false);
    
    /**
     * The text field for entering a user name for the SignalHead.
     */
    private JTextField UNameField;
    
    /**
     * the constructor.
     *
     * @param e is an Enumerator for getting the presentation Strings for the head.
     *
     * @param clist is the current commands for the head, to be edited; or it is
     * null to create a new list of commands.
     *
     * @see designer.layout.items.HeadStates
     */
    
    public HeadPanel(Enumeration<String> e, HeadStates clist) {
        IOSpec cmd;
        JPanel aState;
        JPanel uName;
        JPanel decoders = new JPanel(new GridLayout(0, 3));
        IOPanel panel;
        String stateLabel;
        boolean flashing = false;
        
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        // Generate a row for each presentation.
        while (e.hasMoreElements()) {
            aState = new JPanel(new FlowLayout());
            stateLabel = e.nextElement();
            aState.setBorder(BorderFactory.createTitledBorder(
                    BorderFactory.createLineBorder(Color.gray), stateLabel));
            cmd = null;
            if (clist != null) {
                cmd = clist.findDecoders(stateLabel);
            }
            panel = new IOPanel(cmd, DECLABEL, true);
            aState.add(panel);
            Labels.addElement(stateLabel);
            flashing |= stateLabel.startsWith(AspectDialog.FLASHING);
            States.addElement(panel);
            decoders.add(aState);
        }
        add(decoders);
        
        // Generate the Flashing and SignalHead widgets
        aState = new JPanel();
        aState.setLayout(new BoxLayout(aState, BoxLayout.X_AXIS));

        // first, add the optional Flashing checkbox
        if (flashing) {
            if (clist != null) {
                Assist.setSelected(clist.getAssist());
            }
            aState.add(Assist);
        }
        
        // add the JMRI username jtextfield
        uName = new JPanel(new FlowLayout());
        uName.add(new JLabel("User Name:"));
        if ((clist != null) && (clist.getName() != null)) {
            UNameField = new JTextField(clist.getName(),
                    clist.getName().length() < 15 ? 15 : clist.getName().length());
        }
        else {
            UNameField = new JTextField(15);
        }
        uName.add(UNameField);
        aState.add(uName);
        add(aState);
    }
    
    /**
     * is the method for returning the results.
     *
     * @return a newly created HeadStates containing the decoder values
     * for all presentations for a single head.
     *
     * @see designer.layout.items.HeadStates
     */
    public HeadStates getCommands() {
        HeadStates newHS = new HeadStates();
        AspectCommand ac;
        String uName;
        
        // Generate the AspectCommand for each presentation.
        Enumeration<IOPanel> s = States.elements();
        for (Enumeration<String> l = Labels.elements(); s.hasMoreElements(); ) {
            
            // Generate the AspectCommand.
            ac = new AspectCommand( l.nextElement());
            ac.setCommand(s.nextElement().getSpec());
            newHS.addElement(ac);
        }
        newHS.setAssist(Assist.isSelected());
        uName = UNameField.getText().trim();
        if (uName.length() != 0) {
            newHS.setName(uName);
        }
        return newHS;
    }
}
/* @(#)HeadPanel.java */
