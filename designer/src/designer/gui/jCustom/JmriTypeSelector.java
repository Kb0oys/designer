/**
 * What: JmriTypeSelector.java
 *
 * What:
 *  This is a concrete class for generating a Java TableCell
 *  editor and renderer for a list of JMRI device types.
 */
package designer.gui.jCustom;

import java.util.Vector;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import designer.gui.store.ListSpec;

/**
 *  This is a concrete class for generating a Java TableCell
 *  editor and renderer for a list of JMRI device types.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2006, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class JmriTypeSelector extends ListSpec {


    /**
     * are constants for defining the JMRI system types
     */
	/**
	 * tag for a Decoder Chain
	 */
    public static final String CHAIN = "Decoder Chain";
    
    /**
     * tag for a Signal Head
     */
    public static final String HEAD = "Signal Head";
    
    /**
     * tag for a Light
     */
    public static final String LIGHT = "Light";
    
    /**
     * tag for a Memory recorder
     */
    public static final String MEMORY = "Memory";
    
    /**
     * tag for a Power Manager
     */
    public static final String POWER = "Power Manager";
    
    /**
     * tag for a Reporter
     */
    public static final String REPORTER = "Reporter";
    
    /**
     * tag for a JMRI Route
     */
    public static final String ROUTE = "Route";
    
    /**
     * tag for a Sensor
     */
    public static final String SENSOR = "Sensor";
    
    /**
     * tag for a Turnout
     */
    public static final String TURNOUT = "Turnout";
    
    /**
     * tag for Logix
     */
    public static final String LOGIX = "logiX";

    /**
     * is the list of JMRI types.
     */
    private static final String[] DefinedTypes = {
        CHAIN,
        HEAD,
        LIGHT,
        MEMORY,
        POWER,
        REPORTER,
        ROUTE,
        SENSOR,
        TURNOUT,
        LOGIX
    };
    
    /**
     * is the sub-class specific method for building the Vector of labels
     * in the Selection dialog.
     *
     * @return the Vector of Strings that forms the contents of the Selection
     * Dialog.
     */
    protected Vector<String> createSpecificList() {
        return ListSpec.arrayToVector(DefinedTypes);
    }

    /**
     * creates a TableCellRenderer for a list of JMRI device types
     * @return the TableCellRenderer
     */
    static public TableCellRenderer getRenderer() {
        return new JmriTypeSelector().getCellRenderer();
    }

    /**
     * creates a TableCellEditor for a list of JMRI device types
     * 
     * @return the TableCellEditor
     */
    static public TableCellEditor getEditor() {
        return new JmriTypeSelector().getCellEditor();
    }
}
/* @(#)JmriTypeSelector.java */
