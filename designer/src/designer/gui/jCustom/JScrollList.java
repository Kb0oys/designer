/* Name: JScrollList.java
 *
 * What:
 *  This file contains the class definition for a JList embedded in a
 *  JScrollPane.  Thus, it is derived from JScrollPane, extending it
 *  with a method for determining the item in the JList selected.
 */
package designer.gui.jCustom;

import javax.swing.JList;
import javax.swing.JScrollPane;

/**
 * This class is derived from JScrollPane.  It encloses a JList and
 * adds a method for determining which item in the JList was selected.
 * <p>
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class JScrollList
    extends JScrollPane {
  private JList<?> MyList;

  /**
   * the constructor.
   *
   * @param list is the contents of the JScrollPane.
   */
  public JScrollList(JList<?> list) {
    super(list);
    MyList = list;
  }

  /**
   * enables or disables the List.
   *
   * @param state is true to enable the JList or false to disable it.
   */
  public void setEnabled(boolean state) {
    MyList.setEnabled(state);
  }

  /**
   * is the extension - a way to determine what item was selected.
   *
   * @return the index of the item selected, or -1.
   */
  public int getSelection() {
    return MyList.getSelectedIndex();
  }
}
/* @(#)JScrollList.java */