/*
 * Name: JScrollDialog.java
 *
 * What:
 *  JScrollDialog is a class for creating a JDialog with a Jlist as its
 *  main body and two buttons - Accept and Cancel.
 */
package designer.gui.jCustom;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Container;
import java.awt.Frame;
import java.awt.BorderLayout;

/**
 * a class for constructing a JDialog whose main body is a JList and has
 * 2 buttons for completing the transaction.  Accept to indicate that
 * a selection has been made and Cancel to not do anything.  This
 * is a modal JDialog so nothing happens with the rest of the system
 * until the user disposes of it.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class JScrollDialog {

  /**
   *   Result button from JDialog box
   */
  private boolean Result;

  /**
   *   The button to push to change the line width
   */
  private JButton Accept = new JButton("Accept");

  /**
   *   The button to Cancel the change
   */
  private JButton Cancel = new JButton("Cancel");

  /**
   *   The Buttons, as a group
   */
  private JPanel Buttons = new JPanel();

  /**
   *   The JDialog used for setting the name values.
   */
  JDialog Dialog;

  /**
   * is the constructor.  Since the caller provides the JList, the
   * caller can read it and alter it, as needed.  For example, the
   * caller can set the SINGLE/MULTI SELECTION flag, or read the
   * selected values.
   *
   * @param selList is the JList which forms the body.
   * @param title is a title for the JDialog.
   */
  public JScrollDialog(JList<?> selList, String title) {
    Dialog = new JDialog( (Frame)null, title, true);
    Container dialogContentPane = Dialog.getContentPane();
    JScrollList sl = new JScrollList(selList);
    Buttons.add(Accept);
    Buttons.add(Cancel);
    Accept.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        Result = true;
        Dialog.dispose();
      }
    });
    Cancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        Result = false;
        Dialog.dispose();
      }
    });
    dialogContentPane.setLayout(new BorderLayout());
    dialogContentPane.add("North", sl);
    dialogContentPane.add("South", Buttons);
    Dialog.pack();
    Dialog.setVisible(true);
  }

  /**
   * creates the JDialog, displays it, and tells the user what closing
   * button was pushed.
   *
   * @param selList is the JList for the contents.
   * @param title is the title of the JDialog.
   *
   * @return true if the user pushed the Accept button or false if the
   * user pushed the Cancel button.
   */
  static public boolean select(JList<?> selList, String title) {
    JScrollDialog dialog = new JScrollDialog(selList, title);
    return dialog.Result;
  }

}
/* @(#)JScrollDialog.java */