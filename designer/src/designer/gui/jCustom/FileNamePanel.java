/* Name: FileNamePanel.java
 *
 * What:
 * FileNamePanel is a JPanel for capturing a filename.  It consists of 2 pieces:
 * <ol>
 * <li>a JTextField containing the file name</li>
 * <li>a JButton labeled "Browse", when pushed pulls up a FileChooser</li>
 * </ol>
 * Anything selected through the FileChooser is captured in the JTextField.  Thus,
 * the user can enter an absolute or relative file path name or using the FileChooser,
 * navigate to a file.  In either case, the resulting file name is checked for existing.
 */
package designer.gui.jCustom;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *  ButtonItemDialog defines a JPanel (with Accept and Cancel buttons) for
 *  creating or editing a Button to be painted in a GridTile.  A Button
 *  contains 4 pieces of information:
 *  <ol>
 *  <li>an IOSpec to be thrown/closed when clicked on</li>
 *  <li>an image to be displayed when the primary IOSpec is selected</li>
 *  <li>an image to be displayed when the alternate IOSpec is selected</li>
 *  <li>a timer to flip the IOSpec from Closed to Thrown, simulating
 *  a momentary button push.</li>
 *  </ol>
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class FileNamePanel extends JPanel {
    /**
     * The file name.
     */
    private JTextField FileName;
    
    /**
     * The "browse" JButton.
     */
    private JButton Browse = new JButton("browse");
    
    /**
     * the ctor
     * @param label is the label on the panel
     * @param fileName is the initial value of the file name
     */
    
    public FileNamePanel(final String label, JTextField fileName) {
    	FileName = fileName;
    	
        Browse.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent be) {
                int result;
                JFileChooser chooser = new JFileChooser(new File(designer.gui.DispPanel.FilePath));
                chooser.setDialogTitle("Select image file:");
                result = chooser.showOpenDialog(null);
                File fileobj = chooser.getSelectedFile();
                if (result == JFileChooser.APPROVE_OPTION) {
                    if (fileobj.exists() && fileobj.canRead()) {
                        designer.gui.DispPanel.FilePath = fileobj.getAbsolutePath();
                        FileName.setText(fileobj.getAbsolutePath());
                    }
                }
            }
        });
        setLayout(new FlowLayout());
        setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.gray), label));
    	add(FileName);
    	add(Browse);
    }
}
/* @(#)FileNamePanel.java */
