/* What: DecoderPopUp.java
 *
 * What:
 *  This class creates objects which list all decoder addresses used.
 *
 *  Eventually, it will segment the addresses into Outputs (signals,
 *  turnout control, turnout indicators) and Inputs (detection and turnout
 *  direction selection).  Each address should be labeled with the user.
 *
 *  A print button is provided so that a hard copy can be made of the
 *  addresses and users.
 */
package designer.gui;

import java.util.Enumeration;

import designer.layout.Layout;

/**
 * is a class for generating a list of decoder addresses used on the
 * layout.  Its intent is to provide a way of documenting the addresses
 * used and where they are used.
 * <p>
 * The addresses are divided into Inputs (detection and panel switches) and
 * Outputs (signals, turnouts, other indicators).  A button creates a
 * print job for printing the list.
 * <p>
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class DecoderPopUp {

  /**
   * the constructor.
   */
  public DecoderPopUp() {
    Layout layout = Ctc.getLayout();
    for (Enumeration<?> e = layout.elements(); e.hasMoreElements(); ) {

    }
  }

}
/* @(#)DecoderPopUp.java */
