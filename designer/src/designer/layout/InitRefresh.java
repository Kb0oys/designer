/* Name: InitRefresh.java
 *
 * What:
 *  Compression is a Singleton object with a boolean value.  It is set
 *  to true if CATS should try to read the JMRI values of IOSpecs being
 *  monitored when CATS starts up.  One initialization problem is that
 *  if JMRI starts before CATS, then JMRI never tells CATS the value of
 *  IOSPec until it changes.  If CATS starts first (or the layout is
 *  powered up after CATS), then JMRI sees values change from "UNKNOWN"
 *  to something and tells CATS.  This flag should be set when
 *  JMRI has completed its initialization before CATS is up.  This is
 *  a flag because CATS can have bad values if it initializes first.
 */
package designer.layout;

import designer.gui.BooleanGui;

/**
 *  InitRefresh is a Singleton object with a boolean value.  It is set
 *  to true if CATS should try to read the JMRI values of IOSpecs being
 *  monitored when CATS starts up.  One initialization problem is that
 *  if JMRI starts before CATS, then JMRI never tells CATS the value of
 *  IOSPec until it changes.  If CATS starts first (or the layout is
 *  powered up after CATS), then JMRI sees values change from "UNKNOWN"
 *  to something and tells CATS.  This flag should be set when
 *  JMRI has completed its initialization before CATS is up.  This is
 *  a flag because CATS can have bad values if it initializes first.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2007, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class InitRefresh extends BooleanGui {

    /**
     * is the tag for identifying a Compression Object in the XML file.
     */
    static final String XMLTag = "REFRESHTAG";
  
  /**
   * is the label on the JCheckBoxMenuItem
   */
  static final String Label = "Initial Refresh";

  /**
   * constructs the factory.
   */
  public InitRefresh() {
    super(Label, XMLTag, false);
  }
}
/* @(#)InitRefresh.java */
