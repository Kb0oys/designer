/*
 * Name: TranspondingBit.java
 * 
 * What:
 *  TranspondingFlag is a Singleton object with a boolean value.  It is set
 *  to true if CATS should print whatever is received from a Block Transponder.
 *  Sometimes the format of what is encapsulated in the transponding Reporter
 *  is unknown.  The user can turn on this flag and see the String that is the
 *  value of the transponder.
 */
package designer.layout;

import designer.gui.BooleanGui;

/**
 *  TranspondingBit is a Singleton object with a boolean value.  It is set
 *  to true if CATS should print whatever is received from a Block Transponder.
 *  Sometimes the format of what is encapsulated in the transponding Reporter
 *  is unknown.  The user can turn on this flag and see the String that is the
 *  value of the transponder.
 * <p>Title: cats</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class TranspondingBit 
extends BooleanGui {

	/**
	 * is the tag for identifying a TranspondingBit Object in the XML file.
	 */
	static final String XMLTag = "TRANSPONDINGBIT";

	/**
	 * is the label on the JCheckBoxMenuItem
	 */
	static final String Label = "Trace Transponding Messages";

	/**
	 * constructs the factory.
	 */
	public TranspondingBit() {
		super(Label, XMLTag, false);
	}
}
/* @(#)TranspondingBit.java */
