/* Name EnumerationLocks.java
 *
 * What:
 *  This class holds an enumeration defining the various kinds of locks that can affect
 *  Signals and Switches.  It should mirror the CATS file with the same name (except it
 *  does not have the ancillary methods).
 *  <p>
 *  The defined locks are:
 *  <ul>
 *  <li> Call-on - the dispatcher has set call-on to override a Halt indication
 *  <li> Traffic Lock - the signals and turnouts are locked because a route has
 *  been reserved through the track circuit.
 *  <li> Detection (or Route) Lock - the signals and turnouts are locked because
 *  the track circuit shows occupied.
 *  <li> Opposing Signal Lock - the signal is locked on Halt because an opposing signal
 *  has been cleared.
 *  <li> Conflicting Signal Lock - the signal is locked on Halt because a conflicting
 *  signal has been cleared.
 *  <li> Switch Indication Lock (fouling) - the points are fouling
 *  <li> Time Lock - signals and turnouts are locked because the dispatcher has knocked
 *  down a route and time is running before they can be reset.
 *  <li> Approach Lock - a precondition for time lock.  Time is not locked if the approach
 *  traffic circuit is unoccupied.
 *  <li> Track Warrant - the signals and turnouts are locked because the dispatcher
 *  has granted a track warrant for a train to move across the track circuit.
 *  <li> Unbonded (or Dark) Track - the track is unsignaled.
 *  <li> Track and Time - the signals and turnouts are locked because the dispatcher
 *  has granted Track Authority.
 *  <li> Out of Service - the signals and turnouts are locked because the dispatcher
 *  has taken the track circuit out of service.
 *  <li> Switch Unlocked - the field has unlocked a switch
 *  <li> drill
 *  </ul>
 *  <p>
 *  This also defines an enumeration of the affects of the lock.
 *  <ul>
 *  <li> None - the lock does not change a signal's indication.  It locks a turnout or prevents
 *  a signal from changing its indication
 *  <li> Halt - the lock forces a Halt indication (unless overridden)
 *  <li> Restricting - the lock forces a Restricting indication (unless a higher priority lock
 *  is in place).
 *  <li> Stop and Proceed - an obsolete form of Restricting.  It is included to distinguish
 *  dispatcher initiated locks from those initiated by the track.
 *  </ul>
 */
package designer.layout;

/**
 *  This class holds an enumeration defining the various kinds of locks that can affect
 *  Signals and Switches.
 *  <p>
 *  The defined locks are:
 *  <ul>
 *  <li> Call-on - the dispatcher has set call-on to override a Halt indication
 *  <li> Traffic Lock - the signals and turnouts are locked because a route has
 *  been reserved through the track circuit.  This appears only in the Tracks
 *  involved in the route.
 *  <li> Detection (or Route) Lock - the signals and turnouts are locked because
 *  the track circuit shows occupied.
 *  <li> Opposing Signal Lock - the signal is locked on Halt because an opposing signal
 *  has been cleared.
 *  <li> Conflicting Signal Lock - the signal is locked on Halt because a conflicting
 *  signal has been cleared.
 *  <li> Switch Indication Lock (fouling) - the points are fouling
 *  <li> Time Lock - signals and turnouts are locked because the dispatcher has knocked
 *  down a route and time is running before they can be reset.
 *  <li> Approach Lock - a precondition for time lock.  Time is not locked if the approach
 *  traffic circuit is unoccupied.
 *  <li> Track Warrant - the signals and turnouts are locked because the dispatcher
 *  has granted a track warrant for a train to move across the track circuit.
 *  <li> Unbonded (or Dark) Track - the track is unsignaled.
 *  <li> Track and Time - the signals and turnouts are locked because the dispatcher
 *  has granted Track Authority.
 *  <li> Out of Service - the signals and turnouts are locked because the dispatcher
 *  has taken the track circuit out of service.
 *  <li> Switch Unlocked - the field has unlocked a switch
 *  <li> ColorOnDetection - the Track object should color the track frill on occupancy
 *  <li> EntryEdge - the BlkEdge is the entry into the track circuit for a route.
 *  <li> CTC Hold - the signal should show Halt because it protects CTC blocks and there
 *  is no route set through the block
 *  <li> Fleeting - the dispatcher (and only the dispatcher) has enabled fleeting through
 *  the Signal 
 *  </ul>
 *  </p>
 *  <p>
 *  This also defines an enumeration of the affects of the lock.
 *  <ul>
 *  <li> None - the lock does not change a signal's indication.  It locks a turnout or prevents
 *  a signal from changing its indication
 *  <li> Halt - the lock forces a Halt indication (unless overridden)
 *  <li> Restricting - the lock forces a Restricting indication (unless a higher priority lock
 *  is in place).
 *  <li> Stop and Proceed - an obsolete form of Restricting.  It is included to distinguish
 *  dispatcher initiated locks from those initiated by the track.
 *  </ul>
 * 
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2011, 2012
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public enum EnumerationLocks {
//    /**
//     * the types of restrictive indications.  Each has an associated parameter, which
//     * describes how the lock affects the signal.
//     * <p>
//     * The comments for each lock indicate where it is added to the EnumSet:
//     * <p>
//     * B - Block Level
//     * T - Track level
//     * E - Edge level
//     * S - SecEdge level
//     * BE - Block Edge level
//     * I - Intermediate Edge level
//     * C - CP Edge level
//     * P - Points Edge level
//     */
//    DetectionLock(SpecialIndication.Halt),          // (x00001(B)) the track circuit reports occupied
//    OpposingSignalLock(SpecialIndication.Halt),     // (x00002)(E) an opposing signal has been cleared
//    ConflictingSignalLock(SpecialIndication.Halt),  // (x00004)(E) a conflicting signal has been cleared
//    SwitchLock(SpecialIndication.Halt),             // (x00008)(P) a switch is fouling
//    CTCLock(SpecialIndication.Halt),                // (x00010)(B) the signal is held at Halt because it protects CTC
//    HoldLock(SpecialIndication.Halt),               // (x00020) (I) the signal is held at Halt because of an external reason
//    Unbonded(SpecialIndication.Restricting),        // (x00040)(B) the track circuit is not signaled
//    SwitchUnlock(SpecialIndication.Restricting),    // (x00080)(P) a switch has been unlocked in the field
//    CallOn(SpecialIndication.Restricting),          // (x00100)(CP) the dispatcher has set Call-on
//    TrackWarrant(SpecialIndication.StopAndProceed), // (x00200)(CP) the dispatcher has granted a track warrant on the track circuit
//    TrackAndTime(SpecialIndication.StopAndProceed), // (x00400)(B) the dispatcher has granted track authority on the track circuit
//    OutOfService(SpecialIndication.StopAndProceed), // (x00800)(B) the dispatcher has taken the track circuit out of service
//    EntryEdge(SpecialIndication.None),              // (x01000)(BE) a route into a traffic circuit begins on this edge
//    ColorOnDetection(SpecialIndication.None),       // (x02000)(B) the Track object should color the frill on detection
//    ApproachLock(SpecialIndication.None),           // (x04000)(B)the track circuit on the signal approach is occupied
//    TimeLock(SpecialIndication.None),               // (x08000)(C) a signal protecting the track circuit is running time
//    TrafficLock(SpecialIndication.None),            // (x10000)(C) a route has been reserved through the track circuit
//    FleetingLock(SpecialIndication.None);           // (x20000) (CP) the dispatcher has turned on fleeting
//    
//    public enum SpecialIndication {
//      None,           // does not affect the signal indication
//      Halt,           // absolute Stop
//      Restricting,    // Stop and continue at reduced speed
//      StopAndProceed  // obsolete version of Restricting
//    }
//
//    /**
//     * the ctor
//     * 
//     * @param halt is true if this lock spawns a Halt indication and false if
//     * it results in a Restricting indication.
//     */
//    EnumerationLocks(SpecialIndication ind) {
//    }

	/**
	 * The locks exposed by CATS VitalLogic (@see cats.layout.vitalLogic.LogicLocks.java).
	 * They provide an interface for other programs to monitor and stimulate CATS logic.
	 */
//	DetectionLock is internal to CATS VitalLogic.  It comes from the Block detection.
//	/** (x00001) the track circuit reports occupied */
//	  DetectionLock,
	    /** (x00002) an opposing signal has been cleared */
	  OpposingSignalLock,
	    /** (x00004) a conflicting signal has been cleared */
	  ConflictingSignalLock,
	    /**(x00008) a switch is fouling  */
	  SwitchLock,
	    /** (x00010) the signal is held at Halt because of an external reason */
	  HoldLock,
	    /** (x00020) a switch has been unlocked in the field */
	  SwitchUnlock,
	    /** (x00040) a route into a traffic circuit begins on this edge (Stick Relay) */
	  DirectionOfTravel,
	    /** (x00080) the track circuit on the signal approach is occupied */
	  ApproachLock,
	    /** (x00100) a signal protecting the track circuit is running time */
	  TimeLock,
	    /** (x00200) a route has been reserved through the track circuit */
	  TrafficLock,
	    /** (x00400) a Stop hold on CTC Vital Logic because no route has been set */
	  CTCLock;
}
