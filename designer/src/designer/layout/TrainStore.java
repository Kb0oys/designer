/*
 * Name: TrainStore
 *
 * What:
 *  This class provides a repository for holding all the Trains.
 */
package designer.layout;

import designer.gui.store.AlignmentList;
import designer.gui.store.ExtraList;
import designer.gui.store.FontSpec;
import designer.gui.store.StoreTableModel;
import designer.gui.store.TimeSpec;
import designer.layout.store.FieldInfo;
import designer.layout.store.AbstractStore;
import designer.layout.store.FieldVector;
import designer.layout.store.GenericRecord;
import designer.layout.store.RecordVector;
import designer.layout.items.Train;
import designer.layout.xml.*;

/**
 * Holds all the Trains.  A helpful tip is to arrange them in departure
 * order.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2015, 2020, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class TrainStore
extends AbstractStore
implements Savable {

    /**
     * The Tag for identifying the TrainStore in the XML file.
     */
    private static final String XML_TAG = "TRAINSTORE";

    /**
     * The Tag for identifying the TrainStore edit control FieldStore.
     */
    private static final String EDIT_TAG = "TRAINEDIT";

    /**
     * The Tag for identifying the TrainStore Train Records.
     */
    private static final String DATA_TAG = "TRAINDATA";

    /**
     * are the default FieldInfos - one FieldInfo for each field.  The order of
     * constructor parameters are:
     * 1. tag
     * 2. visible flag
     * 3. column label on JTable
     * 4. edit flag
     * 5. mandatory flag
     * 6. initial column width
     * 7. alignment in JTable cell
     * 8. default value of Objects of this class
     * 9. class of objects of this class
     * The first field is critical - it references the Object being stored.
     */
    private static final FieldInfo DEFAULT_INFOS[] = {
        new FieldInfo(new String(Train.TRAIN_NAME), true, new String(Train.TRAIN_NAME), true, true, FieldInfo.WIDE_WIDTH,
                AlignmentList.DEFAULT, "", String.class),
        new FieldInfo(new String(Train.TRAIN_SYMBOL), true, new String(Train.TRAIN_SYMBOL), true, true, FieldInfo.MEDIUM_WIDTH,
                AlignmentList.DEFAULT, "", String.class),
        new FieldInfo(new String(Train.ENGINE), true, new String(Train.ENGINE), true, true, FieldInfo.NARROW_WIDTH,
                AlignmentList.DEFAULT, "", String.class),
        new FieldInfo(new String(Train.TRANSPONDING), false, new String(Train.TRANSPONDING), true, true, FieldInfo.MEDIUM_WIDTH,
                AlignmentList.DEFAULT, "", String.class),
        new FieldInfo(new String(Train.CABOOSE), true, new String(Train.CABOOSE), true, true, FieldInfo.NARROW_WIDTH,
                AlignmentList.DEFAULT, "", String.class),
        new FieldInfo(new String(Train.CREW), true, new String(Train.CREW), true, true, FieldInfo.MEDIUM_WIDTH,
                AlignmentList.DEFAULT, "", ExtraList.class),
        new FieldInfo(new String(Train.ONDUTY), false, new String(Train.ONDUTY), true, true, FieldInfo.NARROW_WIDTH,
                AlignmentList.DEFAULT, "", TimeSpec.class),
        new FieldInfo(new String(Train.DEPARTURE), false, new String(Train.DEPARTURE), true, true, FieldInfo.NARROW_WIDTH,
                AlignmentList.DEFAULT, "", TimeSpec.class),
        new FieldInfo(new String(JobStore.FONT), false, new String(JobStore.FONT), true, true, FieldInfo.MEDIUM_WIDTH,
                AlignmentList.DEFAULT, FontList.FONT_LABEL, FontSpec.class),
        new FieldInfo(new String(Train.LENGTH), false, new String(Train.LENGTH), true, true, FieldInfo.MEDIUM_WIDTH,
                AlignmentList.DEFAULT, "", Integer.class),
        new FieldInfo(new String(Train.WEIGHT), false, new String(Train.WEIGHT), true, true, FieldInfo.MEDIUM_WIDTH,
                AlignmentList.DEFAULT, "", Integer.class),
        new FieldInfo(new String(Train.CARS), false, new String(Train.CARS), true, true, FieldInfo.MEDIUM_WIDTH,
                AlignmentList.DEFAULT, "", Integer.class),
        new FieldInfo(new String(Train.AUTOTERMINATE), false, new String(Train.AUTOTERMINATE), true, true, FieldInfo.NARROW_WIDTH,
                AlignmentList.DEFAULT, new Boolean(false), Boolean.class),
        new FieldInfo(new String(Train.LABELBACKGROUND), false, new String(Train.LABELBACKGROUND), true, true, FieldInfo.NARROW_WIDTH,
                AlignmentList.DEFAULT, new Boolean(false), Boolean.class)
   };


    /**
     * the singleton, which is known by all clients.
     */
    public static TrainStore TrainKeeper = new TrainStore();

    /**
     * the constructor.
     */
    public TrainStore() {
        super(XML_TAG, EDIT_TAG, DATA_TAG, DEFAULT_INFOS);
        FieldInfoStore.getFieldInfo(Train.TRAIN_SYMBOL).makeRecordKey();
        init();
        Train.init();
    }

    /**
     * adds a new Train.  This method is for compatibility with Versions prior
     * to 16.
     *
     * @param tag is the Tag on the Train Record.
     * @param train is the Train to be added.
     */
    public void add(String tag, Train train) {
        DataStore.setObject(tag, train);
    }

    /**
     * constructs a StoreTableModel for the concrete class
     * @param data is a copy of the DataStore
     * @param format is a copy of the FieldInfoStore
     * @return a concrete StoreTableModel for the concrete Store.
     */
    protected StoreTableModel createModel(RecordVector<GenericRecord> data,
            FieldVector format) {
        return new StoreTableModel(data, format, this);
    }

    @Override
    public String doneXML() {
  	  // the type for the Transponding field changed.  This corrects it
  	  // on any TrainStores loaded.
  	  FieldInfo trans = FieldInfoStore.getFieldInfo(Train.TRANSPONDING);
  	  if (trans != null) {
  		  trans.replacePair(new designer.layout.store.FieldPair(FieldInfo.DEFAULT_TAG, ""));
  		  trans.replacePair(new designer.layout.store.FieldPair(FieldInfo.CLASS_TAG, String.class));
  	  }
      return null;
    }

}
/* @(#)TrainStore.java */
