/*
 * Name: AbstractListManager
 *
 * What:
 *  This class provides an abstract facility for holding lists of simple data structures.
 *  The list does not appear explicitly in the XML file, but its elements do.  Thus, the
 *  doneXML() method of each element must add themselves to the concrete list.
 */
package designer.layout;

import java.util.Enumeration;
import java.util.Vector;

import org.jdom2.Element;

import designer.gui.jCustom.AbstractManagerTableModel;
import designer.layout.store.FieldPair;
import designer.layout.store.GenericRecord;
import designer.layout.store.RecordVector;
import designer.layout.xml.Savable;

/**
 *  This class provides an abstract facility for holding lists of simple data structures.
 *  The list does not appear explicitly in the XML file, but its elements do.  Thus, the
 *  doneXML() method of each element must add themselves to the concrete list.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2009, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
abstract public class AbstractListManager implements ListManager, Savable {
    
    /**
     * The list being wrapped
     */
    protected Vector<Object> TheList;

    /**
     * A copy of the list to allow editing to be
     * cancelled.
     */
    protected Vector<Object> TheListCopy;
    
    
    /**
     *  a true flag means that it has been saved,
     */
    protected boolean Saved = true;
    
    /**
     * the constructor.
     */
    public AbstractListManager() {
        TheList = new Vector<Object>();
        init();
        registerFactory();
    }
    
    /**
     * adds a new list element.
     *
     * @param temp is the element to be added.
     * @return true if the element was added
     */
    public boolean add(Savable temp) {
        TheList.add(temp);
        Saved = false;
        return true;
    }

    /**
     * returns the number of elements that have been defined.
     * @return the number of elements.
     */
    public int size() {
        return TheList.size();
    }
 
    /**
     * creates an Enumeration over the elements.
     * @return the Enumeration.
     */
    private Enumeration<Object> elements() {
        return TheList.elements();
    }
    
    /**
     * locates an element by the field by which it is known
     * @param key is the value used as the search key
     * @return the first element found with a matching key
     * or null.
     */
    public AbstractListElement findElement(String key) {
        AbstractListElement temp;
        if (key != null) {
            for (Enumeration<Object> iter = TheList.elements(); iter.hasMoreElements(); ) {
                temp = (AbstractListElement) iter.nextElement();
                if (key.equals(temp.getElementName())) {
                    return temp;
                }
            }
        }
        return null;
    }
    
    /**
     * creates a Vector for listing the elements in a JComboBox
     * @return the Vector of element names.
     */
    public Vector<String> getListList() {
        Vector<String> tags = new Vector<String>();
        AbstractListElement temp;
        
        for (Enumeration<Object> iter = TheList.elements(); iter.hasMoreElements(); ) {
            temp = (AbstractListElement) iter.nextElement();
            tags.add(temp.getElementName());
        }
        tags.add("");
        return tags;
    }

    /**
     * generates a copy of the list, then uses it to
     * create an AbstractManagerTableModel for editing.
     * @return the AbstractManagerTableModel
     */
    abstract public AbstractManagerTableModel createModel(); 

    /**
     * makes a copy of the list
     * @return a copy of the list
     */
    public Vector<Object> makeCopy() {
        TheListCopy = new Vector<Object>();
        for (Enumeration<Object> iter = TheList.elements(); iter.hasMoreElements(); ) {
            TheListCopy.add(((AbstractListElement) iter.nextElement()).copy());
        }
        return TheListCopy;
    }
    
    /**
     * commits the changes in the JTable.
     */
    public void commit() {
        TheList = TheListCopy;
        TheListCopy = null;
        Saved = false;
    }
       
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        if (Saved) {
            for (Enumeration<Object> iter = elements(); iter.hasMoreElements(); ) {
                if (!((Savable) iter.nextElement()).isSaved()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        return null;
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        for (Enumeration<Object> iter = elements(); iter.hasMoreElements(); ) {
            ((Savable) iter.nextElement()).putXML(parent);
        }
        Saved = true;
        return null;
    }

    /**
     * conditionally dumps the contents of the List to an XML file.  For each
     * element in the list, it asks the RecordStore parameter to find a record
     * that uses the key name of the element.  If the RecordStore uses it, then
     * the element is written to the XML file.  This method is called by 
     * individual RecordStores (Trains, Crews, Jobs) when saving the individual
     * Store.
     * @param parent is the XML Element that will contain the selected list items.
     * @param owner is the RecordStore that has possible references to items in the List.
     * @param tagField is the tag value of the property where the reference might exist.
     * @return null if there were no problems.  An error string if there is a problem.
     */
    public String putXML(Element parent, RecordVector<GenericRecord> owner, String tagField) {
        AbstractListElement element;
        FieldPair pair = new FieldPair(tagField, null);
        String msg = null;
        for (Enumeration<Object> iter = elements(); iter.hasMoreElements(); ) {
            element = ((AbstractListElement) iter.nextElement());
            pair.FieldValue = element.getElementName();
            if (owner.search(pair, null) != null) {
                msg = element.putXML(parent);
                if (msg != null) {
                    break;
                }
            }
        }
        Saved = true;
        return msg;
    }

    /**
     * clears out the manager's state.
     */
    public void init() {
        TheList.clear();
        Saved = true;
    }

    /**
     * registers the factory with the document reader
     *
     */
    abstract public void registerFactory();
}
/* @(#)AbstractListManager.java */
