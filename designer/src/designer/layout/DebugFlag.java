/*
 * Name: DebugFlag.java
 * 
 * What:
 *  DebugFlag is a Singleton object with a boolean value.  It is set
 *  to true if CATS should print some debug information.  The information
 *  is not permanently attached to the flag; thus, is expected to vary
 *  over time and meet temporary needs for information on what is going on.
 */
package designer.layout;

import designer.gui.BooleanGui;

/**
 *  DebugFlag is a Singleton object with a boolean value.  It is set
 *  to true if CATS should print some debug information.  The information
 *  is not permanently attached to the flag; thus, is expected to vary
 *  over time and meet temporary needs for information on what is going on.
 * <p>Title: cats</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class DebugFlag
extends BooleanGui {

	/**
	 * is the tag for identifying a DebugFlag Object in the XML file.
	 */
	static final String XMLTag = "DEBUGFLAG";

	/**
	 * is the label on the JCheckBoxMenuItem
	 */
	static final String Label = "General Purpose Debug Flag";

	/**
	 * constructs the factory.
	 */
	public DebugFlag() {
		super(Label, XMLTag, false);
	}
}
/* @(#)DebugFlag.java */
