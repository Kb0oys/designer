/* Name NamedBean.java
 *
 * What:
 *  This Interface defines methods for
 *  <ul>registering a property change listener with an object
 *  <li>unregistering a property change listener with an object
 *  <li>broadcasting mchange evetns to the listeners
 *  </ul>
 *  <p>
 *  The code is simplified from the JMRI NamedBean Interface
 */
package designer.layout;

/**
 *  This Interface defines methods for
 *  <ul>
 *  <li>registering a property change listener with an object</li>
 *  <li>unregistering a property change listener with an object</li>
 *  <li>broadcasting mchange evetns to the listeners</li>
 *  </ul>
 *  <p>
 *  The code is simplified from the JMRI NamedBean Interface
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface NamedBean {
    /**
     * Request a call-back when a bound property changes.
     * Bound properties are the known state, commanded state, user and system names.
     * @param l something that wants to listen for changes
     */
    public void addPropertyChangeListener(java.beans.PropertyChangeListener l);

    /**
     * Remove a request for a call-back when a bound property changes.
     * @param l something that no longer wants to listen for changes
     */
    public void removePropertyChangeListener(java.beans.PropertyChangeListener l);

    /**
     * Number of current listeners. May return -1 if the 
     * information is not available for some reason.
     * @return the number of listeners
     */
    public int getNumPropertyChangeListeners();

    /**
     * Deactivate this object, so that it releases as many
     * resources as possible and no longer effects others.
     *<P>
     * For example, if this object has listeners, after
     * a call to this method it should no longer notify
     * those listeners.  Any native or system-wide resources
     * it maintains should be released, including threads, files, etc.
     * <P>
     * It is an error to invoke any other methods on this 
     * object once dispose() has been called.  Note, however,
     * that there is no guarantee about behavior in that case.
     * <P>
     * Afterwards, references to this object may still exist
     * elsewhere, preventing its garbage collection.  But it's formally
     * dead, and shouldn't be keeping any other objects alive.
     * Therefore, this method should null out any references to
     * other objects that this NamedBean contained.
     */
    public void dispose();  // remove _all_ connections!
   
    /**
     * Attach a key/value pair to the 
     * NamedBean, which can be retrieved later.
     * These are not bound properties as yet, 
     * and don't throw events on modification.
     * Key must not be null.
     * @param key is the key for identifying which property is to be changed
     * @param value is the new value of the property
     */
    public void setProperty(Object key, Object value);
    
    /**
     * Retrieve the value associated with a key.
     * If no value has been set for that key, returns null.
     * @param key is the key for identifying which property is to be changed
     * @return the value of the property
     */
    public Object getProperty(Object key);

}
/* @(#)NamedBean.java */
