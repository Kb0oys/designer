/* Name: FastClock.java
 *
 * What;
 *  FastClock is a Singleton object with a boolean value.  It is set
 *  to true if all logging and timing is done via a fast clock and
 *  false if the real time clock is used.
 */
package designer.layout;
import designer.gui.BooleanGui;

/**
 *  FastClock is a Singleton object with a boolean value.  It is set
 *  to true if all logging and timing is done via a fast clock and
 *  false if the real time clock is used.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2005, 2007</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class FastClock
extends BooleanGui {

    /**
     * is the tag for identifying a FastClock Object in the XMl file.
     */
    static final String XMLTag = "FASTCLOCK";

    /**
     * is the singleton.
     */
    public static FastClock TheClock;
  
  /**
   * is the label on the JCheckBoxMenuItem
   */
  static final String Label = "Fast Clock";

  /**
   * constructs the factory.
   */
  public FastClock() {
    super(Label, XMLTag, false);
    TheClock = this;
  }
}
/* @(#)FastClock.java */
