/* Name: Layout.java
 *
 * What:
 *  This class is a singleton that contains the physical structure of the
 *  layout.  It uses Nodes to glue the track segments together.
 */
package designer.layout;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Enumeration;

import javax.swing.JMenu;

import designer.gui.EditPane;
import designer.layout.items.Edge;
import designer.layout.items.Section;
import designer.layout.items.TrackGroup;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 * is a Singleton class for containing the physical structure of the
 * layout.  It provides the two dimensional relationship between segments
 * of track.
 * <p>
 * Each row and each column is held in a ring list of Nodes.
 * Each Node on the X and Y axis are headers, with one link to its
 * adjoining header and another link to the first track Node in the column or
 * row.  The header nodes do not contain track segments, but hold the column
 * or row number.  The node in the upper left corner (X=0, Y=0) is
 * known by this class.  It is unique in that neither of its links refer
 * to track nodes, but only other header nodes.  This illustrates a design
 * invariant - each row and each column has a header Node.  Thus, "one"
 * indexing is used - the first track column is numbered "1" (not "0") and the
 * first track row is numbered "1" (not "0") because the headers occupy the "0"
 * index.
 * <p>
 * Each track Node is a member of two rings - one for the column and one for
 * the row.  Thus, a tightly coupled grid is created with a Node at each
 * row-column intersection.  It was possible to use a sparse matrix in
 * which there were nodes for only the intersections that hold segments,
 * but that made copy and paste operations more difficult because the
 * missing Nodes would have to be filled in.  Furthermore, special code would
 * be needed to let the user use an unallocated Node.
 *
 * Interior headers are never deleted - only the headers at the extreme ends.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class Layout
implements XMLEleObject, Savable {
    /**
     * the current Layout for use by other objects.
     */
    static private Layout RRLayout;
    
    /**
     * the XML tag for the global layout geometry.
     */
    static public String LayoutTag = "TRACKPLAN"; // The XML name for an
    
    /**
     * the XML attribute tag for identifying the number of columns.
     */
    static String ColTag = "COLUMNS";
    
    /**
     * the XML attribute tag for identifying the number of rows.
     */
    static String RowTag = "ROWS"; // The XML attribute name for the rows
    
    /**
     * the place where the layout is being drawn.
     */
    static EditPane ThePane;
    
    /**
     * a linkage to the Menu bar item for showing Section details.  This
     * item is enabled only if a single Node is selected.
     */
    static JMenu Details;
    
    /**
     *  the upper left header.  The column header for the column
     *  of row headers and the row header for the row of column
     *  headers
     */
    private Node Root;
    
    /**
     * the number of columns (excluding the row headers).
     */
    int Columns;
    
    /**
     * the number of rows (excluding the column headers).
     */
    int Rows;
    
    /**
     * a description of a group of selected Nodes.
     * This is not a true Rectangle because the point
     * is the coordinates of the anchor and the width
     * and height may be negative; thus. the anchor
     * point may have to computed when the Selection
     * is accessed.
     */
    Rectangle Selected;
    
    /**
     * The Node the cursor is on.
     */
    Node Cursor;
    
    /**
     * true if the layout has been saved.
     */
    boolean Saved;
    
    /**
     * constructs a Layout of the requested size; sets the cursor to the
     * upper left corner; and sets the Layout reference to the newly
     * constructed Layout.
     *
     * @param rows is the number of rows
     * @param cols is the number of columns
     */
    public Layout(int rows, int cols) {
        Node iter1;
        Node iter2;
        int index;
        
        Root = createBlk(cols + 1, rows + 1);
        
        // Label the column headers and link the bottom row to the headers
        for (iter2 = Root; iter2.VLink != null; iter2 = iter2.VLink) {
        }
        iter1 = Root;
        for (index = 0; index < cols; ++index) {
            iter1.Position = index;
            iter1.Contents = null;
            iter1.MyTile = null;
            iter2.VLink = iter1;
            iter2 = iter2.HLink;
            iter1 = iter1.HLink;
        }
        iter1.Position = index;
        iter1.Contents = null;
        iter1.MyTile = null;
        iter2.VLink = iter1;
        
        // Label the row headers and link the rightmost column to the headers
        iter2 = Root;
        for (index = 0; index <= rows; ++index) {
            iter2.Position = index;
            iter2.Contents = null;
            iter2.MyTile = null;
            iter1.HLink = iter2;
            iter2 = iter2.VLink;
            iter1 = iter1.VLink;
        }
        
        Rows = rows;
        Columns = cols;
        Cursor = Root.VLink.HLink;
        Selected = new Rectangle(Cursor.getColumn(), Cursor.getRow(), 1, 1);
        Cursor.getTile().setSelect(true);
        RRLayout = this;
        Saved = true;
    }

    /**
     * tells the Layout manager that a new XML file has been opened for
     * reading.  The Layout manager then clears the under construction
     * member, which will be set by the LayoutFactory when a TRACKPLAN
     * tag is encountered (in the process of creating a new Layout).
     *
     */
    public static void startConstruction() {
        RRLayout = null;
    }
    
    /**
     * returns a layout that is being read in from a file.  This could
     * be null, if no TRACKPLAN tag was encountered.
     * @return the Layout being read in or the last read in
     */
    public static Layout getConstruction() {
        return RRLayout;
    }
    
    /**
     * returns the number of columns and rows in the layout.
     *
     * @return a Dimension where the width is the number of columns and
     *         the height is the number of rows.
     */
    
    public Dimension getSize() {
        return new Dimension(Columns, Rows);
    }
    
    /**
     * returns the Nodes in the layout that have been selected by the user.
     * <p>
     * The Nodes must form a rectangle.
     *
     * @return a Rectangle describing the Node(s) under the cursor.
     */
    public Rectangle getSelection() {
        int x = Selected.x;
        int y = Selected.y;
        int height = Selected.height;
        int width = Selected.width;
        if (Selected.height < 0) {
            y += height;
            height = -height;
        }
        if (Selected.width < 0) {
            x += width;
            width = -width;
        }
        return new Rectangle(x, y, width, height);
    }
    
    /**
     * selects a rectangular block of Nodes.  This routine assumes the selected
     * Nodes all exist in the Layout.
     *
     * @param sel describes the Nodes being selected
     */
    public void setSelection(Rectangle sel) {
        Node n;
        if (sel == null) {
            clrSelected();
            Selected = null;
        }
        else {
            Node vlink;
            vlink = locate(sel.y, sel.x);
            Cursor = vlink;
            for (int row = 0; row < sel.height; ++row) {
                n = vlink;
                for (int col = 0; col < sel.width; ++col) {
                    n.getTile().setSelect(true);
                    n = n.HLink;
                }
                vlink = vlink.VLink;
            }
            Selected = new Rectangle(sel);
        }
        enableDetails();
    }
    
    /**
     * tells the selected Nodes that they are no longer selected.
     * This version de-selects all selected Node.
     */
    private void clrSelected() {
        clrSelected(getSelection());
    }
    
    /**
     * tells the selected Nodes that they are no longer selected.
     * This version de-selects a specified block of Nodes.
     *
     * @param blk is the block being de-selected.
     */
    private void clrSelected(Rectangle blk) {
        Node n;
        if (blk != null) {
            for (int row = 0; row < blk.height; ++row) {
                n = locate(blk.y + row, blk.x);
                for (int col = 0; col < blk.width; ++col) {
                    n.getTile().setSelect(false);
                    n = n.HLink;
                }
            }
        }
    }
    
    /**
     * adjusts the bounds on selection area.
     *
     * @param edge is a Node on the new perimeter of the selection area.
     *
     * @see Node
     */
    void newSelBounds(Node edge) {
        int newX = edge.getColumn();
        int newY = edge.getRow();
        int oldX = getXY().x;
        int oldY = getXY().y;
        int deltaX = newX - Selected.x;
        int deltaY = newY - Selected.y;
        
        if (Selected == null) {
            moveCursor(newX, newY);
        }
        else {
            /*
             * We know several points - the four corners of the selection area
             * and the new corner.
             * 1. if the new corner is within the column span of the selected area,
             *    then the selected area will shrink.  If the distance from the
             *    anchor is negative, then columns will be removed from the left.
             *    If the distance is positive, then columns will be removed from the
             *    right.
             * 2. if the corner is not within the column span of the selected area
             *    then the area will grow columns.  If the distance from the anchor
             *    is negative then columns will be added on the left.  If the
             *    distance is positive, then columns will be added on the right.
             * 3. if the new corner is within the row space of the selected area
             *    then the selected area will lose rows.  If the distance from the
             *    anchor is negative, then rows will be removed from the top.  If
             *    the distance is positive, then rows will be removed from the
             *    bottom.
             * 4. if the new corner is not within the row span of the selected area,
             *    then the selected area will gain rows.  If the distance from the
             *    anchor is negative, then rows will be added at the top.  If the
             *    distance is positive, then rows will be added at the bottom.
             *
             * Warning: width and height cannot be 0! (-1 == 1, which means the
             * selection is 1 column wide).
             */
            
            // Unselect the old rectangle
            clrSelected();
            
            // determine the new selection rectangle
            if (newX < oldX) {
                deltaX = oldX - newX + 1;
            }
            else {
                deltaX = newX - oldX + 1;
                newX = oldX;
            }
            if (newY < oldY) {
                deltaY = oldY - newY + 1;
            }
            else {
                deltaY = newY - oldY + 1;
                newY = oldY;
            }
            setSelection(new Rectangle(newX, newY, deltaX, deltaY));
        }
    }
    
    /**
     * returns the contents of the Node under the cursor.
     *
     * @return the Section under the cursor or null.
     *
     * @see designer.layout.items.Section
     */
    public Section getCursorSection() {
        if (Cursor != null) {
            return Cursor.getContents();
        }
        return null;
    }
    
    /**
     * retrieves the current cursor location.
     *
     * @return a Point defining the Cursor location.
     */
    public Point getXY() {
        return new Point(Cursor.getColumn(), Cursor.getRow());
    }
    
    /**
     * moves the Cursor to a specific Node.
     *
     * @param point defines the new Cursor location
     *
     */
    public void gotoXY(Point point) {
        if ( (point.x > 0) && (point.x <= Columns) && (point.y > 0) &&
                (point.y <= Rows)) {
            moveCursor(point);
        }
    }
    
    /**
     * counts and labels the column headers.  This sets the column headers as
     * headers.
     */
    private void countCols() {
        Node col = Root;
        int cnt = 0;
        while (col != null) {
            col.Position = cnt++;
            col = col.HLink;
            if (col == Root) {
                break;
            }
        }
        Columns = cnt - 1;
    }
    
    /**
     * counts and labels the row headers.  This sets the row headers as
     * headers.
     */
    private void countRows() {
        Node row = Root;
        int cnt = 0;
        while (row != null) {
            row.Position = cnt++;
            row = row.VLink;
            if (row == Root) {
                break;
            }
        }
        Rows = cnt - 1;
    }
    
    /**
     * creates a new block of Nodes.  The Nodes are initialized to be track
     * nodes, so the caller has the responsibility to convert any nodes to
     * header nodes.  The block created has all internal Nodes linked
     * together, except the last column and row have no links.
     *
     * @param width  is the number of columns
     * @param height is the number of rows
     *
     * @return a reference to the upper left Node, so at least one Node is
     *         created.
     *
     * @see Node
     */
    public Node createBlk(int width, int height) {
        Node corner = new Node();
        Node row;
        Node row2;
        Node col;
        
        corner.Position = Node.TRACKNODE;
        corner.HLink = corner.VLink = null;
        row = corner;
        
        // Construct the first column.
        for (int y = 1; y < height; ++y) {
            row.VLink = new Node();
            
            row = row.VLink;
            row.Position = Node.TRACKNODE;
        }
        row.VLink = null;
        
        col = corner;
        for (int x = 1; x < width; ++x) {
            
            col.HLink = new Node();
            col = col.HLink;
            col.Position = Node.TRACKNODE;
            row = col;
            
            // construct the next column
            for (int y = 1; y < height; ++y) {
                row.VLink = new Node();
                row = row.VLink;
                row.Position = Node.TRACKNODE;
            }
            row.VLink = null;
        }
        col.HLink = null;
        
        // Now, tie the rows together.  The first row has already been linked
        for (col = corner; col.HLink != null; col = col.HLink) {
            row2 = col.HLink.VLink;
            for (row = col.VLink; row != null; row = row.VLink) {
                row.HLink = row2;
                row2 = row2.VLink;
            }
        }
        
        // Finally, null out the last column's HLink.
        for (row = col.VLink; row != null; row = row.VLink) {
            row.HLink = null;
        }
        return corner;
    }
    
    /**
     * finds a specific Node on the layout.
     *
     * @param y is the Y (vertical) coordinate of the desired Node.
     * @param x is the X (horizontal) coordinate of the desired Node.
     *
     * @return the Node if it exists; or null, if it doesn't.
     *
     * @see Node
     */
    public Node locate(int y, int x) {
        Node n;
        if ( (x > Columns) || (y > Rows)) {
            return null;
        }
        
        for (n = Root; n.Position != x; n = n.HLink) {
        }
        for (int row = 0; row != y; ++row) {
            n = n.VLink;
        }
        return n;
    }
    
    /**
     * adds new columns to the grid.  This function assumes that there is
     * always at least one row header or nothing will be inserted, but it
     * will create the column header.
     *
     * @param left is a Node in the column to the left of the inserted columns.
     * @param cols is the number of columns being inserted.
     *
     * @return the upper left Node of the inserted block or null, if left is
     * not in a column chain.
     *
     * @see Node
     */
    public Node newColumn(Node left, int cols) {
        Node newBlk = createBlk(cols, Rows);
        
        addColumn(left, newBlk);
        return newBlk;
    }
    
    /**
     * adds new rows to the grid.  This function assumes that there is
     * always at least one column header or nothing will be inserted, but it
     * will create the row header.
     *
     * @param above is a Node in the row above the inserted rows.
     * @param rows is the number of rows being inserted.
     *
     * @return the upper left Node of the inserted block or null, if above is
     * not in a row chain.
     *
     * @see Node
     */
    public Node newRow(Node above, int rows) {
        Node newBlk = createBlk(Columns, rows);
        
        addRow(above, newBlk);
        return newBlk;
    }
    
    /**
     * inserts a block of Nodes, as a column.  This routine adds column
     * headers to accomodate the new columns.  It assumes that the inserted
     * block has exactly the same number of non-header rows as in the
     * layout (Rows) and that all nodes are connected.  The HLink goes
     * to the right neighbor (except the rightmost) and the VLink goes to
     * the lower neighbor (except the bottom row).
     *
     * @param where is a Node in the column immediately to the left of
     *              where the block will be inserted.
     * @param block is the block of nodes constituting the new columns.
     *
     * @see Node
     */
    public void addColumn(Node where, Node block) {
        int cols; // the number of columns in the inserted block
        Node newHdr;
        Node rHdr;
        Node n;
        Node bot;
        /*
         * Count the number of columns being inserted.
         */
        cols = 1;
        for (rHdr = block; rHdr.HLink != null; rHdr = rHdr.HLink) {
            ++cols;
        }
        
        /*
         * Find the left column, bottom row in the inserted block.
         */
        for (bot = block; bot.VLink != null; bot = bot.VLink) {
            ;
        }
        
        /*
         * Create a new row of headers and link them on top of the inserted block.
         * Link the bottom row to the header row.
         */
        newHdr = createBlk(cols, 1);
        for (rHdr = newHdr; rHdr.HLink != null; rHdr = rHdr.HLink) {
            rHdr.VLink = block;
            bot.VLink = rHdr;
            block = block.HLink;
            bot = bot.HLink;
        }
        rHdr.VLink = block;
        bot.VLink = rHdr;
        
        /*
         * Find the column header for the insertion point.  If it is a
         * header, then it could be a column header or a row header.  If the
         * former, then we are done.  If the latter, then Root is the
         * column header.
         */
        
        while (where.Position == Node.TRACKNODE) {
            where = where.VLink;
        }
        if (where.VLink.Position != Node.TRACKNODE) {
            where = Root;
        }
        
        /*
         * Finally stitch the insertion block in between the insertion point
         * and its right hand neighbor.
         */
        n = newHdr;
        do {
            if (where.Position == Node.TRACKNODE) {
                where.Contents.unlinkAdjacent(Edge.RIGHT);
            }
            rHdr.HLink = where.HLink;
            where.HLink = n;
            where = where.VLink;
            n = n.VLink;
            rHdr = rHdr.VLink;
        }
        while (n != newHdr);
        countCols();
        
        // Connect the internal Nodes of the new block of Nodes.
        linkBlk(newHdr.VLink, cols, Rows);
        
        // check the columns bordering the inserted block.
//        where = newHdr.getLeft().VLink;
//        rHdr = rHdr.VLink;
//        for (int y = 0; y < Rows; ++y) {
//            if (where.Position == Node.TRACKNODE) {
//                where.getContents().linkEdge(Edge.RIGHT);
//            }
//            if (rHdr.Position == Node.TRACKNODE) {
//                rHdr.getContents().linkEdge(Edge.LEFT);
//            }
//        }
        if (ThePane != null) {
            ThePane.reformat(this);
        }
        Saved = false;
    }
    
    /**
     * inserts a block of Nodes, as a column.  This routine adds column
     * headers to accomodate the new columnss.  It assumes that the inserted
     * block has exactly the same number of non-header rows as in the
     * layout (Rows) and that all nodes are connected.  The HLink goes
     * to the right neighbor (except the rightmost) and the VLink goes to
     * the lower neighbor (except the bottom row).
     *
     * @param where is the column where the block will be inserted.
     * @param block is the block of nodes constituting the new columns.
     *
     * @see Node
     */
    public void addColumn(int where, Node block) {
        addColumn(locate(0, where), block);
    }
    
    /**
     * inserts a block of Nodes, as a row.  This routine adds row
     * headers to accomodate the new rows.  It assumes that the inserted
     * block has exactly the same number of non-header columns as in the
     * layout (Columns) and that all nodes are connected.  The HLink goes
     * to the right neighbor (except the rightmost) and the VLink goes to
     * the lower neighbor (except the bottom row).
     *
     * @param where is a Node in the row immediately above
     *              where the block will be inserted.
     * @param block is the block of nodes constituting the new rows.
     *
     * @see Node
     */
    public void addRow(Node where, Node block) {
        int rows; // the number of rows in the inserted block
        Node newHdr;
        Node bHdr;
        Node n;
        Node top;
        /*
         * Count the number of rows being inserted.
         */
        rows = 1;
        for (bHdr = block; bHdr.VLink != null; bHdr = bHdr.VLink) {
            ++rows;
        }
        
        /*
         * Find the right column, top row in the inserted block.
         */
        for (top = block; top.HLink != null; top = top.HLink) {
            ;
        }
        
        /*
         * Create a new column of headers and link them left of the inserted block.
         * Link the right column to the header row.
         */
        newHdr = createBlk(1, rows);
        for (bHdr = newHdr; bHdr.VLink != null; bHdr = bHdr.VLink) {
            bHdr.HLink = block;
            top.HLink = bHdr;
            block = block.VLink;
            top = top.VLink;
        }
        bHdr.HLink = block;
        top.HLink = bHdr;
        
        /*
         * Find the row header for the insertion point.  If it is a
         * header, then it could be a column header or a row header.  If the
         * latter, then we are done.  If the former, then Root is the
         * row header.
         */
        
        while (where.Position == Node.TRACKNODE) {
            where = where.HLink;
        }
        if (where.HLink.Position != Node.TRACKNODE) {
            where = Root;
        }
        
        /*
         * Finally stitch the insertion block in between the insertion point
         * and its lower neighbor.
         */
        n = newHdr;
        do {
            if (where.Position == Node.TRACKNODE) {
                where.Contents.unlinkAdjacent(Edge.BOTTOM);
            }
            bHdr.VLink = where.VLink;
            where.VLink = n;
            where = where.HLink;
            n = n.HLink;
            bHdr = bHdr.HLink;
        }
        while (n != newHdr);
        countRows();
        
        // Connect the internal Nodes of the new block of Nodes.
        linkBlk(newHdr.HLink, Columns, rows);
        
        // check the rows bordering the inserted block.
//        where = newHdr.getAbove().HLink;
//        bHdr = bHdr.HLink;
//        for (int x = 0; x < Columns; ++x) {
//            if (where.Position == Node.TRACKNODE) {
//                where.getContents().linkEdge(Edge.BOTTOM);
//            }
//            if (bHdr.Position == Node.TRACKNODE) {
//                bHdr.getContents().linkEdge(Edge.TOP);
//            }
//        }
        
        if (ThePane != null) {
            ThePane.reformat(this);
        }
        Saved = false;
    }
    
    /**
     * inserts a block of Nodes, as a row.  This routine adds row
     * headers to accomodate the new rows.  It assumes that the inserted
     * block has exactly the same number of non-header columns as in the
     * layout (Columns) and that all nodes are connected.  The HLink goes
     * to the right neighbor (except the rightmost) and the VLink goes to
     * the lower neighbor (except the bottom row).
     *
     * @param where is the row where the block will be inserted.
     * @param block is the block of nodes constituting the new rows.
     *
     * @see Node
     */
    public void addRow(int where, Node block) {
        addRow(locate(where, 0), block);
    }
    
    /**
     * deletes a group of columns from the grid.  This is the insecure version
     * because the Node may not be on the layout.
     *
     * @param left is a Node in the left column of the block to be deleted.
     * @param cols is the number of columns to be deleted.  If there are fewer
     * columns to the right of the deletion point thean cols, then fewer
     * columns are deleted.
     *
     * @return a reference to the upper left column of the columns deleted
     * (still in a block) so that they can be placed in a history trail for
     * undo.  If the left column node is null, then return null.  If performing
     * the deletion will remove all columns, then return null.
     *
     * @see Node
     */
    public Node delColumn(Node left, int cols) {
        Node del = null; // the node in the upper left corner of the deleted block
        Node right;
        Node last;
        int i;
        
        if (left != null) {
            /*
             * Find the column header.  If the Node to the left is a header, then
             * we are deleting the first column (because the public routines do
             * not know about headers).
             */
            if (left.Position != Node.TRACKNODE) {
                left = Root;
            }
            else {
                while (left.Position == Node.TRACKNODE) {
                    left = left.VLink;
                }
            }
            
            // Do not delete more columns than exist and also leave at least one
            // column.
            cols = Math.min(cols, Columns - left.Position);
            if (cols == Columns) {
                --cols;
            }
            
            // Remove connections between the deleted block and the rest.
            unlinkBlk(left.HLink.VLink, cols, Rows);
            
            last = left;
            /*
             * Find the rightmost column to be deleted.  Do not delete more columns
             * than there are.
             */
            right = left;
            for (i = 0; i < cols; ++i) {
                right = right.HLink;
            }
            del = left.HLink.VLink;
            
            // Remove the block of columns.
            while (left.VLink != last) {
                left.HLink = right.HLink;
                right.HLink = null;
                left = left.VLink;
                right = right.VLink;
            }
            last = left.HLink;
            left.HLink = right.HLink;
            right.HLink = null;
            
            // null out the VLink on the bottom row
            while (last != right) {
                last.VLink = null;
                last = last.HLink;
            }
            last.VLink = null;
            
            countCols();
            
            // finally join any adjacent tracks between the two columns stitched
            // together.
            left = left.VLink; // back at the header.
            for (last = left.VLink; last != left; last = last.VLink) {
                if (last.Position == Node.TRACKNODE) {
                    last.Contents.hideSec();
                    last.Contents.linkEdge(Edge.RIGHT);
                    last.Contents.showSec();
                }
                if (last.HLink.Position == Node.TRACKNODE) {
                    last.HLink.Contents.hideSec();
                    last.HLink.Contents.linkEdge(Edge.LEFT);
                    last.HLink.Contents.showSec();
                }
            }
        }
        
        ThePane.reformat(this);
        Saved = false;
        
        return del;
    }
    
    /**
     * deletes a group of columns from the grid.  This is the robust form of this
     * operation because it deletes the columns selected; thus, they must be in
     * the layout.
     *
     * @param col is the index of the column being deleted.
     * @param width is the number of columns to be deleted.
     *
     * @return a reference to the upper left column of the columns deleted
     * (still in a block) so that they can be placed in a history trail for
     * undo.  If there is no such column, then return null.  If deleting the
     * block will delete all the columns, then return null.
     *
     * @see Node
     */
    public Node delColumn(int col, int width) {
        if (col <= Columns) {
            return delColumn(locate(1, col - 1), width);
        }
        return null;
    }
    
    /**
     * deletes a group of rows from the grid.  This is the insecure version
     * because the Node may not be on the layout.
     *
     * @param above is a Node in the row above the block to be deleted.
     * @param rows is the number of rows to be deleted.
     *
     * @return a reference to the upper left row of the rows deleted
     * (still in a block) so that they can be placed in a history trail for
     * undo.  If the upper left node is null, then return null.  If performing
     * the deletion will remove all rows, then return null.
     *
     * @see Node
     */
    public Node delRow(Node above, int rows) {
        Node del = null; // reference to the upper left Node in the deleted block.
        Node below;
        Node last;
        int i;
        
        if (above != null) {
            /*
             * Find the row header.  If the Node to the above is a header, then
             * we are deleting the top row (because the public routines do
             * not know about headers).
             */
            if (above.Position != Node.TRACKNODE) {
                above = Root;
            }
            else {
                while (above.Position == Node.TRACKNODE) {
                    above = above.HLink;
                }
            }
            
            // Do not delete more rows than exist and also leave at least one
            // row.
            rows = Math.min(rows, Rows - above.Position);
            if (rows == Rows) {
                --rows;
            }
            
            // Remove connections between the deleted block and the rest.
            unlinkBlk(above.HLink.VLink, Columns, rows);
            
            last = above;
            
            /*
             * Find the bottom row to be deleted.  Do not delete more rows
             * than there are.
             */
            below = above;
            for (i = 0; (i < rows) && (below.VLink.Position != 0); ++i) {
                below = below.VLink;
            }
            del = above.VLink.HLink;
            
            /*
             * Remove the block of rows.
             */
            
            while (above.HLink != last) {
                above.VLink = below.VLink;
                below.VLink = null;
                above = above.HLink;
                below = below.HLink;
            }
            last = above.VLink;
            above.VLink = below.VLink;
            below.VLink = null;
            
            // null out the HLink on the right column.
            while (last != below) {
                last.HLink = null;
                last = last.VLink;
            }
            last.HLink = null;
            countRows();
            
            // finally join any adjacent tracks between the two rows stitched
            // together.
            above = above.HLink; // back at the header.
            for (last = above.HLink; last != above; last = last.HLink) {
                if (last.Position == Node.TRACKNODE) {
                    last.Contents.hideSec();
                    last.Contents.linkEdge(Edge.BOTTOM);
                    last.Contents.showSec();
                }
                if (last.VLink.Position == Node.TRACKNODE) {
                    last.VLink.Contents.hideSec();
                    last.VLink.Contents.linkEdge(Edge.TOP);
                    last.VLink.Contents.showSec();
                }
            }
        }
        
        ThePane.reformat(this);
        
        Saved = false;
        return del;
    }
    
    /**
     * deletes a group of rows from the grid.  This is the robust form of this
     * operation because it deletes the rows selected; thus, they must be in
     * the layout.
     *
     * @param row is the index of the row being deleted.
     * @param height is the number of rows to be deleted.
     *
     * @return a reference to the upper left column of the columns deleted
     * (still in a block) so that they can be placed in a history trail for
     * undo.  If there is no such column, then return null.  If deleting the
     * block will delete all the columns, then return null.
     *
     * @see Node
     */
    public Node delRow(int row, int height) {
        if (row <= Rows) {
            return delRow(locate(row - 1, 1), height);
        }
        return null;
    }
    
    /**
     * exchanges a rectangular block of Nodes with another rectangular
     * block of Nodes of the same dimensions in the Layout.
     * <p>
     * This method makes assumptions about its inputs and does not verify
     * them.
     * <ul>
     * <li>
     *      There are not more rows in the insertion block, than below the
     *      insertion point (in other words, there are enough row headers).
     * <li>
     *      There are not more columns in the insertion block than to the
     *      right of the insertion point (in other words, there are enough
     *      column headers).
     * <li>
     *      The insertion block has all its internal Nodes linked, except
     *      the rightmost column and lowest row are both null.
     * </ul>
     *<p>
     * Nodes are not actually exchanged - their contents are.  The reason
     * for doing it this way is to keep the scroll window positioned.
     *
     * @param oldRoot is the Node in the Layout where the insert will be made -
     *               newRoot.upperLeft is placed on top of oldRoot.
     * @param newRoot describes the block being inserted.
     * @return oldRoot, the Nodes after being exchanged
     *
     * @see Node
     */
    public Node exchange(Node oldRoot, RangeBlk newRoot) {
        Node oldx;
        Node oldy;
        Node newx;
        Node newy;
        Section oldContents;
        
        oldx = oldRoot;
        newx = newRoot.UpperLeft;
        for (int x = 0; x < newRoot.Width; ++x) {
            oldy = oldx;
            newy = newx;
            for (int y = 0; y < newRoot.Height; ++y) {
                oldContents = oldy.getContents();
                oldContents.unlinkSection();
                oldy.replaceContents(newy.getContents());
                newy.replaceContents(oldContents);
                oldy = oldy.VLink;                
                newy = newy.VLink;
            }
            oldx = oldx.HLink;
            newx = newx.HLink;
        }
        oldx = oldRoot;
        for (int x = 0; x < newRoot.Width; ++x) {
            oldy = oldx;
            for (int y = 0; y < newRoot.Height; ++y) {
                oldy.getContents().linkSection(oldy);
                oldy = oldy.VLink;                
            }
            oldx = oldx.HLink;
        }
        Saved = false;
        return oldRoot;
    }
    
    /**
     * unlinks a rectangle of Track nodes (not headers) from the rest.
     * The strategy is to walk around the perimeter of the rectangle,
     * unbinding all adjacent connections.  All connections to non-adjacent
     * nodes are set back to their adjacent neighbors, or possibly deleted.
     * After the perimeter is unlinked, then all Nodes in the interior are
     * visited and if any has a non-adjacent link, then it is removed.  This
     * is overkill, but easier than trying to fugure out if the link is
     * outside of the rectangle or not.
     * <p>
     * This must work with a rectangle of only one Node.
     *
     * @param anchor is the upperleft Node in the rectangle.
     * @param cols is the number of columns in the rectangle.
     * @param rows is the number of rows in the rectangle.
     */
    private void unlinkBlk(Node anchor, int cols, int rows) {
        Node left;
//        Node right;
        Node top;
//        Node bottom;
        
//        left = top = anchor;
//        
//        // unlink the top while searching for the right column.
//        for (int width = 1; width < cols; ++width) {
//            top.getContents().unlinkAdjacent(Edge.TOP);
//            top = top.HLink;
//        }
//        // unlink the top edge of the right column.
//        top.getContents().unlinkAdjacent(Edge.TOP);
//        
//        right = top;
//        // unlink the left and right edges while searching for the bottom.
//        for (int height = 1; height < rows; ++height) {
//            left.getContents().unlinkAdjacent(Edge.LEFT);
//            right.getContents().unlinkAdjacent(Edge.RIGHT);
//            left = left.VLink;
//            right = right.VLink;
//        }
//        left.getContents().unlinkAdjacent(Edge.LEFT);
//        right.getContents().unlinkAdjacent(Edge.RIGHT);
//        
//        // unlink the bottom edge.
//        bottom = left;
//        for (int width = 1; width < cols; ++width) {
//            bottom.getContents().unlinkAdjacent(Edge.BOTTOM);
//            bottom = bottom.HLink;
//        }
//        bottom.getContents().unlinkAdjacent(Edge.BOTTOM);
//        
        // visit all the interior Nodes.
        left = anchor;
        for (int width = 0; width < cols; ++width) {
            top = left;
            for (int height = 0; height < rows; ++height) {
                top.getContents().unlinkSection();
                top = top.VLink;
            }
            left = left.HLink;
        }
    }
    
    /**
     * links a rectangle of Track nodes (not headers) to the rest.
     * It binds internal edges.
     * <p>
     * This must work with a rectangle of only one Node.
     *
     * @param anchor is the upperleft Node in the rectangle.
     * @param cols is the number of columns in the rectangle.
     * @param rows is the number of rows in the rectangle.
     */
    private void linkBlk(Node anchor, int cols, int rows) {
        Node left;
        Node top;
        
        // visit all Nodes.
        left = anchor;
        for (int width = 0; width < cols; ++width) {
            top = left;
            for (int height = 0; height < rows; ++height) {
                if (top.Contents != null) {
                    top.Contents.linkSection(top);
                }
                top = top.VLink;
            }
            left = left.HLink;
        }
    }
    
    /**
     * generates an Enumeration over the Layout in row minor order.
     *
     * @return the Enumeration.  The Objects being enumerated are the Nodes
     * in the grid.
     *
     * @see Node
     */
    public Enumeration<Node> elements() {
        return new LayoutEnumeration();
    }
    
    /**
     * moves the Node cursor.  This method reduces the Selected block to
     * a single Node.
     *
     * @param newSel are the coordinates of the Node that the cursor will be
     *  placed on.
     */
    void moveCursor(Point newSel) {
        moveCursor(newSel.x, newSel.y);
    }
    
    /**
     * moves the Node cursor.  This method reduces the Selected block to
     * a single Node.
     *
     * @param x is the column to move the cursor to
     * @param y is the column to move the cursor to
     */
    void moveCursor(int x, int y) {
        if (Selected != null) {
            clrSelected();
            Cursor.getTile().setSelect(false);
            Selected.x = x;
            Selected.y = y;
            Selected.width = 1;
            Selected.height = 1;
        }
        else {
            Selected = new Rectangle(x, y, 1, 1);
        }
        Cursor = locate(y, x);
        Cursor.getTile().setSelect(true);
        enableDetails();
    }
    
    /**
     * moves the Node cursor.  This method reduces the Selected block to
     * a single Node.
     *
     * @param newSel is the Node that the cursor will be placed on.
     *
     * @see Node
     */
    void moveCursor(Node newSel) {
        moveCursor(newSel.getColumn(), newSel.getRow());
    }
    
    /**
     * moves the cursor one Node to the right, if possible.
     * <p>
     * If the Node to the right is a header, then the cursor does not move.
     */
    public void moveRight() {
        Node n = Cursor.getRight();
        if ((n != null) && (n.Position == Node.TRACKNODE)) {
            boolean before = ThePane.isVisible(Cursor);
            moveCursor(Selected.x + 1, Selected.y);
            if (before && !ThePane.isVisible(Cursor)) {
                ThePane.scroll(1, 0);
            }
        }
    }
    
    /**
     * moves the cursor one Node down, if possible.
     * <p>
     * If the Node below is a header, then the cursor does not move.
     */
    public void moveDown() {
        Node n = Cursor.getBelow();
        if ((n != null) && (n.Position == Node.TRACKNODE)) {
            boolean before = ThePane.isVisible(Cursor);
            moveCursor(Selected.x, Selected.y + 1);
            if (before && !ThePane.isVisible(Cursor)) {
                ThePane.scroll(0, 1);
            }
        }
    }
    
    /**
     * moves the cursor one Node to the left, if possible.
     * <p>
     * If the Node to the left is a header, then the cursor does not move.
     */
    public void moveLeft() {
        Node n = Cursor.getLeft();
        if ((n != null) && (n.Position == Node.TRACKNODE)) {
            boolean before = ThePane.isVisible(Cursor);
            moveCursor(n);
            if (before && !ThePane.isVisible(Cursor)) {
                ThePane.scroll(-1, 0);
            }
        }
    }
    
    /**
     * moves the cursor one Node up, if possible.
     * <p>
     * If the Node above is a header, then the cursor does not move.
     */
    public void moveUp() {
        Node n = Cursor.getAbove();
        if ((n != null) && (n.Position == Node.TRACKNODE)) {
            boolean before = ThePane.isVisible(Cursor);
            moveCursor(n);
            if (before && !ThePane.isVisible(Cursor)) {
                ThePane.scroll(0, -1);
            }
        }
    }
    
    /**
     * cycles through all the known tracks, asking each what speed
     * it is.  It then records that speed in an array of speeds by
     * setting the corresponding element to true.
     * @param speedLog is the array of speeds.  It must have
     * Track.TrackSpeed.length elements.  Each element is a
     * boolean.  This method depends upon the order of elements being
     * unchanged because it sets Normal and Medium, if Default is set.
     */
    public void determineSpeeds(boolean[] speedLog) {
        Node n;
        Section s;
        TrackGroup group;
        if (Root != null) {
            for (int t = 0; t < speedLog.length; ++t) {
                speedLog[t] = false;
            }
            for (Node y = Root.VLink; y.Position != 0; y = y.VLink) {
                n = y.HLink;
                for (int x = 1; x <= Columns; ++x) {
                    if (((s = n.getContents()) != null) && ((group = s.getTrackGroup()) != null)) {
                        for (Enumeration<Integer> trks = group.getTracks(); trks.hasMoreElements();){
                            speedLog[group.getTrkSpeed(trks.nextElement().intValue())] = true;
                        }
                    }
                    n = n.getRight();
                }
            }
            if (speedLog[0]) {  // this adjusts default speed
                speedLog[1] =
                    speedLog[3] = true;
            }           
        }
    }
    
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        Enumeration<Node> e = elements();
        while (e.hasMoreElements()) {
            if (! e.nextElement().isSaved()) {
                return false;
            }
        }
        return Saved;
    }
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element thisObject = new Element(LayoutTag);
        Node n;
        thisObject.setAttribute(ColTag, String.valueOf(Columns));
        thisObject.setAttribute(RowTag, String.valueOf(Rows));
        for (Node y = Root.VLink; y.Position != 0; y = y.VLink) {
            n = y.HLink;
            for (int x = 1; x <= Columns; ++x) {
                n.putXML(thisObject);
                n = n.getRight();
            }
        }
        parent.addContent(thisObject);
        Saved = true;
        return null;
    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String(LayoutTag + " XML Elements do not have Text fields ("
                + eleValue + ").");
    }
    
    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        return null;
    }
    
    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return LayoutTag;
    }
    
    /**
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        return null;
    }
    
    /**
     * registers an EditPanel with the Layout.
     * <p>
     * The Layout notifies the EditPanel whenever the Layouts dimensions change
     * so that the EditPanel can readjust its screen presentation.  This is
     * a simple inplementation of the Observer design pattern.
     *
     * @param ePanel is the EditPane being registered.
     *
     * @see designer.gui.EditPane
     */
    static public void register(EditPane ePanel) {
        ThePane = ePanel;
    }
    
    /**
     * registers the JMenu for the Details pulldown with the Layout.  By doing
     * this, the Layout can enable and disable the pulldown, depending
     * upon how many grids are selected.
     *
     * @param details is the JMenu
     */
    static public void setDetailMenu(JMenu details) {
        Details = details;
    }
    
    /**
     * enables or disables the Details JMenu, depending upon the Nodes selected.
     * If only one Node (the Cursor) is selected, then it is enabled; otherwise,
     * it is disabled.
     */
    private void enableDetails() {
        if ( (Selected == null) || ( (Selected.width == 1) && (Selected.height == 1))) {
            Details.setEnabled(true);
        }
        else {
            Details.setEnabled(false);
        }
    }
    
    /**
     * is a method for stimulating the Layout to refresh its presentation.
     */
    public void showMe() {
        for (Enumeration<Node> e = elements(); e.hasMoreElements(); ) {
            e.nextElement().getTile().update();
        }
        ThePane.reformat(this);
    }
    
    /**
     * registers a LayoutFactory and all the factories for creating persistent
     * embedded objects with the XMLReader
     */
    static public void initLayout() {
        XMLReader.registerFactory(LayoutTag, new LayoutFactory());
        Node.initNode();
    }
    
    /**************************************************************************
     * An inner class.
     ************************************************************************/
    
    /**
     * is an inner class for providing an Enumeration over the Nodes.  To
     * fit with filling up a GridLayout, the elements are traversed column
     * first.  nextElement() does not return the headers.
     */
    class LayoutEnumeration
    implements Enumeration<Node> {
        Node NextNode;
        
        /**
         * constructs the Enumeration.
         */
        LayoutEnumeration() {
            NextNode = Root.HLink.VLink;
        }
        
        /**
         * indicates if all the Nodes have been given out.
         *
         * @return true if nextElement() has been called for all the Nodes or
         * false if there are some remaining.
         */
        
        public boolean hasMoreElements() {
            return (NextNode != null);
        }
        
        /**
         * returns the Node in the Layout.
         *
         * @return the next Node in row minor order, if there is one, or
         * null.
         *
         * @see Node
         */
        public Node nextElement() {
            Node retVal = NextNode;
            NextNode = NextNode.HLink;
            if (NextNode.Position != Node.TRACKNODE) {
                NextNode = NextNode.VLink;
                if (NextNode == Root) {
                    NextNode = null;
                }
                else {
                    NextNode = NextNode.HLink;
                }
            }
            return retVal;
        }
    }
    
}

/**
 * is a Class known only to the Layout class for creating Layouts from
 * an XML document.  Its purpose is to pick up the number of rows and
 * columns and create the Layout with those dimensions.  Unfortunately,
 * it has more detailed knowledge of Layout than it probably should.
 */
class LayoutFactory
implements XMLEleFactory {
    private String RowSpec;
    private String ColSpec;
    
    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        RowSpec = null;
        ColSpec = null;
    }
    
    /**
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        String errorCode = null;
        if (tag.equals(Layout.RowTag)) {
            if (RowSpec == null) {
                RowSpec = new String(value);
            }
            else {
                errorCode = new String(tag + " appears to be duplicated in " +
                        Layout.LayoutTag);
            }
        }
        else if (tag.equals(Layout.ColTag)) {
            if (ColSpec == null) {
                ColSpec = new String(value);
            }
            else {
                errorCode = new String(tag + " appears to be duplicated in " +
                        Layout.LayoutTag);
            }
        }
        else {
            System.out.println(tag + " is not a known attribute for " +
                    Layout.LayoutTag);
        }
        return errorCode;
    }
    
    /**
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        if ( (RowSpec != null) && (ColSpec != null)) {
            int rows = Integer.parseInt(RowSpec);
            int cols = Integer.parseInt(ColSpec);
            if (rows < 1) {
                System.out.println(
                "The number of rows in the layout must be greater than 0");
                RowSpec = null;
            }
            else if (cols < 1) {
                System.out.println(
                "The number of columns in the layout must be greater than 0");
                ColSpec = null;
            }
            else {
                // The root layout must be set before its constituents; otherwise,
                // the constituents will be added to the current Layout.
                Layout newLayout = new Layout(rows, cols);
//                Ctc.RootCTC.replaceLayout(newLayout);
                return newLayout;
            }
        }
        return null;
    }
    
    /**
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the object described by the XML element
     */
    public String getTag() {
        return Layout.LayoutTag;
    }
}
/* @(#)Layout.java */
