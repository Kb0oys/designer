/*
 * Name: JmriNameList.java
 *
 * What:
 *  This class defines a list of JmriNames.  It holds all the JmriNames used by the
 *  layout.
 */
package designer.layout;

import java.util.Iterator;

import designer.gui.jCustom.AbstractManagerTableModel;
import designer.gui.jCustom.JmriNameModel;

/**
 *  This class defines a list of JmriNames.  It holds all the JmriNames used by the
 *  layout.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class JmriNameList extends AbstractListManager {

    /**
     * the Singleton that holds the list of Jmri names
     */
    private static JmriNameList JmriNames;
    
    /**
     * the accessor to the Singleton
     * 
     * @return the Singleton
     */
    public static JmriNameList instance() {
        if (JmriNames == null) {
            JmriNames = new JmriNameList();
        }
        return JmriNames;
    }

    /**
     * creates an Iterator over the contents.
     * @return an Iterator over the JmriNames
     */
    public Iterator<Object> iterator() {
        return TheList.iterator();
    }
    
    public AbstractManagerTableModel createModel() {
        return new JmriNameModel(makeCopy());
    }

    public void registerFactory() {
    }
}
/* @(#)JmriNameList.java */