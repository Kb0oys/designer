/*
 * Name: AspectMap.java
 *
 * What:
 *  This file contains a data structure for associating Indications (the
 *  meaning of signals) with aspects (the presentation of signals).
 */

package designer.layout;

import designer.gui.AspectDialog;
import designer.gui.Ctc;
import designer.gui.CtcFont;
import designer.layout.items.Track;
import designer.layout.xml.*;

import java.util.Enumeration;
import java.util.StringTokenizer;

import org.jdom2.Element;

/**
 *  A class for associating Indications (the
 *  meaning of signals) with aspects (the presentation of signals).
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2012, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class AspectMap
implements XMLEleObject, Savable {

    /**
     * is the tag for identifying an AspectMap in the XMl file.
     */
    static final String XML_TAG = "ASPECTMAP";

    /**
     * is the separator between heads states for each indication.  It is a
     * String, even though it is a single character, so that it can be
     * passed to the Streamtokenzier without alteration.
     */
    static final String SEP = "|";

    /**
     * is the tag for the approach attribute.
     */
    static final String APP_TAG = "APPROACH";

    /**
     * is the value of the Approach attribute, when approach is enabled.
     */
    static final String ISAPPROACH = "true";

    /**
     * is the tag for the advance signal attribute (three block protection)
     */
    static final String ADVANCE_TAG = "ADVANCE";

    /**
     * is the value of the advance attribute, when it is enabled
     */
    static final String ISADVANCE = "true";

    /**
     * is the tag for the "Hold Only" attribute
     */
    static final String HOLD_TAG = "HOLD_ONLY";
    
    /**
     * is the value of the hold attribute, when it is enabled
     */
    static final String ISHOLDONLY = "true";
    
    /**
     * is the tag for the SW Flashing attribute.
     */
    static final String SWFLASHING = "SWFLASHING";

    /**
     * is the SW Flashing value when SW is disabled
     */
    static final String NO_SW = "false";

    /**
     * is the prefix on the Advance speed definitions
     */
    static private final String ADV_PREFIX = "ADV_";
    
    /**
     * is the tag for a Restricting definition from an older panel.  It is
     * used to populate the definitions in a new panel.
     */
    static final String RESTRICTING_TAG = "R290";

    /**
     * is the prefix on the Restricting definitions
     */
    static final String RESTRICTING_PREFIX = "RES_";
    
    /**
     * is the tag for a Halt definition.
     */
    static final String HALT_TAG = "R292";

    /**
     * is the tag a Stop and Proceed definition.
     */
    static final String STOP_GO_TAG = "R291";
    
    /**
     * is the suffix on the Normal speed definition
     */
    static private final String NORMAL_SUFFIX = "NORM";
    
    /**
     * is the suffix on the Limited speed definition
     */
    static private final String LIMITED_SUFFIX = "LIM";
    
    /**
     * is the suffix on the Medium speed definition
     */
    static private final String MEDIUM_SUFFIX = "MED";
    
    /**
     * is the suffix on the SLow speed definition
     */
    static private final String SLO_SUFFIX = "SLO";
    
    /**
     * The following is a single row in the AspectDialog.  It is an
     * array of Strings, with each column having the following meaning:
     * 0 - rule (ARA or CROR).  It is a speed indication.
     * 1 - XML tag for identifying the Aspect in the file
     * 2 - speed in block(s) being protected.
     * 3 - speed in block(s) protected by the next signal.
     */
    public static final String[][] IndicationNames = {
        {
            "ARA 281", "R281", "Normal", "Normal"}
        , {
            "ARA 281B", "R281B", "Normal", "Limited"}
        , {
            "ARA 282", "R282", "Normal", "Medium"}
        , {
            "ARA 284", "R284", "Normal", "Slow"}
        , {
            "Restrict Normal", RESTRICTING_PREFIX + NORMAL_SUFFIX, "Normal", "None"}
        , {
            "Adv Normal", ADV_PREFIX + NORMAL_SUFFIX, "Normal", "Approach"}
        , {
            "ARA 285", "R285", "Normal", "Halt"}
        , {
            "ARA 281C", "R281C", "Limited", "Normal"}
        , {
            "CROR 412", "C412", "Limited", "Limited"}
        , {
            "CROR 413", "C413", "Limited", "Medium"}
        , {
            "CROR 414", "C414", "Limited", "Slow"}
        , {
            "Restrict Limited", RESTRICTING_PREFIX + LIMITED_SUFFIX, "Limited", "None"}
        , {
            "Adv Limited", ADV_PREFIX + LIMITED_SUFFIX, "Limited", "Approach"}
        , {
            "ARA 281D", "R281D", "Limited", "Halt"}
        , {
            "ARA 283", "R283", "Medium", "Normal"}
        , {
            "CROR 417", "C417", "Medium", "Limited"}
        , {
            "ARA 283A", "R283A", "Medium", "Medium"}
        , {
            "ARA 283B", "R283B", "Medium", "Slow"}
        , {
            "Restrict Medium", RESTRICTING_PREFIX + MEDIUM_SUFFIX, "Medium", "None"}
        , {
            "Adv Medium", ADV_PREFIX + MEDIUM_SUFFIX, "Medium", "Approach"}
        , {
            "ARA 286", "R286", "Medium", "Halt"}
        , {
            "ARA 287", "R287", "Slow", "Normal"}
        , {
            "CROR 422", "C422", "Slow", "Limited"}
        , {
            "CROR 423", "C423", "Slow", "Medium"}
        , {
            "CROR 424", "C424", "Slow", "Slow"}
        , {
            "Restrict Slow", RESTRICTING_PREFIX + SLO_SUFFIX, "Slow", "None"}      
        , {
            "Adv Slow", ADV_PREFIX + SLO_SUFFIX, "Slow", "Approach"}      
        , {
            "ARA 288", "R288", "Slow", "Halt"}
        , {
            "ARA 292", "R292", "Halt", ""}
        , {
            "ARA 291", "R291", "Proceed", ""}
    };

    /**
     * The following are typical default aspects for the above,
     * when displayed on lights.
     */
    public static final String[][][] DefaultLights = {
        /** the single head signals **/
        {
            { // Normal/Normal
            "green"}
            , { // Normal/Limited
            "green"}
            , { // Normal/Medium
            "green"}
            , { // Normal/Slow
            "green"}
            , { // Normal/Restricting
            "flashing red"}
            , {   //Normal/Approach
            "green"}
            , { // Normal/Halt
            "yellow"}
            , { // Limited/Normal
            "green"}
            , { // Limited/Limited
            "green"}
            , { // Limited/Medium
            "green"}
            , { // Limited/Slow
            "green"}
            , { // Limited/Restricting
            "flashing red"}
            , {   //Limited/Approach
            "green"}
            , { // Limited/Halt
            "yellow"}
            , { // Medium/Normal
            "green"}
            , { // Medium/Limited
            "green"}
            , { // Medium/medium
            "green"}
            , { // Medium/Slow
            "green"}
            , { // Medium/Restricting
            "flashing red"}
            , {   //Medium/Approach
            "green"}
            , { // Medium/Halt
            "yellow"}
            , { // Slow/Normal
            "green"}
            , { // Slow/Limited
            "green"}
            , { // Slow/Medium
            "green"}
            , { // Slow/Slow
            "green"}
            , { // Slow/Restricting
            "flashing red"}
            , {   //Slow/Approach
            "green"}
            , { // Slow/Halt
            "yellow"}
            , { // Halt
            "red"}
            , { // Stop and Proceed
            "red"}
        }
        ,
        /** the dual head signals **/
        {
            { // Normal/Normal
                "green", "red"}
            , { // Normal/Limited
                "green", "red"}
            , { // Normal/Medium
                "yellow", "yellow"}
            , { // Normal/Slow
                "yellow", "yellow"}
            , { // Normal/Restricting
                "flashing red", "red"}
            , {   //Normal/Approach
                "yellow", "yellow"}
            , { // Normal/Halt
                "yellow", "red"}
            , { // Limited/Normal
                "green", "red"}
            , { // Limited/Limited
                "green", "red"}
            , { // Limited/Medium
                "yellow", "yellow"}
            , { // Limited/Slow
                "yellow", "yellow"}
            , { // Limited/Restricting
                "red", "flashing red"}
            , {   //Limited/Approach
                "yellow", "yellow"}
            , { // Limited/Halt
                "yellow", "red"}
            , { // Medium/Normal
                "red", "green"}
            , { // Medium/Limited
                "red", "green"}
            , { // Medium/Medium
                "red", "green"}
            , { // Medium/Slow
                "red", "green"}
            , { // Medium/Restricting
                "red", "flashing red"}
            , {   //Medium/Approach
                "red", "green"}
            , { // Medium/Halt
                "red", "yellow"}
            , { // Slow/Normal
                "red", "green"}
            , { // Slow/Limited
                "red", "green"}
            , { // Slow/Medium
                "red", "green"}
            , { // Slow/Slow
                "red", "green"}
            , { // Slow/Restricting
                "red", "flashing red"}
            , {   //Slow/Approach
                "red", "green"}
            , { // Slow/Halt
                "red", "yellow"}
            , { // Halt
                "red", "red"}
            , { // Stop and Proceed
                "red", "red"}
        }
        ,
        /** the triple head signals **/
        {
            { // Normal/Normal
                "green", "red", "red"}
            , { // Normal/Limited
                "green", "red", "red"}
            , { // Normal/Medium
                "yellow", "yellow", "red"}
            , { // Normal/Slow
                "yellow", "yellow", "red"}
            , { // Normal/Restricting
                "flashing red", "red", "red"}
            , {   //Normal/Approach
                "yellow", "yellow", "red"}
            , { // Normal/Halt
                "yellow", "red", "red"}
            , { // Limited/Normal
                "green", "red", "red"}
            , { // Limited/Limited
                "green", "red", "red"}
            , { // Limited/Medium
                "yellow", "yellow", "red"}
            , { // Limited/Slow
                "yellow", "yellow", "red"}
            , { // Limited/Restricting
                "red", "flashing red", "red"}
            , {   //Limited/Approach
                "yellow", "yellow", "red"}
            , { // Limited/Halt
                "yellow", "red", "red"}
            , { // Medium/Normal
                "red", "green", "red"}
            , { // Medium/Limited
                "red", "green", "red"}
            , { // Medium/Medium
                "red", "green", "red"}
            , { // Medium/Slow
                "red", "green", "red"}
            , { // Medium/Restricting
                "red", "red", "flashing red"}
            , {   //Medium/Approach
                "red", "green", "red"}
            , { // Medium/Halt
                "red", "yellow", "red"}
            , { // Slow/Normal
                "red", "red", "green"}
            , { // Slow/Limited
                "red", "red", "green"}
            , { // Slow/Medium
                "red", "red", "green"}
            , { // Slow/Slow
                "red", "red", "green"}
            , { // Slow/Restricting
                "red", "red", "flashing red"}
            , {   //Slow/Approach
                "red", "red", "green"}
            , { // Slow/Halt
                "red", "red", "yellow"}
            ,{    //Halt on Normal speed
                "red", "red", "red"}
            , { // Halt
                "red", "red", "red"}
            , { // Stop and Proceed
                "red", "red", "red"}
        }
    };

    /**
     * The following are typical default aspects for the above when
     * displayed on semaphores.
     */
    public static final String[][][] DefaultSemaphores = {
        /** the single head signals **/
        {
            { // Normal/Normal
            "vertical"}
            , { // Normal/Limited
            "vertical"}
            , { // Normal/Medium
            "vertical"}
            , { // Normal/Slow
            "vertical"}
            , { // Normal/Restricting
            "diagonal"}
            , {   //Normal/Approach
            "vertical"}
            , { // Normal/Halt
            "diagonal"}
            , { // Limited/Normal
            "vertical"}
            , { // Limited/Limited
            "vertical"}
            , { // Limited/Medium
            "vertical"}
            , { // Limited/Slow
            "vertical"}
            , { // Limited/Restricting
            "diagonal"}
            , {   //Limited/Approach
            "vertical"}
            , { // Limited/Halt
            "diagonal"}
            , { // Medium/Normal
            "vertical"}
            , { // Medium/Limited
            "vertical"}
            , { // Medium/medium
            "vertical"}
            , { // Medium/Slow
            "vertical"}
            , { // Medium/Restricting
            "diagonal"}
            , {   //Medium/Approach
            "vertical"}
            , { // Medium/Halt
            "diagonal"}
            , { // Slow/Normal
            "vertical"}
            , { // Slow/Limited
            "vertical"}
            , { // Slow/Medium
            "vertical"}
            , { // Slow/Slow
            "vertical"}
            , { // Slow/Restricting
            "diagonal"}
            , {   //Slow/Approach
            "vertical"}
            , { // Slow/Halt
            "diagonal"}
            , { // Halt
            "horizontal"}
            , { // Stop and Proceed
            "horizontal"}
        }
        ,
        /** the dual head signals **/
        {
            { // Normal/Normal
                "vertical", "horizontal"}
            , { // Normal/Limited
                "vertical", "horizontal"}
            , { // Normal/Medium
                "diagonal", "diagonal"}
            , { // Normal/Slow
                "diagonal", "diagonal"}
            , { // Normal/Restricting
                "diagonal", "horizontal"}
            , {   //Normal/Approach
                "diagonal", "diagonal"}
            , { // Normal/Halt
                "diagonal", "horizontal"}
            , { // Limited/Normal
                "vertical", "horizontal"}
            , { // Limited/Limited
                "vertical", "horizontal"}
            , { // Limited/Medium
                "diagonal", "diagonal"}
            , { // Limited/Slow
                "diagonal", "diagonal"}
            , { // Limited/Restricting
                "diagonal", "horizontal"}
            , {   //Limited/Approach
                "diagonal", "diagonal"}
            , { // Limited/Halt
                "diagonal", "horizontal"}
            , { // Medium/Normal
                "horizontal", "vertical"}
            , { // Medium/Limited
                "horizontal", "vertical"}
            , { // Medium/Medium
                "horizontal", "vertical"}
            , { // Medium/Slow
                "horizontal", "vertical"}
            , { // Medium/Restricting
                "horizontal", "diagonal"}
            , {   //Medium/Approach
                "horizontal", "vertical"}
            , { // Medium/Halt
                "horizontal", "diagonal"}
            , { // Slow/Normal
                "horizontal", "vertical"}
            , { // Slow/Limited
                "horizontal", "vertical"}
            , { // Slow/Medium
                "horizontal", "vertical"}
            , { // Slow/Slow
                "horizontal", "vertical"}
            , { // Slow/Restricting
                "horizontal", "diagonal"}
            , {   //Slow/Approach
                "horizontal", "vertical"}
            , { // Slow/Halt
                "horizontal", "diagonal"}
            , { // Halt
                "horizontal", "horizontal"}
            , { // Stop and Proceed
                "horizontal", "horizontal"}
        }
        ,
        /** the triple head signals **/
        {
            { // Normal/Normal
                "vertical", "horizontal", "horizontal"}
            , { // Normal/Limited
                "vertical", "horizontal", "horizontal"}
            , { // Normal/Medium
                "diagonal", "diagonal", "horizontal"}
            , { // Normal/Slow
                "diagonal", "diagonal", "horizontal"}
            , { // Normal/Restricting
                "diagonal", "horizontal", "horizontal"}
            , {   //Normal/Approach
                "diagonal", "diagonal", "horizontal"}
            , { // Normal/Halt
                "diagonal", "horizontal", "horizontal"}
            , { // Limited/Normal
                "vertical", "horizontal", "horizontal"}
            , { // Limited/Limited
                "vertical", "horizontal", "horizontal"}
            , { // Limited/Medium
                "diagonal", "diagonal", "horizontal"}
            , { // Limited/Slow
                "diagonal", "diagonal", "horizontal"}
            , { // Limited/Restricting
                "horizontal", "diagonal", "horizontal"}
            , {   //Limited/Approach
                "diagonal", "diagonal", "horizontal"}
            , { // Limited/Halt
                "horizontal", "diagonal", "horizontal"}
            , { // Medium/Normal
                "horizontal", "vertical", "horizontal"}
            , { // Medium/Limited
                "horizontal", "vertical", "horizontal"}
            , { // Medium/Medium
                "horizontal", "vertical", "horizontal"}
            , { // Medium/Slow
                "horizontal", "vertical", "horizontal"}
            , { // Medium/Restricting
                "horizontal", "diagonal", "horizontal"}
            , {   //Medium/Approach
                "horizontal", "vertical", "horizontal"}
            , { // Medium/Halt
                "horizontal", "diagonal", "horizontal"}
            , { // Slow/Normal
                "horizontal", "horizontal", "vertical"}
            , { // Slow/Limited
                "horizontal", "horizontal", "vertical"}
            , { // Slow/Medium
                "horizontal", "horizontal", "vertical"}
            , { // Slow/Slow
                "horizontal", "horizontal", "vertical"}
            , { // Medium/Restricting
                "horizontal", "horizontal", "diagonal"}
            , {   //Slow/Approach
                "horizontal", "horizontal", "vertical"}
            , { // Slow/Halt
                "horizontal", "horizontal", "diagonal"}
            , { // Halt
                "horizontal", "horizontal", "horizontal"}
            , { // Stop and Proceed
                "horizontal", "horizontal", "horizontal"}
        }
    };

    /**
     * The index into IndicationNames of the dialog label.
     */
    public static final int LABEL = 0;

    /**
     * the index into IndicationNames of the XML tag.
     */
    public static final int XML = 1;

    /**
     * the index into IndicationNames of the speed of the protected block.
     */
    public static final int PROT_BLOCK = 2;

    /**
     * the index into IndicationNames of the speed of the next block.
     */
    public static final int NEXT_BLOCK = 3;

    /**
     * the mapping of indication to lights/semaphore position.
     */
    private String[][] Map;

    /**
     * the number of heads.  This should be 0 (for an object which has
     * been instantiated, but not initialized) or a positive.  Once set to
     * the latter, then all indications should have the same number of
     * heads (columns).
     */
    private int NumHeads;

    /**
     * a flag which is true when the signal shows approach lighting.
     * Approach lighting means the signal is off until the block
     * approaching it is occupied.
     */
    private boolean Approach;

    /**
     * a flag which is true if the signal should check the next
     * signal for an approach indication when computing its indication.  This
     * facilitates a advance signal.
     */
    private boolean Advance;

    /**
     * a flag which is true if the signal should set the SignalMast
     * aspect (if false) or only set and clear hold (for SignalMasts
     * that have their own aspect logic).
     */
    private boolean HoldOnly;

    /**
     *  a true flag means that it has been saved,
     */
    private boolean Saved = true;

    /**
     * constructs the table of signal aspects for each indication.
     */
    public AspectMap() {
        Map = new String[IndicationNames.length][];
        NumHeads = 0;
        Approach = false;
        Advance = false;
        HoldOnly = false;
    }

    /**
     * constructs the tables of signal aspects for each indication from
     * an array of strings.
     *
     * @param settings is an array of Strings.  Each entry is one indication.
     * An entry could be null, meaning there is no indication for it.  Each
     * string should have the same number of SEP separated substrings,
     * for each substring describes the head's state for that indication.
     */
    public AspectMap(String[] settings) {
        this();
        if (settings.length == IndicationNames.length) {
            StringTokenizer st;
            String[] indication;
            int heads;
            int position;

            // Extract the composite Strings from each indication.
            for (int ind = 0; ind < IndicationNames.length; ++ind) {
                if (settings[ind] != null) {
                    // Find out how many heads are represented by counting separators.
                    heads = 1;
                    position = 0;

                    while (position < settings[ind].length()) {
                        if (settings[ind].charAt(position) == SEP.charAt(0)) {
                            ++heads;
                        }
                        ++position;
                    }
                    st = new StringTokenizer(settings[ind], SEP);
                    indication = new String[heads];
                    heads = 0;
                    while (st.hasMoreElements()) {
                        indication[heads] = st.nextToken();
                        // Flush the separator.
                        if (!indication[heads].equals(SEP)) {
                            ++heads;
                        }
                    }
                    addAspect(ind, indication);
                }
            }

        }
        else {
            System.out.println(
                    "Error in creating an AspectMap - wrong number of indications:"
                            + settings.length);
        }
    }

    /**
     * retrieves the number of heads in the Aspect.
     *
     * @return the number of heads contributing to the aspect.
     */
    public int getHeadCount() {
        return NumHeads;
    }

    /**
     * sets the number of heads in the Aspect.
     *
     * @param heads is the number of heads.
     */
    public void setHeadCount(int heads) {
        NumHeads = heads;
    }

    /**
     * retrieves the signal presentation for a particular head for a particular
     * indication.
     *
     * @param ind is the index of the indication.
     * @param head is the head.
     *
     * @return a String describing the Light/Semaphore.  Null is legal if the
     * light/semaphore does not exist or has no value.
     */
    public String getPresentation(int ind, int head) {
        if ( (ind >= 0) && (ind < IndicationNames.length) && (head >= 0) &&
                (head < NumHeads)) {
            if ( (Map[ind] != null) && (Map[ind][head] != null)) {
                return new String(Map[ind][head]);
            }
        }
        return null;
    }

    /**
     * retrieves the set of all presentations defined for a head.
     *
     * @param head is the number of the head.
     *
     * @return an Enumeration over the Strings describing the expected presentation
     * of the head or null, if there are none.
     */
    public HeadEnumeration getState(int head) {
        if ( (head >= 0) && (head < NumHeads)) {
            return new HeadEnumeration(head);
        }
        return null;
    }

    /**
     * fills out a row in the Map, based on index.
     *
     * @param index is where in the array to place the Signal information.
     * @param aspects is the signal information
     */
    public void addAspect(int index, String[] aspects) {
        int heads;
        if (aspects == null) {
            Map[index] = null;
        }
        else {
            heads = aspects.length;
            Map[index] = new String[heads];
            if (NumHeads == 0) {
                NumHeads = heads;
            }
            else if (NumHeads != heads) {
                System.out.println("Inconsistent number of heads for an AspectMap:"
                        + " expecting " + NumHeads + " received " +
                        heads);
                heads = 0;
            }
            for (int a = 0; a < heads; ++a) {
                Map[index][a] = new String(aspects[a]);
            }
        }
    }

    /**
     * locates the rule for a specified protected speed and next speed
     * @param pSpeed is the String naming the protected speed
     * @param nSpeed is the speed naming the next speed
     * 
     * @return the index of the rule in IndicationNames.  Returns -1
     * if a rule cannot be found.
     * @see designer.gui.AspectDialog
     */
    static public int findRule(String pSpeed, String nSpeed) {
        for (int i = 0; i < IndicationNames.length; ++i) {
            if (IndicationNames[i][PROT_BLOCK].equals(pSpeed) &&
                    IndicationNames[i][NEXT_BLOCK].equals(nSpeed)) {
                return i;
            }
        }
        System.out.println("No rule for " + pSpeed + ":" + nSpeed);
        return -1;
    }

    /**
     * returns the value of Approach
     *
     * @return true if the Aspect is off when no train is in the block
     * approaching the signal.
     */
    public boolean getApproach() {
        return Approach;
    }

    /**
     * sets the Approach flag.
     *
     * @param app is true if the signal uses approach lighting.
     */
    public void setApproach(boolean app) {
        Approach = app;
    }

    /**
     * returns the value of Advance.
     *
     * @return true if the signal is a advance signal.  If it is,
     * the signal will honor an approach indication on the next
     * signal before considering the speed of the next block;
     * otherwise, it will look only at the speed.
     */
    public boolean getAdvance() {
        return Advance;
    }

    /**
     * sets the Advance flag.
     *
     * @param dis is true if the signal is a advance signal.
     */
    public void setAdvance(boolean dis) {
        Advance = dis;
    }

    /**
     * returns the value of HoldOnly.
     *
     * @return true if CATS defers setting the SignalMast aspect to
     * the mast logic.  If true, CATS holds the signal when set to STOP.
     * When false, CATS sets the aspect.
     */
    public boolean getHoldOnly() {
        return HoldOnly;
    }

    /**
     * sets the HoldOnly flag.
     *
     * @param dis is true if CATS only holds/releases the SignalMast aspect.
     */
    public void setHoldOnly(boolean dis) {
        HoldOnly = dis;
    }

    /**************************************************************************
     * An inner class.
     ************************************************************************/

    /**
     * is an inner class for providing an Enumeration over the Strings
     * describing the presentation of a head.  This is not thread safe.
     */
    class HeadEnumeration
    implements Enumeration<String> {

        /**
         * is the number of the head being traversed.
         */
        int EnumHead;

        /**
         * is the Indication that will be given out next.
         */
        int EnumInd;

        /**
         * is a flag indicating that an indication has a
         * flashing aspect; therefore, the "off" option
         * should be enabled.
         */
        boolean foundFlashing = false;

        /**
         * is a bit vector of all speeds that have been used.
         */
        private boolean[] UsedSpeeds;

        /**
         * constructs the Enumeration.
         * @param head is the number of the SignalHead
         */
        HeadEnumeration(int head) {
            EnumHead = head;
            EnumInd = search(0);
        }

        /**
         * indicates if all the presentations have been given out.  Approach
         * lighting is handled as a special case (which is why the comparison
         * is against length + 1, rather than just length).
         *
         * @return true if nextElement() has been called for all the Indications or
         * false if there are some remaining.
         */

        public boolean hasMoreElements() {

            return (EnumInd <= IndicationNames.length);
        }

        /**
         * returns the next String that has not been given out.
         *
         * To Do: if Approach is true, check if "off" has been given out.
         * If not, then return "off".
         *
         * @return the next String, starting with the current, that has not been
         * given out.
         */
        public String nextElement() {
            int result = EnumInd;
            EnumInd = search(EnumInd + 1);
            if (result < IndicationNames.length) {
                return Map[result][EnumHead];
            }
            else if (result == IndicationNames.length) {
                return AspectDialog.LightAspect[0];
            }
            return null;
        }

        /**
         * finds the next String that has not been given out.
         *
         * @param start is the index of the place to start looking from.
         *
         * @return the index of a String if one is found which is not at a
         * lower index or IndicationName.length, if none is found.
         */
        private int search(int start) {
            int ind;
            String mapLabel = null;
            UsedSpeeds = new boolean[Track.TrackSpeed.length];
            Ctc.getLayout().determineSpeeds(UsedSpeeds);
            while (start <= IndicationNames.length) {
                if (start == IndicationNames.length) {
                    if (Approach || foundFlashing) {
                        mapLabel = AspectDialog.LightAspect[0];
                    }
                    else {
                        mapLabel = null;
                    }
                }
                else if ( (Map[start] != null) && (Map[start][EnumHead] != null)) {
                    mapLabel = filterAspect(start, EnumHead);
                }
                if (mapLabel != null) {
                    foundFlashing |= mapLabel.startsWith(AspectDialog.FLASHING);

                    // Let's see if the String is at a lower indication.
                    for (ind = 0; ind < start; ++ind) {
                        if ( (Map[ind] != null) && (Map[ind][EnumHead] != null) &&
                                mapLabel.equals(filterAspect(ind, EnumHead))) {
                            break;
                        }
                    }
                    if (start == ind) {
                        break;
                    }
                }
                ++start;
            }
            return start;
        }

        /**
         * looks through the Map for the aspect for an indication
         * and head.  The indication is filtered.  If no track
         * conditions are located that could generate it, null is
         * returned.
         * @param ind is the indication
         * @param head is the signal head (arm)
         * @return the String that describes the aspect or null
         */
        private String filterAspect(int ind, int head) {
            String speedName = IndicationNames[ind][PROT_BLOCK];
            int speed = CtcFont.findString(speedName, Track.TrackSpeed);
            if ((speed > -1) && !UsedSpeeds[speed]) {
                return null;
            }
            speedName = IndicationNames[ind][NEXT_BLOCK];
            speed = CtcFont.findString(speedName, Track.TrackSpeed);
            if ((speed > -1) && !UsedSpeeds[speed]) {
                return null;
            }
            return Map[ind][head];
        }
    }

    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("A " + XML_TAG + " cannot contain a text field ("
                + eleValue + ").");
    }

    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        return new String("A " + XML_TAG + " cannot contain an Element ("
                + objName + ").");
    }

    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }

    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        Saved = true;
        return null;
    }

    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved;
    }

    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element myObject = new Element(XML_TAG);
        StringBuffer attr;
        int head;

        for (int ind = 0; ind < Map.length; ++ind) {
            if ( (Map[ind] != null) && (Map[ind][0] != null)) {
                attr = new StringBuffer();
                head = 0;
                while (head < (Map[ind].length - 1)) {
                    attr.append(Map[ind][head] + SEP);
                    ++head;
                }
                attr.append(Map[ind][head]);
                myObject.setAttribute(IndicationNames[ind][XML], attr.toString());
            }
        }
        if (Approach) {
            myObject.setAttribute(APP_TAG, ISAPPROACH);
        }
        if (Advance) {
            myObject.setAttribute(ADVANCE_TAG, ISADVANCE);
        }
        if (HoldOnly) {
        	myObject.setAttribute(HOLD_TAG, ISHOLDONLY);
        }
        parent.addContent(myObject);
        Saved = true;
        return null;
    }

    /**
     * registers an AspectMapFactory with the XMLReader.
     */
    static public void init() {
        XMLReader.registerFactory(XML_TAG, new AspectMapFactory());
    }
}

/**
 * is a Class known only to the AspectMap class for creating AspectMaps from
 * an XML document.
 */
class AspectMapFactory
implements XMLEleFactory {

    /**
     * is the array of strings representing the head values for each indication.
     */
    private String[] HeadStr;

    /**
     * is the Approach flag.
     */
    private boolean SetApproach;

    /**
     * is the Advance flag.
     */
    private boolean SetAdvance;
    
    /**
     * is a flag indicating that CATS should only "hold" the signal mast (toggle between
     * STOP and whatever the SignalMast Logic wants).
     */
    private boolean SetHold;

    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        HeadStr = new String[AspectMap.IndicationNames.length];
        SetApproach = false;
        SetAdvance = false;
        SetHold = false;
    }

    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        String resultMsg = null;

        // locate the indication that the tag corresponds to.
        for (int ind = 0; ind < AspectMap.IndicationNames.length; ++ind) {
            if (AspectMap.IndicationNames[ind][AspectMap.XML].equals(tag)) {
                HeadStr[ind] = new String(value);
                return null;
            }
        }

        // this provides a conversion from older panels (where there was a single
        // Restricting indication) to the version where there is a restricting for
        // each speed
        if (AspectMap.RESTRICTING_TAG.equals(tag)) {
            for (int ind = 0; ind < AspectMap.IndicationNames.length; ++ind) {
                if (AspectMap.IndicationNames[ind][AspectMap.XML].startsWith(AspectMap.RESTRICTING_PREFIX)) {
                    HeadStr[ind] = new String(value);
                }
            }
            return null;
        }

        // looks for the approach flag
        if (AspectMap.APP_TAG.equals(tag)) {
            if (AspectMap.ISAPPROACH.equals(value)) {
                SetApproach = true;
            }
            return null;
        }

        // looks for the advance flag
        if (AspectMap.ADVANCE_TAG.equals(tag)) {
            if (AspectMap.ISADVANCE.equals(value)) {
                SetAdvance = true;
            }
            return null;
        }

        // looks for the hold only flag
        if (AspectMap.HOLD_TAG.equals(tag)) {
            if (AspectMap.ISHOLDONLY.equals(value)) {
                SetHold = true;
            }
            return null;
        }

        resultMsg = new String("A " + AspectMap.XML_TAG +
                " XML Element cannot have a " + tag +
                " attribute.");
        return resultMsg;
    }

    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        AspectMap am = new AspectMap(HeadStr);
        am.setApproach(SetApproach);
        am.setAdvance(SetAdvance);
        am.setHoldOnly(SetHold);
        return am;
    }
}
/* @(#)AspectMap.java */
