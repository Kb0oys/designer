/*
 * Name: TemplateStore
 *
 * What:
 *  This class provides a repository for holding all the SignalTemplates.
 */
package designer.layout;

import designer.layout.xml.Savable;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.JList;

import org.jdom2.Element;

/**
 * Holds all the SignalTemplates.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class TemplateStore
    implements Savable {

  /**
   * the singleton, which is known by all clients.
   */
  public static TemplateStore SignalKeeper = new TemplateStore();

  /**
   * The container for holding all the SignalTemplates.
   */
  private Vector<SignalTemplate> TheStore;

  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * the constructor.
   */
  public TemplateStore() {
    TheStore = new Vector<SignalTemplate>();
  }

  /**
   * is a query for if any SignalTemplates have been defined or not.
   *
   * @return true if none have been defined or false if at least one has
   * been defined.
   */
  public boolean isEmpty() {
    return TheStore.isEmpty();
  }

  /**
   * adds a new SignalTemplate, if one with the same name doesn't already
   * exist.
   *
   * @param temp is the SignalTemplate to be added.
   * @return true if the SignalTemplate was added or false if it already
   * existed.
   *
   * @see SignalTemplate
   */
  public boolean add(SignalTemplate temp) {
    SignalTemplate st;

    for (Enumeration<SignalTemplate> iter = TheStore.elements(); iter.hasMoreElements(); ) {
      st = iter.nextElement();
      if (st.getName().equals(temp.getName())) {
        return false;
      }
    }
    TheStore.add(temp);
    Saved = false;
    return true;
  }

  /**
   * finds the SignalTemplate.
   *
   * @param tag is the tag that identifies the SignalTemplate being
   * requested.
   *
   * @return the SignalTemplate that matches or null.
   *
   * @see SignalTemplate
   */
  public SignalTemplate find(String tag) {
    SignalTemplate temp = null;

    for (Enumeration<SignalTemplate> iter = TheStore.elements(); iter.hasMoreElements(); ) {
      temp = iter.nextElement();
      if (temp.getName().equals(tag)) {
        return temp;
      }
    }
    return null;
  }

  /**
   * locates the SignalTemplate at a specific index.
   *
   * @param index is the index of the SignalTemplate being requested.
   *
   * @return the SignalTemplate at that index or null, if the index
   * is invalid.
   *
   * @see SignalTemplate
   */
  public SignalTemplate elementAt(int index) {
    if ( (index >= 0) && (index < TheStore.size())) {
      return TheStore.elementAt(index);
    }
    return null;
  }

  /**
   * locates the index of a SignalTemplate.
   *
   * @param name is the name of the SignalTemplate.
   *
   * @return the index, if a SignalTemplate with name exists or -1.
   */
  public int indexOf(String name) {
    SignalTemplate temp = null;
    int index = 0;

    for (Enumeration<SignalTemplate> iter = TheStore.elements(); iter.hasMoreElements(); ) {
      temp = iter.nextElement();
      if (temp.getName().equals(name)) {
        return index;
      }
      ++index;
    }
    return -1;
  }
  /**
   * removes the SignalTemplate with a specific tag.
   *
   * @param tag is the identity of the SignalTemplate being removed.
   */
  public void remove(String tag) {
    SignalTemplate temp = null;

    for (Enumeration<SignalTemplate> iter = TheStore.elements(); iter.hasMoreElements(); ) {
      temp = iter.nextElement();
      if (temp.getName().equals(tag)) {
        TheStore.remove(temp);
        Saved = false;
      }
    }
  }

  /**
   * removes the SignalTemplate at a specific index.
   *
   * @param index is the index of the SignalTemplate being removed.
   */
//  public void removeElementAt(int index) {
//    if ( (index >= 0) && (index < TheStore.size())) {
//      TheStore.removeElementAt(index);
//    }
//  }

  /**
   * creates a JList for listing the names of the SignalTemplates.
   * @return the JList of template names.
   *
   */
  public JList getTemplateList() {
    Vector<String> tags = new Vector<String>();
    SignalTemplate temp;

    for (Enumeration<SignalTemplate> iter = TheStore.elements(); iter.hasMoreElements(); ) {
      temp = iter.nextElement();
      tags.add(temp.getName());
    }
    return new JList(tags);
  }

  /**
   * clears out all Signal Templates.
   */
  public void reNew() {
      TheStore.clear();
      Saved = true;
  }
  
  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    return Saved;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    SignalTemplate temp;

    for (Enumeration<SignalTemplate> iter = TheStore.elements(); iter.hasMoreElements(); ) {
      temp = iter.nextElement();
      temp.putXML(parent);
    }
    Saved = true;
    return null;
  }
}
/* @(#)TemplateStore.java */