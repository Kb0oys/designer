/* Name: RangeBlk.java
 *
 * What:
 *  a data structure for describing a rectangular block of Nodes in the Layout.
 *  Each RangeBlk consists of:
 *    - the Node in the upper left corner
 *    - the width (number of columns wide)
 *    - the height (number of rows high)
 */
package designer.layout;

import designer.gui.Ctc;

/**
 * is a data structure for describing a rectangular block of Nodes in the
 * Layout.  Each RangeBlk consists of:
 * <ul>
 *    <li> the Node in the upper left corner
 *    <li> the width (number of columns wide)
 *    <li> the height (number of rows high)
 * </ul>
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RangeBlk
{
    /**
     * is the Node in the upper left corner of the block.
     */
    public Node UpperLeft;

    /**
     * is the number of columns in the block.
     */
    public int Width;

    /**
     * is the number of rows in the block.
     */
    public int Height;

    /**
     * constructs a data structure for describing a block of Nodes.
     *
     * @param ul is the Node of the upper left corner.
     * @param width is the number of columns in the block.
     * @param height is the number of rows i the block.
     *
     * @see designer.layout.Node
     */
    public RangeBlk(Node ul, int width, int height)
    {
	UpperLeft = ul;
	Width = width;
	Height = height;
    }

    /**
     * tests if two RangeBlks are the same by comparing their contents.
     *
     * @param other is RangeBlk being compared.
     *
     * @return true if the contents are the same; false if any field is
     *         different.
     */
    public boolean equals(RangeBlk other)
    {
      return ((other != null) && (UpperLeft == other.UpperLeft) &&
              (Width == other.Width) && (Height == other.Height));
    }

    /**
     * makes a copy of itself.
     *
     * @return the copy.
     */
    public RangeBlk copy()
    {
      RangeBlk newBlk = new RangeBlk(Ctc.getLayout().createBlk(Width, Height),
                              Width, Height);
      Node newLeft = newBlk.UpperLeft;
      Node oldLeft = UpperLeft;
      Node newNode;
      Node oldNode;
      for (int y = 0; y < Height; ++y)
      {
        newNode = newLeft;
        newLeft = newLeft.VLink;
        oldNode = oldLeft;
        oldLeft = oldLeft.VLink;
        for (int x = 0; x < Width; ++x)
        {
          newNode.copy(oldNode);
          newNode = newNode.HLink;
          oldNode = oldNode.HLink;
        }
      }
      return newBlk;
    }

    /**
     * makes a shallow copy of itself.  A shallow copy copies just the structure
     * of the layout, omitting details, such as decoder addresses.
     *
     * @return the copy.
     */
    public RangeBlk shallowCopy()
    {
      RangeBlk newBlk = new RangeBlk(Ctc.getLayout().createBlk(Width, Height),
                              Width, Height);
      Node newLeft = newBlk.UpperLeft;
      Node oldLeft = UpperLeft;
      Node newNode;
      Node oldNode;
      for (int y = 0; y < Height; ++y)
      {
        newNode = newLeft;
        newLeft = newLeft.VLink;
        oldNode = oldLeft;
        oldLeft = oldLeft.VLink;
        for (int x = 0; x < Width; ++x)
        {
          newNode.shallowCopy(oldNode);
          newNode = newNode.HLink;
          oldNode = oldNode.HLink;
        }
      }
      return newBlk;
    }
}
/* @(#)RangeBlk.java */