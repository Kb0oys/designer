package designer.layout;

import designer.gui.BooleanGui;


/**
 *  TrainLabel is a Singleton object with a boolean value.  It is set
 *  to true if engine numbers should be displayed on the
 *  dispatcher program to identify trains.  If false, then
 *  the train symbol will be used.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2006, 2007</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class TrainLabel
extends BooleanGui {
    
    
    /**
     * is the tag for identifying a TrainLabel Object in the XMl file.
     */
    static final String XMLTag = "TRAINLABEL";
    
    /**
     * is the label on the JCheckBoxMenuItem
     */
    static final String Label = "Engine Labels";
    
    /**
     * constructs the factory.
     */
    public TrainLabel() {
        super(Label, XMLTag, false);
    }
}
/* @(#)TrainLabel.java */
