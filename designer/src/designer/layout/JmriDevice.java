/**
 * Name: JmriDeviceSelector.java
 *
 * What:
 *  This file contains an enumerations of the types of JMRI devices
 *  used by CATS.  Associated with each device is the single letter
 *  JMRI device type identifier, used in a System Prefix.
 */
package designer.layout;

/**
 *  This file contains an enumerations of the types of JMRI devices
 *  used by CATS.  Associated with each device is the single letter
 *  JMRI device type identifier, used in a System Prefix.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * @author Rodney Black
 * @version $Revision$
 */
public enum JmriDevice {
	Head('H'),
	Light('L'),
	Memory('M'),
	Power('P'),
	Reporter('R'),
	Route('O'),
	Sensor('S'),
	Turnout('T'),
	none(' ');

	/**
	 * the single character used in a JMRI system name for
	 * identifying the device type
	 */
	private final char Mnemonic;

	JmriDevice(char m) {
		Mnemonic = m;
	}

	/**
	 * finds the JMRI device type identifier for a device
	 * @return teh 1 letter type identifier
	 */
	public char getNmemonic() {
		return Mnemonic;
	}
	
	/**
	 * finds the JMRI mnemonic for a device name
	 * @param name is the candidate for a device name. Beware of
	 * the case sensitivities of the candidate name.
	 * @return the JMRI character if name is a valid device
	 * name or space if not.
	 */
	public char nameToMnenomic(final String name) {
		JmriDevice device;
		if ((name != null) && (name.length() > 1)) {
			StringBuffer s = new StringBuffer(name.toLowerCase());
			s.setCharAt(0, Character.toUpperCase(s.charAt(0)));
			device = JmriDevice.valueOf(s.toString());
			if (device != null) {
				return device.Mnemonic;
			}
		}
		return ' ';
	}

	/**
	 * searches for the JmriDevice that has the requested Mnemonic.
	 * @param identifier is the single letter JMRI identifier.  It can
	 * be upper or lower case.
	 * @return a JmriDevice with that letter or null.
	 */
	static public JmriDevice mnemonicToDevice(final char identifier) {
		char id = Character.toUpperCase(identifier);
		for (JmriDevice d : JmriDevice.values()) {
			if (d.Mnemonic == id) {
				return d;
			}
		}
		return none;
	}
}
/* @(#)JmriDeviceSelector.java */
