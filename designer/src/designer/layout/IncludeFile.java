/* Name: IncludeFile.java
 *
 * What:
 *  This contains the name of a file to be included when CATS
 *  runs.
 */
package designer.layout;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import designer.gui.DispPanel;
import designer.gui.XmlFileFilter;

/**
 *  IncludeFile is a Singleton object with a String value.  It
 *  contains a file name.  If not null, CATS will use the JMRI
 *  loader to load the file before CATS runs.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class IncludeFile {
    /**
     * is the tag for identifying an Include File name in the XMl file.
     */
    public static final String XML_TAG = "INCLUDEFILE";
    
    /**
     * is the singleton.
     */
    public static IncludeFile TheIncludeFile;
    
    /**
     * is the JMenuItem on the Menu.
     */
    private JMenuItem FileMenu = new JMenuItem("Include File ...");
    
    /**
     * is the name of the included file.
     */
    private String FileName;
    
    /**
     *  a true flag means that it has been saved,
     */
    protected boolean Saved = true;
    
    /**
     * the constructor.
     *
     * @param parent is the Panel which will hold the JMenuItem.
     */
    public IncludeFile(DispPanel parent) {
        if (TheIncludeFile == null) {
            FileName = null;
            FileMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    getFileName(ae);
                }
            });
            parent.addAppearanceItem(FileMenu);
            TheIncludeFile = this;
        }
    }
    
    /**
     * returns the name of the file to include.
     *
     * @return the name of the file.  null is a valid value
     * indicating that there is none.
     */
    public String getIncludeFile() {
        return FileName;
    }

    /**
     * changes the name of the included file.  It sets Saved to true because
     * this method is invoked only from non-interactive methods (such as when
     * reading a file).  The interactive method sets it to false.
     * @param name is the name of the included file.
     */
    public void setIncludeFile(String name) {
        FileName = new String(name);
        Saved = true;
    }
    
    /**
     * Appearance | Include menu item handler.
     * <p>
     * This method is used to specify an additional XML file
     * to load (presumably created through another JMRI tool).
     *
     * @param e is the event triggering this method.
     */
    void getFileName(ActionEvent e) {
        int result;
        XmlFileFilter fFilter = new XmlFileFilter(".xml", "XML file");
        File fileobj;
        
        if (FileName != null) {
            fileobj = new File(FileName);
            if (!fileobj.exists()) {
                fileobj = new File(DispPanel.FilePath);
            }
        }
        else {
            fileobj = new File(".");
        }
        JFileChooser chooser = new JFileChooser(fileobj);
        chooser.addChoosableFileFilter(fFilter);
        chooser.setFileFilter(fFilter);
        chooser.setDialogTitle("Select file to include:");
        result = chooser.showOpenDialog(null);
        fileobj = chooser.getSelectedFile();
        if (result == JFileChooser.APPROVE_OPTION) {
            if (fileobj.exists() && fileobj.canRead()) {
                FileName = fileobj.getAbsolutePath();
                Saved = false;
            }
            else {
                FileName = null;
                JOptionPane.showMessageDialog( (Component)null, fileobj
                        + " does not exist - selection cleared", "Open Error",
                        JOptionPane.ERROR_MESSAGE);
            }
            Saved = false;
        }
        else if (result == JFileChooser.CANCEL_OPTION) {
            System.out.println("Not opening file for reading");
        }
    }
    
    /**
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public static String getTag() {
        return new String(XML_TAG);
    }
    
    /**
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved;
    }   
}
/* @(#)IncludeFile.java */
