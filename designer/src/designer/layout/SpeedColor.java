/*
 * Name: SpeedColor.java
 * 
 * What:
 *  SpeedColor is a Singleton object with a boolean value.  It is set
 *  to true if designer should paint the tracks in colors representing the
 *  speed of each track segment.  If it is false, then the tracks are
 *  painted for dark, error, and normal.  This flag is used only within
 *  designer and is not saved for CATS.
 */
package designer.layout;

import java.awt.event.ActionEvent;
import java.util.Vector;

import org.jdom2.Element;

import designer.gui.BooleanGui;
import designer.layout.items.Block;

/**
 *  SpeedColor is a Singleton object with a boolean value.  It is set
 *  to true if designer should paint the tracks in colors representing the
 *  speed of each track segment.  If it is false, then the tracks are
 *  painted for dark, error, and normal.  This flag is used only within
 *  designer and is not saved for CATS.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2013</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class SpeedColor extends BooleanGui {
	  /**
	   * is the label on the JCheckBoxMenuItem
	   */
	  static final String Label = "Speed Coloring";

	  /**
	   * is the singleton.
	   */
	  public static SpeedColor SpeedColor;


	  /**
	   * constructs the factory.
	   */
	  public SpeedColor() {
	    super(Label, "", false);
	    SpeedColor = this;
	  }

	    /**
	     * is the ActionListener for setting or clearing the flag.
	     */
	    public void actionPerformed(ActionEvent arg0) {
	    	Vector<Block> blocks = Block.getBlocks();
	    	for (Block b : blocks) {
	    		b.refreshTrackColor();
	    	}
	    }
	    
	    /**
	     * this setting is never saved
	     */
	    @Override
	    public boolean isSaved() {
	    	return true;
	    }
	    
	    @Override
	    public String putXML(Element parent) {
	    	return null;
	    }
}
/* @(#)SpeedColor.java */