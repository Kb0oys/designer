/*
 * Name: SignalTemplate.java
 *
 * What:
 *  This class contains the information about the physical characteristics
 *  of a signal - number of heads, possible states of each head, and so on.
 */
package designer.layout;

import designer.layout.items.LoadFilter;
import designer.layout.xml.*;

import org.jdom2.Element;

/**
 *  This class contains the information about the physical characteristics
 *  of a signal - number of heads, possible states of each head, and so on.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2015, 2019, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class SignalTemplate
    implements XMLEleObject, Savable {

  /**
   * is the tag for identifying a SignalTemplate in the XMl file.
   */
  static final String XML_TAG = "SIGNALTEMPLATE";

  /**
   * is the attribute tag for the number of heads in the XML file.
   */
  static final String TEMPLATEHEADS = "TEMPLATEHEADS";

  /**
   * is the kind of signal in the template.
   */
  static final String TEMPLATEKIND = "TEMPLATEKIND";

  /**
   * is the name of the template.
   */
  static final String TEMPLATENAME = "TEMPLATENAME";

  /**
   * is the value of TEMPLATEKIND for a lamp.
   */
  static final String LAMPKIND = "Lamp";

  /**
   * is the value of TEMPLATEKIND for a Semaphore.
   */
  static final String SEMKIND = "Semaphore";

  /**
   * the name of the SignalType.
   */
  private String TypeName;

  /**
   * the number of heads in the signal.
   */
  private int SignalHeads;

  /**
   * a true flag means the heads are lights.  A false flag means they are
   * semaphores.  This method is not really needed for
   * operating the layout.  However, it makes the screen presentation nicer
   * because a different icon can be used for semaphores as for lights.  Note
   * that all heads must be of the same type.
   */
  private boolean IsLights;

  /**
   * Each head has a list of Labels for identifying the states the head
   * can assume.  A state can be
   * <ul>
   * <li>
   *     a color (such as red, yellow, green)
   * <li>
   *     a dynamic color (such as flashing yellow), as long as the signal
   *     driver can accept a command that will handle the dynamic nature.
   *     The dispatcher panel does not perform low level timing.
   * <li>
   *     a semaphore position
   * </ul>
   * This program (and the dispatcher panel) makes no interpretation of
   * the state label.  The state label provides a bridge between the
   * aspect map and the signal driver.
   */

  /**
   * the table that maps indications to aspects.  The entries in the
   * map are entries in the state table.
   */
  private AspectMap AspectTbl;

  /**
   * the user defined names of the aspects - cells in the grid
   */
  private String AspectNames[] = new String[AspectMap.IndicationNames.length];
  
  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * the SignalTemplate constructor.
   *
   * @param heads is the number of heads (1 - 3)
   * @param lamp is true for lamp and false for semaphore.
   * @param name is the name (must not be blank)
   * @param aspects is an optional list of aliases for aspect names
   */
  public SignalTemplate(int heads, boolean lamp, String name, String[] aspects) {
    SignalHeads = heads;
    IsLights = lamp;
    TypeName = new String(name);
    if (aspects == null) {
    	for (int aspect = 0; aspect < AspectNames.length; aspect++) {
    		AspectNames[aspect] = new String(AspectMap.IndicationNames[aspect][AspectMap.LABEL]);
    	}
    }
    else {
    	AspectNames = aspects;
    }
  }

  /**
   * returns the name of the template.
   *
   * @return the template's name.
   */
  public String getName() {
    return new String(TypeName);
  }

  /**
   * returns the number of signal heads.
   *
   * @return the number of heads.
   */
  public int getNumHeads() {
    return SignalHeads;
  }

  /**
   * returns the kind of Signal.
   *
   * @return true is the Signal is composed of lights or false if it is
   * composed of semaphore blades.
   */
  public boolean isLights() {
    return IsLights;
  }

  /**
   * replaces the AspectMap.
   *
   * @param map is the new AspectMap.
   *
   * @see AspectMap
   */
  public void setAspectMap(AspectMap map) {
    AspectTbl = map;
  }

  /**
   * retrieves the AspectMap.
   *
   * @return the signal presentation for each Indication.
   *
   * @see AspectMap
   */
  public AspectMap getAspectMap() {
    return AspectTbl;
  }
  
  /**
   * replaces the names of the aspects
   * @param names is the list of names.  There must be exactly
   * the same number of names as entries in AspectMap.IndicationNames.
   */
  public void setAspectNames(String names[]) {
	  AspectNames = names;
  }
  
  /**
   * retrieves the list of aspect names.
   * @return the list
   */
  public String[] getAspectNames() {
	  return AspectNames;
  }
  
  /**
   * generates a copy of the list of aspect names and returns it
   * @return a copy of the aspect names
   */
  public String[] copyAspectNames() {
	  String copy[] = new String[AspectNames.length];
	  for (int name = 0; name < AspectNames.length; ++name) {
		  copy[name] = new String(AspectNames[name]);
	  }
	  return copy;
  }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
	String defaultMap[][];
	int heads = getNumHeads();
    if (AspectMap.XML_TAG.equals(objName)) {
      AspectTbl = (AspectMap) objValue;
      if (AspectTbl.getHeadCount() != heads) {
        if (AspectTbl.getHeadCount() == 0) {
          AspectTbl.setHeadCount(heads);
          for (int ind = 0; ind < AspectMap.IndicationNames.length; ++ind) {
            if (isLights()) {
              AspectTbl.addAspect(ind,
                                  AspectMap.DefaultLights[heads - 1][ind]);
            }
            else {
              AspectTbl.addAspect(ind,
                                  AspectMap.DefaultSemaphores[heads - 1][ind]);
            }
          }
        }
        else {
          return new String("The number of signal heads disagrees between "
                            + XML_TAG + " and " + AspectMap.XML_TAG + ".");
        }
      }
      else {
    	  // this checks for undefined indications - needed when the template changes
    	  if (isLights()) {
    		  defaultMap = AspectMap.DefaultLights[heads - 1];
    	  }
    	  else {
    		  defaultMap = AspectMap.DefaultSemaphores[heads -1];
    	  }
    	  for (int ind = 0; ind < AspectMap.IndicationNames.length; ++ind) {
    		  if (AspectTbl.getPresentation(ind, 0) == null) {
    			  AspectTbl.addAspect(ind, defaultMap[ind]);
    		  }
    	  }
      }
      return null;
    }
    return new String("A " + XML_TAG + " cannot contain an Element ("
                      + objName + ").");
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    Saved = true;
    return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    return Saved;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    Element myObject = new Element(XML_TAG);
    String typeString;
    String headsString = new String(String.valueOf(SignalHeads));

    if (IsLights) {
      typeString = new String(LAMPKIND);
    }
    else {
      typeString = new String(SEMKIND);
    }
    myObject.setAttribute(TEMPLATEKIND, typeString);
    myObject.setAttribute(TEMPLATEHEADS, headsString);
    myObject.setAttribute(TEMPLATENAME, TypeName);
    for (int aspect = 0; aspect < AspectNames.length; aspect++) {
    	// have to use the XML field because XML will not accept spaces in names
    	if (!AspectNames[aspect].equals(AspectMap.IndicationNames[aspect][AspectMap.LABEL])) {
    		myObject.setAttribute(AspectMap.IndicationNames[aspect][AspectMap.XML], AspectNames[aspect]);
    	}
    }
    if (AspectTbl != null) {
      AspectTbl.putXML(myObject);
    }
    parent.addContent(myObject);
    Saved = true;
    return null;
  }

  /**
   * registers a SignalTemplateFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new SignalTemplateFactory());
    AspectMap.init();
  }
}

/**
 * is a Class known only to the SignalTemplate class for creating
 * SignalTemplates from an XML document.
 */
class SignalTemplateFactory
    implements XMLEleFactory {

  private String tempName;
  private boolean lamp;
  private int heads;
  private String AspectNames[] = new String[AspectMap.IndicationNames.length];

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    tempName = null;
    lamp = true;
    heads = 1;
    AspectNames = new String[AspectMap.IndicationNames.length];
	for (int aspect = 0; aspect < AspectNames.length; aspect++) {
		AspectNames[aspect] = new String(AspectMap.IndicationNames[aspect][AspectMap.LABEL]);
	}
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    int aspect;

    if (SignalTemplate.TEMPLATEKIND.equals(tag)) {
      if (SignalTemplate.LAMPKIND.equals(value)) {
        lamp = true;
      }
      else if (SignalTemplate.SEMKIND.equals(value)) {
        lamp = false;
      }
      else {
        resultMsg = new String("A " + value + " is not a " +
                               SignalTemplate.XML_TAG + "XML " +
                               SignalTemplate.TEMPLATEKIND + " attribute.");
      }
    }
    else if (SignalTemplate.TEMPLATEHEADS.equals(tag)) {
      try {
        heads = Integer.parseInt(value);
        if ( (heads < 1) || (heads > 3)) {
          heads = 1;
          resultMsg = new String(value + ": Illegal value for " +
                                 SignalTemplate.XML_TAG + "XML " +
                                 SignalTemplate.TEMPLATEKIND + " attribute.");
        }
      }
      catch (NumberFormatException nfe) {
        resultMsg = new String(value + ": Illegal " +
                               SignalTemplate.XML_TAG + "XML " +
                               SignalTemplate.TEMPLATEKIND + " attribute.");
      }
    }
    else if (SignalTemplate.TEMPLATENAME.equals(tag)) {
      if ( (value == null) || (value.trim().length() == 0)) {
        resultMsg = new String("A " + SignalTemplate.XML_TAG +
                               " XML " + SignalTemplate.TEMPLATENAME +
                               " attribute can not be blank.");
      }
      else {
        tempName = new String(value);
      }
    }
    else {
    	for (aspect = 0; aspect < AspectMap.IndicationNames.length; ++aspect) {
    		if (AspectMap.IndicationNames[aspect][AspectMap.XML].equals(tag)) {
    			AspectNames[aspect] = new String(value);
    			break;
    		}
    	}
    	if (aspect == AspectMap.IndicationNames.length)  {
    		resultMsg = new String("A " + SignalTemplate.XML_TAG +
    				" XML Element cannot have a " + tag +
    				" attribute.");
    	}
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    SignalTemplate st;
    if (tempName == null) {
      System.out.println("Missing " + SignalTemplate.XML_TAG +
                         SignalTemplate.TEMPLATENAME + " attribute.");
      return null;
    }
    st = new SignalTemplate(heads, lamp, tempName, AspectNames);
    if (LoadFilter.instance().isEnabled(LoadFilter.XML_SIGNALTEMPLATE)) {
        TemplateStore.SignalKeeper.add(st);
    }
    return st;
  }
}
/* @(#)SignalTemplate.java */