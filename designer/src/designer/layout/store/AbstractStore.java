/* Name: AbstractStore.java
 *
 * What;
 * This object contains the structure for a mini-database.  The database
 * holds records of things needed by CATS.  The intent is that it is edited
 * by a Table Editor.  So, the internal structure is driven to be compatible
 * with the Table Editor in jCustom.
 * <p>
 * The heart of the store is two components.
 * <ol>
 * <li>The DataStore holds the contents of the mini-database.  It is  a Vector
 * of Vectors.  The major dimension has one element for each record in the
 * mini-database.  The minor dimension has a pair of values for each field
 * in the mini-database.  One member of the pair is a tag identifying the
 * field.  The other is the value of the field.
 * <li>
 * The FieldInfoStore holds the presentation of the DataStore in CATS.  It
 * is a Vector of Vectors.  The major dimension has one
 * element for describing each field in a DataStore record.  The minor
 * dimension has a pair of values for each Field property.  One member of
 * the pair is a tag identifying the property and the other is the value
 * of the property.
 * </ol>
 * <p>
 * The Store is converted to XML by first writing the presentation Vector of
 * Vectors.  There is one FieldInfo element for each Field in a record.
 * The properties of the FieldInfo are written as element attributes.  The
 * DataStore is written next, one record per XML element.  The fields
 * in the records are written as attributes with the field identifiers
 * being the attribute tags.
 */
package designer.layout.store;

import designer.gui.store.FormatTableModel;
import designer.gui.store.StoreEditPane;
import designer.gui.store.StoreTableModel;
import designer.layout.FontList;
import designer.layout.items.LoadFilter;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 * This object contains the structure for a mini-database.  The database
 * holds records of things needed by CATS.  The intent is that it is edited
 * by a Table Editor.  So, the internal structure is driven to be compatible
 * with the Table Editor in jCustom.
 * <p>
 * The heart of the store is two components.
 * <ol>
 * <li>The DataStore holds the contents of the mini-database.  It is  a Vector
 * of Vectors.  The major dimension has one element for each record in the
 * mini-database.  The minor dimension has a pair of values for each field
 * in the mini-database.  One member of the pair is a tag identifying the
 * field.  The other is the value of the field.
 * <li>
 * The FieldInfoStore holds the presentation of the DataStore in CATS.  It
 * is a Vector of Vectors.  The major dimension has one
 * element for describing each field in a DataStore record.  The minor
 * dimension has a pair of values for each Field property.  One member of
 * the pair is a tag identifying the property and the other is the value
 * of the property.
 * </ol>
 * <p>
 * The Store is converted to XML by first writing the presentation Vector of
 * Vectors.  There is one FieldInfo element for each Field in a record.
 * The properties of the FieldInfo are written as element attributes.  The
 * DataStore is written next, one record per XML element.  The fields
 * in the records are written as attributes with the field identifiers
 * being the attribute tags.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2005, 2009, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public abstract class AbstractStore
implements XMLEleObject, Savable {

    /**
     * are the default attributes (columns)
     */
    private final FieldInfo[] DEFAULT_ATTRIBUTES;

    /**
     * is TAG in the XML file for identifying the RecordVector
     */
    private final String REC_TAG;
    
    /**
     * is the TAG in the XML file for identifying the FieldVector
     */
    private final String FIELD_TAG;
    
    /**
     * is the TAG in the XML file for identifying this instantiation of the
     * AbstractStore.
     */
    protected final String XML_TAG;

    /**
     * are the Field descriptions.
     */
    protected FieldVector FieldInfoStore;

    /**
     * are the contents.
     */
    protected RecordVector<GenericRecord> DataStore;

    /**
     * is the constructor.
     *
     * @param sTag is the XML tag of the AbstractStore.
     * @param fTag is the XML tag of the FieldStore.
     * @param rTag is the XML tag of the RecordStore.
     * @param prop is the initial Field keys.
     *
     */
    public AbstractStore(String sTag, String fTag, String rTag, FieldInfo prop[]) {
        DEFAULT_ATTRIBUTES = prop;
        XML_TAG = sTag;
        REC_TAG = rTag;
        FIELD_TAG = fTag;
        reNew();
    }

    /**
     * constructs a StoreTableModel for the concrete class
     * @param data is a copy of the DataStore
     * @param format is a copy of the FieldInfoStore
     * @return a concrete StoreTableModel for the concrete Store.
     */
    abstract protected StoreTableModel createModel(RecordVector<GenericRecord> data,
            FieldVector format);
    
    /**
     * is the method that starts off the editor on the DataStore.
     */
    public void editData() {
        FieldVector fields = FieldInfoStore.toFormatting();
        RecordVector<GenericRecord> trial = DataStore.makeCopy();
        trial.setStrategy(new DefaultSelectionStrategy(trial));
        if (StoreEditPane.editRecords(createModel(trial, fields))) {
            DataStore = trial;
            FieldInfoStore = fields;
        }
    }

    /**
     * is the method that starts off the editor on the FieldInfoStore
     */
    public void editControls() {
        String recordKeyName = FieldInfoStore.getKeyField();
        FieldVector fields = MetaFieldStore.FieldOfFields.toFormatting();
        RecordVector<GenericRecord> trial = FieldInfoStore.toData();
        trial.setStrategy(new FormatSelectionStrategy(trial));
        if (StoreEditPane.editRecords(new FormatTableModel(trial, fields, this))) {
            FieldInfoStore = trial.toFieldVector(FieldInfoStore.getTag());
            FieldInfoStore.getFieldInfo(recordKeyName).makeRecordKey();
            FieldInfoStore.syncUpVector(DataStore);
        }       
    }

    /**
     * is invoked to verify that the results of the JTable editing is complete
     * and the values are consistent.  If they are not, an error string is
     * created, which StoreEditPane puts in a pop-up.  This method is often
     * overridden.
     *
     * @param model is the TableModel controlling the editing.  It is the
     * conduit for retrieving values from the JTable.
     *
     * @return null - nothing is being tested.
     */
    public String checkConsistency(StoreTableModel model) {
        return StoreTableModel.defaultVerifyResults(model.getContents(), getKeyTag(),
                FieldInfoStore.getFieldInfo(getKeyTag()).getLabel());
    }

    /**
     * is called to get the tag on the key field for the enclosed GenericRecords.
     * This is a plug-in, supplied by the concrete instantiation.
     * 
     * @return the tag on the key field of the concrete instantiation.
     */
    public String getKeyTag() {
         return FieldInfoStore.getKeyField();
    }
    
    /**
     * restores a Store to a clean (empty) state.
     *
     */
    public void reNew() {
        FieldInfoStore = new FieldVector(DEFAULT_ATTRIBUTES, FIELD_TAG);
        DataStore = new RecordVector<GenericRecord>(REC_TAG);
    }

    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("A " + XML_TAG + " cannot contain a text field ("
                + eleValue + ").");
    }

    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptable or an error String
     * if it is not.
     */
    @SuppressWarnings("unchecked")
    public String setObject(String objName, Object objValue) {
        String resultMsg = null;
        String recordKeyName;
        if (LoadFilter.instance().isEnabled(getTag())) {
            if (FieldInfoStore.getTag().equals(objName)) {
                recordKeyName = FieldInfoStore.getKeyField();
                FieldInfoStore = (FieldVector) objValue;
                FieldInfoStore.getFieldInfo(recordKeyName).makeRecordKey();
            }
            else if ( (DataStore != null) && (DataStore.getTag() != null) &&
                    (DataStore.getTag().equals(objName))) {
                DataStore = (RecordVector<GenericRecord>) objValue;
                if (FieldInfoStore != null) {
                    FieldInfoStore.vectorFromXML(DataStore);
                }
            }
        }
        return resultMsg;
    }

    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }

    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        return null;
    }

    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return DataStore.isSaved() && FieldInfoStore.isSaved();
    }
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element thisObject = new Element(XML_TAG);
        parent.addContent(thisObject);
        FieldInfoStore.putXML(thisObject);
        DataStore.putXML(thisObject);
        return null;
    }

    /**
     * writes only the Object (and any referenced Objects) to an XML file.
     * This method is invoked from the "Save" option of the StoreMenu.
     * @param parent is the XML Element that this object is added to.
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putReducedXML(Element parent) {
        Element thisObject = new Element(XML_TAG);
        FontList.instance().putXML(parent, DataStore, "FONT");
        parent.addContent(thisObject);
        FieldInfoStore.putXML(thisObject);
        DataStore.putXML(thisObject);
        return null;
    }

    /**
     * registers a Factory for accepting a Store
     * with the XMLReader.
     */
    public void init() {
        XMLReader.registerFactory(XML_TAG, new Factory(this, XML_TAG));
        FieldInfoStore.init(FIELD_TAG, DEFAULT_ATTRIBUTES);
        DataStore.init(REC_TAG);
    }

    /**
     * is a Class known only to the AbstractStore class for creating a
     * Database from an XML document.
     */
    class Factory
    implements XMLEleFactory {

        /**
         * is the product produced by the Factory.
         */
        private AbstractStore Product;

        /**
         * is the XML Tag of the product.
         */
        private String ProductTag;

        /**
         * is the constructor.
         *
         * @param product is the product produced by the Factory.
         *
         *@param tag is the XML tag of the product.
         */
        Factory(AbstractStore product, String tag) {
            Product = product;
            ProductTag = tag;
        }

        /*
         * tells the factory that an XMLEleObject is to be created.  Thus,
         * its contents can be set from the information in an XML Element
         * description.
         */
        public void newElement() {
        }

        /*
         * gives the factory an initialization value for the created XMLEleObject.
         *
         * @param tag is the name of the attribute.
         * @param value is it value.
         *
         * @return null if the tag:value are accepted; otherwise, an error
         * string.
         */
        public String addAttribute(String tag, String value) {
            String resultMsg = null;

            resultMsg = new String("A " + ProductTag + " XML Element cannot have a " +
                    tag +
            " attribute.");
            return resultMsg;
        }

        /*
         * tells the factory that the attributes have been seen; therefore,
         * return the XMLEleObject created.
         *
         * @return the newly created XMLEleObject or null (if there was a problem
         * in creating it).
         */
        public XMLEleObject getObject() {
            return Product;
        }
    }
}
/* @(#)AbstractStore.java */
