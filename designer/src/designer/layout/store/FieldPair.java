/* Name: FieldPair.java
 *
 * What;
 *  This file defines a data structure for pairing a Field value to the
 *  Name of the Field.
 */
package designer.layout.store;

import designer.gui.store.ClassSpec;
import designer.layout.xml.Savable;
import org.jdom2.Element;

/**
 *  This file defines a data structure for pairing a Field value to the
 *  Name of the Field.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2005, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class FieldPair
    implements Savable {

  /**
   * is true if the object has not been saved.
   */
  private boolean Saved;

  /**
   * is the Field Name.
   */
  public String FieldTag;

  /**
   * is the value of the Field.
   */
  public Object FieldValue;

  /**
   * the constructor.
   * @param tag is the name of the Field.
   * @param value is the value of the Field.
   */
  public FieldPair(String tag, Object value) {
    FieldTag = tag;
    FieldValue = value;
    Saved = false;
  }

  /**
   * sets the FieldValue and marks the FieldPair as not saved.
   *
   * @param newValue is the new value.
   */
  public void setValue(Object newValue) {
    FieldValue = newValue;
    Saved = false;
  }

  /**
   * is called to ensure that FieldValue is an instance of the required Class.
   * If it is, then there are no changes.  If it is not, then it is set to the
   * "default" value for instances of the required Class.
   *
   * @param newClass is the class
   */
  public void verifyClass(Class<?> newClass) {
      if (newClass != null) {
          if (FieldValue == null) {
              FieldValue = new String("");
          }
          Class<? extends Object> myClass = FieldValue.getClass();
          if (Boolean.class.equals(newClass)) {
              if (myClass != Boolean.class) {
                  if (myClass == String.class) {
                      if (FieldValue.equals("true")) {
                          FieldValue = new Boolean(true);
                      }
                      else {
                          FieldValue = new Boolean(false);
                      }
                  }
                  else {
                      FieldValue = new Boolean(false);
                  }
                  Saved = false;
              }
          }
          else if (Integer.class.equals(newClass)) {
              if (myClass != Integer.class) {
                  if (myClass == String.class) {
                      try {
                          FieldValue = Integer.valueOf((String) FieldValue);
                      }
                      catch (NumberFormatException nfe) {
                          FieldValue = new Integer(0);
                      }
                  }
                  else {
                      FieldValue = new Integer(0);
                  }
                  Saved = false;
              }
          }
          else if (String.class.equals(newClass)) {
              if (myClass != String.class) {
                  FieldValue = new String(FieldValue.toString());
                  Saved = false;
              }
          }
          else if (ClassSpec.class.isAssignableFrom(newClass)) {
              if (newClass != myClass) {
                  if (myClass == String.class) {
                      if (FieldValue.equals("")) {
                          FieldValue = String.class;
                      }
                      else {
                          FieldValue = ClassSpec.toClass((String) FieldValue);                          
                      }
                  }
                  else {
                      FieldValue = String.class;
                  }
                  Saved = false;
              }
          }
          else {
//              FieldValue = "";
              Saved = false;
          }
      }
  }

  /*
   * This is never called because the enclosing Record keeps track of saved
   * status.
   *
   * @return true as a placeholder.
   */
  public boolean isSaved() {
    return Saved;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    if (FieldValue == null) {
      parent.setAttribute(FieldTag, "");
    }
    else {
      parent.setAttribute(FieldTag, FieldValue.toString());
    }
    Saved = true;
    return null;
  }
}
/* @(#)FieldPair.java */
