/*
 * Name: DefaultSelectionStrategy.java
 *
 * What:
 *  This class is the default class for implementing the GenericRecord
 *  selection strategy.  It
 *  <ul>
 *  <li>treats all GenericRecords as visible
 *  <li>treats all GenericRecords which do not have the Mandatory attribute set as deleteable
 *  <li>handles unselect by deleting the GenericRecord
 *  </ul>
 */
package designer.layout.store;

/**
 *  This class is the default class for implementing the GenericRecord
 *  selection strategy.  It
 *  <ul>
 *  <li>treats all GenericRecords as visible
 *  <li>treats all GenericRecords which do not have the Mandatory attribute set as deleteable
 *  <li>handles unselect by deleting the GenericRecord
 *  </ul>
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class DefaultSelectionStrategy implements SelectionStrategy {

    /**
     * is the RecordVector that holds the GenericRecords being processed
     */
    protected final RecordVector<GenericRecord> Store;
    
    /**
     * the no argument ctor
     * @param parent is the record container
     */
    public DefaultSelectionStrategy(RecordVector<GenericRecord> parent) {
        Store = parent;
    }
    
    /**
     * is called to determine if the GenericRecord can be deleted from the Vector
     * or not.
     * @param rec is the GenericRecord being queried
     * @return true if it can be.
     */
    public boolean isDeletable(GenericRecord rec) {
        return true;
    }

    /**
     * is called to query if the containing GenericRecord should appear in the
     * JTable or not.
     * @param rec is the GenericRecord being queried
     * @return true if it should appear and false if not
     */
    public boolean isSelected(GenericRecord rec) {
        return true;
    }

    /**
     * is called to hide the GenericRecord on the JTable.  For this simple case,
     * the GenericRecord is simply removed.
     * @param rec is the GenericRecord being unselected
     * 
     * @param reason is why the GenericRecord should be hidden.
     */
    public void unselect(GenericRecord rec, String reason) {
        Store.remove(rec);
    }

}
/* @(#)DefaultSelectionStrategy.java */
