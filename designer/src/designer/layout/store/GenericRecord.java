/* Name: GenericRecord.java
 *
 * What;
 *  This file is a data structure for collecting <tag:value> pairs into
 *  a Vector.  The Vector then forms a database record.
 */
package designer.layout.store;

import java.util.Enumeration;
import java.util.Vector;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 *  This file is a data structure for collecting tag:value pairs into
 *  a Vector.  The Vector then forms a database record.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2005, 2009, 2010, 2011, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class GenericRecord
    extends Vector<FieldPair>
    implements XMLEleObject, Savable {

  /**
   * is the XML Tag for identifying data records.
   */
  public static final String DATARECORD = "DATARECORD";

  /**
   * is the XML Tag for identifying edit control records.
   */
  public static final String EDITRECORD = "EDITRECORD";

  /**
   * is the TAG in the XML file for identifying this kind of Record.
   */
  protected String XmlTag;

  /**
   *  a true flag means that it has been saved,
   */
  protected boolean Saved = false;

  /**
   * is another constructor.
   * @param tag is the XML tag for identifying the Records.
   * @param size is the initial size of the Vector.
   */
  public GenericRecord(String tag, int size) {
    super(size);
    XmlTag = tag;
  }

  /**
   * is the constructor.  It is called only from the RecordFactory;
   * thus, it is automatically saved.
   *
   * @param tag is the XML tag for identifying the Records.
   * Records.
   */
  public GenericRecord(String tag) {
    super();
    XmlTag = tag;
    Saved = true;
  }

  /**
   * this constructor is not used directly, but keeps the Java compiler
   * happy.
   */
  public GenericRecord() {
    super();
    XmlTag = "";
  }

  /**
   * replaces a FieldPair.  If the tag is not in the GenericRecord, it is
   * added.
   *
   * @param newPair is the replacement FieldInfo.
   */
  public void replacePair(FieldPair newPair) {
    boolean match = false;
    FieldPair oldPair;
    for (Enumeration<FieldPair> exists = elements(); exists.hasMoreElements() && !match; ) {
      oldPair = exists.nextElement();
      if ( (oldPair.FieldTag != null) && (newPair.FieldTag != null) &&
          (oldPair.FieldTag.equals(newPair.FieldTag))) {
        oldPair.FieldValue = newPair.FieldValue;
        match = true;
      }
    }
    if (!match) {
      add(newPair);
    }
  }

  /**
   * replaces the contents of the Vector.  If the tag of a pair is not
   * in the Vector, it is added.  If the tag of a pair in the Vector
   * is not in the input, it is left untouched.  The order of
   * checking is driven by the current contents.
   *
   * @param newValues is a Vector of FieldPair.  The value associated with
   * a FieldTag that matches an existing FieldTag, replaces the existing
   * value.
   */
  public void replaceValues(Vector<FieldPair> newValues) {
    for (Enumeration<FieldPair> e = newValues.elements(); e.hasMoreElements(); ) {
      replacePair( e.nextElement());
    }
  }

  /**
   * searches for a FieldPair with a particular tag.
   *
   * @param tag is the FieldTag being requested.  It may not be null
   * because all fields have non-null tags.
   *
   * @return the FieldPair with a matching tag or null, if none is found.
   */
  public FieldPair findPair(String tag) {
    FieldPair pair;
    if (tag != null) {
      for (Enumeration<FieldPair> e = elements(); e.hasMoreElements(); ) {
        if (tag.equals( (pair = e.nextElement()).FieldTag)) {
          return pair;
        }
      }
    }
    return null;
  }

  /**
   * searches for a FieldPair with a particular Tag, then returns the value
   * Field from the pair.
   *
   * @param tag is the tag for the desired Field.
   *
   * @return the Value of a selected Field in a GenericRecord.
   */
  public Object findValue(String tag) {
    FieldPair pair = findPair(tag);
    if (pair != null) {
      return pair.FieldValue;
    }
    return null;
  }

  /**
   * is a predicate for testing if the GenericRecord contains a particular
   * FieldPair.
   *
   * @param request is the FieldPair being searched for.
   *
   * @return true if this GenericRecord has a FieldPair whose contents
   * match the request.  false is returned if it doesn't.  Multiple matches
   * should not happen because the Field Tags should be unique.
   */
  public boolean doesInclude(FieldPair request) {
    FieldPair pair = findPair(request.FieldTag);
    if (pair != null)  {
      if (request.FieldValue == null) {
        return ( (pair.FieldValue == null) ? true : false);
      }
      if ( (pair.FieldValue != null) &&
          (request.FieldValue.equals(pair.FieldValue))) {
        return true;
      }
    }
    return false;
  }

  /**
   * converts the Vector of Field Pairs to a Vector of Values.
   * @return a Vector containing the values of all attributes.
   */
  public Vector<Object> extractValues() {
    Vector<Object> values = new Vector<Object>(size());
    for (Enumeration<FieldPair> e = elements(); e.hasMoreElements(); ) {
      values.add( e.nextElement().FieldValue);
    }
    return values;
  }

  /**
   * makes a copy of this GenericRecord, except Saved is false.
   * The copy is intended to be used by the editor.
   * @return the copy
   */
  public GenericRecord copyRecord() {
      GenericRecord newRec = new GenericRecord(XmlTag, size());
      FieldPair pair;
      for (Enumeration<FieldPair> e = elements(); e.hasMoreElements(); ) {
          pair = e.nextElement();
          newRec.add(new FieldPair(pair.FieldTag, pair.FieldValue));
      }
      return newRec;
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XmlTag + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    return new String("A " + XmlTag + " cannot have an embedded object ("
                      + objName + ").");
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XmlTag);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    Saved = true;
    return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    return Saved;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    FieldPair pair;
    Element thisObject = new Element(XmlTag);
    for (Enumeration<FieldPair> e = elements(); e.hasMoreElements(); ) {
      pair = e.nextElement();
      pair.putXML(thisObject);
    }
    parent.addContent(thisObject);
    Saved = true;
    return null;
  }

  /**
   * registers a Factory for accepting a Store
   * with the XMLReader.
   *
   * @param tag is the XML Tag for identifying the kind of Generic Record.
   */
  static public void init(String tag) {
    XMLReader.registerFactory(tag, new RecordFactory(tag));
  }
}

/**
 * is a Class known only to the GenericRecord class for creating Records
 * from an XML document.
 */
class RecordFactory
    implements XMLEleFactory {

  /**
   * is the XML Tag of the product.
   */
  private final String RecordTag;

  /**
   * is the GenericRecord being created.
   */
  private GenericRecord NewRecord;

  /**
   * is the constructor.
   *
   *@param tag is the XML tag of the Record.
   */
  RecordFactory(String tag) {
    RecordTag = tag;
  }

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    NewRecord = new GenericRecord(RecordTag);
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    NewRecord.add(new FieldPair(tag, value));
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return NewRecord;
  }
}
/* @(#)GenericRecord.java */
