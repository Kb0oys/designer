/* Name: MetaFieldStore.java
 *
 * What;
 *  This file creates a constant Singleton that is the Vector of
 *  FieldInfos for controlling the editing of an AbstractStore FieldInfoStore
 *  (FieldVector).  Some of these values do not matter because they are properties
 *  of the FieldInfo, not of the MetaFieldStore, which controls editing a FieldInfo.
 *  Specifically, the following are not used:
 *  <ul>
 *  <li>Mandatory
 *  <li>Key tag
 *  </ul>
 */
package designer.layout.store;

import designer.gui.store.AlignmentList;
import designer.gui.store.ClassSpec;

/**
 *  This file creates a constant Singleton that is the Vector of
 *  FieldInfos for controlling the editing of an AbstractStore FieldInfoStore
 *  (FieldVector).  Some of these values do not matter because they are properties
 *  of the FieldInfo, not of the MetaFieldStore, which controls editing a FieldInfo.
 *  Specifically, the following are not used:
 *  <ul>
 *  <li>Mandatory
 *  <li>Key tag
 *  </ul>
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2005, 2009, 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class MetaFieldStore
extends FieldVector {
    /**
     * is the Singleton constant.
     */
    public static final FieldVector FieldOfFields = new MetaFieldStore();
    
    /**
     * is the constructor.  The order in which the meta-fields are added
     * is critical.  It must be the same as the order in which they reside
     * in FieldInfo, because of how JTable works - there is a field description
     * for each column (field)
     */
    public MetaFieldStore() {
        add(new FieldInfo(FieldInfo.KEY_TAG, false,
                FieldInfo.KEY_LABEL,
                false, true, FieldInfo.WIDE_WIDTH, AlignmentList.DEFAULT, "",
                String.class));
        elementAt(0).makeRecordKey();
        add(new FieldInfo(FieldInfo.VISIBLE_TAG, true,
                FieldInfo.VISIBLE_LABEL,
                true, true, FieldInfo.NARROW_WIDTH, AlignmentList.DEFAULT, new Boolean(true),
                Boolean.class));
        add(new FieldInfo(FieldInfo.LABEL_TAG, true,
                FieldInfo.LABEL_LABEL,
                true, true, FieldInfo.WIDE_WIDTH, AlignmentList.DEFAULT, "", String.class));
        add(new FieldInfo(FieldInfo.EDIT_TAG, true,
                FieldInfo.EDIT_LABEL,
                true, true, FieldInfo.NARROW_WIDTH, AlignmentList.DEFAULT, new Boolean(true),
                Boolean.class));
        add(new FieldInfo(FieldInfo.MANDATORY_TAG, false,
                FieldInfo.MANDATORY_LABEL,
                false, true, FieldInfo.MEDIUM_WIDTH, AlignmentList.DEFAULT, new Boolean(false),
                Boolean.class));
        add(new FieldInfo(FieldInfo.WIDTH_TAG, true,
                FieldInfo.WIDTH_LABEL,
                true, true, FieldInfo.NARROW_WIDTH, AlignmentList.DEFAULT, 
                new Integer(FieldInfo.NARROW_WIDTH), Integer.class));
        add(new FieldInfo(FieldInfo.ALIGNMENT_TAG, true,
                FieldInfo.ALIGNMENT_LABEL,
                true, true, FieldInfo.MEDIUM_WIDTH, AlignmentList.DEFAULT, AlignmentList.DEFAULT,
                AlignmentList.class));
        add(new FieldInfo(FieldInfo.DEFAULT_TAG, false,
                FieldInfo.DEFAULT_LABEL,
                true, true, FieldInfo.NARROW_WIDTH, AlignmentList.DEFAULT, "",
                String.class));
        add(new FieldInfo(FieldInfo.CLASS_TAG, true,
                FieldInfo.CLASS_LABEL,
                true, true, FieldInfo.MEDIUM_WIDTH, AlignmentList.DEFAULT, "",
                ClassSpec.class));
    }
}
/* @(#)MetaFieldStore.java */
