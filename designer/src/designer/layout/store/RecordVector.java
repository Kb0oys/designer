/* Name: RecordVector.java
 *
 * What;
 *  This file is a specialized class derived from Vector, that is a Vector
 *  of Vectors.  Each member Vector contains FieldPairs, where the Tag is
 *  the name of a Field and the Value piece is the Value of the Field.
 */
package designer.layout.store;

import java.util.Enumeration;
import java.util.Vector;

import org.jdom2.Element;

import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 *  This file is a specialized class derived from Vector, that is a Vector
 *  of Vectors.  Each member Vector contains FieldPairs, where the Tag is
 *  the name of a Field and the Value piece is the Value of the Field.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

/**
 * @param <T> is the generic type of constituent records.  The only possible values
 * are GenericRecord and FieldINfo
 */
public class RecordVector <T extends GenericRecord>
extends Vector<GenericRecord> implements XMLEleObject, Savable {

    /**
     * is the TAG in the XML file for identifying this instantiation of the
     * AbstractStore.
     */
    private final String XmlTag;

    /**
     * is the tag in the XML file for identifying Vector components
     */
    private final String RecordTag;
    
    /**
     * is the filter used for determining how a GenericRecord is handled by
     * the StoreTableModel.
     */
    protected SelectionStrategy MyFilter;
    
    /**
     * constructs a new instance of a RecordVector
     * @param myTag is the TAG by which the RecordVector is known in the XML file
     */
    public RecordVector(String myTag) {
        XmlTag = new String(myTag);
        RecordTag = new String(GenericRecord.DATARECORD);
      }

    /**
     * creates and returns a copy of the RecordVector.  This is intended
     * for use in editing.  If the editing is canceled, the RecordVector
     * is discarded.
     * 
     * @return a copy of the RecordVector
     */
    public RecordVector<T> makeCopy() {
        RecordVector<T> r = new RecordVector<T>(XmlTag);
        for (GenericRecord element : this) {
            r.add(element.copyRecord());
        }
        return r;
    }

    /**
     * This method converts a RecordVector to a FieldVector.  It is
     * a downcast, so is inherently unsafe.  The RecordVector should contain
     * GenericRecords which also have only the tags of FieldInfos.  They should
     * have all the tags specified by MetaFieldStore.  It may be necessary to
     * cast some Strings to classes.
     * 
     * @param tag is the XML tag to use with the FieldVector.
     * 
     * @return a copy of the RecordVector, converted to a FieldVector.
     */
    public FieldVector toFieldVector(String tag) {
        FieldVector fields = new FieldVector(size(), tag);
        for (GenericRecord element : this) {
            fields.add(new FieldInfo(element));
        }
        return fields;
    }

    /**
     * sets the selection strategy
     * @param strat is the selection strategy
     */
    public void setStrategy(SelectionStrategy strat) {
        MyFilter = strat;
    }
    
    /**
     * searches the RecordVector for a Record with a FieldPair matching
     * request.  If one is found, the Record is returned.  Though it is
     * assumed that request is for a key field:value pair, it need not
     * be.  If start is null, searching begins on the first Record in
     * RecordStore.  If start is not null and start can be found, searching
     * resumes at the following Record.
     *
     * @param request is a FieldPair that is the search pattern (wild cards
     * are not supported).
     * @param start is a Record assumed to be in the RecordStore.  If it is
     * null, searching begins at the beginning.  Otherwise, searching begins
     * at the following Record.
     *
     * @return the first Record found that contains request or null if none
     * is found.
     */
    public GenericRecord search(FieldPair request, GenericRecord start) {
      GenericRecord first;
      Enumeration<GenericRecord> e = elements();

      if (start != null) {
        while (e.hasMoreElements()) {
          first = e.nextElement();
          if (first == start) {
            break;
          }
        }
      }
      while (e.hasMoreElements()) {
        first = e.nextElement();
        if (first.doesInclude(request)) {
          return first;
        }
      }
      return null;
    }
    
    /**
     * is called by the TableModel to query if a GenericRecord should be
     * displayed or not.
     * @param rec is the GenericRecord
     * @return true if it should be displayed and false if not.  The default is
     * true.
     */
    public boolean isVisible(T rec) {
        if (MyFilter != null) {
            return MyFilter.isSelected(rec);
        }
        return true;
    }

    /**
     * is called to hide a GenericRecord on the JTable
     * @param rec is the GenericRecord
     * @param reason is an optional String to place in the GenericRecord
     * status field.
     */
    public void hide(T rec, String reason) {
        if (MyFilter != null) {
            MyFilter.unselect(rec, reason);
        }
    }
    
    /**
     * is called to determine if a GenericRecord can be deleted or not.
     * @param rec is the GenericRecord
     * @return true if it can be deleted and false if not.  The default is
     * true.
     */
    public boolean isUnProtected(T rec) {
        if (MyFilter != null) {
            return MyFilter.isDeletable(rec);
        }
        return true;
    }
    
    /**
     * is invoked to re-initialize the RecordStore
     */
    void reinit() {
        clear();
    }
    
    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        return null;
    }

    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XmlTag);
    }

    /*
     * is the method through which the object receives embedded Objects.
     * The embedded object should be a GenericRecord, in which all the value
     * pieces of all the FieldPairs are Strings.  The value pieces are
     * converted into the correct type when used (e.g. passed to the
     * editor).
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptable or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        String resultMsg = null;
        if (RecordTag.equals(objName)) {
            add((GenericRecord) objValue);
        }
        else {
            resultMsg = new String("A " + XmlTag + " cannot contain an Element ("
                    + objName + ").");
        }
        return resultMsg;
    }

    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("A " + XmlTag + " cannot contain a text field ("
                + eleValue + ").");
    }

    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        for (Enumeration<GenericRecord> e = elements(); e.hasMoreElements(); ) {
            if (! e.nextElement().isSaved()) {
                return false;
            }
        }
        return true;
    }

    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element thisObject;
        if (size() > 0) {
            thisObject = new Element(XmlTag);
            parent.addContent(thisObject);
            for (Enumeration<GenericRecord> e = elements(); e.hasMoreElements(); ) {
                ((Savable) e.nextElement()).putXML(thisObject);
            }
        }
        return null;
    }
    
    /**
     * registers a Factory for accepting a Store
     * with the XMLReader.
     * @param tag is the XML tag
     */
    public void init(String tag) {
      XMLReader.registerFactory(tag, new Factory(tag));
    }

    /**
     * is a Class known only to the RecordStore class for creating Fields
     * from an XML document.
     */
    class Factory
    implements XMLEleFactory {

        /**
         * is the product produced by the Factory.
         */
        private RecordVector<T> Product;

        /**
         * is the XML Tag of the product.
         */
        private final String PRODUCT_TAG;

        /**
         * is the constructor.
         *
         *@param tag is the XML tag of the product.
         */
        Factory(String tag) {
            PRODUCT_TAG = tag;
        }

        /*
         * tells the factory that an XMLEleObject is to be created.  Thus,
         * its contents can be set from the information in an XML Element
         * description.
         */
        public void newElement() {
            Product = new RecordVector<T>(PRODUCT_TAG);
        }

        /*
         * gives the factory an initialization value for the created XMLEleObject.
         *
         * @param tag is the name of the attribute.
         * @param value is it value.
         *
         * @return null if the tag:value are accepted; otherwise, an error
         * string.
         */
        public String addAttribute(String tag, String value) {
            String resultMsg = null;

            resultMsg = new String("A " + PRODUCT_TAG + " XML Element cannot have a " +
                    tag +
            " attribute.");
            return resultMsg;
        }

        /*
         * tells the factory that the attributes have been seen; therefore,
         * return the XMLEleObject created.
         *
         * @return the newly created XMLEleObject or null (if there was a problem
         * in creating it).
         */
        public XMLEleObject getObject() {
            return Product;
        }
    }
}
/* @(#)RecordVector.java */
