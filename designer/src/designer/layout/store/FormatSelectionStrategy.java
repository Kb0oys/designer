/*
 * Name: FormatSelectionStrategy.java
 *
 * What:
 *  This class handles the editing of the FieldVectors (RecordVector formatting).
 *  <ul>
 *  <li> the visibility of a FieldInfo is controlled by its VISIBLE attribute.  This applies
 *  to the MANDATORY records because once a FieldInfo is made invisible, there is no
 *  way to make it visible to set the VISIBLE attribute.
 *  <li> when a (user defined) FieldInfo is hidden, it is simply deleted.
 *  <li> the FieldInfo can be deleted if the MANDATORY attribute is not set.
 *  </ul>
 */
package designer.layout.store;

/**
 *  This interface is for defining classes that are contained within a GenericRecord
 *  for querying if the GenericRecord should be visible or not on an edit screen.
 *  It is the interface for the Strategy design pattern.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class FormatSelectionStrategy implements SelectionStrategy {
    /**
     * is the RecordVector that holds the GenericRecords being processed
     */
    protected final RecordVector<GenericRecord> Store;

    /**
     * is the ctor
     * @param parent is the container of the GenericRecord
     */
    public FormatSelectionStrategy(RecordVector<GenericRecord> parent) {
        Store = parent;
    }
    
    /**
     * is called to determine if the GenericRecord can be deleted from the Vector
     * or not.
     * @param rec is the GenericRecord being queried
     * @return true if it can be.
     */
    public boolean isDeletable(GenericRecord rec) {
        return !((Boolean)rec.findValue(FieldInfo.MANDATORY_TAG));
    }

    /**
     * is called to query if the containing GenericRecord should appear in the
     * JTable or not.
     * @param rec is the GenericRecord being queried
     * @return true if it should appear and false if not
     */
    public boolean isSelected(GenericRecord rec) {
        return true;
    }

    /**
     * is called to hide the GenericRecord on the JTable.  For this simple case,
     * the GenericRecord is simply removed.
     * @param rec is the GenericRecord being unselected
     * 
     * @param reason is why the GenericRecord should be hidden.
     */
    public void unselect(GenericRecord rec, String reason) {
        Store.remove(rec);
    }
}
/* @(#)FormatSelectionStrategy.java */
