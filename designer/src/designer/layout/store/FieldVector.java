/* Name: FieldVector.java
 *
 * What;
 *  This file is a specialized class derived from Vector, that is a Vector
 *  of FieldInfo.
 */
package designer.layout.store;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import org.jdom2.Element;

import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 *  This file is a specialized class derived from Vector, that is a Vector
 *  of FieldInfo.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2005, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class FieldVector
    extends Vector<FieldInfo> implements XMLEleObject, Savable {

    /**
     * is the TAG in the XML file for identifying this instantiation of the
     * AbstractStore.
     */
    private final String XmlTag;
    
    /**
     * is the tag in the XML file for identifying Vector components
     */
    private final String RecordTag;

  /**
   * no argument constructor.
   */
  public FieldVector() {
    super();
    XmlTag = "";
    RecordTag = GenericRecord.EDITRECORD;
  }

  /**
   * constructor with known size.
   *
   * @param count is the initial size.
   * @param tag is the XML tag
   */
  public FieldVector(int count, String tag) {
    super(count);
    RecordTag = GenericRecord.EDITRECORD;
    XmlTag = tag;
  }

  /**
   * the usual ctor
   * @param fields are the default field definitions
   * @param tag is the XML tag for this FieldVector
   */
  public FieldVector(FieldInfo[] fields, String tag) {
      XmlTag = tag;
      RecordTag = GenericRecord.EDITRECORD;
      loadFields(fields);
  }
  
  /**
   * loads the FieldVector with the mandatory fields.
   *
   * @param fields are the initial, mandatory fields.
   */
  public void loadFields(FieldInfo[] fields) {
      for (int i = 0; i < fields.length; ++i) {
          addElement(fields[i]);
      }
  }
  
  /**
   * this method extracts the names of all the visible fields from the
   * field description Vector.
   *
   * @return the FieldName property from each field in which the
   * FieldVisible flag is set.
   */
  protected Vector<String> extractVisibleNames() {
    FieldInfo info;
    Vector<String> vec = new Vector<String>(size());
    for (Enumeration<FieldInfo> e = elements(); e.hasMoreElements(); ) {
      info = e.nextElement();
      if (info.getVisible()) {
        vec.add(info.getKeyField());
      }
    }
    return vec;
  }

  /**
   * returns a count of the number of visible Fields (columns).
   *
   * @return the number of FieldInfos in the EditControl Vector with the
   * Visible bit set.
   */
  public int countVisible() {
    int sum = 0;
    FieldInfo info;
    for (Enumeration<FieldInfo> e = elements(); e.hasMoreElements(); ) {
      info = e.nextElement();
      if (info.getVisible()) {
        ++sum;
      }
    }
    return sum;
  }

  /**
   * searches the FieldInfos for one whose name matches the search name.
   *
   * @param match is the name of the FieldInfo being requested.  match should
   * not be null.
   *
   * @return the index of FieldInfo.  If not found, -1 is returned.
   */
  public int getFieldIndex(String match) {
    for (int index = 0; index < size(); ++index) {
      if ( elementAt(index).getKeyField().equals(match)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * searches the FieldInfos for one whose name matches the search name.
   *
   * @param match is the name of the FieldInfo being requested.  match should
   * not be null.
   *
   * @return the FieldInfo if found; null, if not found.
   */
  public FieldInfo getFieldInfo(String match) {
    int index = getFieldIndex(match);
    if (index >= 0) {
      return elementAt(index);
    }
    return null;
  }

  /**
   * locates the index of the key field.
   *
   * @return the index of the first field found that has the Mandatory flag
   * set.
   */
  public int getKeyindex() {
    FieldInfo info;
    for (int iter = 0; iter < size(); ++iter) {
      info = elementAt(iter);
      if (info.getMandatory()) {
        return iter;
      }
    }
    return 0;
  }

  /**
   * determines the tag of the field containing the key.  It is essential
   * that at least one FieldInfo have the Mandatory flag set.  That marks
   * the key field.  The FieldName of the first FieldInfo found with
   * Mandatory set is returned; thus, secondary keys are not recognized.
   *
   * @return the tag of the key field, the one that has unique values.  Null
   * should never be returned, but it will be if no key is found.
   */
  public String getKeyField() {
    FieldInfo info;
    for (Enumeration<FieldInfo> e = elements(); e.hasMoreElements(); ) {
      info = e.nextElement();
      if (info.isRecordKey()) {
        return info.getKeyField();
      }
    }
    return null;
  }

  /**
   * creates a new GenericRecord, using the default values from each
   * FieldInfo.
   *
   * @param key is placed in the FieldPair in the key field.
   * @param tag is the tag for the GenericRecord.
   *
   * @return a GenericRecord of default values.
   */
  public GenericRecord createDefaultRecord(String tag, String key) {
    int keyIndex = getKeyindex();
    GenericRecord rec = new GenericRecord(tag, size());
    FieldInfo info;
    for (Enumeration<FieldInfo> e = elements(); e.hasMoreElements(); ) {
      info = e.nextElement();
      rec.add(info.createPair());
    }
    if (keyIndex >= 0) {
      rec.elementAt(keyIndex).FieldValue = key;
    }
    return rec;
  }

  /**
   * This method starts by making a copy of the FieldVector.  Then it 
   * adjusts the CLASS fields of all contained FieldInfos to be classes,
   * rather than the String names of classes.  This copy is needed so that
   * the user can adjust column widths (the WIDTH field of each FieldInfo),
   * cancel the changes, and have the widths revert back to their previous
   * values.
   *   
   * @return a copy of the FieldVector
   * 
   */
  public FieldVector toFormatting() {
      String recordKeyName = getKeyField();
      FieldVector copy = new FieldVector(size(), getTag());
      for (Enumeration<FieldInfo> e = elements(); e.hasMoreElements(); ) {
          copy.add(e.nextElement().copyFields());
      }
      copy.getFieldInfo(recordKeyName).makeRecordKey();
      return copy;
  }
 
  /**
   * This method converts a FieldVector to a RecordVector.  It actually is one,
   * but due to the restrictions on inheritance in Java generics, it cannot be
   * cast to one.  The way to make it one is to convert each composing FieldInfo
   * to a GenericRecord and add that GenericRecord to a new RecordVector.
   * 
   * @return a copy of the FieldVector converted to a RecordVector
   */
  public RecordVector<GenericRecord> toData() {
      RecordVector<GenericRecord> data = new RecordVector<GenericRecord>(GenericRecord.DATARECORD);
      for (FieldInfo element : this) {
          data.add(element.toGeneric());
      }
      return data;
  }

  /**
   * is a method that runs through a whole RecordVector, fixing up each
   * GenericRecord to fit this format.
   * @param vec is the RecordVector being fixed up.
   */
  public void syncUpVector(RecordVector<GenericRecord> vec) {
      for (GenericRecord element : vec) {
          syncUpRecord(element);
      }
  }
  
  /**
   * This method both verifies that a GenericRecord has all (and only all) the required
   * fields and that each field is of the appropriate class.
   * <p>
   * Each field that does not have a format description (e.g. a FieldInfo in this
   * FieldVector) is removed.
   * <p>
   * A field in the GenericRecord is created for each FieldInfo that that is missing,
   * using the default value for FieldInfo.
   * <p>
   * The value of each field of the wrong class is replaced with the default value from
   * the FieldInfo.
   * 
   * @param rec is the GenericRecord being fixed up.
   */
  public void syncUpRecord(GenericRecord rec) {
      FieldInfo desc;
      FieldPair pair;
      // The first phase is to remove fields that do not have matching FieldInfos.
      // For those that do match, this method ensures that the class of the value
      // matches.
      for (Iterator<FieldPair> element = rec.iterator(); element.hasNext(); ) {
          pair = element.next();
          desc = getFieldInfo(pair.FieldTag);
          if (desc == null) {
              element.remove();
          }
          else {
              pair.verifyClass(desc.getFieldClass());
          }
      }
      
      // The second phase adds fields that are missing
      for (Iterator<FieldInfo> element = iterator(); element.hasNext(); ) {
          desc = element.next();
          pair = rec.findPair(desc.getKeyField());
          if (pair == null) {
              rec.add(desc.createPair());
          }
      }
  }

  /**
   * is a method that runs through a whole RecordVector, converting each
   * GenericRecord to fit this format.  The RecordVector should have been
   * read in  from an XML file, so all values are Strings.
   * @param vec is the RecordVector being converted.
   */
  public void vectorFromXML(RecordVector<GenericRecord> vec) {
      for (GenericRecord element : vec) {
          recordFromXML(element);
      }
  }
  
  /**
   * This method both verifies that a GenericRecord has all (and only all) the required
   * fields and that each field is of the appropriate class.  It is called to convert the
   * XML strings for each XML attribute into the appropriate class.
   * <p>
   * Each field that does not have a format description (e.g. a FieldInfo in this
   * FieldVector) is removed.
   * <p>
   * A field in the GenericRecord is created for each FieldInfo that that is missing,
   * using the default value for FieldInfo.
   * <p>
   * The value of each field is assumed to be a String representation of the real
   * value, created by "toName()" when the value was written to the XML file. 
   * 
   * @param rec is the GenericRecord being fixed up.
   */
  public void recordFromXML(GenericRecord rec) {
      FieldInfo desc;
      FieldPair pair;
      Class<?> requiredClass;
      // The first phase is to remove fields that do not have matching FieldInfos.
      // For those that do match, this method converts from a String representation
      // of the value to the actual value.
      for (Iterator<FieldPair> element = rec.iterator(); element.hasNext(); ) {
          pair = element.next();
          desc = getFieldInfo(pair.FieldTag);
          if (desc == null) {
              element.remove();
          }
          else {
              requiredClass = desc.getFieldClass();
              pair.FieldValue = ((String)pair.FieldValue).trim();
              pair.verifyClass(requiredClass);
          }
      }
      // The second phase adds fields that are missing
      for (Iterator<FieldInfo> element = iterator(); element.hasNext(); ) {
          desc = element.next();
          pair = rec.findPair(desc.getKeyField());
          if (pair == null) {
              rec.add(desc.createPair());
          }
      }
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XmlTag + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   * The embedded object should be a GenericRecord, in which all the value
   * pieces of all the FieldPairs are Strings.  The value pieces are
   * converted into the correct type for use (e.g. passed to the
   * editor).
   * <p>
   * The FieldInfo is constructed as a GenericRecord (FieldInfo uses the same
   * factory as GenericRecord, but with a different XML tag), so must be converted
   * to a FieldInfo.
   * <p>
   * The FieldVector is pre-populated with the default FieldInfos when it is
   * constructed.  Yet, the XML file may contain modified versions of those
   * defaults.  Thus, the defaults are removed.  The order of the FieldInfos
   * in the XML file dictate the order that the fields will appear on the edit
   * screen, so that order should be preserved.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptable or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    FieldPair pair;
    FieldInfo def;
    if (RecordTag.equals(objName)) {
        pair = ((GenericRecord) objValue).findPair(FieldInfo.KEY_TAG);
        if (pair == null) {
            resultMsg = new String(XmlTag + " has a malformed element missing a key tag: " + objName);
        }
        else {
            def = getFieldInfo((String) pair.FieldValue);
            if (def != null) {
                remove(def);
            }
            MetaFieldStore.FieldOfFields.recordFromXML((GenericRecord) objValue);
            def = new FieldInfo((GenericRecord) objValue);
            def.setSaved(true);
            add(def);
        }
    }
    else {
      resultMsg = new String("A " + XmlTag + " cannot contain an Element ("
                             + objName + ").");
    }
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XmlTag);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    return null;
  }

  /**
   * registers a Factory for accepting a Store
   * with the XMLReader.
   * @param tag is the XML tag for the FieldInfo
   * @param defFields is the default FieldInfos
   */
  public void init(String tag, FieldInfo[] defFields) {
    XMLReader.registerFactory(tag, new Factory(tag, defFields));
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
      for (Enumeration<FieldInfo> e = elements(); e.hasMoreElements(); ) {
          if (! e.nextElement().isSaved()) {
              return false;
          }
        }
      return true;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    Element thisObject;
    if (size() > 0) {
      thisObject = new Element(XmlTag);
      parent.addContent(thisObject);
      for (Enumeration<FieldInfo> e = elements(); e.hasMoreElements(); ) {
        e.nextElement().putXML(thisObject);
      }
    }
    return null;
  }

  /**
   * is a Class known only to the RecordStore class for creating Fields
   * from an XML document.
   */
  class Factory
      implements XMLEleFactory {

    /**
     * is the product produced by the Factory.
     */
    private FieldVector Product;

    /**
     * is the XML Tag of the product.
     */
    private final String PRODUCT_TAG;

    /**
     * are the initial fields
     */
    private final FieldInfo[] DEFAULT_FIELDS;
    /**
     * is the constructor.
     *
     * @param recordVector is the product produced by the Factory.
     *
     *@param tag is the XML tag of the product.
     */
    Factory(String tag, FieldInfo[] defFields) {
      PRODUCT_TAG = tag;
      DEFAULT_FIELDS = defFields;
    }

    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
      Product = new FieldVector(DEFAULT_FIELDS, PRODUCT_TAG);
    }

    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
      String resultMsg = null;

      resultMsg = new String("A " + PRODUCT_TAG + " XML Element cannot have a " +
                             tag +
                             " attribute.");
      return resultMsg;
    }

    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
      return Product;
    }
  }
}
/* @(#)FieldVector.java */
