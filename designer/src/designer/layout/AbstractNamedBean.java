/* Name AbstractNamedBean.java
 *
 * What:
 *  This class is a simple implementation of a NamedBean.  It is a stripped
 *  down version of the JMRI AbstractNamedBean class.
 */
package designer.layout;

/**
 *  This class is a simple implementation of a NamedBean.  It is a stripped
 *  down version of the JMRI AbstractNamedBean class.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
abstract public class AbstractNamedBean implements NamedBean {
    java.beans.PropertyChangeSupport pcs = new java.beans.PropertyChangeSupport(this);
    public synchronized void addPropertyChangeListener(java.beans.PropertyChangeListener l) {
        pcs.addPropertyChangeListener(l);
    }
    public synchronized void removePropertyChangeListener(java.beans.PropertyChangeListener l) {
        pcs.removePropertyChangeListener(l);
    }

    /**
     * Number of current listeners. May return -1 if the 
     * information is not available for some reason.
     */
    public synchronized int getNumPropertyChangeListeners() {
        return pcs.getPropertyChangeListeners().length;
    }
    protected void firePropertyChange(String p, Object old, Object n) { 
        if (pcs!=null) pcs.firePropertyChange(p,old,n);
    }

    public void dispose() {
        pcs = null;
    }

    abstract public void setProperty(Object key, Object value);
    
    abstract public Object getProperty(Object key);
}
/* @(#)AbstractNamedBean.java */
