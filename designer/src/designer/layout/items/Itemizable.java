/*
 * Name: Itemizable.java
 *
 * What:
 *   This file contains an interface required by all sub-components of
 *   a Section.
 */
package designer.layout.items;

import designer.gui.GridTile;
import designer.layout.xml.XMLEleObject;

/**
 * defines the interface for Objects that ar sub-components of a Section.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
 public interface Itemizable extends XMLEleObject {

   /**
    * tells the sub-component where its Section is, so that the sub-component
    * can register itself and retrieve anything else it needs from the Section.
    *
    * @param sec is the Section containing the sub-component.
    *
    * @see Section
    */
   public void addSelf(Section sec);

   /**
    * asks the sub-component to create a copy of itself.
    *
    * @param sec is the Section containing the sub-component.
    * @return a copy ofthe Itemizable Object.
    *
    * @see Section
    */
   public Itemizable copy(Section sec);

   /**
    * asks the sub-component to create a shallow copy of itself.  A shallow
    * copy copies just the structure and omits the item details, such as decoder
    * address, block name, etc.
    *
    * @param sec is the Section containing the sub-component.
    * @return a copy ofthe Itemizable Object.
    *
    * @see Section
    */
   public Itemizable shallowCopy(Section sec);

   /**
    * asks the sub-component to install itself on the painting surface.
    *
    * @param tile is the GridTile to install a frill on.
    *
    * @see designer.gui.GridTile
    */
   public void install(GridTile tile);

   /**
    * asks the sub-component to remove itself from the painting surface.
    */
   public void uninstall();
 }
 /* @(#)Itemizable.java */