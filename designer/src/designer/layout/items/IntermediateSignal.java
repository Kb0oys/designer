/* Name: IntermediateSignal.java
 *
 * What:
 *   This class holds the Frill (icon) for an intermediate signal.  An
 *   intermediate signal has a physical signal, but no representation
 *   on the dispatcher panel.  However, it is useful when designing a
 *   panel to see where the intermediates are without fishing for them.
 *   Thus, this class substitutes for a PanelSignal in a SecSignal
 *   without a PanelSignal.  Because an intermediate should never appear
 *   on a CATS screen, this class is never saved.
 */
package designer.layout.items;

import java.io.PrintStream;

import org.jdom2.Element;

import designer.gui.GridTile;
import designer.gui.frills.FrillLoc;
import designer.gui.frills.IntermediateFrill;
/**
 *   This class holds the Frill (icon) for an intermediate signal.  An
 *   intermediate signal has a physical signal, but no representation
 *   on the dispatcher panel.  However, it is useful when designing a
 *   panel to see where the intermediates are without fishing for them.
 *   Thus, this class substitutes for a PanelSignal in a SecSignal
 *   without a PanelSignal.  Because an intermediate should never appear
 *   on a CATS screen, this class is never saved.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2008, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class IntermediateSignal extends PanelSignal {

    /**
     * constructs an intermediate signal icon for a particular
     * edge.
     * @param edge is the edge that the track holding the icon
     * terminates on.
     */
    public IntermediateSignal(int edge) {
        super();
        setEdge(edge);
    }
    
    /**
     * creates the FrillLoc and orientation for the intermediate icon.
     * The FrillLoc is the center of the Edge that the track terminates
     * on.  The orientation is LEFT or TOP, so that no adjustment is made
     * for a non-existing base.
     * @param edge
     */
    private void setEdge(int edge) {
        switch (edge) {
        case Edge.RIGHT:
            MyLocation = FrillLoc.newFrillLoc("RIGHTCENT");
            MyOrientation = Edge.LEFT;
            break;
            
        case Edge.BOTTOM:
            MyLocation = FrillLoc.newFrillLoc("LOWCENT");
            MyOrientation = Edge.TOP;
            break;
            
        case Edge.LEFT:
            MyLocation = FrillLoc.newFrillLoc("LEFTCENT");
            MyOrientation = Edge.LEFT;
            break;
        
        case Edge.TOP:
            MyLocation = FrillLoc.newFrillLoc("UPCENT");
            MyOrientation = Edge.TOP;
            break;
            
        default:
        }
        if (MyLocation != null) {
            TheFrill = new IntermediateFrill(MyLocation, MyOrientation, 1);
        }
    }
    
    /**
     * asks the sub-component to create a copy of itself.
     *
     * @return an identical copy of itself.
     */
    public PanelSignal copy() {
  	  PanelSignal newCopy = new IntermediateSignal(0);
  	  newCopy.MyLocation = FrillLoc.newFrillLoc(MyLocation.toString());
  	  newCopy.MyOrientation = MyOrientation;
  	  newCopy.TheFrill = new IntermediateFrill(MyLocation, MyOrientation, 1);
  	  copyIcon(newCopy);
  	  return newCopy;
    }
    
    /**
     * writes the Object's contents to an XML file.  Since an Intermediate
     * PanelSignal is never saved, this routine does nothing.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
      return null;
    }

    /**
     * tells the Object where to write its state.
     *
     * @param outStream is where to write it
     * @param indent is the indentation level prior to the call
     *
     * @return null if there was no problem writing or an error string
     * if there was.
     */
    public String putXML(PrintStream outStream, String indent) {
      return null;
    }
    
    /**
     * tells the intermediate PanelSignal where its Icon is shown.
     *
     * @param tile is the GridTile where painting happens.
     *
     * @see designer.gui.GridTile
     */
    public void install(GridTile tile) {
      SignalTile = tile;
      if ((tile != null) && (TheFrill != null)) {
        SignalTile.addFrill(TheFrill);
      }
    }


    /**
     * removes itself from the GridTile.
     */
    public void uninstall() {
      if ( (SignalTile != null) && (TheFrill != null)) {
        SignalTile.delFrill(TheFrill);
      }
    }
}
/* @(#)IntermediateSignal.java */
