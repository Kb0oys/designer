/* Name: AspectTable.java
 *
 * What:
 *  This class defines an object that holds the information for sending to
 *  decoders that sets signals.  It the implementation of the indications
 *  defined in the physical Signal's AspectMap.
 */
package designer.layout.items;

import designer.layout.xml.*;
import org.jdom2.Element;

/**
 * is a class for holding the instructions for sending to the decoders the
 * commands to set the lights/semaphore blades, to states corresponding
 * to the strings in the physical signal's AspectMap.  The table is expected
 * to be non-dynamic.  It is created.  IOSpecLists are added to it, but the
 * IOSPecLists are never edited or removed.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2011, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class AspectTable
    implements XMLEleObject, Savable {

  /**
   * is the tag for identifying an AspectTable in the XMl file.
   */
  static final String XML_TAG = "ASPECTTBL";

  /**
   * is the table of states and heads.
   */
  private HeadStates[] Instructions;

  /**
   * is the (optional) trigger for lighting/extinguishing
   * the signal
   */
  private ApproachTrigger Trigger;
  
  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * is the constructor.
   */
  public AspectTable() {
  }

  /**
   * adds a list of commands for a head to the table.
   *
   * @param headList is the list of commands for the new head.
   *
   * @see HeadStates
   */
  public void addHead(HeadStates headList) {
    int i;
    if (Instructions == null) {
      Instructions = new HeadStates[1];
      Instructions[0] = headList;
    }
    else {
      HeadStates[] temp = new HeadStates[Instructions.length + 1];
      for (i = 0; i < Instructions.length; ++i) {
        temp[i] = Instructions[i];
      }
      temp[i] = headList;
      Instructions = temp;
    }
  }

  /**
   * return the command lists.
   *
   * @return the number of heads with command lists.
   */
  public int getListCount() {
    if (Instructions != null) {
      return Instructions.length;
    }
    return 0;
  }

  /**
   * return the list for a specific head.
   *
   * @param head is the number of the head.
   *
   * @return the command list for the head, if it exists or null, if
   * it doesn't exist.
   *
   * @see HeadStates
   */
  public HeadStates getList(int head) {
    if ( (Instructions != null) && (head >= 0) && (head < Instructions.length)) {
      return Instructions[head];
    }
    return null;
  }

  /**
   * constructs an Enumeration over the sorted list of unique IOSPecs
   * used by the signal head.
   *
   * @return the Enumeration.
   *
   * @see IOSpec
   */
//  public Enumeration getDecoderAddresses() {
//    return new AddressEnumeration().elements();
//  }

  /**
   * return the user defined trigger for approach lighting.  If
   * approach lighting is not used or the default (approach block
   * detection) is used, null is returned.
   * @return the IOSpec that triggers lighting.  It may be null.
   */
  public IOSpec getApproachTrigger() {
	  if (Trigger == null) {
		  return null;
	  }
	  return Trigger.getTrigger();
  }
  
  /**
   * sets the user defined approach lighting trigger.
   * It can be null.
   * @param trigger is the IOSpec used to trigger the approach lighting
   */
  public void setApproachTrigger(IOSpec trigger) {
	  if (Trigger == null) {
		  Trigger = new ApproachTrigger();
	  }
	  Trigger.setTrigger(trigger);
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptable or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    if (HeadStates.XML_TAG.equals(objName)) {
      addHead( (HeadStates) objValue);
    }
    else if (ApproachTrigger.XML_TAG.equals(objName)) {
    	Trigger = (ApproachTrigger) objValue;
    }
    else {
      resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                             + objName + ").");
    }
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    Saved = true;
    return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    boolean children = true;
    if (Instructions != null) {
      for (int i = 0; i < Instructions.length; ++i) {
        children &= Instructions[i].isSaved();
      }
    }
    if (Trigger != null) {
    	children &= Trigger.isSaved();
    }
    Saved = children;
    return Saved;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    if (Instructions != null) {
      Element myObject = new Element(XML_TAG);
      for (int i = 0; i < Instructions.length; ++i) {
        Instructions[i].putXML(myObject);
      }
      if (Trigger != null) {
    	  Trigger.putXML(myObject);
      }
      parent.addContent(myObject);
    }
    Saved = true;
    return null;
  }

  /**
   * asks the sub-component to create a copy of itself.
   *
   * @return an identical copy of itself.
   */
  public AspectTable copy() {
    AspectTable newCopy = new AspectTable();
    if (Instructions != null) {
      for (int i = 0; i < Instructions.length; ++i) {
        newCopy.addHead(Instructions[i]);
      }
    }
    newCopy.Saved = false;
    return newCopy;
  }

  /**
   * registers an AspectTableFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new AspectTableFactory());
    XMLReader.registerFactory(ApproachTrigger.XML_TAG, new ApproachTriggerFactory());
    HeadStates.init();
  }

  /**************************************************************************
   * An inner class.
   ************************************************************************/

  /**
   * is an inner class for providing an Enumeration over the decoder
   * addresses used by all Heads.  This is not thread safe.
   *
   * The IOSpecs returned are sorted by ascending address.
   */
//  class AddressEnumeration
//      extends Vector {
//
//    /**
//     * constructs the Enumeration.
//     */
//    AddressEnumeration() {
//      for (int head = 0; head < Instructions.length; ++head) {
//        for (Enumeration e = Instructions[head].elements();
//             e.hasMoreElements(); ) {
//          ( (IOSpec) e.nextElement()).sortInsert(this);
//        }
//      }
//    }
//  }
}

/**
 * is a Class known only to the AspectTable class for creating AspectTables from
 * an XML document.  Its purpose is to collect the decoder instructions for
 * each head, for each head state.
 */
class AspectTableFactory
    implements XMLEleFactory {

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    resultMsg = new String("An " + AspectTable.XML_TAG +
                           " XML Element cannot have a " + tag +
                           " attribute.");
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new AspectTable();
  }
}
/* @(#)AspectTable.java */
