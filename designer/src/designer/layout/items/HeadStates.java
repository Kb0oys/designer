/* Name: HeadStates.java
 *
 * What:
 *  This class collects all the presentation states and decoder instructions
 *  for creating them, into one data structure.
 */
package designer.layout.items;

import designer.layout.xml.*;

import java.util.Enumeration;
import java.util.Vector;

import org.jdom2.Element;

/**
 * is a class for collecting the list of signal head presentation states
 * and instructions into a single data structure.  Objects of this class
 * are intended to be non-dynamic.  They are created, HeadAspects are added
 * to them, but they are not edited.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class HeadStates extends Vector<AspectCommand>
implements XMLEleObject, Savable {
    /**
     * is the tag for identifying a SecSignal in the XMl file.
     */
    static final String XML_TAG = "HEADSTATES";

    /**
     * is the tag for identifying a JMRI SignalHead system name.
     */
    static final String XML_SIGNALHEAD = "SIGNALHEAD";
       
    /**
     * is the attribute tag identifying the aspect
     * as needing software assistance.
     */
    static final String ASSISTANCE ="ASSISTANCE";
    
    /**
     * is the username of a SignalHead.  This
     * is so that scripts have a constant System Name.
     */
    private String UserName;
        
    /**
     * is true if software handles flashing lights.
     */ 
    private boolean SWFlashing = false;
    
    /**
     *  a true flag means that it has been saved,
     */
    private boolean Saved = true;
    
    /**
     * adds a signal presentation String and its commands to the list.
     *
     * @param state is the String an commands.  No checking is done to
     * ensure the String has not already been added.
     */
    public void addElement(AspectCommand state) {
        super.addElement(state);
        Saved = false;
    }
    
    /**
     * finds the IOSpec for a given presentation label.
     *
     * @param label is the presentation label.
     *
     * @return the IOSpec for that label (if it exists) or null
     * (if it does not exist).
     *
     * @see designer.layout.items.IOSpec
     */
    public IOSpec findDecoders(String label) {
        AspectCommand aState;
        
        for (Enumeration<AspectCommand> e = elements(); e.hasMoreElements(); ) {
            aState = e.nextElement();
            if (aState.getPresentationLabel().equals(label)) {
                return aState.getCommand();
            }
        }
        return null;
    }
    
    /**
     * remembers if software performs the flashing.
     * 
     * @param sw is true if software times flashing.
     */
    public void setAssist(boolean sw) {
        SWFlashing = sw;
    }
    
    /**
     * is called to determine if software performs
     * flashing.
     * @return true if software does the timing; false
     * if hardware has internal timing.
     */
    public boolean getAssist() {
        return SWFlashing;
    }

    /**
     * sets the JMRI System Name for a SignalHead
     * 
     * @param name is the System Name by which the
     * SignalHead is known.
     */
    public void setName(String name) {
      UserName = name;
    }

    /**
     * gets the JMRI System Name for a SignalHead
     * 
     * @return the System Name by which the
     * SignalHead is known.
     */
    public String getName() {
        if (UserName != null) {
            return new String(UserName);
        }
        return null;
    }
    
    /**
     * constructs an Enumeration over the sorted list of unique IOSPecs
     * used by the signal head.
     *
     * @return the Enumeration.  IOSpec objects are returned from the
     * Enumeration.
     *
     * @see designer.layout.items.IOSpec
     */
//    public Enumeration getDecoderAddresses() {
//        return new AddressEnumeration().elements();
//    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("A " + XML_TAG + " cannot contain a text field ("
                + eleValue + ").");
    }
    
    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        String resultMsg = null;
        if (AspectCommand.XML_TAG.equals(objName)) {
            addElement((AspectCommand) objValue);
        }
        else {
            resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                    + objName + ").");
        }
        return resultMsg;
    }
    
    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }
    
    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        Saved = true;
        return null;
    }
    
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        boolean children = true;
        for (Enumeration<AspectCommand> e = elements(); e.hasMoreElements(); ) {
            children &= e.nextElement().isSaved();
        }
        return Saved && children;
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element myObject = new Element(XML_TAG);
        for (Enumeration<AspectCommand> e = elements(); e.hasMoreElements(); ) {
            e.nextElement().putXML(myObject);
        }
        if (SWFlashing) {
            myObject.setAttribute(ASSISTANCE, "true");
        }
        if (UserName != null) {
            myObject.setAttribute(XML_SIGNALHEAD, UserName);
        }
        parent.addContent(myObject);
        Saved = true;
        return null;
    }
    
    /**
     * registers a HeadStatesFactory with the XMLReader.
     */
    static public void init() {
        XMLReader.registerFactory(XML_TAG, new HeadStatesFactory());
        AspectCommand.init();
    }
}

/**
 * is a Class known only to the HeadStates class for creating HeadStates from
 * an XML document.  Its purpose is to accumulate all the AspectCommands for
 * the signal head.
 */
class HeadStatesFactory
implements XMLEleFactory {
    
    /**
     * if false, then no software assistance is needed to
     * present the appearance.  If true, then software
     * is needed for flashing.
     */
    private boolean SWAssist = false;

    /**
     * is the name of a JMRI SignalHead object to use for driving
     * the lights.
     */
    private String JMRIUserName;
    
    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        SWAssist = false;
        JMRIUserName = null;
    }
    
    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        String resultMsg = null;
        
        if (HeadStates.ASSISTANCE.equals(tag)) {
            SWAssist = true;
        }
        else if (HeadStates.XML_SIGNALHEAD.equals(tag)) {
            JMRIUserName = value;
          }
        else {
            resultMsg = new String("A " + HeadStates.XML_TAG +
                    " XML Element cannot have a " + tag +
            " attribute.");
        }
        return resultMsg;
    }
    
    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        HeadStates hs = new HeadStates();
        if (JMRIUserName != null) {
            hs.setName(JMRIUserName);
          }
        hs.setAssist(SWAssist);
        return hs;
    }   
}
/* @(#)HeadStates.java */
