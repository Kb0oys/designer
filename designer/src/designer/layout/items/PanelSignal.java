/* Name: PanelSignal.java
 *
 * What:
 *   This class is the container for all the information about displaying
 *   a Section's signal.
 */

package designer.layout.items;

import designer.gui.CtcFont;
import designer.gui.GridTile;
import designer.gui.frills.FrillLoc;
import designer.gui.frills.SignalFrill;
import designer.gui.frills.LightFrill;
import designer.gui.frills.SemaphoreFrill;
import designer.layout.xml.*;

import org.jdom2.Element;

/**
 * is a container for all the information about displaying a Section's signal.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class PanelSignal
    implements XMLEleObject, Savable {
  /**
   * is the tag for indentifying a PanelSignal in the XMl file.
   */
  static final String XML_TAG = "PANELSIGNAL";

  /**
   * is the XML tag for identifying the Signal's location in the GridTile.
   */
  static final String SIG_LOCATION = "SIGLOCATION";

  /**
   * is the XML tag for identifying the Signal's orientation.
   */
  static final String SIG_ORIENT = "SIGORIENT";

  /**
   * is the XML tag for indentifying the Signal's type.
   */
  static final String SIG_PANEL_TYPE = "SIGPANTYPE";

  /**
   * is the values of the Signal's type.
   */
  static final String[] SIG_TYPE = {
      "LAMP1",
      "LAMP2",
      "LAMP3",
      "SEM1",
      "SEM2",
      "SEM3"
  };

  /**
   * is a look up table for the number of heads corresponding to each of the
   * above.
   */
  static final int[] SIGNAL_HEADS = {
      1, 2, 3, 1, 2, 3
  };

  /**
   * is a look up table for the kind of signal, corresponding to the above.
   */

  static final boolean[] LAMP = {
      true, true, true, false, false, false
  };

  /**
   *  The GridTile where painting happens.
   */
  protected GridTile SignalTile;

  /**
   *  The Frill for displaying the Signal Icon.
   */
  protected SignalFrill TheFrill;

  /**
   *  The location of the Signal Icon in the GridTile.
   */
  protected FrillLoc MyLocation;

  /**
   * The orientation of the Signal Icon.
   */
  protected int MyOrientation;

  /**
   * The number of heads.
   */
  private int MyHeadCount;

  /**
   * The kind of Icon - true for light and false for semaphore.
   */
  private boolean MyLamp;

  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * constructs a PanelSignal, given no other information
   */
  public PanelSignal() {
    MyLamp = true;
    MyHeadCount = 1;
  }

  /**
   * retrieves the PanelSignal's location in its GridTile.
   *
   * @return the PanelSignal's location in the Grid.  Null is a valid value,
   * indicating that the PanelSignal is not being shown.
   *
   * @see designer.gui.frills.FrillLoc
   */
  public FrillLoc getLocation() {
    return MyLocation;
  }

  /**
   * changes the location of the PanelSignal in its GridTile.
   *
   * @param loc is the new location.
   *
   * @see designer.gui.frills.FrillLoc
   */
  public void setLocation(FrillLoc loc) {
    MyLocation = loc;
    Saved = false;
  }

  /**
   * retrieves the Icon's orientation.
   *
   * @return the PanelSignal's orientation.  UNKNOWN is a valid value.
   */
  public int getSigOrient() {
    return MyOrientation;
  }

  /**
   * changes the Icon's orientation.
   *
   * @param orient is the new orientation.  UNKNOWN is valid.
   */
  public void setOrient(int orient) {
    MyOrientation = orient;
    Saved = false;
  }

  /**
   * retrieves the number of heads.
   *
   * @return the number of heads.  0 is invalid.
   */
  public int getHeadCount() {
    return MyHeadCount;
  }

  /**
   * retrieves the signal type.
   *
   * @return true for lights or false for semaphore.
   */
  public boolean isLampType() {
    return MyLamp;
  }

  /** sets the type of Icon and number of heads.
   *
   * @param type is true for lights and false for semaphore.
   * @param num is the number of heads (1-3).
   */
  public void setParms(boolean type, int num) {
    MyLamp = type;
    MyHeadCount = num;
  }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    MyLocation = FrillLoc.newFrillLoc(eleValue);
    return null;
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    return new String("A " + XML_TAG + " cannot contain an Element ("
                      + objName + ").");
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    Saved = true;
    return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    return Saved;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    if (TheFrill != null) {
      Element thisObject = new Element(XML_TAG);
      int sigType;

      if (MyLocation != null) {
        thisObject.setAttribute(SIG_LOCATION, MyLocation.toString());
      }
      if (MyOrientation != CtcFont.NOT_FOUND) {
        thisObject.setAttribute(SIG_ORIENT, Edge.fromEdge(MyOrientation));
      }
      
      // This protects against a signal with 0 heads.
      MyHeadCount = Math.max(MyHeadCount, 1);
      if (MyLamp) {
        sigType = MyHeadCount - 1;
      }
      else {
        sigType = MyHeadCount + 2;
      }
      thisObject.setAttribute(SIG_PANEL_TYPE, SIG_TYPE[sigType]);
      parent.addContent(thisObject);
    }
    Saved = true;
    return null;
  }

  /**
   * tells the PanelSignal where its Icon is shown.
   *
   * @param tile is the GridTile where painting happens.
   *
   * @see designer.gui.GridTile
   */
  public void install(GridTile tile) {

    SignalTile = tile;
    if (tile != null) {
      if (MyLamp) {
        TheFrill = new LightFrill(MyLocation, MyOrientation, MyHeadCount);
      }
      else {
        TheFrill = new SemaphoreFrill(MyLocation, MyOrientation, MyHeadCount);
      }
      SignalTile.addFrill(TheFrill);
    }
  }

  /**
   * asks the sub-component to create a copy of itself.
   *
   * @return an identical copy of itself.
   */
  public PanelSignal copy() {
	  PanelSignal newCopy = new PanelSignal();
	  copyIcon(newCopy);
	  return newCopy;
  }
  
  /**
   * fills in the contents of a PanelSignal copy
   * @param newCopy is the empty copy
   */
  protected void copyIcon(PanelSignal newCopy) {
    newCopy.MyLocation = MyLocation;
    newCopy.MyOrientation = MyOrientation;
    newCopy.MyHeadCount = MyHeadCount;
    newCopy.MyLamp = MyLamp;
    newCopy.Saved = false;
  }

  /**
   * removes itself from the GridTile.
   */
  public void uninstall() {
    if ( (SignalTile != null) && (TheFrill != null)) {
      SignalTile.delFrill(TheFrill);
    }
  }

  /**
   * registers a PanelSignalFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new PanelSignalFactory());
  }
}

/**
     * is a Class known only to the PanelSignal class for creating PanelSignals from
 * an XML document.  Its purpose is to pick up the location of the SecSignal
 * in the GridTile, its orientation, and physical attributes on the layout.
 */
class PanelSignalFactory
    implements XMLEleFactory {

  FrillLoc Location;
  int Orientation;
  boolean Lamp;
  int Heads;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    Location = null;
    Orientation = CtcFont.NOT_FOUND;
    Lamp = true;
    Heads = 1;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    if (PanelSignal.SIG_ORIENT.equals(tag)) {
      Orientation = Edge.toEdge(value);
    }
    else if (PanelSignal.SIG_LOCATION.equals(tag)) {
      Location = FrillLoc.newFrillLoc(value);
    }
    else if (PanelSignal.SIG_PANEL_TYPE.equals(tag)) {
      int type = CtcFont.findString(value, PanelSignal.SIG_TYPE);
      if (type != CtcFont.NOT_FOUND) {
        Lamp = PanelSignal.LAMP[type];
        Heads = PanelSignal.SIGNAL_HEADS[type];
      }
    }
    else {
      resultMsg = new String("A " + PanelSignal.XML_TAG +
                             " XML Element cannot have a " + tag +
                             " attribute.");
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    PanelSignal p = new PanelSignal();
    p.setLocation(Location);
    p.setOrient(Orientation);
    p.setParms(Lamp, Heads);
    return p;
  }
}
/* @(#)PanelSignal.java */