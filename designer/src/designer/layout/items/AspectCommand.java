/* Name: AspectCommand.java
 *
 * What:
 *  This class of objects binds a signal presentation state String with
 *  the list of decoder commands needed to make it happen.
 */
package designer.layout.items;

import designer.layout.JmriName;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 * is a class for binding a signal presentation String with the list of
 * decoder commands needed to make it happen.  Objects of this class are
 * intended to be non-dynamic.  They are created, the String and list
 * of commands is added to it, then nothing is edited.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class AspectCommand
    implements XMLEleObject, Savable {
  /**
   * is the tag for identifying a SecSignal in the XMl file.
   */
  static final String XML_TAG = "ASPECTCOMMAND";

  /**
   * is the XML tag for the head presentation label.
   */
  static final String NAME_TAG = "PLABEL";

  /**
   * the String for identifying the head's presentation.
   */
  String AspectLabel;

  /**
   * the command to the decoder.  This could be a list.
   */
  IOSpec Command;

  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * the constructor.
   *
   * @param label is the head presentation String used to identify
   * the commands.
   */
  public AspectCommand(String label) {
    AspectLabel = label;
  }

  /**
   * retrieves the presentation label.
   *
   * @return the label.
   */
  public String getPresentationLabel() {
    String label = null;
    if (AspectLabel != null) {
      label = new String(AspectLabel);
    }
    return label;
  }

  /**
   * saves the decoder command.
   *
   * @param cmd is the command.
   */
  public void setCommand(IOSpec cmd) {
    Command = cmd;
    Saved = false;
  }

  /**
   * retrieves the command.
   *
   * @return the command.
   *
   * @see IOSpec
   */
  public IOSpec getCommand() {
    return Command;
  }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
      String resultMsg = null;
      if (IOSpec.XML_TAG.equals(objName)) {
          if (Command == null) {
              Command = (IOSpec) objValue;
          }
          else if (IOSpecChain.class.isInstance(Command)) {
              ((IOSpecChain) Command).add((IOSpec) objValue);
          }
          else {
              IOSpecChain list = new IOSpecChain();
              list.add(Command);
              list.add((IOSpec) objValue);
              Command = list;
              IOSpecChainManager.IOSpecChainKeeper.add(list);
              JmriName.registerPrefix(list);
          }
      }
      else {
          resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                  + objName + ").");
      }
      return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
//      if ((Command != null) && (Command.getPrefix() == null)) {
//          Command.setPrefix(IOSpec.DEFAULT_REQ);
//      }
      Saved = true;
      return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    return Saved && Command.isSaved();
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    if (Command != null) {
      Element myObject = new Element(XML_TAG);
      myObject.setAttribute(NAME_TAG, AspectLabel);
      Command.putXML(myObject);
      parent.addContent(myObject);
    }
    Saved = true;
    return null;
  }

  /**
   * asks the sub-component to create a copy of itself.
   *
   * @return an identical copy.
   */
  public AspectCommand copy() {
    AspectCommand newCopy = new AspectCommand(AspectLabel);
    newCopy.AspectLabel = new String(AspectLabel);
    newCopy.Command = (IOSpec) Command.copy();
    newCopy.Saved = false;
    return newCopy;
  }

  /**
   * registers a AspectCommandFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new AspectCommandFactory());
  }
}

/**
 * is a Class known only to the AspectCommand class for creating an
 * AspectCommand from an XML document.  Its purpose is to pick up the
 * signal presentation name and the decoder command list.
 */
class AspectCommandFactory
    implements XMLEleFactory {

  /**
   * is the head presentation label, from the attribute.
   */
  String AttrLabel;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    AttrLabel = null;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    if (AspectCommand.NAME_TAG.equals(tag)) {
      AttrLabel = new String(value);
    }
    else {
      resultMsg = new String("A " + HeadStates.XML_TAG +
                             " XML Element cannot have a " + tag +
                             " attribute.");
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    if (AttrLabel == null) {
      System.out.println("Missing attribute in " + AspectCommand.XML_TAG
                         + " XML Element.");
      return null;
    }
    return new AspectCommand(AttrLabel);
  }

}
/* @(#)AspectCommand.java */
