/* Name: XEdge.java
 *
 * What:
 *  This class of Objects holds the information needed for defining half
 *  of a SecEdge that forms a crossing.  A crossing is formed by two tracks
 *  terminating on an edge with two complementary tracks terminating on the
 *  adjacent edge.  Each track end is logically connected to its complement.
 *  This means the two tracks cross each other.  There are no movable points.
 *  The only path from one track is to its complement.
 *  <p>
 *  Though nothing prevents it, the adjacent edge should physically touch
 *  this edge.
 *  <p>
 *  Each ot the two track ends can independently be a block boundary or
 *  not.  Naturally the complementary track end must match.
 */
package designer.layout.items;

import java.util.BitSet;

import org.jdom2.Element;

import designer.gui.CtcFont;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 *  This class of Objects holds the information needed for defining half
 *  of a SecEdge that forms a crossing.  A crossing is formed by two tracks
 *  terminating on an edge with two complementary tracks terminating on the
 *  adjacent edge.  Each track end is logically connected to its complement.
 *  This means the two tracks cross each other.  There are no movable points.
 *  The only path from one track is to its complement.
 *  <p>
 *  Though nothing prevents it, the adjacent edge should physically touch
 *  this edge.
 *  <p>
 *  Each of the two track ends can independently be a block boundary or
 *  not.  Naturally the complementary track end must match.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2009, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class XEdge implements XMLEleObject, Savable {

    /**
     * is the tag for identifying an Xedge Object in the XML file.
     */
    static final String XML_TAG = "CROSSINGEDGE";

    /**
     * is a lookup table for specifying which Track in the adjoining
     * Section matches up with a Track.  The Tracks are identified by
     * the SecEdge that the other end terminates on.  For example, suppose
     * this SecEdge is 0 (RIGHT) and the track is "LOWERSLASH".  Then, the
     * Track is identified as 1 (it terminates on the BOTTOM edge - 1).
     * The "key" (index) of the table is the identity of a Track in this
     * SecEdge and the value is the identity of the complementary Track
     * in the adjoining SecEdge.
     */
    static private final int[] COMPLEMENT = {
        Edge.LEFT,
        Edge.TOP,
        Edge.RIGHT,
        Edge.BOTTOM
    };

    /**
     * the state of a track from across the Section that may terminate on the SecEdge:
     * <ol>
     * <li>
     * NO_TRACK means no track from the identified edge terminates on the SecEdge
     * <li>
     * NO_BLOCK means a track from the identified edge terminates on the SecEdge
     * and it does not have a block boundary
     * <li>
     * BLOCK means that a track from the identified edge terminates on the SecEdge
     * and it is a block boundary
     * </ol>
     */
    static public final String[] TRACKSTATE = {
        "NO_TRACK",
        "NO_BLOCK",
        "BLOCK"
    };

    /**
     * Constants for identifying the above
     */
    /**
     * no track terminates on the edge
     */
    static public final int NO_TRACK = 0;
    
    /**
     * a track terminates on the edge.  It is not a Block boundary
     */
    static public final int NO_BLOCK = 1;
    
    /**
     * a track terminates on the edge.  It is a Block boundary.
     */
    static public final int BLOCK = 2;
    
    /**
     * the state of each of the four possible tracks.
     */
    private int[] TrackEnd = {
            NO_TRACK,
            NO_TRACK,
            NO_TRACK,
            NO_TRACK
    };
    
    /**
     *  a true flag means that it has been saved,
     */
    private boolean Saved = true;
    
    /**
     * sets the state of one of the tracks that could terminate on the SecEdge
     * @param tEnd is the track identifier (edge ot the other end of the track)
     * @param state is one of the values from above
     */
    public void setTrackEnd(int tEnd, int state) {
        TrackEnd[tEnd] = state;
    }

    /**
     * returns the state of any possible track that might terminate on the SecEdge
     * @param tEnd is the other end of the track being queried
     * @return NO_TRACK, NO_BLOCK, or BLOCK
     */
    public int getTrackEnd(int tEnd) {
        return TrackEnd[tEnd];
    }
    
    /**
     * tests a SecEdge to see if it is a candidate for being a cross
     * over.  To be a cross oever, the SecEdge must have
     * <ul>
     * <li> two (and only 2 tracks terminating on it
     * <li> the adjoining SecEdge must exist
     * <li> the adjoining SecEdge must have exactly two tracks terminating on it
     * <li> the 2 tracks must be the complements of the ones on the candidate
     * </ul>
     * @param candidate is one of the SecEdges being tested
     * @param adjoining is its potential peer - the other SecEdge being tested
     * @return true if all the above conditions are met and false if any is
     * not met
     */
    public static boolean isPossibleXEdge(SecEdge candidate, SecEdge adjoining) {
        BitSet myTracks;
        BitSet otherTracks;
        
        if (candidate.getSection().countTracks(candidate.getEdge()) == 2) {
            if (adjoining  != null) {
                if (adjoining.getSection().countTracks(adjoining.getEdge()) == 2) {
                    otherTracks = adjoining.getSection().getTrackGroup().getEnds(adjoining.getEdge());
                    myTracks = candidate.getSection().getTrackGroup().getEnds(candidate.getEdge());
                    for (int i = 0; i < COMPLEMENT.length; ++i) {
                        if (myTracks.get(i) && !otherTracks.get(COMPLEMENT[i])) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * returns the index of the matching track to a track
     * @param trk is the index of the track being matched
     * @return the index of the matching track (if it exists)
     * or -1 (if it doesn't exist).
     */
    public static int getComplement(int trk) {
        if ((trk >= 0) && (trk < COMPLEMENT.length)) {
            return COMPLEMENT[trk];
        }
        return -1;
    }
    
    /**
     * creates a new XEdge that is the matching complement to this one
     * @return the mirror XEdge
     */
    public XEdge makeComplement() {
        XEdge comp = new XEdge();
        for (int i = 0; i < COMPLEMENT.length; ++i) {
            comp.TrackEnd[COMPLEMENT[i]] = TrackEnd[i];
        }
        return comp;
    }

    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("A " + XML_TAG + " cannot contain a text field ("
                + eleValue + ").");
    }

    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        return new String("A " + XML_TAG + " cannot contain an Element ("
                + objName + ").");
    }

    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }

    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        Saved = true;
        return null;
    }

    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved;
    }

    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element thisObject = new Element(XML_TAG);
        for (int i = 0; i < Edge.EDGENAME.length; ++i) {
            if (TrackEnd[i] != NO_TRACK) {
                thisObject.setAttribute(Edge.EDGENAME[i], TRACKSTATE[TrackEnd[i]]);
            }
        }
        parent.addContent(thisObject);
        Saved = true;
        return null;
    }

    /**
     * registers a SwitchPointsFactory with the XMLReader.
     */
    static public void init() {
      XMLReader.registerFactory(XML_TAG, new XEdgeFactory());
    }
}

/**
 * is a Class known only to the SwitchPoints class for creating SwitchPoints
 * from an XML document.
 */
class XEdgeFactory
    implements XMLEleFactory {

    /**
     * is the XEdge created from the XML file
     */
    private XEdge newEdge;
    
  /**
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
      newEdge = new XEdge();
  }

  /**
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    int trk;
    int state;
    if ((trk = CtcFont.findString(tag, Edge.EDGENAME)) == CtcFont.NOT_FOUND) {
        resultMsg = "Unknown XEdge track identifier: " + tag;
    }
    else if ((state = CtcFont.findString(value, XEdge.TRACKSTATE)) == CtcFont.NOT_FOUND){
        resultMsg = "Unknown condition for XEdge track: " + value;
    }
    else {
        newEdge.setTrackEnd(trk, state);
    }
    return resultMsg;
  }

  /**
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return newEdge;
  }
}

/* @(#)XEdge.java */
