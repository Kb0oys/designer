/* Name: SignalLock.java
 *
 * What:
 *   This class is an association between an EnumerationLock and an IOSpec.  It provides
 *   a way to store and retrieve the JMRI object that is the API into a Signal.
 */
package designer.layout.items;

import org.jdom2.Element;

import designer.layout.EnumerationLocks;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 *   This class is an association between an EnumerationLock and an IOSpec.  It provides
 *   a way to store and retrieve the JMRI object that is the API into a Signal.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2012, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class SignalLock implements XMLEleObject, Savable {

    /**
     * is the XML tag on a Lock Element
     */
    static final String XML_TAG = "LOCK";
    
    /**
     * is the XML attribute tag for the Lock's name
     */
    static final String LOCK_NAME = "LOCKNAME";
    
    /**
     * is the EnumerationLock
     */
    private final EnumerationLocks MyLock;
    
    /**
     * is the IOSpec
     */
    private IOSpec MySpec;
    
    /**
     *  a true flag means that it has been saved,
     */
    private boolean Saved = true;
    
    /**
     * the ctor
     * @param lock is the Enumeration Lock
     */
    public SignalLock(EnumerationLocks lock) {
        MyLock = lock;
    }

    /**
     * a ctor to assist in generating the XML
     * @param lock is the Lock
     * @param spec is the IOSpec associated with heh Lock
     */
    public SignalLock(EnumerationLocks lock, IOSpec spec) {
        MyLock = lock;
        MySpec = spec;
    }
    
    /**
     * retrieves the Lock field
     * @return the EnumerationLock
     */
    public EnumerationLocks getLock() {
        return MyLock;
    }
    
    /**
     * sets the IOSpec
     * @param spec is the specification for the external Lock
     */
    public void setSpec(IOSpec spec) {
        MySpec = spec;
        Saved = false;
    }
    
    /**
     * retrieves the IOSpec for the Lock
     * @return the IOSpec
     */
    public IOSpec getSpec() {
        return MySpec;
    }
    
    @Override
    public String putXML(Element parent) {
        Element thisObject = new Element(XML_TAG);
        thisObject.setAttribute(LOCK_NAME, MyLock.toString());
        MySpec.putXML(thisObject);
        parent.addContent(thisObject);
        Saved = true;
        return null;
    }

    @Override
    public String setValue(String eleValue) {
        return new String("A " + XML_TAG + " cannot have a value");
    }

    @Override
    public String setObject(String objName, Object objValue) {
        String resultMsg = null;
        if (IOSpec.XML_TAG.equals(objName)) {
            MySpec = (IOSpec) objValue;
            Saved = true;
        }
        else {
            resultMsg = new String("Expecting " + XML_TAG + " and received " + objName);
        }
        return resultMsg;
    }

    @Override
    public String getTag() {
        return XML_TAG;
    }

    @Override
    public String doneXML() {
        if (MySpec == null) {
            return new String("Missing IOSPecification for Lock " + MyLock.toString());
        }
        return null;
    }
    
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
      return Saved;
    }
    
    /**
     * registers a SignalLockFactory with the XMLReader.
     */
    static public void init() {
      XMLReader.registerFactory(XML_TAG, new SignalLockFactory());
    }
}

/**
 * is a class known only to the SignalLock for creating a SignalLock from a
 * Lock XML Element
 */
class SignalLockFactory implements XMLEleFactory {

    /**
     * is the name of the EnumerationLock
     */
    private String LockName = null;
    
    @Override
    public void newElement() {
    }

    @Override
    public String addAttribute(String tag, String value) {
        if (SignalLock.LOCK_NAME.equals(tag)) {
            LockName = value;
            LockName.trim();
            return null;
        }
        return new String(tag + " is not a valid attribute for a " + SignalLock.XML_TAG);
    }

    @Override
    public XMLEleObject getObject() {
        if ((LockName != null) && !LockName.isEmpty()) {
            return new SignalLock(EnumerationLocks.valueOf(LockName));
        }
        return null;
    }
}
/* @(#)SignalLock.java */