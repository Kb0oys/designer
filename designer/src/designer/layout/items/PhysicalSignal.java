/* Name: PhysicalSignal.java
 *
 * What:
 *   This class is the container for all the information about the physical
 *   signal on the layout.
 */

package designer.layout.items;

import designer.layout.xml.*;
import org.jdom2.Element;

/**
 * is a container for all the information about the physical signal on the
 * layout.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class PhysicalSignal
    implements XMLEleObject, Savable {
  /**
   * is the tag for identifying a SecSignal in the XMl file.
   */
  static final String XML_TAG = "PHYSIGNAL";

  /**
   * is (temporarily) the number of heads in the Signal.  This field
   * should come from the Signal Type object.
   */
  private int Heads = 1;

  /**
   * is the Name of the SignalTemplate being described.
   */
  private String TemplateName;

  /**
   * is the commands to the decoder.
   */
  AspectTable DecoderList;

  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * constructs a SecSignal, given no other information
   */
  public PhysicalSignal() {
  }

  /**
   * retrieves the number of signal heads.
   *
   * @return the number of heads (1 - 3).  0 is not allowed because
   * the signal may be on the panel, yet not exist on the layout.
   */
  public int getHeads() {
    return Heads;
  }

  /**
   * retrieves the name of the SignalTemplate.
   *
   * @return the name of the SignalTemplate so that it can be located
   * in the TemplateStore.
   */
  public String getTemplateName() {
    return new String(TemplateName);
  }

  /**
   * sets the name of the SignalTemplate.
   *
   * @param template is the name of the SignalTemplate
   */
  public void setTemplateName(String template) {
    TemplateName = template;
    Saved = false;
  }

  /**
   * sets the decoder command list.
   *
   * @param dlist is the new list.
   *
   * @see AspectTable
   */
  public void setCommandTable(AspectTable dlist) {
    DecoderList = dlist;
  }

  /**
   * retrieves the decoder command list,
   *
   * @return the table describing the decoder commands for each head, to set
   * the aspects.
   *
   * @see AspectTable
   */
  public AspectTable getCommandList() {
    return DecoderList;
  }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
      TemplateName = new String(eleValue);
      return null;
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    if (AspectTable.XML_TAG.equals(objName)) {
      setCommandTable( (AspectTable) objValue);
    }
    else {
      resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                             + objName + ").");
    }
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    if (TemplateName != null) {
      setTemplateName(TemplateName);
    }
    Saved = true;
    return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    boolean aspectSaved = true;
    if (DecoderList != null) {
      aspectSaved = DecoderList.isSaved();
    }
    return (aspectSaved && Saved);
  }

  /**
   * sets the saved status of the signal
   * @param save is the new value
   */
  public void setSaved(boolean save) {
      Saved = save;
  }
  
  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    if (TemplateName != null) {
      Element thisObject = new Element(XML_TAG);
      thisObject.addContent(TemplateName);
      if (DecoderList != null) {
        DecoderList.putXML(thisObject);
      }
      parent.addContent(thisObject);
    }
    Saved = true;
    return null;
  }

  /**
   * asks the sub-component to create a copy of itself.
   *
   * @return an identical copy of itself.
   */
  public PhysicalSignal copy() {
    PhysicalSignal newCopy = new PhysicalSignal();
    newCopy.TemplateName = new String(TemplateName);
    newCopy.Heads = Heads;
    if (DecoderList != null) {
      newCopy.DecoderList = DecoderList.copy();
    }
    else {
      newCopy.DecoderList = null;
    }
    newCopy.Saved = false;
    return newCopy;
  }

  /**
   * asks the sub-component to create a shallow copy of itself.  A shallow
   * copy copies everything but the list of decoder definitions.
   *
   * @return an identical copy of itself.
   */
  public PhysicalSignal shallowCopy() {
    PhysicalSignal newCopy = new PhysicalSignal();
    newCopy.TemplateName = new String(TemplateName);
    newCopy.Heads = Heads;
    newCopy.DecoderList = null;
    newCopy.Saved = false;
    return newCopy;
  }

  /**
   * registers a PhySignalFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new PhySignalFactory());
    AspectTable.init();
  }
}

/**
 * is a Class known only to the PhysicalSignal class for creating PhysicalSignals from
 * an XML document.  Its purpose is to pick up the location of the SecSignal
 * in the GridTile, its orientation, and physical attributes on the layout.
 */
class PhySignalFactory
    implements XMLEleFactory {

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    resultMsg = new String("A " + PhysicalSignal.XML_TAG +
                           " XML Element cannot have a " + tag +
                           " attribute.");
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new PhysicalSignal();
  }
}
/* @(#)PhysicalSignal.java */