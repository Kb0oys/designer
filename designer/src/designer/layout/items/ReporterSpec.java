/*
 * Name: ReporterSpec.java
 *
 * What:
 *  This is a class for holding the information for constructing a JMRI
 *  Reporter object
 */package designer.layout.items;

import org.jdom2.Element;

import designer.layout.AbstractListElement;
import designer.layout.JmriDevice;
import designer.layout.JmriName;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;

/**
 * is a class for holding the information needed to construct a JMRI
 * Reporter object.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2012, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class ReporterSpec extends IOSpec {
    /**
     * is the tag for identifying an ReporterSpec in the XML file.
     */
    static final String XML_TAG = "REPORTERSPEC";

    /**
     * the constructor.  It is called from the GUI.
     *
     * @param address is the address the command should be sent to.
     */
    public ReporterSpec(String address) {
      super(address);
      MyDevice = JmriDevice.Reporter;
    }

    @Override
    public AbstractListElement copy() {
        ReporterSpec newCopy = new ReporterSpec(DecAddr);
        copyContents(newCopy);
        return newCopy;
    }
    
    @Override
    public boolean isComplete() {
    	if (isValidUserName()) {
    		return true;
    	}
    	if (JmriName.isValidPrefix(JmriPrefix) && (JmriPrefix.equals("none") || isValidAddress())) {
    		return true;
    	}
    	return false;
    }

    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("A " + XML_TAG + "cannot have a value.");
    }

    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptable or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
      return new String("A " + XML_TAG + " cannot contain an Element ("
                        + objName + ").");
    }

    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
      return XML_TAG;
    }

    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
      String resultMsg = null;
      if (!isComplete()) {
          resultMsg = new String("A " + XML_TAG + " is missing its address.");
      }
      JmriName.registerPrefix(this);
      Saved = true;
      return resultMsg;
    }

    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
      Element thisObject = new Element(XML_TAG);

      if ((DecAddr != null) && !DecAddr.isEmpty()) {
          thisObject.setAttribute(DECADDR, DecAddr);
      }
      if ((JmriPrefix != null) && !JmriPrefix.equals("none") && !JmriPrefix.isEmpty()) {
        thisObject.setAttribute(JMRIPREFIX, JmriPrefix);
      }
      if ((UserName != null) && (UserName.length() > 0)) {
          thisObject.setAttribute(USER_NAME, UserName);
      }
      parent.addContent(thisObject);
      Saved = true;
      return null;
    }
  }

  /**
   * is a Class known only to the ReporterSpec class for creating ReporterSpecs from
   * an XML document.
   */
  class ReporterSpecFactory
      implements XMLEleFactory {

    /**
     * is the address to send the command to.
     */
    String Recipient;

    /**
     * is the JMRI name prefix
     */
    String Prefix;

    /**
     * is the User Name.
     */
    String UserName;

    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
      Recipient = "";
      Prefix = null;
      UserName = null;
    }

    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
      String resultMsg = null;

      if (ReporterSpec.DECADDR.equals(tag)) {
          Recipient = value;
      }
      else if (ReporterSpec.JMRIPREFIX.equals(tag)) {
        Prefix = value;
      }
      else if (IOSpec.USER_NAME.equals(tag)) {
          UserName = value;
      }
      else {
        resultMsg = new String("A " + IOSpec.XML_TAG +
                               " XML Element cannot have a " + tag +
                               " attribute.");
      }
      return resultMsg;
    }

    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
      ReporterSpec spec = new ReporterSpec(Recipient);
      if (Prefix != null) {
        spec.setPrefix(Prefix);
      }
      spec.setUserName(UserName);
      return spec;
    }
}
/* @(#)ReporterSpec.java */
