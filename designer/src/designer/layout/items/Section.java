/* Name: Section.java
 *
 * What:
 *   This class is the container for all the information within a section
 *   of track, as represented by a GridTIle.
 */

package designer.layout.items;

import java.util.Enumeration;
import java.util.Vector;

import designer.gui.GridTile;
import designer.layout.Node;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleObject;

import org.jdom2.Element;

/**
 * is a container for all the information within a section of track, as
 * represented by a GridTile.
 * <p>
 * A section holds:
 * <ul>
 * <li>four edges, RIGHT, BOTTOM, LEFT, and TOP
 * <li>a list of Tracks which traverse the Section and terminate in the middle
 * of an edge
 * <li>Signals for the terminations
 * <li>an optional station
 * <li>a label
 * <li>a filename containing an Image. This should be mutually exclusive with
 * all other items, but the mutual exclusion is not enforced.
 * </ul>
 * <p>
 * There are some constraints:
 * <ol>
 * <li>two tracks need not share a common edge
 * <li>non-points terminations may also be a block boundary
 * <li>Blocks may span multiple Sections
 * <li>A Section may contain Tracks from disjoint Blocks (if they don't share a
 * common termination edge)
 * <li>Blocks have more information, such as signal discipline and detector
 * address.
 * <li>Signals reside only on block boundaries, protecting entry into the block
 * <li>Signals have more detailed descriptions (such as number of heads)
 * <li>Points may be manual or automatic
 * <li>Points have a description of how to throw them
 * <li>Points may have a description of the conditions for indicating the user
 * is throwing them (the computer can trigger the movement, depending upon the
 * discipline on the block containing the turnout)
 * <li>Points may have a description of the feedback mechanism
 * <li>each displayable item within a Section has an Object that implements
 * Frills
 * </ol>
 * <p>
 * A section envelopes other items (such as Name, Tracks, etc.). It, and the
 * values of its contents, can be altered in three ways:
 * <ol>
 * <li>via an XML file (in which case, the Section and its contents begin as
 * empty)
 * <li>via a Dialog (which allows existing contents to be edited [actually,
 * replaced])
 * <li>via paste (which is similar to creating a new Section via XML)
 * </ol>
 * In any of the above cases, the process is that the Section is created with
 * nulls for all items. Items are not edited, but replaced. This facilitates
 * undo/redo. Before adding the first item, a GridTile must be created and
 * associated with the Section. Then, the item's replaceSelf method is called to
 * add the item. replaceSelf() will retrieve anything it needs from the Section
 * (such as the GridTile) and call the appropriate insertion mechanism.
 *
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003, 2010, 2011, 2020, 2021
 * </p>
 * <p>
 * Company:
 * </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */
public class Section implements XMLEleObject, Savable {
    
    /**
     * a constant value for indicating something is not in use.
     */
    public static final int UNUSED = -1;
    
    /**
     * The Layout container for the Section.
     */
    private Node MyNode;
    
    /**
     * A list of all the Savable things in the Section.
     */
    private Vector<Itemizable> Contents;
    
    /**
     * The edges.
     */
    private SecEdge MyEdge[] = new SecEdge[Edge.EDGENAME.length];
    
    /**
     * The Station, if it exists.
     */
    private Depot MyDepot;
    
    /**
     * The descriptions of the Tracks.
     */
    private TrackGroup Rails;
    
    /**
     * The information about the Section's name.
     */
    private SecName MySecName;
    
    /**
     * The information for painting an Image.
     */
    private ImageItem MyImage;
    
    /**
     * The information for painting a ButtonItem
     */
    private ButtonItem MyButton;
    
    /**
     * a flag indicating that the Section is hidden
     */
    private boolean Hidden = true;
    
    /**
     * A true flag means that the Section has been saved.
     */
    private boolean Saved = true;
    
    /**
     * constructs the contents for a Grid.
     *
     * @param node
     *            is the Node containing the Section.
     *
     * @see designer.layout.Node
     */
    public Section(Node node) {
        Contents = new Vector<Itemizable>();
        MyNode = node;
    }
    
    /**
     * returns the in use status of the Section.
     *
     * @return true if the Section has something in it or false if it does not.
     */
    public boolean inUse() {
        return !Contents.isEmpty();
    }
    
    /**
     * copies the contents of another Section to this Section. The GridTile must
     * be set before this method is called.
     *
     * @param master
     *            is the Section being copied.
     */
    public void copy(Section master) {
        Itemizable item;
        for (Enumeration<Itemizable> e = master.Contents.elements(); e.hasMoreElements();) {
            item = e.nextElement();
            item.copy(this);
        }
        Saved = false;
    }
    
    /**
     * copies the contents of another Section to this Section. The GridTile must
     * be set before this method is called.
     *
     * @param master
     *            is the Section being copied.
     */
    public void shallowCopy(Section master) {
        Itemizable item;
        for (Enumeration<Itemizable> e = master.Contents.elements(); e.hasMoreElements();) {
            item = e.nextElement();
            item.shallowCopy(this);
        }
        Saved = false;
    }
    
    /**
     * retrieves the Section's Node.
     *
     * @return the Node that positions the Section.
     *
     * @see designer.layout.Node
     */
    public Node getNode() {
        return MyNode;
    }
    
    /**
     * retrieves the Section's GridTile.
     *
     * @return the GridTile that the Section writes to.
     */
    GridTile getTile() {
        return MyNode.getTile();
    }
    
    /**
     * retrieves the requested SecEdge.
     *
     * @param edge
     *            is the SecEdge desired.
     *
     * @return the requested SecEdge or null. null means that edge is invalid or
     *         doesn't exist.
     *
     * @see SecEdge
     */
    public SecEdge getEdge(int edge) {
        if ((edge >= 0) && (edge < Edge.EDGENAME.length)) {
            return MyEdge[edge];
        }
        return null;
    }
    
    /**
     * replaces one of the Edges.
     * <p>
     *
     * @param edge
     *            is the SecEdge being replaced.
     * @param value
     *            is the new SecEdge.
     *
     * @see SecEdge
     */
    public void replaceEdge(int edge, SecEdge value) {
        
        if (MyEdge[edge] != null) {
            Contents.remove(MyEdge[edge]);
            if (value == null) {
                MyEdge[edge] = null;
            }
        }
        if (value != null) {
            Contents.add(value);
        }
        MyEdge[edge] = value;
    }
    
    /**
     * changes the Node that contains the Section
     * 
     * @param n is the new node.  It should not be null.
     */
    public void replaceNode(Node n) {
        MyNode = n;
    }
    
    /**
     * counts the number of Track terminations on a SecEdge. There is no need to
     * verify the requested edge because it is used only in testing, so if it is
     * invalid, the count will come back as 0.
     *
     * @param edge
     *            is the index of the SecEdge.
     *
     * @return the number of Tracks with a termination on the edge. O means
     *         none, 1 means straight track, and 2 or more means there are
     *         switch points.
     */
    public int countTracks(int edge) {
        if (Rails != null) {
            return Rails.countEnds(edge);
        }
        return 0;
    }
    
    /**
     * generates an Enumeration over the Tracks.
     *
     * @return the Enumeration. The Objects being enumerated are the kinds of
     *         Tracks included in the Section.
     *
     * @see TrackGroup
     */
    public Enumeration<Integer> getTracks() {
        if (Rails != null) {
            return Rails.getTracks();
        }
        return null;
    }
    
    /**
     * retrieves the Section's name information.
     *
     * @return the SecName object containing the section's name information.
     *
     * @see SecName
     */
    public SecName getName() {
        return MySecName;
    }
    
    /**
     * replaces the Section Name field,
     *
     * @param newName
     *            is the new value.
     *
     * @see SecName
     */
    public void replaceSecName(SecName newName) {
        if (MySecName != null) {
            MySecName.uninstall();
            Contents.remove(MySecName);
        }
        MySecName = newName;
        if (newName != null) {
            Contents.add(newName);
        }
    }
    
    /**
     * retrieves the Location of the Station.
     *
     * @return the FrillLoc describing where the Station is, or null if there is
     *         no Station.
     *
     * @see Depot
     */
    public Depot getStation() {
        return MyDepot;
    }
    
    /**
     * replaces the station.
     *
     * @param station
     *            is the new station.
     *
     * @see Depot
     */
    public void replaceStation(Depot station) {
        if (MyDepot != null) {
            Contents.remove(MyDepot);
            MyDepot.uninstall();
        }
        MyDepot = station;
        if (station != null) {
            Contents.add(station);
        }
    }
    
    /**
     * retrieves the Name of an Image file.
     *
     * @return the name of a file containing an Image, or null if there is no
     *         Image defined.
     *
     * @see ImageItem
     */
    public ImageItem getImage() {
        return MyImage;
    }
    
    /**
     * replaces the Image information.
     *
     * @param image
     *            is the new Image file name.
     *
     * @see ImageItem
     */
    public void replaceImage(ImageItem image) {
        if (MyImage != null) {
            Contents.remove(MyImage);
            MyImage.uninstall();
        }
        MyImage = image;
        if (image != null) {
            Contents.add(image);
        }
    }
    
   
    /**
     * retrieves a ButtonItem definition.
     *
     * @return the definition of a ButtonItem or null if none is defined
     * in this Section.
     *
     * @see ButtonItem
     */
    public ButtonItem getButton() {
        return MyButton;
    }
    
    /**
     * replaces the ButtonItem information.
     *
     * @param button is the new ButtomItem
     *
     * @see ButtonItem
     */
    public void replaceButton(ButtonItem button) {
        if (MyButton != null) {
            Contents.remove(MyButton);
            MyButton.uninstall();
        }
        MyButton = button;
        if (button != null) {
            Contents.add(button);
        }
    }
    
    /**
     * retrieves the Tracks enclosed by the Section.
     *
     * @return the Tracks, or null if there are none.
     */
    public TrackGroup getTrackGroup() {
        return Rails;
    }
    
    /**
     * replaces the Tracks.  It does nothing with the associated
     * SecEdges, as they will be added by XML shortly.
     *
     * @param group
     *            is the replacement group.
     */
    public void replaceTrackGroup(TrackGroup group) {
        if (Rails != null) {
            Contents.remove(Rails);
            Rails.uninstall();
        }
        Rails = group;
        if (group != null) {
            Contents.add(group);
        }
    }

    /**
     * is called by TrackGroupDialog to replace the TrackGroup in a Section.
     * Since this method adds and removes track terminations, it changes
     * SecEdges.
     * @param group is the TrackGroup being modified
     */
    public void modifyTrackGroup(TrackGroup group) {
        SecEdge newEdge;
        if (Rails != null) {
            Contents.remove(Rails);
            Rails.uninstall();
        }
        Rails = group;
        if (group == null) {
        	return;
        }
        Contents.add(group);
        group.setSection(this);
        // Remove the SecEdges that no longer have terminations and add
        // the new SecEdges.
        for (int trk = 0; trk < Edge.EDGENAME.length; ++trk) {
            if (group.getEnds(trk).cardinality() == 0) {
                if (MyEdge[trk] != null) {
                    MyEdge[trk].freeUp();
                    replaceEdge(trk, null);
                }
            } else if (MyEdge[trk] == null) {
                newEdge = new SecEdge(trk);
                newEdge.setSection(this);
                replaceEdge(trk, newEdge);
            }
        }
        
        // Finally, fix up pre-existing edges
        for (int trk = 0; trk < Edge.EDGENAME.length; ++trk) {
            if (MyEdge[trk] != null) {
                if (countTracks(trk) < 2) {
                    MyEdge[trk].setPoints(null);
                    MyEdge[trk].setCrossing(null);
                }
                else {
                    if (MyEdge[trk].getPoints() == null) {
                        MyEdge[trk].setPoints(new SwitchPoints());
                    }
                    else {
                        MyEdge[trk].getPoints().validateRoutes(group.getEnds(trk));
                    }
                    MyEdge[trk].setBlock(null);
                    MyEdge[trk].setSignal(null);
                }
                MyEdge[trk].addEdge(this);
            }
        }
    }
    
    /**
     * removes all icons from the printable screen.
     */
    public void hideSec() {
        if (!Hidden) {
            GridTile tile = MyNode.getTile();
            for (Enumeration<Itemizable> item = Contents.elements(); item.hasMoreElements();) {
                item.nextElement().uninstall();
            }
            for (int edge = 0; edge < Edge.EDGENAME.length; ++edge) {
                tile.setBumber(edge, false);
            }
            tile.update();
            Hidden = true;
        }
    }
    
    /**
     * shows all icons on the printable screen.
     */
    public void showSec() {
        if (Hidden) {
            for (Enumeration<Itemizable> item = Contents.elements(); item.hasMoreElements();) {
                item.nextElement().install(MyNode.getTile());
            }
            MyNode.getTile().update();
            Hidden = false;
        }
    }
    
    /**
     * removes connections between this Section and other Sections. The
     * connections of interest are SecEdges which adjoin to non-adjacent
     * SecEdges. The first requirement is that the Shared SecEdge must be
     * relinked to Null. It would be nice to leave the SecEdge in this Section
     * untouched, but that won't work because Shared is not valid (see the first
     * requirement) and the description may not be describing the right target
     * (the geometry of the Layout may have changed after the description was
     * created).
     */
    public void unlinkSection() {
        hideSec();
        for (int trk = 0; trk < Edge.EDGENAME.length; ++trk) {
            if (MyEdge[trk] != null) {
                MyEdge[trk].unbind();
            }
        }
//      showSec();
    }
    
    /**
     * matches the adjacent SecEdge to one from the Section.
     *
     * @param edge
     *            is the SecEdge to match.
     */
    public void linkEdge(int edge) {
        if (MyEdge[edge] != null) {
            MyEdge[edge].bindTo();
        }
    }
    
    /**
     * unlinks a SecEdge, if it is linked to its geographic neighbor.
     *
     * @param edge
     *            is the SecEdge to test and unlink.
     */
    public void unlinkAdjacent(int edge) {
        if ((MyEdge[edge] != null) && (MyEdge[edge].isAdjacentPeer())) {
            MyEdge[edge].divorce();
        }
    }
    
    /**
     * adjusts the links to other SecEdges when a Section is inserted into a
     * layout.
     * <p>
     * This method is called from paste and undo.
     *
     * @param node
     *            is the Node containing the Section.
     *
     * @see designer.layout.Node
     */
    public void linkSection(Node node) {
        MyNode = node;
        for (int edge = 0; edge < Edge.EDGENAME.length; ++edge) {
            if (MyEdge[edge] != null) {
                MyEdge[edge].rebindTo();
            }
        }
        showSec();
    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String(Node.NODE_TAG
                + " XML elements do not have Text Fields.");
    }
    
    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object @param objValue is the
     * value of the embedded object
     *
     * @return null if the Object is acceptible or an error String if it is not.
     */
    public String setObject(String objName, Object objValue) {
        String resultMsg = null;
        try {
            ((Itemizable) objValue).addSelf(this);
        } catch (ClassCastException cce) {
            resultMsg = new String(objName + " is not a valid Element in a "
                    + getTag() + ".");
        }
        return resultMsg;
    }
    
    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the Element
     * tag).
     */
    public String getTag() {
        return Node.NODE_TAG;
    }
    
    /**
     * tells the XMLEleObject that no more setValue or setObject calls will be
     * made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error string
     *         if something isn't correct.
     */
    public String doneXML() {
        showSec();
        Saved = true;
        return null;
    }
    
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should be
     * written.
     */
    public boolean isSaved() {
        Savable field;
        for (Enumeration<Itemizable> e = Contents.elements(); e.hasMoreElements();) {
            field = (Savable) e.nextElement();
            if (!field.isSaved()) {
                return false;
            }
        }
        return Saved;
    }
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Savable field;
        for (Enumeration<Itemizable> e = Contents.elements(); e.hasMoreElements();) {
            field = (Savable) e.nextElement();
            field.putXML(parent);
        }
        Saved = true;
        return null;
    }
    
    /**
     * registers the factories for all contained objects with the XMLReader.
     */
    public static void init() {
        SecName.init();
        SecEdge.init();
        Depot.init();
        TrackGroup.init();
        ImageItem.init();
        ButtonItem.init();
    }
}
/* @(#)Section.java */
