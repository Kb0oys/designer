/* Name: TrackGroup.java
 *
 * What:
 *   This class is the container for describing all the Tracks in a Section.
 */

package designer.layout.items;

import designer.gui.CtcFont;
import designer.gui.GridTile;
import designer.gui.frills.RailFrill;
import designer.layout.ColorList;
import designer.layout.SpeedColor;
import designer.layout.xml.*;

import java.util.BitSet;
import java.util.Enumeration;

import org.jdom2.Element;

/**
 * is the container for describing all the Tracks in a Section.  Since each
 * Track bridges one edge to another and there are four edges, there can
 * be only 6 possible Tracks (4 things, taken 2 at a time).  A BitSet
 * is used to represent which are included in a Section.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2013, 2015, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TrackGroup
extends BitSet
implements Itemizable, Savable {
    
    /**
     *   The XML tag for recognizing the TrackGroup description.
     */
    public static final String XML_TAG = "TRACKGROUP";
    
    /**
     * The names of the 6 type of Tracks
     */
    public static final String TrackName[] = {
        "VERTICAL", // TOP-BOTTOM
        "HORIZONTAL", // RIGHT-LEFT
        "UPPERSLASH", // LEFT-TOP
        "LOWERSLASH", // RIGHT-BOTTOM
        "UPPERBACKSLASH", // RIGHT-TOP
        "LOWERBACKSLASH" // LEFT-BOTTOM
    };
    
    /**
     * flags indicating which Edge a Track terminates on.  There must be one
     * element for each of the TrackNames above, in the order listed above.
     * Each element has four bits, one for each row, in the order listed
     * in Edge: RIGHT, BOTTOM, LEFT, TOP.
     */
    public static BitSet Termination[] = new BitSet[TrackName.length];
    
    /**
     * the default rail color
     */
    private static final String DEF_COLOR = ColorList.EMPTY;
    
    /**
     * the prescribed speed for each track.
     */
    private int[] TrkSpeed;
    
    /**
     * the enclosing Section.
     */
    private Section GroupSection;
    
    /**
     * The GridTile where drawing happens.
     */
    private GridTile TrackTile;
    
    /**
     * The Tracks that are in the Section.  Each is represented by an
     * Icon.
     */
    private RailFrill Tracks[];
    
    /**
     *   a true flag means that it has been saved,
     */
    private boolean Saved = false;
    
    /**
     * constructs the Track from scratch.
     */
    public TrackGroup() {
        super(TrackName.length);
        Tracks = new RailFrill[TrackName.length];
        TrkSpeed = new int[TrackName.length];
    }
 
    /**
     * sets the Section that contains the TrackGroup
     *
     *@param sec is the Section containing the TrackGroup
     */
    public void setSection(Section sec) {
        GroupSection = sec;
        
    }

    /**
     * returns the authorized speed for a Track.
     *
     * @param trk is an index of the Track.
     *
     * @return the maximum speed defined for the trk.  If trk is an
     * invalid index or not defined, then -1 is returned.
     */
    public int getTrkSpeed(int trk) {
        if ( (trk >= 0) && (trk < size())) {
            return TrkSpeed[trk];
        }
        return -1;
    }
    
    /**
     * sets the authorized track speed for a Track.
     *
     * @param trk is an index of the Track
     * @param speed is the maximum speed
     */
    public void setTrkSpeed(int trk, int speed) {
        if ( (trk >= 0) && (trk < size())) {
            TrkSpeed[trk] = speed;
        }
    }
    
    /**
     * is a query to see if a particular Track is included.
     *
     * @param trk is an index of the Track.  If trk is an invalid index,
     * then false is returned.
     *
     * @return true if it is included and false if it is not included.
     */
    public boolean isIncluded(int trk) {
        if ( (trk >= 0) && (trk < size())) {
            return get(trk);
        }
        return false;
    }
    
    /**
     * is a query to determine all Edges connected to a particular Edge.
     *
     * @param edge is the Edge being queried.
     *
     * @return a 4 bit BitSet with a true for each Edge sharing a track with
     * edge.
     */
    public BitSet getEnds(int edge) {
        BitSet connections = new BitSet(Edge.EDGENAME.length);
        int trk;
        
        if ( (edge >= 0) && (edge < Edge.EDGENAME.length)) {
            for (Enumeration<Integer> e = getTracks(); e.hasMoreElements(); ) {
                trk = e.nextElement().intValue();
                if (Termination[trk].get(edge)) {
                    connections.or(Termination[trk]);
                }
            }
        }
        connections.clear(edge);
        return connections;
    }
    
    /**
     * counts the number of terminations on an edge.
     *
     * @param edge is the Edge being queried.
     *
     * @return the number of Tracks included that have a termination on an
     * particular Edge.  If edg eis out of range, then 0 is returned.
     */
    public int countEnds(int edge) {
        int count = 0;
        int trk;
        for (Enumeration<Integer> e = getTracks(); e.hasMoreElements(); ) {
            trk = e.nextElement().intValue();
            if (Termination[trk].get(edge)) {
                ++count;
            }
        }
        return count;
    }

    /**
     * determines the other edge number for a track, given one edge.
     * The index of the first set bit not given is returned.
     * @param track is the kind of track
     * @param trkEnd is the index of the given end
     * @return the edge number that the other end of the track is on
     */
    private int getOtherEnd(BitSet track, int trkEnd) {
        for (int i = 0; i < TrackName.length; ++i) {
            if (track.get(i) && (i != trkEnd)) {
                return i;
            }
        }
        return -1;  // this should never happen, but will trigger an exception!
    }
    
    /*
     * adds the icon for the Track to the GridTile
     */
    public void install(GridTile tile) {
        TrackTile = tile;
        int edge;
        if (TrackTile != null) {
            for (int rail = 0; rail < TrackName.length; ++rail) {
                if (get(rail)) {
                    edge = Termination[rail].nextSetBit(0);
                    Tracks[rail] = new RailFrill(edge, Termination[rail].nextSetBit(edge + 1));
                    Tracks[rail].setColorFinder(determineRailColor(rail, null));
                    TrackTile.addFrill(Tracks[rail]);
                }
            }
        }
    }
    
    /*
     * removes the Track Icons from the GridTile.
     */
    public void uninstall() {
        if (TrackTile != null) {
            for (int rail = 0; rail < TrackName.length; ++rail) {
                if (Tracks[rail] != null) {
                    TrackTile.delFrill(Tracks[rail]);
                }
            }
        }
    }

    /**
     * cycles through all the tracks in
     * the RailGroup that have a termination on a requested edge.  For each, this method checks that it
     * is the appropriate color.  If it is not, the corresponding RailFrill
     * is repainted in the appropriate color.
     * 
     * @param edge is the identifier of one end of the Tracks of interest
     * @param color is the name of the Color key for the block
     */
//    public void checkRailColor(int edge) {
//        RailFrill frill;
//        String current;
//        String appropriate;
//        BitSet track;
//        boolean doRepaint = false;
//        if (TrackTile != null) {
//            for (int rail = 0; rail < TrackName.length; ++rail) {
//                if (get(rail)) {
//                    track = Termination[rail];
//                    if (track.get(edge)) {  // filter out uninvolved tracks
//                        frill = Tracks[rail];
//                        if (frill != null) {
//                            current = frill.getColorName();
//                            appropriate = determineRailColor(rail);
//                            if (!current.equals(appropriate)) {
//                                frill.setColorFinder(appropriate);
//                                doRepaint = true;
//                            }
//                        }
//                    }
//                }
//            }
//            if (doRepaint) {
//                GroupSection.getNode().getTile().update();
//            }
//        }
//    }
    public void checkRailColor(int edge, String color) {
        RailFrill frill;
        String current;
        String appropriate;
        boolean doRepaint = false;
        boolean addChange;
        BitSet included = new BitSet(TrackName.length);
        BitSet edges = new BitSet(Edge.EDGENAME.length);
        edges.set(edge);
        int other;
        if (TrackTile != null) {
            do {
                addChange = false;
                
                // This gets confusing.  The goal is to identify all
                // tracks in the Section that are in the same detection
                // Block.  The ones that terminate on the requested end
                // are in the Block (unless they are an XEdge).  However,
                // SwitchPoints connect two tracks in a Block, so if the
                // SwitchPoints are at the other end of a track, they can
                // include other tracks that do not terminate on the edge.
                for (int trk = 0; trk < TrackName.length; ++ trk){
                    if (get(trk) && !included.get(trk)) {
                        for (int e = 0; e < Edge.EDGENAME.length; ++e) {
                            if (edges.get(e) && Termination[trk].get(e)) {
                                included.set(trk);
                                other = getOtherEnd(Termination[trk], e);
                                if ((GroupSection.countTracks(other) < 2) ||
                                        GroupSection.getEdge(other).getPreference()) {
                                    edges.set(other);
                                    addChange = true;
                                }
                            }
                        }
                    }
                }
            } while (addChange);
            
            // Since included has all the tracks of interest, it is possible to
            // determine what color they should be
            for (int trk = 0; trk < TrackName.length; ++ trk) {
                if (included.get(trk)) {
                    frill = Tracks[trk];
                    if (frill != null) {
                        current = frill.getColorName();
                        appropriate = determineRailColor(trk, color);
                        if (!current.equals(appropriate)) {
                            frill.setColorFinder(appropriate);
                            doRepaint = true;
                        }
                    }
                }
            }
            if (doRepaint) {
                GroupSection.getNode().getTile().update();
            }
        }
    }
    
    /**
     * is called to determine what color a rail should be painted in.
     * @param rail is the index of the rail in the TrackGroup (e.g.
     * the index of "VERTICAL").
     * @param bColor is the color of the enclosing Block
     * @return the Color that should be given to the RailFrill.  If the
     * the request is illegal (e.g. invalid index or no rail has been defined),
     * ERRORCOLOR is returned.
     */
    private String determineRailColor(int rail, String bColor) {
    	int edge1;
    	int edge2;
    	String colorKey = DEF_COLOR;
    	SecEdge e;
    	Block b;
    	if ((rail >= 0)  && (rail < TrackName.length) && get(rail)) {
    		if (SpeedColor.SpeedColor.getFlagValue()) {
    			switch (TrkSpeed[rail]) {
    			case 1:
    				colorKey = ColorList.NORMALSPEED;
    				break;
    			case 2:
    				colorKey = ColorList.LIMITEDSPEED;
    				break;
    			case 3:
    				colorKey = ColorList.MEDIUMSPEED;
    				break;
    			case 4:
    				colorKey = ColorList.SLOWSPEED;
    				break;
    			default:
    				colorKey = ColorList.DEFAULTSPEED;
    			}
    			return colorKey;
    		}
    		else {
    			edge1 = Termination[rail].nextSetBit(0);
    			edge2 = Termination[rail].nextSetBit(edge1 + 1);
    			colorKey = determineEdgeColor(edge1);
    			if (colorKey.equals(DEF_COLOR)) {
    				colorKey = determineEdgeColor(edge2);
    			}
    			if (colorKey.equals(DEF_COLOR)) {
    				if (bColor == null) {
    					e = GroupSection.getEdge(edge1);
    					if (!e.getPreference()) {
    						e = GroupSection.getEdge(edge2);
    					}
    					if (e != null) {
    						b = e.getBlockInfo();
    						if (b != null) {
    							bColor = b.getBlockColor();
    						}
    						else {
    							bColor = ColorList.ERRORCOLOR;
    						}
    					}
    				}
    				colorKey = bColor;
    			}
    			return colorKey;
    		}
    	}
    	return ColorList.ERRORCOLOR;
    }
    
    /**
     * is called to determine if the edge definition is complete.
     * The only edge that can have an incomplete definition is a Points
     * edge and that is when a Normal route has not been identified.
     * @param edge is the edge in question
     * @return DEF_COLOR if the edge looks to be defined or whatever the
     * SecEdge returns if not competely defined.
     */
    private String determineEdgeColor(int edge) {
        if ((countEnds(edge) > 1) && !GroupSection.getEdge(edge).isEdgeComplete()) {
            return ColorList.ERRORCOLOR;
        }
        return DEF_COLOR;
    }
    
    /**
     * generates an Enumeration over the Tracks.
     *
     * @return an Integer which indexes the next Track included.
     */
    public Enumeration<Integer> getTracks() {
        return new TrackEnumeration();
    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String(XML_TAG + " XML Elements do not have Text Fields ("
                + eleValue + ").");
    }
    
    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        String resultMsg = null;
        if (Track.XML_TAG.equals(objName)) {
            int position = CtcFont.findString(((Track)objValue).getTrakType(),
                    TrackName);
            if (position == CtcFont.NOT_FOUND) {
                resultMsg = new String(objName + " is not a valid XML text element for "
                        + XML_TAG + ".");
            }
            else {
                set(position);
                TrkSpeed[position] = ((Track) objValue).getSpeed();
            }
        }
        else {
            resultMsg = new String(XML_TAG +
                    " XML Elements do not have embedded objects ("
                    + objValue + ").");
        }
        return resultMsg;
    }
    
    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return XML_TAG;
    }
    
    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        Saved = true;
        return null;
    }
    
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved;
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        if (!isEmpty()) {
            Element thisObject = new Element(XML_TAG);
            for (int i = 0; i < length(); ++i) {
                if (get(i)) {
                    Element speed = new Element(Track.XML_TAG);
                    if (TrkSpeed[i] != 0) {
                        speed.setAttribute(Track.SPEED_TAG,
                                Track.TrackSpeed[TrkSpeed[i]]);
                    }
                    speed.addContent(TrackName[i]);
                    thisObject.addContent(speed);
                }
            }
            parent.addContent(thisObject);
        }
        Saved = true;
        return null;
    }
    
    /*
     * tells the sub-component where its Section is, so that the sub-component
     * can replace itself and retrieve anything else it needs from the Section.
     */
    public void addSelf(Section sec) {
        GroupSection = sec;
        sec.replaceTrackGroup(this);
    }
    
    /**
     * asks the sub-component to create a copy of itself.
     *
     * @param sec is the Section where the copy will be installed.
     */
    public Itemizable copy(Section sec) {
        TrackGroup newGroup = new TrackGroup();
        for (int rail = 0; rail < TrackName.length; ++rail) {
            newGroup.set(rail, get(rail));
        }
        newGroup.GroupSection = GroupSection;
        newGroup.addSelf(sec);
        return newGroup;
    }
    
    /**
     * asks the sub-component to create a shallow copy of itself.
     *
     * @param sec is the Section where the copy will be installed.
     */
    public Itemizable shallowCopy(Section sec) {
        return copy(sec);
    }
    
    /**************************************************************************
     * An inner class.
     ************************************************************************/
    
    /**
     * is an inner class for providing an Enumeration over the Tracks.
     */
    class TrackEnumeration
    implements Enumeration<Integer> {
        
        /**
         *  The index of the next Track in the group to be given out.
         */
        
        int NextTrack;
        
        /**
         * constructs the Enumeration.
         */
        TrackEnumeration() {
            NextTrack = nextSetBit(0);
        }
        
        /**
         * indicates if all the Tracks have been given out.
         *
         * @return true if nextElement() has been called for all the Tracks or
         * false if there are some remaining.
         */
        
        public boolean hasMoreElements() {
            return ( (NextTrack >= 0) && (NextTrack < size()));
        }
        
        /**
         * returns a Track in the Section.
         *
         * @return the next Track from MyTracks.
         */
        public Integer nextElement() {
            Integer rail = new Integer(NextTrack);
            ++NextTrack;
            if (NextTrack <= length()) {
                NextTrack = nextSetBit(NextTrack);
            }
            return rail;
        }
    }
    
    /**
     * registers a TrackGroupFactory with the XMLReader.
     */
    static public void init() {
        XMLReader.registerFactory(XML_TAG, new TrackGroupFactory());
        Track.init();
        
        // Vertical
        Termination[0] = new BitSet(Edge.EDGENAME.length);
        Termination[0].set(Edge.TOP);
        Termination[0].set(Edge.BOTTOM);
        
        // Horizontal
        Termination[1] = new BitSet(Edge.EDGENAME.length);
        Termination[1].set(Edge.RIGHT);
        Termination[1].set(Edge.LEFT);
        
        //UpperSlash
        Termination[2] = new BitSet(Edge.EDGENAME.length);
        Termination[2].set(Edge.TOP);
        Termination[2].set(Edge.LEFT);
        
        // LowerSlash
        Termination[3] = new BitSet(Edge.EDGENAME.length);
        Termination[3].set(Edge.RIGHT);
        Termination[3].set(Edge.BOTTOM);
        
        // UpperBackSlash
        Termination[4] = new BitSet(Edge.EDGENAME.length);
        Termination[4].set(Edge.TOP);
        Termination[4].set(Edge.RIGHT);
        
        // LowerBackSlash
        Termination[5] = new BitSet(Edge.EDGENAME.length);
        Termination[5].set(Edge.LEFT);
        Termination[5].set(Edge.BOTTOM);
    }
}

/**
 * is a Class known only to the TrackGroup class for creating TrackGroups from
 * an XML document.
 */
class TrackGroupFactory
implements XMLEleFactory {
    
    /**
     * the TrackGroup being created.
     */
    private TrackGroup TGroup;
    
    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        TGroup = new TrackGroup();
    }
    
    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        String resultMsg = new String(TrackGroup.XML_TAG +
                " XML Elements do not have " + tag + "attributes.");
        return resultMsg;
    }
    
    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        return TGroup;
    }
}
/* @(#)TrackGroup.java */
