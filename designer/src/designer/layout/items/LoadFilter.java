/* Name: LoadFilter.java
 *
 * What:
 *   This class is a Singleton that holds the type of file being loaded.  It is
 *   interrogated by XMLEleObjects when they are invoked.  The type of file determines
 *   if the XMLEleObject should do something with the data read in or nothing; thus, this
 *   class provides an XML filter.
 */

package designer.layout.items;

import java.util.BitSet;

import designer.gui.Ctc;
import designer.layout.CrewStore;
import designer.layout.JobStore;
import designer.layout.Layout;
import designer.layout.TrainStore;

/**
 *   This class is a Singleton that holds the type of file being loaded.  It is
 *   interrogated by XMLEleObjects when they are invoked.  The type of file determines
 *   if the XMLEleObject should do something with the data read in or nothing; thus, this
 *   class provides an XML filter.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class LoadFilter {

    /**
     * The following constants enumerate the filters.
     */
	/**
	 * no filter - a way of catching that no filter has been assigned
	 */
    public static final int NOFILTER = 0;
    
    /**
     * the full track plan is being loaded.  It replaces the existing plan.
     */
    public static final int TRACKPLAN = 1;
    
    /**
     * a track plan stored as a library is being loaded.  It is merged into
     * the existing plan.
     */
    public static final int LIBRARY = 2;
    
    /**
     * the train lineup is being laoded
     */
    public static final int LINEUP = 3;
    
    /**
     * the crew list is being loaded
     */
    public static final int CALLBOARD = 4;
    
    /**
     * the job list is being loaded
     */
    public static final int ASSIGNMENTS = 5;
    
    /**
     * The following constants identify the first level XMLEleObjects in an
     * XML file.
     */
    
    /**
     * defines fonts
     */
    public static final int XML_FONTDEFINITION = 0;
    
    /**
     * defines lines
     */
    public static final int XML_LINE = XML_FONTDEFINITION + 1;
    
    /**
     * defines counters
     */
    public static final int XML_COUNTER = XML_LINE + 1;
    
    /**
     * defines signal templates
     */
    public static final int XML_SIGNALTEMPLATE = XML_COUNTER + 1;
    
    /**
     * defines the TrainStore
     */
    public static final int XML_TRAINSTORE = XML_SIGNALTEMPLATE + 1;
    
    /**
     * defines the JobStore
     */
    public static final int XML_JOBSTORE = XML_TRAINSTORE + 1;
    
    /**
     * defines the CrewStore
     */
    public static final int XML_CREWSTORE = XML_JOBSTORE + 1;
    
    /**
     * defines color definitions
     */
    public static final int XML_COLORDEFINITION = XML_CREWSTORE + 1;
    
    /**
     * defines JMRI Names
     */
    public static final int XML_JMRINAME = XML_COLORDEFINITION + 1;
    
    /**
     * defines Hours
     */
    public static final int XML_HOURS = XML_JMRINAME + 1;
    
    /**
     * defines IOSpecChains
     */
    public static final int XML_IOSPECCHAIN = XML_HOURS + 1;
    
    /**
     * defines the TrackPlan
     */
    public static final int XML_TRACKPLAN = XML_IOSPECCHAIN + 1;
    
    /**
     * is the number of things being filtered
     */
    private static final int FILTER_SIZE = XML_TRACKPLAN + 1;

    /**
     * is the active filter
     */
    private int Active = NOFILTER;
    
    /**
     * are the filters
     */
    private final BitSet Filters[];
    
    /**
     * is the Singleton
     */
    private static LoadFilter Filter;
    
    /**
     * the ctor.  This constructor creates the bit maps, controlling
     * which XMLElements are saved (not ignored) when reading in a
     * configuration file.
     */
    private LoadFilter() {
        Filters = new BitSet[ASSIGNMENTS + 1];
        Filters[NOFILTER] = new BitSet(FILTER_SIZE + 1);
        Filters[NOFILTER].clear(0, FILTER_SIZE);
        
        Filters[TRACKPLAN] = new BitSet(FILTER_SIZE + 1);
        Filters[TRACKPLAN].set(0, FILTER_SIZE);
        
        Filters[LIBRARY] = new BitSet(FILTER_SIZE + 1);
        Filters[LIBRARY].set(XML_FONTDEFINITION);
        Filters[LIBRARY].set(XML_SIGNALTEMPLATE);
        Filters[LIBRARY].set(XML_COLORDEFINITION);
        Filters[LIBRARY].set(XML_JMRINAME);
        Filters[LIBRARY].set(XML_IOSPECCHAIN);
        Filters[LIBRARY].set(XML_TRACKPLAN);
        
        Filters[LINEUP] = new BitSet(FILTER_SIZE + 1);
        Filters[LINEUP].set(XML_TRAINSTORE);
        
        Filters[CALLBOARD] = new BitSet(FILTER_SIZE + 1);
        Filters[CALLBOARD].set(XML_CREWSTORE);
        
        Filters[ASSIGNMENTS] = new BitSet(FILTER_SIZE + 1);
        Filters[ASSIGNMENTS].set(XML_JOBSTORE);
        
        Active = TRACKPLAN;
    }
    
    /**
     * the Singleton locator.  If the LoadFilter does not exist, the first
     * call to this method creates it.  This ensures that only one and only
     * one instance exists - that it is a Singleton.
     * 
     * @return the LoadFilter
     */
    public static LoadFilter instance() {
        if (Filter == null) {
            Filter = new LoadFilter();
        }
        return Filter;
    }
    
    /**
     * is called to select a filter
     * 
     * @param filter is the index of the selected filter
     */
    public void setFilter(int filter) {
        if ((filter >= 0) && (filter <= ASSIGNMENTS)) {
            Active = filter;
        }
    }

    /**
     * is called to select a filter for loading the contents of a Store.
     * @param storeTag is the XML tag of the Store.  It should be one of
     * the three for Trains, Crew, or Jobs.  If none of these, TRACKPLAN
     * will be used.
     */
    public void setFilter(String storeTag) {
        setFilter(TRACKPLAN);       /* the default filter */
        if (storeTag != null) {
            if (TrainStore.TrainKeeper.getTag().equals(storeTag)) {
                setFilter(LINEUP);
            }
            else if (CrewStore.CrewKeeper.getTag().equals(storeTag)) {
                setFilter(CALLBOARD);                
            }
            else if (JobStore.JobsKeeper.getTag().equals(storeTag)) {
                setFilter(ASSIGNMENTS);
            }
        }
    }
    
    /**
     * is called to determine if the XMLEleObject should be processed
     * 
     * @param xml is the type of XML object
     * 
     * @return true if it should be processed and false if it should be
     * ignored
     */
    public boolean isEnabled(int xml) {
        return Filters[Active].get(xml);
    }

    /**
     * is called to determine if the Store should be processed
     * @param storeTag is the XML tag of the store
     * @return true if it should be processed and false if it should be
     * ignored
     */
    public boolean isEnabled(String storeTag) {
        int inquiry = XML_TRACKPLAN;
        if (storeTag != null) {
            if (TrainStore.TrainKeeper.getTag().equals(storeTag)) {
                inquiry = XML_TRAINSTORE;;
            }
            else if (CrewStore.CrewKeeper.getTag().equals(storeTag)) {
                inquiry = XML_CREWSTORE;                
            }
            else if (JobStore.JobsKeeper.getTag().equals(storeTag)) {
                inquiry = XML_JOBSTORE;
            }
        }
        return isEnabled(inquiry);
    }

    /**
     * determines what Layout to use for queries.  If a layout is
     * being read in, then use the one under construction; otherwise,
     * use the one Ctc knows about.
     * @return either the layout from Ctc or the one under construction
     */
    public Layout getLayout() {
        if (Filters[Active].get(XML_TRACKPLAN)) {
            return Layout.getConstruction();
        }
        return Ctc.getLayout();
    }
}
/* @(#)LoadFilter.java */
