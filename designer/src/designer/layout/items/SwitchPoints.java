/* Name: SwitchPoints.java
 *
 * What:
 *  This class of Objects holds the information needed about Switch Points.
 */

package designer.layout.items;

import java.util.BitSet;

import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 * contains the information about Switch Points:
 * <ol>
 *    <li> the command to turn on the locked light.
 *    <li> the command to turn off the locked light.
 *    <li> the command to turn on the unlocked light.
 *    <li> the command to turn off the unlocked light.
 *    <li> the report when the turnout is locally unlocked.
 *    <li> the report when the turnout is locally locked.
 *    <li> the requests to move the turnout to each route.
 *    <li> the commands to move the turnout to each route.
 *    <li> the reports of the turnout reaching each route.
 *    <li> the reports of the turnout moving away from each route.
 * </ol>
 * No distinction is made between input messages and output messages.
 * So, if that changes in the future, this class will undergo some
 * major changes.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class SwitchPoints
    implements XMLEleObject, Savable {

  /**
   * is the tag for identifying a SwitchPoints Object in the XMl file.
   */
  static final String XML_TAG = "SWITCHPOINTS";

  /**
   * is the tag for identifying one of the messages in the XML file.
   */
  static final String MSG_TAG = "POINTSMSG";

  /**
   * is the tag for labeling the SwitchPoints as a spur or not.
   */
  static final String SPUR_TAG = "SPUR";

  /**
   * this table contains two loosely related items:
   * <ol>
   * <li>
   *     the labels for each IOSPEC as they appear on dialogs
   * <li>
   *     the tag for each IOSpec in the XML file
   * </ol>
   * The reason they are combined in this table is to reinforce that there
   * is a string of both uses for each IOSPEC.
   */
  public static final String[][] MessageName = {
      {
      "Turn Locked Light On", "LOCKONCMD"}
      , {
      "Turn Unlocked Light On", "UNLOCKONCMD"}
      , {
      "Turnout Unlocked Report", "UNLOCKREPORT"}
      , {
      "Turnout Locked Report", "LOCKREPORT"}
  };

  /**
   * The index into MessageName of the dialog label.
   */
  public static final int MSGLABEL = 0;

  /**
   * the index into MessageName of the XML tag.
   */
  public static final int MSGXML = 1;

  /**
   * the IOSpecs for each of the above.  Some of these entries
   * can be null.  For example, if there are no reports of the
   * turnout's position, then the last four will be null.  Others may
   * be null beause the function is duplicated.  For example,
   * turning the lock light on may turn the unlock light off, but
   * both are included to handle the layout where different decoders
   * control the lock and unlock lights.
   */
  private IOSpec[] Decoders = new IOSpec[MessageName.length];

  /**
   * the list of requests, commands, and reports for each route.
   */
  private RouteInfo[] Routes = new RouteInfo[Edge.EDGENAME.length];

  /**
   * the number of messages defined for this SwitchPoints.
   */
  private int NumMessages = 0;

  /**
   * is a flag for indicating the SwitchPoints can be aligned by the dispatcher
   * or not.  true means they are a spur (cannot be moved by the dispatcher)
   * and false means they are completely under dispatcher control.
   */
  private boolean Spur = false;

  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * the constructor.
   */
  public SwitchPoints() {
  }

  /**
   * fetch the IOSpec.
   *
   * @param io is the number of the IOSPec being requested.
   *
   * @return the IOSpec requested, or null if it doesn't exist.
   *
   * @see IOSpec
   */
  public IOSpec getIOSpec(int io) {
    if ( (io >= 0) && (io < MessageName.length)) {
      return Decoders[io];
    }
    return null;
  }

  /**
   * save the IOSpec.
   *
   * @param io is the number of the IOSPec being updated.
   *
   * @param spec is the updated IOSpec; null is valid.
   *
   * @see IOSpec
   */
  public void setIOSpec(int io, IOSpec spec) {
    if ( (io >= 0) && (io < MessageName.length)) {
      if (Decoders[io] == null) {
        if (spec != null) {
          ++NumMessages;
        }
      }
      else if (spec == null) {
        --NumMessages;
      }
      Decoders[io] = spec;
      Saved = false;
    }
  }

  /**
   * fetches the RouteInfo for a route.
   *
   * @param route is the index of the route (SecEdge that the other end
   * terminates on).
   *
   * @return the Route selection information.  It can be null, if there is
   * no information, or the route index is invalid.
   */
  public RouteInfo getRoute(int route) {
    if ( (route >= 0) && (route < Routes.length)) {
      return Routes[route];
    }
    return null;
  }

  /**
   * sets the RouteInfo for a route.
   *
   * @param route is the index of the route (SecEdge that the other end
   * terminates on).
   *
   * @param info is the route information.  It can be null.
   */
  public void setRoute(int route, RouteInfo info) {
    if ( (route >= 0) && (route < Routes.length)) {
      Routes[route] = info;
      Saved = false;
    }
  }

  /**
   * steps through the possible routes in the SwitchPoints, ensuring that all without
   * destinations do not have definitions.
   * @param routes is a 4 element BitSet with bits set for each edge that has a track
   * directly reachable from this edge.
   */
  public void validateRoutes(BitSet routes) {
      for (int dest = 0; dest < Edge.EDGENAME.length; ++dest) {
          if (!routes.get(dest)) {
              Routes[dest] = null;
          }
      }
  }
  
  /**
   * sets the value of the Spur characteristic.
   *
   * @param spur is true if the SwitchPoints cannot be aligned by the
   *        dispatcher and false if they can.
   */
  public void setSpur(boolean spur) {
    Spur = spur;
  }

  /**
   * retrieves the kind of SwitchPoints.
   *
   * @return true if the SwitchPoints are for a spur and false if they
   * are for an OS section.
   */
  public boolean getSpur() {
    return Spur;
  }

  /**
   * is called to determine if the definition of the SwitchPoints is complete.
   * It is complete if at least one route is defined as Normal.  It does not check
   * for multiple Normals because the GUI ensures only one Normla button is checked.
   * 
   * @return true if CATS can handle the information supplied
   */
  public boolean isComplete() {
      for (int dest = 0; dest < Routes.length; ++dest) {
          if ((Routes[dest] != null) && Routes[dest].getNormal()) {
              return true;
          }
      }
      return false;
  }
 
  /**
   * looks for the IOSpec by the name of the message
   * @param name is the label on the message for the spec
   * @return the index of the message or -1, if not found
   */
  public int findSpecByName(String name) {
      for (int i = 0; i < MessageName.length; ++i) {
          if (MessageName[i][MSGXML].equals(name)) {
              return i;
          }
      }
      return -1;
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    SwitchMessage msg;
    RouteInfo newRoute;

    if (MSG_TAG.equals(objName)) {
      msg = (SwitchMessage) objValue;
      if ( (msg.Function >= 0) && (msg.Function < MessageName.length)) {
        Decoders[msg.Function] = msg.Message;
        ++NumMessages;
      }
      else {
        resultMsg = new String("A " + XML_TAG + " found an invalid message:"
                               + msg.Function);
      }
    }
    else if (RouteInfo.XML_TAG.equals(objName)) {
      newRoute = (RouteInfo) objValue;
      Routes[newRoute.getRouteId()] = newRoute;
    }
    else {
      resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                             + objName + ").");
    }
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    Saved = true;
    return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    boolean childStatus = true;
    for (int i = 0; i < MessageName.length; ++i) {
      if (Decoders[i] != null) {
        childStatus &= Decoders[i].isSaved();
      }
    }
    return Saved && childStatus;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    boolean hasRoute = false;

    for (int route = 0; route < Routes.length; ++route) {
      if (Routes[route] != null) {
        hasRoute = true;
      }
    }

    if ( (NumMessages > 0) || hasRoute || Spur) {
      Element thisObject = new Element(XML_TAG);
      if (Spur) {
        thisObject.setAttribute(SPUR_TAG, Block.TRUE);
      }
      for (int msg = 0; msg < MessageName.length; ++msg) {
        if (Decoders[msg] != null) {
          Element dec = new Element(MSG_TAG);
          dec.addContent(MessageName[msg][MSGXML]);
          Decoders[msg].putXML(dec);
          thisObject.addContent(dec);
        }
      }
      for (int route = 0; route < Routes.length; ++route) {
        if (Routes[route] != null) {
          Routes[route].putXML(thisObject);
        }
      }
      parent.addContent(thisObject);
    }
    Saved = true;
    return null;
  }

  /**
   * asks the sub-component to create a copy of itself.
   *
   * @return an identical copy of itself.
   */
  public SwitchPoints copy() {
    SwitchPoints newCopy = new SwitchPoints();
    System.arraycopy(Decoders, 0, newCopy.Decoders, 0, Decoders.length);
    System.arraycopy(Routes, 0, newCopy.Routes, 0, Routes.length);
    newCopy.Spur = Spur;
    newCopy.NumMessages = NumMessages;
    newCopy.Saved = false;
    return newCopy;
  }

  /**
   * asks the sub-component to create a shallow copy of itself.  A shallow
   * copy does not include the decoder definitions.
   *
   * @return an identical copy of itself.
   */
  public SwitchPoints shallowCopy() {
    SwitchPoints newCopy = new SwitchPoints();
    for (int e = 0; e < Routes.length; ++e) {
        if (Routes[e] != null) {
            newCopy.Routes[e] = Routes[e].shallowCopy();
        }
    }
    newCopy.Spur = Spur;
    newCopy.Saved = false;
    return newCopy;
  }

  /**
   * registers a SwitchPointsFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new SwitchPointsFactory());
    XMLReader.registerFactory(MSG_TAG, new SwitchMessageFactory());
    RouteInfo.init();
  }
}

/**
 * is an inner class for constructing messages.
 */
class SwitchMessage
    implements XMLEleObject {

  /**
   * is where the message is constructed.
   */
  private String Msg = null;

  /**
   * is the function of the IOSpec.
   */
  public int Function = -1;

  /**
   * is the IOSpec.
   */
  public IOSpec Message;

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
      Msg = new String(eleValue);
      return null;
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    if (IOSpec.XML_TAG.equals(objName)) {
      Message = (IOSpec) objValue;
    }
    else {
      resultMsg = new String("A " + SwitchPoints.MSG_TAG +
                             " cannot contain an Element ("
                             + objName + ").");
    }
    return resultMsg;

  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(SwitchPoints.MSG_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    for (int i = 0; i < SwitchPoints.MessageName.length; ++i) {
      if (SwitchPoints.MessageName[i][SwitchPoints.MSGXML].equals(Msg)) {
        Function = i;
        return null;
      }
    }
    return new String("A " + SwitchPoints.MSG_TAG +
                      " cannot contain a text field ("
                      + Msg + ").");
  }
}

/**
 * is a Class known only to the SwitchPoints class for creating SwitchPoints
 * from an XML document.
 */
class SwitchPointsFactory
    implements XMLEleFactory {

  /**
   * is true if a "Spur=true" attribute is found.
   */
  boolean foundSpur;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    foundSpur = false;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    if (SwitchPoints.SPUR_TAG.equals(tag)) {
      if (Block.TRUE.equals(value)) {
        foundSpur = true;
      }
    }
    else {
      resultMsg = new String("A " + SwitchPoints.XML_TAG +
                             " XML Element cannot have a " + tag +
                             " attribute.");
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    SwitchPoints newPts = new SwitchPoints();
    newPts.setSpur(foundSpur);
    return newPts;
  }
}

/**
 * is a Class known only to the SwitchPoints class for creating messages
 * from an XML document.
 */
class SwitchMessageFactory
    implements XMLEleFactory {

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    resultMsg = new String("A " + SwitchPoints.MSG_TAG +
                           " XML Element cannot have a " + tag +
                           " attribute.");
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new SwitchMessage();
  }
}
/* @(#)SwitchPoints.java */
