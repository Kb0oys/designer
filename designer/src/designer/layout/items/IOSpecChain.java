/* Name: IOSpecChain.java
 *
 * What:
 *  This is a class for holding multiple IOSpecs.
 */
package designer.layout.items;

import designer.gui.jCustom.AbstractManagerTableModel;
import designer.gui.jCustom.ChainModel;
import designer.gui.jCustom.ManagerEditPane;
import designer.layout.AbstractListElement;
import designer.layout.AbstractListManager;
import designer.layout.JmriName;
import designer.layout.ListManager;
import designer.layout.xml.*;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.jdom2.Element;

/**
 * is a class for holding multiple IOSpecs.  It is assumed to be non-changing.
 * It is created and IOSpecs are added to it, but they are not changed or
 * removed.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class IOSpecChain
extends IOSpec implements ListManager {
    /**
     *   The XML tag for recognizing the ListManager description.
     */
    public static final String XML_TAG = "IOSPECCHAIN";
    
    /**
     * The JMRI prefix for an IOSpecChain
     */
    public static final String CHAIN_PREFIX = "IC";
    
    /**
     * is the list of composite IOSpecs.
     */
    private IOSpecList Specs;
    
    /**
     * is a counter for anonymous chains
     */
    private static int Counter = 1;
    
    /**
     * is the null parameter ctor.  It is called when an
     * anonymous IOSpecChain is encountered.  This happens
     * only when migrating from a version of CATS prior to
     * the designer 21 version.  This means if one is seen,
     * no non-anonymous IOSpecLists will be seen, so we can
     * sequentially number them, as found.
     */
    public IOSpecChain() {
        this(String.valueOf(Counter));
    }
    
    /**
     * is the ctor, when the "system address" (number) is
     * known.
     * 
     * @param addr is the system identifier
     */
    public IOSpecChain(String addr) {
        super(addr);
        JmriPrefix = new String(CHAIN_PREFIX);
        Specs = new IOSpecList();
        if (addr != null) {
        	Counter = Math.max(Counter, Integer.parseInt(addr) + 1);
        }
    }
    
    /**
     * retrieves the list of IOSpecs.  This is a "friend" method.
     * @return the list of IOSpecs.
     */
    IOSpecList getSpecs() {
        return Specs;
    }

    /**
     * updates the contents of the list of IOSpec
     * 
     * @param is the Vector of IOSpecs that form the new list
     */
    void setSpecs(Vector<Object> newSpecs) {
        Specs.setList(newSpecs);
    }
    
    /**
     * is the "value" of an IOSpecChain, needed for editing.
     * @return is the IOSpecChain, itself
     */
    public IOSpecChain getSelf()
    {
        return this;
    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("An " + XML_TAG + " cannot have a text value (" +
                eleValue + ").");
    }
    
    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        String result = null;
        if (IOSpec.XML_TAG.equals(objName)) {
            Specs.add((Savable) objValue);
        }
        else {
            result = new String("An " + XML_TAG + " cannot contain a " +
                    objName + ".");
        }
        return result;
    }
    
    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }
    
    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        String resultMsg = null;
        if (LoadFilter.instance().isEnabled(LoadFilter.XML_IOSPECCHAIN)) {
            IOSpecChainManager.IOSpecChainKeeper.add(this);
            JmriName.registerPrefix(this);
        }
        return resultMsg;
    }
    
    
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved && Specs.isSaved();
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element thisObject = new Element(XML_TAG);
        thisObject.setAttribute(DECADDR, String.valueOf(DecAddr));
        if ((UserName != null) && (UserName.length() > 0)) {
            thisObject.setAttribute(USER_NAME, UserName);
        }
        if (Delay != 0) {
            thisObject.setAttribute(DELAY, String.valueOf(Delay));
        }
        Specs.putXML(thisObject);
        parent.addContent(thisObject);
        Saved = true;
        return null;
    }
    
    /**
     * asks the sub-component to create a copy of itself.
     *
     * @return an identical copy of itself.
     */
    public AbstractListElement copy() {
        IOSpecChain newCopy;
        if ((DecAddr == null) || "".equals(DecAddr)) {
        	newCopy = new IOSpecChain();
        }
        else {
        	newCopy = new IOSpecChain(DecAddr);
        }
        newCopy.ThrowCmd = ThrowCmd;
        newCopy.JmriPrefix = JmriPrefix;
        newCopy.TurnOff = TurnOff;
        if (UserName != null) {
            newCopy.UserName = new String(UserName);
        }
        newCopy.Delay = Delay;
        newCopy.Saved = false;
        newCopy.setSpecs(Specs.makeCopy());
        return newCopy;
    }
    
    public int size() {
        return Specs.size();
    }
    
    public boolean add(Savable spec) {
        Specs.add(spec);
        Saved = false;
        return true;
    }
    
    public AbstractListElement findElement(String key) {
        return Specs.findElement(key);
    }
    
    public Vector<String> getListList() {
        return Specs.getListList();
    }
    
    public AbstractManagerTableModel createModel() {
        // TODO Auto-generated method stub
        return null;
    }
    
    public void commit() {
        super.commit();
        Specs.commit();
    }
    
    public void init() {
        Specs.init();
    }
    
    /**
     * is the accessor method for creating an IOSpecChainRenderer
     * @return an instance of an IOSpecChainRenderer
     */
    static public TableCellRenderer getRenderer() {
        return new IOSpecChainRenderer(true);
    }
    
    /**
     * is the accessor method for creating an IOSpecChain Editor
     * @return an instance of an IOSpecChainEditor
     */
    static public TableCellEditor getEditor() {
        return new IOSpecChainEditor();
    }
    
    
    /**
     * is a concrete implementation of an AbstractListManager for holding the
     * list of IOSpecs.
     */
    class IOSpecList extends AbstractListManager {
        
        public AbstractManagerTableModel createModel() {
            return new ChainModel(makeCopy());
        }
        
        public void copy() {
            IOSpecList newCopy = new IOSpecList();
            super.makeCopy();
            newCopy.TheList = TheListCopy;
        }
        
        /**
         * replaces the list of IOSpecs
         * @param newList is the replacement list
         */
        public void setList(Vector<Object> newList) {
            TheList = newList;
        }

        public void registerFactory() {
        }
    }
}

/**
 * is a class for "rendering" an IOSpecChain.  Since an IOSpecChain is a
 * dynamic data structure, rendering amounts to an Edit button.
 */
class IOSpecChainRenderer extends JLabel
implements TableCellRenderer {
    Border unselectedBorder = null;
    Border selectedBorder = null;
    boolean isBordered = true;
    
    public IOSpecChainRenderer(boolean isBordered) {
        this.isBordered = isBordered;
        setOpaque(true); //MUST do this for background to show up.
    }
    
    public Component getTableCellRendererComponent(
            JTable table, Object chain,
            boolean isSelected, boolean hasFocus,
            int row, int column) {
        if (isBordered) {
            if (isSelected) {
                if (selectedBorder == null) {
                    selectedBorder = BorderFactory.createMatteBorder(2,5,2,5,
                            table.getSelectionBackground());
                }
                setBorder(selectedBorder);
            } else {
                if (unselectedBorder == null) {
                    unselectedBorder = BorderFactory.createMatteBorder(2,5,2,5,
                            table.getBackground());
                }
                setBorder(unselectedBorder);
            }
        }
        return new JLabel("edit");
    }
}

/**
 * is a class for invoking the table editor on an IOSpecChain
 */
class IOSpecChainEditor extends DefaultCellEditor
implements TableCellEditor,
ActionListener {
    /**
     * The editor button that brings up the dialog.
     * We extend DefaultCellEditor for convenience,
     * even though it means we have to create a dummy
     * check box.  Another approach would be to copy
     * the implementation of TableCellEditor methods
     * from the source code for DefaultCellEditor.
     */
    JButton button;
    IOSpecChain currentChain;
    protected static final String EDIT = "edit";
    
    public IOSpecChainEditor() {
        super(new JCheckBox());
//      Set up the editor (from the table's point of view),
//      which is a button.
//      This button brings up the color chooser dialog,
//      which is the editor from the user's point of view.
        
        button = new JButton();
        button.setActionCommand(EDIT);
        button.addActionListener(this);
        button.setBorderPainted(false);
    }
    
    /**
     * Handles events from the editor button and from
     * the dialog's OK button.
     */
    public void actionPerformed(ActionEvent e) {
        if (EDIT.equals(e.getActionCommand())) {
            if (ManagerEditPane.editList(new ChainModel(currentChain.getSpecs().makeCopy()))) {
                currentChain.commit();
            }
            
//          Make the renderer reappear.
            fireEditingStopped();
            
        } else { //User pressed dialog's "OK" button.
//          currentColor = colorChooser.getColor();
        }
    }
    
//  Implement the one CellEditor method that AbstractCellEditor doesn't.
    public Object getCellEditorValue() {
        return currentChain;
    }
    
//  Implement the one method defined by TableCellEditor.
    public Component getTableCellEditorComponent(JTable table,
            Object value,
            boolean isSelected,
            int row,
            int column) {
        currentChain = (IOSpecChain)value;
        return button;
    }
}

/**
 * is a Class known only to the IOSpecChain class for creating IOSpecChains from
 * an XML document.
 */
class IOSpecChainFactory
implements XMLEleFactory {
    
    /**
     * is the address to send the command to.
     */
    String Recipient;
    
    /**
     * is the User Name.
     */
    String UserName;
    
    /**
     * is the Delay after executing this Chain to wait before
     * executing the next link
     */
    int Delay;
    
    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        Recipient = "";
        UserName = null;
        Delay = 0;
    }
    
    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        String resultMsg = null;
        if (IOSpec.DECADDR.equals(tag)) {
            Recipient = value;
        }
        else if (IOSpec.DELAY.equals(tag)) {
            try {
                int addr = Integer.parseInt(value);
                Delay = addr;
            }
            catch (NumberFormatException nfe) {
                System.out.println("Illegal delay for " + IOSpec.XML_TAG
                        + "XML Element:" + value);
            }        
        }
        else if (IOSpec.USER_NAME.equals(tag)) {
            UserName = value;
        }
        else {
            resultMsg = new String("A " + IOSpecChain.XML_TAG +
                    " XML Element cannot have a " + tag +
            " attribute.");
        }
        return resultMsg;
    }
    
    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        IOSpecChain spec = new IOSpecChain(Recipient);
        spec.setUserName(UserName);
        spec.setDelay(Delay);
        return spec;
    }
}
/* @(#)IOSpecChain.java */
