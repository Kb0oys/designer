/* Name: SecSignal.java
 *
 * What:
 *   This class is the container for all the information about a Section's
 *   Signal.
 */

package designer.layout.items;

import designer.gui.GridTile;
import designer.layout.SignalTemplate;
import designer.layout.TemplateStore;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 * is a container for all the information about a Section's Signal.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2012, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class SecSignal
    implements XMLEleObject, Savable {
  /**
   * is the tag for identifying a SecSignal in the XMl file.
   */
  static final String XML_TAG = "SECSIGNAL";

  /**
   * The Name of the SecSignal.
   */
  private String SigName;

  /**
   * the number of heads in the Icon.
   */
  int IconHeads;

  /**
   * true if the Icon is a lamp signal.
   */
  boolean IconLamp;

  /**
   * describes the Signal's Icon on the dispatcher's panel.
   */
  private PanelSignal MyIcon;

  /**
   * describes the actual signal on the layout.
   */
  private PhysicalSignal MySignal;

  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * constructs a SecSignal, given no other information
   */
  public SecSignal() {
  }

  /**
   * retrieves the SecSignal's name.
   *
   * @return the SecSignal's name.  Null is a valid value.
   */
  public String getSigName() {
    if (SigName != null) {
      return new String(SigName);
    }
    return null;
  }

  /**
   * changes the SecSignal's name.
   *
   * @param name is the new name.  Null is valid.
   */
  public void setSigName(String name) {
    SigName = name;
    Saved = false;
  }

  /**
   * retrieves the SecSignal's Icon information.
   *
   * @return the SecSignal's Icon.  Null is a valid value.
   *
   * @see PanelSignal
   */
  public PanelSignal getSigIcon() {
      if ((MyIcon == null) || IntermediateSignal.class.isInstance(MyIcon)) {
          return null;
      }
    return MyIcon;
  }

  /**
   * changes the SecSignal's Icon.
   *
   * @param icon is the new Icon information.  Null is valid.
   *
   * @see PanelSignal
   */
  public void setSigIcon(PanelSignal icon) {
    MyIcon = icon;
    Saved = false;
  }

  /**
   * retrieves the actual signal's description.
   *
   * @return the signal's description.  Null is a valid value.
   *
   * @see PhysicalSignal
   */
  public PhysicalSignal getSigDescription() {
    return MySignal;
  }

  /**
   * changes the actual signal's description.
   *
   * @param des is the new description.  Null is valid.
   *
   * @see PhysicalSignal
   */
  public void setSigDescription(PhysicalSignal des) {
    MySignal = des;
    Saved = false;
  }

  
    /**
     * constructs an Enumeration over the sorted list of unique IOSPecs
     * used by the signal head.
     *
     * @return the Enumeration.
     *
     * @see IOSpec
     */
//    public Enumeration getDecoderAddresses() {
//      return MySignal.getCommandList().getDecoderAddresses();
//    }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    SigName = new String(eleValue);
    return null;
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    if (PanelSignal.XML_TAG.equals(objName)) {
      MyIcon = (PanelSignal) objValue;
    }
    else if (PhysicalSignal.XML_TAG.equals(objName)) {
      MySignal = (PhysicalSignal) objValue;
    }
    else {
      resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                             + objName + ").");
    }
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    Saved = true;
    return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    return Saved;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    Element thisObject = new Element(XML_TAG);
    if ( (SigName != null) && (!SigName.equals(""))) {
      thisObject.addContent(SigName);
    }
    if (MyIcon != null) {
      MyIcon.putXML(thisObject);
    }
    if (MySignal != null) {
      MySignal.putXML(thisObject);
    }
    parent.addContent(thisObject);
    Saved = true;
    return null;
  }

  /**
   * tells the SecSignal to link itself into the various data structures.
   *
   * @param tile is the GridTile where painting happens.
   */
  public void install(GridTile tile) {
    int heads = 1;
    boolean lamp = true;

    if (MyIcon != null) {
      if (MySignal != null) {
        SignalTemplate template = TemplateStore.SignalKeeper.
            find(MySignal.getTemplateName());
        if (template != null) {
          heads = template.getNumHeads();
          if (heads == 0) {
            heads = 1;
          }
          else {
            lamp = template.isLights();
          }
          MyIcon.setParms(lamp, heads);
        }
      }
      MyIcon.install(tile);
    }
  }

  /**
   * asks the sub-component to create a copy of itself.
   *
   * @return an identical copy of itself.
   */
  public SecSignal copy() {
    SecSignal newCopy = new SecSignal();
    if (MyIcon != null) {
      newCopy.MyIcon = MyIcon.copy();
    }
    if (MySignal != null) {
      newCopy.MySignal = MySignal.copy();
    }
    newCopy.IconHeads = IconHeads;
    newCopy.IconLamp = IconLamp;
    newCopy.Saved = false;
    return newCopy;
  }

  /**
   * asks the sub-component to create a shallow copy of itself.
   *
   * @return an identical copy of itself.
   */
  public SecSignal shallowCopy() {
    SecSignal newCopy = new SecSignal();
    if (MyIcon != null) {
      newCopy.MyIcon = MyIcon.copy();
    }
    if (MySignal != null) {
      newCopy.MySignal = MySignal.shallowCopy();
    }
    newCopy.IconHeads = IconHeads;
    newCopy.IconLamp = IconLamp;
    newCopy.Saved = false;
    return newCopy;
  }

 /**
   * removes itself from the various data structures.
   */
  public void uninstall() {
    if (MyIcon != null) {
      MyIcon.uninstall();
    }
  }

  /**
   * registers a SecSignalFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new SecSignalFactory());
    PanelSignal.init();
    PhysicalSignal.init();
  }
}

/**
 * is a Class known only to the SecSignal class for creating SecSignals from
 * an XML document.  Its purpose is to pick up the location of the SecSignal
 * in the GridTile, its orientation, and physical attributes on the layout.
 */
class SecSignalFactory
    implements XMLEleFactory {

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    resultMsg = new String("A " + SecSignal.XML_TAG +
                           " XML Element cannot have a " + tag +
                           " attribute.");
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new SecSignal();
  }
}
/* @(#)SecSignal.java */