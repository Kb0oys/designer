/* Name: Train.java
 *
 * What:
 *   This class contains the information about a train.  It is not needed
 *   with Version 0.16 and later because the mini-database classes
 *   (@see designer.layout.store) eliminate the need for named Record
 *   classes.  However, it is included so that XML files created under
 *   previous versions can be read.
 *
 */
package designer.layout.items;

import designer.layout.store.FieldPair;
import designer.layout.store.GenericRecord;
import designer.layout.TrainStore;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 *   This class contains the information about a train.  It is not needed
 *   with Version 0.16 and later because the mini-database classes
 *   (@see designer.layout.store) elminiate the need for named Record
 *   classes.  However, it is included so that XML files created under
 *   previous versions can be read.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2004, 2011, 2015, 2020, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class Train extends GenericRecord {
  /**
   * is the tag for identifying a Train in the XML file.
   */
  public static final String XML_TAG = "TRAIN";

  /**
   * is the property tag for picking out the Train's name.
   */
  public static final String TRAIN_NAME = "TRAIN_NAME";

  /**
   * is the property tag for picking out the Train's identity.
   */
  public static final String TRAIN_SYMBOL = "TRAIN_SYMBOL";

  /**
   * is the property tag for picking out the lead engine.
   */
  public static final String ENGINE = "ENGINE";

  /**
   * is the property tag for picking out the transponding flag.
   */
  public static final String TRANSPONDING = "TRANSPONDING";

  /**
   * is the property tag for picking out the Caboose.
   */
  public static final String CABOOSE = "CABOOSE";

  /**
   * is the XML tag for identifying the Crew Field descriptions.
   */
  public static final String CREW = "CREW";

  /**
   * is the property tag for picking out the crew on duty time.
   */
  public static final String ONDUTY = "ONDUTY";

  /**
   * is the property tag for picking out the scheduled departure time.
   */
  public static final String DEPARTURE = "DEPARTURE";

  /**
   * is the property tag for the TrainStat Font of the train.
   */
  public static final String FONT = "FONT";

  /**
   * is the property tag for the length of the train received from Operations
   * (or manually)
   */
  public static final String LENGTH = "LENGTH";
  
  /**
   * is the property tag for the weight of the train received from Operations
   * (or manually)
   */
  public static final String WEIGHT = "WEIGHT";
  
  /**
   * is the number of cars in the train
   */
  public static final String CARS = "CARS";
 
  /**
   * is the property tag for the auto-terminate in Operations flag
   */
  public static final String AUTOTERMINATE = "AUTOTERMINATE";
  
  /**
   * is the property tag for the label background enable flag
   */
  public static final String LABELBACKGROUND = "LABELBACKGROUND";
  
  /**
   * is the String "true" for the XML file.
   */
  public static final String TRUE = "true";

  /**
   * is the String "false" for the XML file.
   */
  public static final String FALSE = "false";

  /**
   * is another constructor.
   * @param tag is the XML tag for identifying teh Records.
   * @param size is the initial size of the Vector.
   */
  public Train(String tag, int size) {
    super(tag, size);
  }

  /**
   * is the constructor.
   *
   * @param tag is the XML tag for identifying the Records.
   * Records.
   */
  public Train(String tag) {
    super(tag);
  }


  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    return new String("A " + XML_TAG + " cannot contain an Element ("
                      + objName + ").");
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /**
   * registers a TrainFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new TrainFactory(GenericRecord.DATARECORD));
  }
}

/**
 * is a Class known only to the Traintemp class for creating
 * Trains from an XML document.
 */
class TrainFactory
    implements XMLEleFactory {
  /**
   * is the XML Tag of the product.
   */
  private String RecordTag;

  /**
   * is the GenericRecord being created.
   */
  private Train NewRecord;

  /**
   * is the constructor.
   *
   *@param tag is the XML tag of the Record.
   */
  TrainFactory(String tag) {
    RecordTag = tag;
  }

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    NewRecord = new Train(RecordTag);
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    NewRecord.add(new FieldPair(tag, value));
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    TrainStore.TrainKeeper.add(RecordTag, NewRecord);
    return NewRecord;
  }
}
/* @(#)Train.java */
