/* Name: InputSpec.java
 *
 * What:
 *  This is a class for holding the information about an input Event
 *  from the railroad.
 */
package designer.layout.items;

/**
 * a class for holding the information about an input about an input Event
 * from the railroad.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version 1.0
 */

public class InputSpec {
  /**
   * the constructor.
   */
  public InputSpec() {
  }

}
