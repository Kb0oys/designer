/* Name Block.java
 *
 * What:
 *  This class holds the information needed for a detection block.
 */
package designer.layout.items;

import designer.gui.CtcFont;
import designer.layout.AbstractNamedBean;
import designer.layout.ColorList;
import designer.layout.Layout;
import designer.layout.Node;
import designer.layout.xml.*;

import java.util.Enumeration;
import java.util.Vector;

import org.jdom2.Element;

/**
 * contains the information needed for a detection block:
 * <ul>
 *   <li>the name of the block
 *   <li>the IOSpec of the detector
 *   <li>the discipline used on the block
 *   <li>if the block is hidden on the dispatcher panel or not
 * </ul>
 * <p>
 * There are probably better ways of doing this because two tracks can be in
 * the same Section, but be in different blocks.  So, this should probably be
 * tied to the Tracks, rather than the Section.  Furthermore, splitting
 * a Block into two separate Blocks or merging a Block can be something of
 * a problem.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2012, 2013, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class Block extends AbstractNamedBean
implements XMLEleObject, Savable {
    
    /**
     * a list of all Blocks saved, so that the Saved flag can be
     * cleared after the save is completed (so that they will be
     * written at the next Save).
     */
    private static Vector<Block> BlockKeeper;
    
    /**
     *   The XML tag for recognizing the Block description.
     */
    public static final String XML_TAG = "BLOCK";
    
    /**
     * The XML attribute tag for the signaling discipline.
     */
    static final String DISCIPLINE = "DISCIPLINE";
    
    /**
     * The XML attribute tag for the Block's name.
     */
    static final String NAME = "NAME";
    
    /**
     * The XML attribute tag for the Block's Station.
     */
    static final String STATION = "STATION";
    
    /**
     * The XML attribute tag for the Visible flag.
     */
    static final String VISIBLE = "VISIBLE";
    
    /**
     * The XML object tag for the occupied IOSpec.
     */
    static final String OCCUPIED = "OCCUPIEDSPEC";
    
    /**
     * The XML object tag for the unoccupied IOSpec.
     */
    static final String UNOCCUPIED = "UNOCCUPIEDSPEC";
    
    /**
     * The XML object tag for the Reporter holding the train name
     */
    static final String TRAINREPORTER = "TRAINREPORTER";
    
    /**
     * The "true" string.
     */
    static final String TRUE = "true";
    
    /**
     * The "false" string.
     */
    static final String FALSE = "false";
    
    /**
     * The signal disciplines that have been defined.
     */
    public static final String[] DisciplineName = {
        "Unknown",
        "CTC",
        "ABS",
        "APB-2",
        "APB-3",
        "DTC"
    };
    
    /**
     * The default discipline, which is simply a placeholder.
     */
    public static final int UNDEFINED = 0;
    
    /**
     * the Discipline defined for the Block.
     */
    private int Discipline = 0; // default is Unknown - it can be overridden.
    
    /**
     * the name of the Block.
     */
    private String BlockName;
    
    /**
     * the name of the Station.
     */
    private String StationName;
    
    /**
     * a flag, where true means the dispatcher panel should display the Block
     * and false means it should be hidden.
     */
    private boolean Visible = true;
    
    /**
     * the IOSPec for identifying when the block is occupied.
     */
    private IOSpec Occupied;
    
    /**
     * the IOSPec for identifying when the Block is unoccupied.  It should be
     * the opposite of Occupied, but is included for those cases when it
     * isn't.
     */
    private IOSpec Unoccupied;
    
    /**
     * the ReporterSpec (actually Internal Reporter) for holding the name of the train
     * occupying the Block.
     */
    private ReporterSpec TrainReporter;
    
    /**
     *   a true flag means that it has been saved, so it should not be written
     *   again.  This use is a little different from how other classes use
     *   Saved because other classes use it as a request for initiating a Save;
     *   this class uses it to keep from writing multiple times.
     */
    private boolean Saved = false;
    
    /**
     * the constructor.
     *
     * @param name is the name of the Block.  It can be null.
     */
    public Block(String name) {
        if (name != null) {
            BlockName = new String(name);
        }
    }
 
    /**
     * invoked to construct a Vector of Blocks.  The Vector is constructed
     * by traversing the Layout, looking for BlkEdges, and checking if a
     * BlkEdge has not already been seen.  The technique was chosen because
     * the active Blocks are not kept in any one place.
     * @return the Vector of Blocks
     */
    public static Vector<Block> getBlocks() {
        Vector<Block> blkList = new Vector<Block>();
        Section s;
        Block b;
        boolean found;
        for (Enumeration<Node> e = Layout.getConstruction().elements(); e.hasMoreElements(); ) {
            s = e.nextElement().getContents();
            for (int edge = 0; edge < Edge.EDGENAME.length; edge++) {
                if ((s.getEdge(edge) != null) && ((b = s.getEdge(edge).getBlock()) != null)) {
                    found = false;
                    for (Block blk : blkList) {
                        if (blk == b) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        blkList.add(b);
                    }
                }
            }
        }
        return blkList;
    }
 
    /**
     * determines the Track color for all tracks in the Block and corrects the display.
     */
    public void refreshTrackColor() {
        Section s;
        for (Enumeration<Node> e = Layout.getConstruction().elements(); e.hasMoreElements(); ) {
            s = e.nextElement().getContents();
            for (int edge = 0; edge < Edge.EDGENAME.length; edge++) {
                if ((s.getEdge(edge) != null) && (s.getEdge(edge).getBlock() == this)) {
                    s.getEdge(edge).refreshColors();
                    return;
                }
            }
        }        
    }
    
    /**
     * return the Block's discipline.
     *
     * @return an index into DisciplineName
     */
    public int getDiscipline() {
        return Discipline;
    }
    
    /**
     * sets the Block's Discipline.
     *
     * @param discipline should be a valid index into DisciplineName.
     */
    public void setDiscipline(int discipline) {
        if ( (discipline >= 0) && (discipline < DisciplineName.length)) {
            Discipline = discipline;
        }
    }
    
    /**
     * return the Block's name.
     * @return the Block's name.
     */
    public String getBlockName() {
        if (BlockName != null) {
            return new String(BlockName);
        }
        return null;
    }
 
    /**
     * changes the Block's name.  It does trim the Name, which
     * means that the name could be the empty sting.
     * @param newName is the new name for the block
     */
    public void setBlockName(String newName) {
        BlockName = new String(newName).trim();
    }
    
    /**
     * return the Name of the Station the Block is at.
     * @return the Station name.
     */
    public String getStationName() {
        if (StationName != null) {
            return new String(StationName);
        }
        return null;
    }
    
    /**
     * saves the name of the Station at the Block.  Multiple Blocks
     * can be at the same Station (e.g. the main and a siding).
     * @param station is the Station encompassing the Block
     */
    public void setStationName(String station) {
        if (station != null) {
            StationName = new String(station);  
        }
        else {
            StationName = null;
        }
    }
    
    /**
     * return the value of Visible.
     *
     * @return true if the Block is to be shown; false if it is to be hidden.
     */
    public boolean getVisible() {
        return Visible;
    }
    
    /**
     * sets the value of Visible.
     *
     * @param visible is true if the Block is to be shown on
     * the dispatcher panel or false if it is to be hidden.
     */
    public void setVisible(boolean visible) {
        Visible = visible;
    }
    
    /**
     * returns the IOSpec for indicating an occupied Block.
     *
     * @return the IOSPec sent by a detector when the Block is occupied.
     * null is valid.
     */
    public IOSpec getOccupied() {
        return Occupied;
    }
    
    /**
     * sets the IOSPec for indicating an occupied Block.
     *
     * @param occupied names the detector for when the Block is occupied.
     * null is valid.
     */
    public void setOccupied(IOSpec occupied) {
        Occupied = occupied;
    }
    
    /**
     * returns the IOSpec for indicating an unoccupied Block.
     *
     * @return the IOSPec sent by a detector when the Block is emptied.
     * null is valid.
     */
    public IOSpec getUnoccupied() {
        return Unoccupied;
    }
    
    /**
     * sets the IOSPec for indicating an unoccupied Block.
     *
     * @param unoccupied names the detector when the Block is emptied.
     * null is valid.
     */
    public void setUnoccupied(IOSpec unoccupied) {
        Unoccupied = unoccupied;
    }
    
    /**
     * returns the IOSpec for the Reporter holding the occupying train's symbol.
     *
     * @return the IOSPec holding the occupying train's symbol for communicating
     * with the world outside of CATS.
     * null is valid.
     */
    public ReporterSpec getReporter() {
        return TrainReporter;
    }
    
    /**
     * sets the IOSPec for the Reporter holding the occupying train's symbol.
     *
     * @param reporter names the IOSPec holding the occupying train's symbol for communicating
     * with the world outside of CATS.
     * null is valid.
     */
    public void setReporter(ReporterSpec reporter) {
        TrainReporter = reporter;
    }
    
    /**
     * clears the Saved flag on all Blocks that have been written, so they can
     * be written once on the next Save.  It should be called after all the
     * Blocks have been written.
     */
    public static void clearSaved() {
        if (BlockKeeper != null) {
            for (Enumeration<Block> e = BlockKeeper.elements(); e.hasMoreElements();
            ) {
                e.nextElement().Saved = false;
            }
            BlockKeeper = null;
        }
    }

    /**
     * is called to determine what color the tracks in the Block should be painted.
     * It looks at several things:
     * <ul>
     * <li>if the Block has a name
     * <li>if the Block has detector addresses
     * </ul>
     * @return the ColorList key for what properties of the Block have
     * been defined
     */
    public String getBlockColor() {
        if ((BlockName == null) || (BlockName.equals(""))) {
            return (ColorList.ERRORCOLOR);
        }
        if ((Occupied == null) && (Unoccupied == null)) {
            return (ColorList.DARK);
        }
        return (ColorList.EMPTY);
    }

    /**
     * creates a new Block that is a shallow copy of this Block.  It
     * does not copy
     * <ul>
     * <li> the Block name
     * <li> Station name
     * <li> detectors
     * </ul>
     * @return the shallow copy
     */
    public Block shallowCopy() {
        Block cp = new Block(null);
        cp.Discipline = Discipline;
        cp.Visible = Visible;
        cp.Saved = false;
        return cp;
    }
    
    /**
     * performs a deep copy - all fields - from this Block
     * to another Block
     * @param copy is the Block receiving this Block's contents
     */
    public void deepCopy(Block copy) {
        if (BlockName != null) {
            copy.BlockName = new String(BlockName);
        }
        if (StationName != null) {
            copy.StationName = new String(StationName);
        }
        copy.Discipline = Discipline;
        copy.Visible = Visible;
        copy.Saved = Saved;
        if ((Occupied != null) && Occupied.isComplete()) {
            copy.Occupied = (IOSpec) Occupied.copy();
        }
        else {
            copy.Occupied = null;
        }
        if ((Unoccupied != null) && Unoccupied.isComplete()) {
            copy.Unoccupied = (IOSpec) Unoccupied.copy();
        }
        else {
            copy.Unoccupied = null;
        }
        if ((TrainReporter != null) && TrainReporter.isComplete()) {
            copy.TrainReporter = (ReporterSpec) TrainReporter.copy();
        }
    }
    
    /**
     * This method copies the contents of another Block into
     * this Block using a deep copy and marks the Block as
     * not saved
     * @param replacement is the source block of the new contents
     */
    public void replaceBlock(Block replacement) {
        replacement.deepCopy(this);
        refreshTrackColor();
        Saved = false;
    }
    
    public void setProperty(Object key, Object value) {
    }
    
    public Object getProperty(Object key) {
        return getBlockColor();
    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return null;
    }
    
    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        String resultMsg = null;
        if (OCCUPIED.equals(objName)) {
            Occupied = ( (Detector) objValue).getSpec();
        }
        else if (UNOCCUPIED.equals(objName)) {
            Unoccupied = ( (Detector) objValue).getSpec();
        }
        else if (TRAINREPORTER.equals(objName)) {
            TrainReporter = (ReporterSpec) ((Detector) objValue).getSpec();
        }
        else {
            resultMsg = new String(XML_TAG + " XML elements cannot have " +
                    objName + " embedded objects.");
        }
        return resultMsg;
    }
    
    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }
    
    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        Saved = false;
        return null;
    }
    
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved;
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element thisObject = new Element(XML_TAG);
        if (!Saved) {
            if (BlockKeeper == null) {
                BlockKeeper = new Vector<Block>();
            }
            if (BlockName != null) {
                thisObject.setAttribute(NAME, BlockName);
            }
            if (StationName != null) {
                thisObject.setAttribute(STATION, StationName);
            }

            thisObject.setAttribute(DISCIPLINE, DisciplineName[Discipline]);
            thisObject.setAttribute(VISIBLE,
                    (Visible ? TRUE : FALSE));
            if (Occupied != null) {
                Element occupied = new Element(OCCUPIED);
                Occupied.putXML(occupied);
                thisObject.addContent(occupied);
            }
            if (Unoccupied != null) {
                Element unoccupied = new Element(UNOCCUPIED);
                Unoccupied.putXML(unoccupied);
                thisObject.addContent(unoccupied);
            }
            if (TrainReporter != null) {
                Element hold = new Element(TRAINREPORTER);
                TrainReporter.putXML(hold);
                thisObject.addContent(hold);
            }

            Saved = true;
            BlockKeeper.add(this);
        }
        parent.addContent(thisObject);
        return null;
    }
    
    /**
     * registers a BlockFactory with the XMLReader.
     */
    static public void init() {
        XMLReader.registerFactory(XML_TAG, new BlockFactory());
        Detector.init(OCCUPIED);
        Detector.init(UNOCCUPIED);
        Detector.init(TRAINREPORTER);
    }    
}

/**
 * is a Class known only to the Block class for creating Blocks from
 * an XML document.
 */
class BlockFactory
implements XMLEleFactory {
    
    /**
     * the Block's name in an attribute.
     */
    private String AttrName;
    
    /**
     * the Block's Station name is an attribute.
     */
    private String AttrStation;
    
    /**
     * the Block's discipline in an attribute.
     */
    private int AttrDiscipline;
    
    /**
     * the Block's visibility in an attribute.
     */
    private boolean AttrVisible;
    
    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        AttrName = null;
        AttrStation = null;
        AttrDiscipline = -1;
        AttrVisible = true;
    }
    
    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        String resultMsg = null;
        if (Block.NAME.equals(tag)) {
            AttrName = value;
        }
        else if (Block.STATION.equals(tag)) {
            AttrStation = value;
        }
        else if (Block.DISCIPLINE.equals(tag)) {
            int disp = CtcFont.findString(value, Block.DisciplineName);
            if (disp == CtcFont.NOT_FOUND) {
                resultMsg = new String(value + " is an invalid value for a " +
                        Block.XML_TAG + " XML element " +
                        Block.DISCIPLINE + " attribute.");
            }
            else {
                AttrDiscipline = disp;
            }
        }
        else if (Block.VISIBLE.equals(tag)) {
            if (Block.TRUE.equals(value)) {
                AttrVisible = true;
            }
            else if (Block.FALSE.equals(value)) {
                AttrVisible = false;
            }
            else {
                resultMsg = new String(value + " is an invalid value for a " +
                        Block.XML_TAG + " XML element " +
                        Block.VISIBLE + " attribute.");
            }
        }
        else {
            resultMsg = new String(Block.XML_TAG + " XML elements do not have " +
                    tag + " attributes.");
        }
        return resultMsg;
    }
    
    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        Block newBlock = new Block(AttrName);
        newBlock.setStationName(AttrStation);
        newBlock.setDiscipline(AttrDiscipline);
        newBlock.setVisible(AttrVisible);
        return newBlock;
    }
}
/* @(#)Block.java */
