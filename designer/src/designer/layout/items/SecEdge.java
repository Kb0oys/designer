/* Name: SecEdge.java
 *
 * What:
 *   This class is the container for all the information about one
 *   of a Section's Edges.
 */

package designer.layout.items;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import designer.gui.CtcFont;
import designer.gui.GridTile;
import designer.layout.ColorList;
import designer.layout.EnumerationLocks;
import designer.layout.Layout;
import designer.layout.Node;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 * is a container for all the information about one of a Section's Edges.
 * <p>
 * All Edges have several pieces of information:
 *<ul>
 *<li>
 * a reference to a SecEdge, which is the link to the SecEdge this one
 * connects to.  If the track does not go anywhere (is a dead end), the
 * link is null.  If the Section is not linked into the Layout, then the
 * link is null.
 *<li>
 * if the SecEdge is the end of a detection Block, then it contains
 * information about the Block, which it shares with all other SecEdges
 * that are also in the Block and are Block boundaries.
 *<li>
 * if the SecEdge is a Block boundary and has a Signal protecting entry
 * into the Block through the edge, then the Signal information.
 *<li>
 * if multiple Tracks terminate on the SecEdge (forming switch points),
 * then the information for controlling the points.  Alternatively, the 
 * multiple tracks could form a crossing.  Each of the tracks in the crossing
 * can independently of the other be a Block boundary or not.  To keep things simple,
 * Block boundaries and switch points are mutually exclusive.
 * <p>
 * Thus, a SecEdge may be
 * <ul>
 * <li>
 * end of track (does not connect to anything)
 * <li>
 * simple connection to another track
 * <li>
 *  a Block boundary
 * <li>
 *  a set of switch Points (two or three terminations)
 * <li>
 *  a crossing
 * </ul>
 * <li>
 * a description of where to find the companion SecEdge, needed for
 * setting up the link.
 * </ul>
 * <p>
 * The design intent is that every Track has two terminations.
 * Each of those terminations is associated with a SecEdge for describing
 * where to find the connecting Track, how to set the Signals, how to
 * move the switch points, etc.  Therefore, a SecEdge may be created when a
 * Track is defined (if the SecEdge exists when the Track is defined, then
 * it is used and another is not created).  When the Section is linked into
 * the Layout, then the track link is set up.  The link binds two SecEdges
 * through references.  By using references, the geometry of the Layout
 * can be altered, leaving the connections intact.  Furthermore, connections
 * can be altered with minimal disruption to the partner.
 * <p>
 * For two SecEdges to be linked (bound),
 * they must be compatible.  The following are compatible:
 * <ol>
 *  <li>
 *      block boundary, block boundary
 *  <li>
 *      crossing, crossing
 *  <li>
 *      points, points
 *  <li>
 *      points, nothing
 *  <li>
 *      nothing, nothing
 *  </ol>
 *   When creating a SecEdge (through the GUI, a file load, an import, or a paste),
 *   there are several cases to consider:
 * <ol>
 * <li>
 * Does the neighbor exist?  If no, then it is not created.
 * <li>
 * Is the neighbor bound?  If yes, then it remains bound and this SecEdge
 * is end of track.
 * <li>
 * Is the neighbor compatible?
 * <table border="1">
 * <caption>Edge compatibility</caption>
 * <tr>
 *   <td colspan=2 rowspan=2></td>
 *   <th colspan=3 style="text-align: center">Neighbor</th>
 * </tr>
 * <tr style="text-align: center">
 *   <th>nothing</th>
 *   <th>block boundary</th>
 *   <th>points</th>
 * </tr>
 * <tr style="text-align: center">
 *   <th rowspan=3>Added</th>
 *   <th>nothing</th>
 *   <td>compatible</td>
 *   <td>added->block boundary</td>
 *   <td>compatible</td>
 * </tr>
 * <tr style="text-align: center">
 *   <th>block boundary</th>
 *   <td>neighbor->block boundary</td>
 *   <td>compatible</td>
 *   <td>added->nothing</td>
 * </tr>
 * <tr style="text-align: center">
 *   <th>points</th>
 *   <td>compatible</td>
 *   <td>neighbor->nothing</td>
 *   <td>compatible (points)</td>
 * </tr>
 * </table>
 * </ol>
 * There are also several cases to consider when removing (unbinding) a SecEdge:
 * <ul>
 * <li>
 * Is it unbound?  If yes, then there is nothing else to do.
 * <li>
 * Is it bound to its neighbor?  If yes, then its neighbor becomes unbound.
 * <li>
 * It is bound to a non-neighbor.  So, unbind the non-neighbor and attempt to
 * bind it to its neighbor.
 * </ul>
 * Finally, there is a lot of state information implied in the SecEdge with
 * regards to the track link.
 * <ol>
 * <li>
 * if the link is null, then the SecEdge is not bound to anything.
 * <li>
 * if the SecEdge is not a Block boundary, then both MyBlock and Signal
 * are null.
 * <li>
 * if the SecEdge is a Block boundary then MyBlock is not null and MyBlock
 * describes the Block.  Signal will have a value if a Signal protects
 * entry into the Block through the SecEdge.
 * <li>
 * if both SecEdges are points and they qualify as a potential crossing, then
 * PreferPts is true if they are points and false if a crossing.
 * <li>
 * if a crossing then, MyBlock and MyBlock on the peer track must both be
 * set or both be null (the crossing track is a block boundary or not).
 * </ol>
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2012, 2013, 2020, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class SecEdge
implements Itemizable, Savable {
    static final String XML_TAG = "SEC_EDGE";
    static final String EDGE = "EDGE";
    static final String SHARED = "SHARED";
    static final String X = "X";
    static final String Y = "Y";
    
    /**
     * the Edge identification of this SecEdge.
     */
    private int MyEdge;
    
    /**
     * the Section containing the SecEdge.  This field
     * is typically populated when the SecEdge is added to the Section.
     */
    private Section MySection;
    
    /**
     * a placeholder for the Signal associated with a Block boundary.
     */
    private SecSignal MySignal;
    
    /**
     * a description of the detection Block terminating on the SecEdge.
     * If MyBlock is null, then the SecEdge is not a block boundary.
     */
    private Block MyBlock;
    
    /**
     * the last Block value saved
     */
    private Block SavedBlock;
    
    /**
     * the decoder information for controlling switch points.  Both MyPoints
     * and MyCrossing could be defined.  They are kept around so that the
     * user can change his mind - until the layout is saved and loaded from
     * a file.
     */
    private SwitchPoints MyPoints;
    
    /**
     * a Crossing edge
     */
    private XEdge MyCrossing;
    
    /**
     * when both MyPoints and MyCrossing are set, this flag indicates which
     * is preferred.
     */
    private boolean PreferPts = true;
    
    /**
     * the GridTile that the SecEdge paints.
     */
    private GridTile EdgeTile;
    
    /**
     * a description of the Shared SecEdge.  The description is created by
     * either the XML reader or the Dialog.  In the case of Paste, the SecEdge
     * will be linked to null.
     * <p>
     * If this field is non-null, then the SecEdge is not linked into the
     * trackplan and DescribeEdge contains the Section coordinates and edge
     * of the adjoining SecEdge.  If this field is null, then the link
     * contains a referance to the adjoining SecEdge.
     */
    private Edge DescribeEdge;
    
    /**
     * is a copy of this SecEdge, needed during "undo" when it was changed
     * to be compatible
     */
    private SecEdge MyCopy;
    
    /**
     * the connection to the logically adjacent track.  If it is null,
     * then this track is "unbound", equivalant to "end of track".  When
     * the Section containing the SecEdge is not in the layout, the
     * DescribeEdge variable tells where the linking should go.
     */
    private SecEdge Link;
    
    /**
     * is the association between the Signal's Locks and external JMRI
     * objects that provide the API into the SecEdge's state
     */
    private EnumMap<EnumerationLocks, IOSpec> MyLocks = new EnumMap<EnumerationLocks, IOSpec>(EnumerationLocks.class);

    /**
     * a true flag means that the locks have been saved
     */
    private boolean LocksSaved = true;
    
    /**
     * a flag indicating the SecEdge has already been visited when
     * searching for Block boundaries.  This prevents infinite
     * recursion.
     */
    private boolean Visited;
    
    /**
     *   a true flag means that it has been saved,
     */
    private boolean Saved = true;
    
    /**
     * constructs a SecEdge with only its Edge identifier.  The rest of
     * the values will be read from the XML document or set by the JDialog.
     *
     * @param edge identifies the side of the GridTile the SecEdge is on.
     */
    public SecEdge(int edge) {
        MyEdge = edge;
        Visited = false;
        
    }
    
    /**
     * constructs a description of where it is located.
     *
     * @return an Edge with the, X coordinates, Y coordinates, and Edge id.
     *
     * @see Edge
     */
    public Edge toEdge() {
        return new Edge(getX(), getY(), MyEdge);
    }
    
    /**
     * finds the SecEdge that is described by the parameters.
     * <p>
     * If the Section exists and it has a SecEdge on the edge, then
     * the SecEdge is returned.
     * <p>
     * If the SecEdge or Section does not exist, then null is returned.
     * <p>
     * @param xCoord is the x coordinate of the Section.
     * @param yCoord is the y coordinate of the Section.
     * @param edge is the edge on the Section.
     *
     * @return a SecEdge in a Section (if it exists) or null.
     */
    public static SecEdge findEdge(int xCoord, int yCoord, int edge) {
        Node node = LoadFilter.instance().getLayout().locate(yCoord, xCoord);
        if (node != null) {
            Section sec = node.getContents();
            SecEdge sEdge = sec.getEdge(edge);
            return sEdge;
        }
        return null;
    }

    /**
     * is a predicate for determining if a SecEdge is linked to
     * another SecEdge
     * @return true if the SecEdge does not link to another SecEdge
     */
    public boolean isEndOfTrack() {
        return Link == null;
    }
 
    /**
     * is a predicate for verifying that a Link is bi-directional -
     * the SecEdge that this SecEdge links to has a return Link to
     * this SecEdge.
     * <p>
     * This will return false if either SecEdge is not linked into
     * the layout (DescribeEdge is not null).
     * @return true if the Link is bidirectional or does not exist;
     * false, if it is not.
     */
    private boolean isConsistent() {
        String errorMsg = new String("");
        if (Link != null) {
            if (this != Link.Link) {
                if (Link.Link != null) {
                    errorMsg = new String(" and " + Link.Link.toEdge().toString());
                }
                log.warn("Inconsistent Rail linkages at " + Link.toEdge().toString() + errorMsg);
                return false;
            }
        }
        return true;
    }
    
    /**
     * Initiates traversal of all tracks that terminate on a SecEdge.  All
     * SecEdges in the same block are collected into an ArrayList and returned.
     * The SecEdge cannot be a Crossing because the tracks may be in different Blocks.
     * 
     * @return an ArrayList containing all the BlockEdges
     */
    private ArrayList<SecEdge> getBlockEdges() {
        ArrayList<SecEdge> al = new ArrayList<SecEdge>();
        if (PreferPts || (MyCrossing == null)) {
            getTracks(al, -1);
            sweep(al);
        }
        return al;
    }
    
    /**
     * finds all Tracks that touch the SecEdge that are also in the
     * same detection Block.  It adds itself to the collecting array,
     * determines if there is a track in the adjacent Section (recursively
     * calling itself on it), and traverses the tracks that terminate
     * on the SecEdge.  A SecEdge containing a crossing is handled
     * differently because the tracks forming the crossing need not be
     * in the same Block and if they are, then there must be two calls
     * into the adjacent Section.
     * 
     * @param collect is the ArrayList where the Secedges are collected.
     * @param edge identifies the SecEdge that invoked the method.
     */
    private void getTracks(ArrayList<SecEdge> collect, int edge) {
        SecEdge other;
        int tracks = MySection.countTracks(MyEdge);
        TrackGroup group = MySection.getTrackGroup();
        BitSet myEnds; 
        int otherEdge;
        
        if (!Visited) {
            Visited = true;
            
            if ((tracks == 2) && !PreferPts && (MyCrossing != null)) {
                
                // look into the peer
                if ((edge >= 0) && (edge < Edge.EDGENAME.length)) {
                    if ((MyCrossing.getTrackEnd(edge) == XEdge.NO_BLOCK) && (Link != null)) {
                        otherEdge = XEdge.getComplement(edge);
                        Link.getTracks(collect, otherEdge);
                    }
                    
                    // look down the track
                    MySection.getEdge(edge).getTracks(collect, MyEdge);
                }
                
                Visited = false;
                
            }
            else {
                // record the SecEdge
                collect.add(this);
                
                // look into the peer
                if (!isBlock() && (Link != null)) {
                    Link.getTracks(collect, -1);
                }
                
                // look down the tracks that terminate on the edge
                if (group != null) {
                    myEnds = group.getEnds(MyEdge);
                    for (int t = myEnds.nextSetBit(0); t >= 0; t = myEnds.nextSetBit(t + 1)) {
                        if ( (edge != t) && (other = MySection.getEdge(t)) != null) {
                            other.getTracks(collect, MyEdge);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * stores the Section containing the SecEdge.
     *
     * @param sec is the Section.
     */
    public void setSection(Section sec) {
        MySection = sec;
    }
    
    /**
     * returns the enclosing Section.
     * @return the Section containing the SecEdge
     *
     * @see Section
     */
    public Section getSection() {
        return MySection;
    }
    
    /**
     * returns the X coordinate of the containing Section.
     *
     * @return the X coordinate.
     */
    public int getX() {
        return MySection.getNode().getColumn();
    }
    
    /**
     * returns the Y coordinate of the containing Section.
     *
     * @return the Row number of the Node.
     */
    public int getY() {
        return MySection.getNode().getRow();
    }
    
    /**
     * returns the Edge identifier.
     *
     * @return RIGHT, BOTTOM, ...
     */
    public int getEdge() {
        return MyEdge;
    }
    
    /**
     * determines if the SecEdge can be a Block boundary.
     *
     * @return true, if the SecEdge can be a Block boundary; otherwise,
     * false.
     */
    public boolean canBeBlock() {
        if (Link == null) {
            return true;
        }
        return (Link.MySection.countTracks(Link.MyEdge) < 2);
    }
    
    /**
     * is a predicate for determining if a SecEdge contains a Block boundary.
     * It contains a Block boundary if MyBlock is not null.  Crossings
     * are a special exception and are not a Block boundary for the purpose
     * of determining what to bind to this SecEdge.
     * @return true if it is a Block boundary.
     */
    private boolean isBlock() {
        return (MyBlock != null) || (MySignal != null);
    }
    
    /**
     * sets the MySignal.  Signals are only on Block boundaries.
     *
     * @param sig is the Signal.  It can be null.
     *
     * @see SecSignal
     */
    public void setSignal(SecSignal sig) {
        MySignal = sig;
        Saved = false;
    }
    
    /**
     * returns the Signal information.
     *
     * @return the Signal.
     *
     * @see SecSignal
     */
    public SecSignal getSignal() {
        return MySignal;
    }
    
    /**
     * clears the Visited flag on all SecEdges in a Block.
     * @param edges is an ArrayList containing all the SecEdges.
     */
    private void sweep(ArrayList<SecEdge> edges) {
        for (int i = 0; i < edges.size(); ++i) {
            edges.get(i).Visited = false;
        }
    }
    
    /**
     * sets the Block information on all Block edges in the Block.
     *
     * @param blk is the Block.  It can be null, but null Blocks are not
     * propagated to the Block boundaries in the Block.
     *
     * @see Block
     */
    public void setBlock(Block blk) {
        MyBlock = blk;
    }

    /**
     * pushes the Block definition out to all Blocks edges
     * connected to this one.  This does not work if the SecEdge
     * is an Xedge because it needs to push the Blocks for each
     * of the Tracks.
     */
    private void pushBlock() {
        Block blk = MyBlock;
        ArrayList<SecEdge> edges = getBlockEdges();
        if (blk != null) {
//            if ((blk.getDiscipline() == Block.UNDEFINED) ||
//                    (blk.getBlockName() == null) || (blk.getBlockColor().length() == 0)) {
            if ((blk.getBlockName() == null) || (blk.getBlockColor().length() == 0)) {
                blk = blkSearch(edges);
            }
        }
        else {
            blk = blkSearch(edges);
        }
        for (int i = 0; i < edges.size(); ++i) {
            if (edges.get(i).isBlock()) {
                edges.get(i).MyBlock = blk;
            }
        }
        refreshRailColor(edges);
    }

    /**
     * attempts to refresh the colors for all tracks in the Block(s)
     * that have tracks touching this SecEdge.  Normally, all tracks
     * will be in the same Block; however, it is possible for the tracks
     * in a crossing (XEdge) to be in separate blocks, in which case,
     * refreshRailColors will be called for each Block.
     *
     */
    public void refreshColors() {
        if (PreferPts || (MyCrossing == null)) {
            refreshRailColor(getBlockEdges());
        }
        else {
            for (int t = 0; t < Edge.EDGENAME.length; ++t) {
                if (MyCrossing.getTrackEnd(t) != XEdge.NO_TRACK) {
                    refreshRailColor(MySection.getEdge(t).getBlockEdges());
                }
            }
        }
    }
    
    /**
     * steps through all the SecEdges in a Block (contained
     * in edges) and checks that the Rails in each enclosing
     * Section are the correct Color.
     * <p>
     * There is a lot of redundant activity in checking each
     * SecEdge.  Checking once checks all its connections
     * within a Section.  Thus, a trail is left of all Sections
     * that have been checked.
     * @param edges is an ArrayList containing all the SecEdges
     * identifying Rails to be checked.  Typically, this will be
     * all the SecEdges in a Block.
     */
    private void refreshRailColor(ArrayList<SecEdge> edges) {
        Section sec;
        TrackGroup group;
        SecEdge anEdge;
        ArrayList<Section> visited = new ArrayList<Section>(edges.size());
        boolean beenThere;
        Block b = getBlockInfo();
        String colorKey = (b == null) ? ColorList.ERRORCOLOR : b.getBlockColor();
        for (int i = 0; i < edges.size(); ++ i) {
            anEdge = edges.get(i);
            sec = anEdge.MySection;
            if (sec != null) {
                beenThere = false;
                for (Iterator<Section> v = visited.iterator(); v.hasNext(); ){
                    if (v.next() == sec) {
                        beenThere = true;
                        break;
                    }
                }
                if (!beenThere) {
                    group = sec.getTrackGroup();
                    if (group != null) {
                        group.checkRailColor(anEdge.MyEdge, colorKey);
                    }
                    visited.add(sec);
                }
            }
        }        
    }
    
    /**
     * returns the Block information.
     *
     * @return the Block.  It will not be null.
     *
     * @see Block
     */
    public Block getBlock() {
        return MyBlock;
    }
    
    /**
     * locate the Block information for the Block containing this SecEdge.
     * It is found by looking for a SecEdge which has a Block defined.
     *
     * @return the first Block information found.  There may be none,
     * in which case one is created with default values.
     *
     * @see Block
     */
    public Block getBlockInfo() {
        Block anyInfo = blkSearch(getBlockEdges());
        if (anyInfo == null) {
            return new Block(null);
        }
        return anyInfo;
    }
    
    /**
     * searches for the definition of the Block that this SecEdge is
     * a member of.  If it contains the Block definition, then the definition
     * is returned.  If it is not a Block boundary then it searches the peer
     * SecEdge.  If that result finds nothing or only an UNDEFINED Block,
     * or if the Block definition is UNDEFINED, then it searches the Tracks
     * that terminate on the SecEdge.
     *
     *@param blkList are the SecEdges in the Block
     *@return the Block definition.  It could return null.
     */
    private Block blkSearch(ArrayList<SecEdge> edges) {
        Block anyInfo = null;
        for (int i = 0; i < edges.size(); ++i) {
            if (edges.get(i).isBlock()) {
                anyInfo = edges.get(i).MyBlock;
                if (anyInfo.getDiscipline() != Block.UNDEFINED) {
                    break;
                }
            }
        }
        return anyInfo;
    }
    
    /**
     * determines if the the SecEdge joins to a SecEdge that it is next to.
     *
     * @return true, if the Shared SecEdge is the geographic neighbor; else,
     * false.
     */
    public boolean isAdjacentPeer() {
        if (Link != null) {
            return (Link == findAdjacentPeer());
        }
        return true;
    }
    
    /**
     * returns a reference to the shared SecEdge.
     *
     * @return a reference to the SecEdge being shared or null, if it doesn't
     * exist.
     */
    public SecEdge getShared() {
        return Link;
    }
    
    /**
     * returns any SwitchPoints associated with the SecEdge.
     *
     * @return a reference to the SwitchPoints, if there are any, or null.
     *
     * @see SwitchPoints
     */
    public SwitchPoints getPoints() {
        return MyPoints;
    }
    
    /**
     * sets the SwitchPoints for the SecEdge.
     *
     * @param points is the SwitchPoints; null is valid.
     */
    public void setPoints(SwitchPoints points) {
        MyPoints = points;
        Saved = false;
    }
    
    /**
     * is called to determine if the SecEdge has a complete definition.  This only
     * applies to points (though could be used on others, if needed).  Points are
     * complete if one route is Normal.
     * 
     * @return true if points are preferred (as opposed to being a crossing),
     * MyPoints are defined, and MyPoints have a Normal route
     */
    public boolean isEdgeComplete() {
        if (getPreference()) {
            return ((MyPoints != null) && MyPoints.isComplete());
        }
        return MyCrossing != null;
    }
    
    /**
     * returns any XEdge associated with the SecEdge.
     *
     * @return a reference to the XEdge, if there is any, or null.
     *
     * @see XEdge
     */
    public XEdge getCrossing() {
        return MyCrossing;
    }
    
    /**
     * sets the XEdge for the SecEdge.
     *
     * @param x is the XEdge; null is valid.
     */
    public void setCrossing(XEdge x) {
        MyCrossing = x;
        Saved = false;
    }
    
    /**
     * when both points and crossing are defined, this indicates
     * which is preferred.
     * @return true if points are preferred.
     */
    public boolean getPreference() {
        return PreferPts;
    }
    
    /**
     * is called to remember which of points and crossing are
     * preferred.
     * @param preference is true if points are preferred.
     */ 
    public void setPreference(boolean preference) {
        PreferPts = preference;
        Saved = false;
    }
    
    /**
     * Converts from a Block boundary to a non-block boundary.
     * It saves the old Block information, for possible restoration.
     *
     */
    private void convertFromBlock() {
        saveSecEdge();
        MyBlock = null;
        MySignal = null;
    }

    /**
     * converts from a non-block boundary to a Block boundary.
     * To ensure that information for a "mirror" block is not
     * discovered, the Link is temporarily set to unbound.
     * <p>
     * isCompatible() uses MySignal to determine if a SecEdge
     * is a Block or not.  However, it looks like MyBlock
     * should work.
     */
    private void convertToBlock() {
        SecEdge oldLink = Link;
        Link = null;
        MyBlock = getBlockInfo();
        saveSecEdge();
        Link = oldLink;
    }

    /**
     * converts the XEdge on a SecEdge to Points.
     *
     */
    private void convertToPoints() {
        saveSecEdge();
        if (MyPoints == null) {
            MyPoints = new SwitchPoints();
        }
        PreferPts = true;
    }
    
    /**
     * retrieves the defined IOspecs for the locks.
     * @return the map of EnumerationLocks to IOSpec
     */
    public EnumMap<EnumerationLocks, IOSpec> getLocks() {
        return MyLocks;
    }

    /**
     * changes the map of Locks
     * @param locks the new map of locks
     */
    public void setLocks(EnumMap<EnumerationLocks, IOSpec> locks) {
        MyLocks = locks;
        LocksSaved = false;
    }

    /**
     * notes that one of the Locks has changed
     */
    public void locksChanged() {
        LocksSaved = false;
    }
    
    /**
     * finds the complementary Edge.
     *
     * @param edge is the Edge whose complement is desired.
     *
     * @return the complementary Edge.
     */
    private static int findOtherEdge(int edge) {
        if ( (edge >= 0) && (edge <= Edge.EDGENAME.length)) {
            switch (edge) {
            case Edge.RIGHT:
                edge = Edge.LEFT;
                break;
                
            case Edge.BOTTOM:
                edge = Edge.TOP;
                break;
                
            case Edge.LEFT:
                edge = Edge.RIGHT;
                break;
                
            case Edge.TOP:
                edge = Edge.BOTTOM;
                break;
                
            default:
            }
        }
        return edge;
    }

    /**
     * is called to find the SecEdge (if it exists) that touches
     * this one.
     * @return the touching SecEdge or null (if there is none)
     */
    private SecEdge findAdjacentPeer() {
        return findAdjacent(getX(), getY(), getEdge());
    }
    
    /**
     * locates the geographically adjacent edge, if it exists.
     *
     * @param x is the X coordinate of the searching SecEdge.
     * @param y is the Y coordinate of the searching SecEdge.
     * @param edge is the edge identity of the searching SecEdge.
     *
     * @return the physically touching SecEdge or null if the SecEdge is
     * on the layout perimeter.
     */
    private static SecEdge findAdjacent(int x, int y, int edge) {
        Section neighbor = findAdjacentSection(x, y, edge);
        if (neighbor != null) {
            return neighbor.getEdge(findOtherEdge(edge));
        }
        return null;
    }
    
    /**
     * attempts to locate the Section touching this edge.
     *
     * @param x is the X coordinate of the known Section
     * @param y is the Y coordinate of the known Section
     * @param edge is an edge on the known Section
     *
     * @return the neighbor Section (if there is one) or null
     */
    private static Section findAdjacentSection(int x, int y, int edge) {
        boolean valid = true;
        Dimension LayoutSize = LoadFilter.instance().getLayout().getSize();
        switch (edge) {
        case Edge.RIGHT:
            if (++x > LayoutSize.width) {
                valid = false;
            }
            break;
            
        case Edge.BOTTOM:
            if (++y > LayoutSize.height) {
                valid = false;
            }
            break;
            
        case Edge.LEFT:
            if (--x < 1) {
                valid = false;
            }
            break;
            
        case Edge.TOP:
            if (--y < 1) {
                valid = false;
            }
            break;
            
        default:
        }
        if (valid && (x > 0) && (y > 0)) {
            return LoadFilter.instance().getLayout().locate(y, x).getContents();
        }
        return null;
    }

    /*********************************************************************************
     * The next few methods are for cutting the connection between SecEdges
     *********************************************************************************/
    /**
     * breaks the relationship with whatever SecEdge it is connected to in
     * anticipation of the SecEdge being placed on the clipboard.
     */
    public void unbind() {
        if (Link != null) {
            if (isAdjacentPeer()) {
                DescribeEdge = null;
            }
            else {
                DescribeEdge = Link.toEdge();
            }
            freeUp();
        }
    }
 
    /**
     * unbinds the "other" end of a non-adjacent connection.  Since the
     * SecEdge is "freed", an attempt is made to bind it to its neighbor
     * (assuming the neighbor exists and is also unbound).
     * <p>
     * This method assumes that prior to the call, Link was referencing
     * a non-adjacent SecEdge!
     */
    private void distantUnbind() {
        MySection.hideSec();
        saveSecEdge();
        rebind();
        MySection.showSec();
    }
    
    /**
     * breaks the relationship with whatever SecEdge it is connected to.
     * Therefore, on completion, this SecEdge will be unbound.  If this
     * SecEdge is bound to a SecEdge other than its neighbor, then
     * an attempt is made to bind that SecEdge to its respective neighbor.
     * This last check is needed to prevent the binding from being
     * immediately reestablished.
     */
    public void freeUp() {
        if (Link != null) {
            Link.PreferPts = true;
            Link.MyCrossing = null;
            if (isAdjacentPeer()) {
                Link.saveSecEdge();
                Link.Link = null;
            }
            else {
                Link.distantUnbind();
            }
            saveSecEdge();
            Link.refreshColors();
            Link = null;
        }
    }
    
    /**
     * breaks the connection between this SecEdge and whatever it is
     * connected to.  If the connection was distant, it is saved in
     * DescribeEdge; thus, it is intended to be called from routines
     * that unlink regions of Sections and place them on the clipboard.
     */
    public void divorce() {
        if (Link != null) {
            Link.MySection.hideSec();
            if (isAdjacentPeer()) {
                Link.DescribeEdge = null;
                Link.Link = null;
            }
            else {
                Link.rebind();
            }
            Link.refreshColors();
            Link.MySection.showSec();
            Link = null;
        }
    }

    /**********************************************************************************
     * The following set up connections between SecEdges (tracks)
     ***********************************************************************************/
    
    /**
     * binds this SecEdge to another.  If DescribeEdge is set,
     * then DescribeEdge is the potential target; otherwise,
     * the geographical neighbor is the target.
     */
    public void bindTo() {
        bindToEdge(DescribeEdge);
    }

    /**
     * binds the SecEdge, forcing the other SecEdge to conform.  If
     * DescribeEdge is non-zero, then a remote connection is attempted.
     */
    public void rebindTo() {
        rebindToEdge(DescribeEdge);
    }
    
    /**
     * binds this SecEdge to another, when the target is specified
     * as an Edge.
     *
     * @param target is a description of the SecEdge to be linked to.
     *
     * @return true if they are compatible and false if they are not.
     */
    public boolean bindToEdge(Edge target) {
        SecEdge tEdge = null;
        if (target == null) {
            tEdge = findAdjacentPeer();
        }
        else {
            if ( (tEdge = target.toSecEdge()) == null) {
                return false;
            }
        }
        return bindToSecEdge(tEdge);
    }
    
    /**
     * binds this SecEdge to another, if they are compatible.  This does not
     * work well for Xedges.  It binds the two Xedges, but not the tracks
     * composing the Xedges; thus, blocks do not propagate.
     *
     * @param target is the SecEdge to be linked to.
     *
     * @return true if they are compatible; false if they are not.
     */
    public boolean bindToSecEdge(SecEdge target) {
        if (target == null) {
            if (Link != null) {
                freeUp();
            }
        }
        else if (Link == target) {
            if (!isConsistent()) {
                Link = null;
                target.Link = null;
                if (!bind2Edges(target)) {
                    return false;
                }
                target.Link = Link;
                target.DescribeEdge = null;
                target.refreshColors();
            }
            else {
                rebindToSecEdge(target);
            }
        }
        else if (target.Link == null) {
            freeUp();
            if (!bind2Edges(target)) {
                return false;
            }
            target.Link = this;
            target.DescribeEdge = null;
            target.pushBlock();
        }
        else {
            return false;
        }
        Link = target;
        DescribeEdge = null;
        return true;
    }
 
    /**
     * is called when either the peer could change or the characteristics of the
     * SecEdge change in such a manner as to affect the peer.
     * @param edge is the new SecEdge
     */
    public void rebindToEdge(Edge edge) {
        SecEdge peer;
        // did the peer change?  First find the peer.
        if (edge == null) {
            peer = findAdjacentPeer();
        }
        else {
            peer = edge.toSecEdge();
        }
        rebindToSecEdge(peer);
    }

    /**
     * handles matching up SecEdges when this SecEdge controls
     * coercing the other on a mismatch.
     * @param peer is the SecEdge to bind to
     */
    private void rebindToSecEdge(SecEdge peer) {
        if (peer == null) {
            divorce();
            Link = null;
            DescribeEdge = null;
        }
        else {
            peer.MySection.hideSec();
            if (Link != peer) {
                // unbind the current connection
                divorce();
                
                // unbind the new target
                peer.divorce();
                
                // just link to the new edge.  It will be adjusted shortly.
                Link = peer;
                DescribeEdge = null;
                peer.Link = this;
                peer.DescribeEdge = null;
            }
            
            // coerce the peer to match.
            if (Link != null) {
                if (MySection.countTracks(MyEdge) > 1) {
                    if (peer.isBlock()) {
                        peer.convertFromBlock();
                    }
                    else if (PreferPts) {
                        if (MyPoints == null) {
                            MyPoints = new SwitchPoints();
                        }
                        Link.PreferPts = true;
                    }
                    else if (MyCrossing != null) {
                        peer.MyCrossing = MyCrossing.makeComplement();
                        peer.PreferPts = false;
                    }
                }
                else if (isBlock()) {
                    if (peer.MySection.countTracks(findOtherEdge(MyEdge)) > 1) {
                        convertFromBlock();
                    }
                    else if (!peer.isBlock()) {
                        peer.convertToBlock();
                        peer.refreshColors();
                    }
                }
                else {
                    peer.PreferPts = true;
                    if (peer.isBlock()) {
                        peer.convertFromBlock();
                    }
                }
            }
            peer.MySection.showSec();
        }
        if (PreferPts || (MyCrossing == null)) {
            pushBlock();            
        }
        else {
            for (int t = 0; t < Edge.EDGENAME.length; ++t) {
                if (MyCrossing.getTrackEnd(t) != XEdge.NO_TRACK) {
                    MySection.getEdge(t).pushBlock();
                }
            }            
        }
    }
    
    /**
     * Attempts to bind a SecEdge to its geographic neighbor.  The
     * neighbor must exist and be unbound to succeed.
     */
    private void rebind() {
        SecEdge neighbor = findAdjacentPeer();
        if ((neighbor != null) && neighbor.isEndOfTrack()) {
            // link them so that compatibility conversion works right
            if (bind2Edges(neighbor)) {
                Link = neighbor;
                DescribeEdge = null;
                neighbor.Link = this;
                neighbor.DescribeEdge = null;
            }
        }
        else {
            Link = null;
            DescribeEdge = null;
            refreshColors();
        }
    }

    /**
     * This method implements one half of the table in the prolog.  It matches up
     * two SecEdges prior to being joined.  The method modifies only the SecEdge
     * to match the peer.  If after being run the SecEdges are still incompatible,
     * then it should be re-run with the roles reversed.  To assist in advising
     * the caller if the roles need to be reversed, it returns true if the two
     * SecEdges are compatible and false if they are not.  Thus, false implies
     * that more converting needs to be done.
     * <p>
     * The conversions that it
     * makes are:
     * <ul>
     * <li>
     * if it has neither a Block boundary or Points and the peer has a Block
     * boundary, it picks up a Block boundary
     * <li>
     * if it has a Block boundary and the peer has Points, the Block information
     * is removed
     * <li> 
     * if it has Points and the peer is a Block boundary the peer will have to
     * conform
     * <li>
     * if it has Points and the Peer has an XEdge, the XEdge will have to convert
     * because the tracks that form an XEdge must be complements.  If they are,
     * then either could convert.  For consistency for when they are not
     * complements, the XEdge will convert.
     * <li>
     * if it has an XEdge and not a matching XEdge, it will convert to Points.
     * <li>
     * if it has an XEdge, the peer has a matching XEdge, either of its tracks
     * that are not Blocks and the peer is, those tracks will get Block boundaries.
     * </ul>
     * <p>
     * Consequently, the kinds of SecEdges are:
     * <ol>
     * <li>
     * plain
     * <li>
     * Block boundary
     * <li>
     * Points
     * <li>
     * XEdge
     * </ol>
     * @param peer is the SecEdge that is to be bound to.  It should not be bound
     * at the time this analysis (and possible conversion) is made.
     * @return true if the SecEdges are compatible after all adjustments are
     * made and false if they are not.
     */
    private boolean makeCompatible(SecEdge peer) {
        if (peer != null) {
            if (isBlock()) {
                if (peer.isBlock()) {
                    return true;
                }
                else if (peer.MySection.countTracks(findOtherEdge(MyEdge)) > 1) {
                    convertFromBlock();
                    return peer.PreferPts;
                }
                else {
                    return false;
                }
            }
            else if (MySection.countTracks(MyEdge) > 1) {
                if (PreferPts) {
                    return (!peer.isBlock() && peer.PreferPts);
                }
                else if (peer.PreferPts){
                    convertToPoints();
                    return true;
                }
                else if (XEdge.isPossibleXEdge(this, peer)) {
                    return true;
                }
                else {
                    convertToPoints();
                    // do not return true!  The peer must also convert.
                    return false;
                }
            }
            else if (peer.isBlock()) {
                convertToBlock();
            }
            else if (peer.MySection.countTracks(findOtherEdge(MyEdge)) > 1) {
                return peer.PreferPts;
            }
        }
        return true;
    }

    /**
     * is called to bind two SecEdges (this and this.link) together, adjusting
     * them to be compatible.
     *
     *@param candidate is the SecEdge being linked to
     *@return true if they are successfully linked and false if not.
     */
    private boolean bind2Edges(SecEdge candidate) {
        boolean success = true;
        if (candidate != null) {
            if (!makeCompatible(candidate)) {
                candidate.MySection.hideSec();
                success = candidate.makeCompatible(this);
                candidate.MySection.showSec();
            }
        }
        return success;
    }
    
    /**
     * is a predicate for determining if the track ending on
     * the SecEdge attaches to its a non-adjacent SecEdge or not.
     * @return true if the SecEdge is bound to a non-adjacent SecEdge;
     * false if it is unbound or bound to its neighbor.
     */
    private boolean isDistantConnection() {
        return (Link != null) && !Link.isAdjacentPeer();
    }
    
    /**
     * tells the SecEdge what Section it is contained in.
     * @param sec is the Section containing the SecEdge
     */
    public void addEdge(Section sec) {
        Block blk;
        MySection = sec;
        sec.replaceEdge(MyEdge, this);
        bindToEdge(DescribeEdge);
        if (isBlock()) {
            if (MyBlock.getDiscipline() == Block.UNDEFINED) {
                blk = getBlockInfo();
            }
            else {
                blk = MyBlock;
            }
            setBlock(blk);
        }     
    }
    
    /*
     * tells the sub-component where its Section is, so that the sub-component
     * can replace itself and retrieve anything else it needs from the Section.
     */
    public void addSelf(Section sec) {
        addEdge(sec);
        SavedBlock = MyBlock;
    }
 
    /**
     * is called to capture the current state of a SecEdge and save it in
     * MyCopy.  MyCopy, then acts as the links in a stack (LIFO) of changes
     * to the SecEdge.
     */
    private void saveSecEdge() {
        SecEdge temp = new SecEdge(MyEdge);
        copyAll(this, temp);
        temp.MyCopy = MyCopy;
        MyCopy = temp;
    }
 
    /**
     * is called to restore the last saved state for a SecEdge.
     * It pops the stack.
     * <p>
     * This replaces fields in the SecEdge (rather than replacing the
     * SecEdge) because other things (e.g. the Section, the Link) have
     * references to it.
     */
//    private void restoreSecEdge() {
//        if (MyCopy != null) {
//            copyAll(MyCopy, this);
//            MyCopy = MyCopy.MyCopy;
//            Saved = false;
//        }
//    }
    
    /**
     * asks the sub-component to create a copy of itself.
     * <p>
     * What should be copied from the original?  Clearly, the edge identity
     * and the flags.  However, because connections cannot be duplicated,
     * Connection is unbound.  For the time being, Signals are duplicated.
     * The Node will be duplicated
     * because it may be used to identify the Section to add the SecEdge to.
     */
    public Itemizable copy(Section sec) {
        SecEdge newEdge = new SecEdge(MyEdge);
        copyAll(this, newEdge);
        newEdge.setSection(sec);
        if (sec != null) {
            sec.replaceEdge(MyEdge, newEdge);
        }
        newEdge.Saved = false;
        return newEdge;
    }
 
    /**
     * retrieves all SecEdges that have a Block defined
     * @return a Vector of SecEdges
     */
    public static Vector<SecEdge> getBlkEdges() {
        Vector<SecEdge> blkList = new Vector<SecEdge>();
        Section s;
        for (Enumeration<Node> e = Layout.getConstruction().elements(); e.hasMoreElements(); ) {
            s = e.nextElement().getContents();
            for (int edge = 0; edge < Edge.EDGENAME.length; edge++) {
                if ((s.getEdge(edge) != null) && (s.getEdge(edge).getBlock() != null)) {
                    blkList.add(s.getEdge(edge));
                }
            }
        }
        return blkList;        
    }
  
    /**
     * retrieves all SecEdges that have Points defined
     * @return a Vector of SecEdges
     */
    public static Vector<SecEdge> getPtsEdges() {
        Vector<SecEdge> ptsList = new Vector<SecEdge>();
        Section s;
        for (Enumeration<Node> e = Layout.getConstruction().elements(); e.hasMoreElements(); ) {
            s = e.nextElement().getContents();
            for (int edge = 0; edge < Edge.EDGENAME.length; edge++) {
                if ((s.getEdge(edge) != null) && (s.getEdge(edge).getPoints() != null)) {
                    ptsList.add(s.getEdge(edge));
                }
            }
        }
        return ptsList;        
    }
  
    /**
     * constructs a list of all SecEdges that have a physical signal
     * @return the list of all SecEdges with a physical signal
     */
    public static ArrayList<SecEdge> getSignals() {
        ArrayList<SecEdge> signals = new ArrayList<SecEdge>();
        Section s;
        SecEdge se;
        for (Enumeration<Node> e = Layout.getConstruction().elements(); e.hasMoreElements(); ) {
            s = e.nextElement().getContents();
            for (int edge = 0; edge < Edge.EDGENAME.length; edge++) {
                if (((se = s.getEdge(edge)) != null) && (se.getSignal() != null) &&
                       (se.getSignal().getSigDescription() != null) ) {
                    signals.add(se);
                }
            }
        }
        return signals;
    }
    /**
     * copies one SecEdge to another, except for MyCopy because that could result in
     * some recursion.
     * @param src is the SecEdge being copied
     * @param dest is where to copy it
     */
    private static void copyAll(SecEdge src, SecEdge dest) {
        IOSpec spec;
        dest.MySection = src.MySection;
        if (src.MySignal == null) {
            dest.MySignal = null;
        }
        else {
            dest.MySignal = src.MySignal.copy();
        }
        if (src.MyBlock == null) {
            dest.MyBlock = null;
        }
        else {
            dest.MyBlock = src.MyBlock;
        }
        if (src.MyPoints == null) {
            dest.MyPoints = null;
        }
        else {
            dest.MyPoints = src.MyPoints.copy();
        }
        if (src.MyCrossing == null) {
            dest.MyCrossing = null;
        }
        else {
            dest.MyCrossing = src.MyCrossing;
        }
        dest.PreferPts = src.PreferPts;
        if ((src.MyLocks != null) && (src.MyLocks.keySet() != null)) {
        	for (Iterator<EnumerationLocks> iter = src.MyLocks.keySet().iterator(); iter.hasNext(); ) {
        		EnumerationLocks lock = iter.next();
        		spec = (IOSpec) src.MyLocks.get(lock).copy();
        		dest.MyLocks.put(lock, spec);
        	}
        }
    }
    
    /**
     * asks the sub-component to create a shallow Copy of itself.
     * <p>
     * What should be copied from the original?  Clearly, the edge identity
     * and the flags.  However, because connections cannot be duplicated,
     * Connection is unbound.  For the time being, Signals are duplicated.
     * The Node will be duplicated
     * because it may be used to identify the Section to add the SecEdge to.
     */
    public Itemizable shallowCopy(Section sec) {
        SecEdge newEdge = new SecEdge(MyEdge);
        newEdge.setSection(sec);
        if (MySignal != null) {
            newEdge.MySignal = MySignal.shallowCopy();
        }
        if (MyBlock != null) {
            newEdge.MyBlock = MyBlock.shallowCopy();
        }
        if (MyPoints != null) {
            newEdge.MyPoints = MyPoints.shallowCopy();
        }
        if (MyCrossing != null) {
            newEdge.MyCrossing = MyCrossing;
        }
        newEdge.PreferPts = PreferPts;
        if (sec != null) {
            sec.replaceEdge(MyEdge, newEdge);
        }
        newEdge.Saved = false;
        return newEdge;
    }
    
    /**
     * registers the SecEdge's icon on the painting surface.
     *
     * @param tile is where the Frills are painted.
     *
     * @see designer.gui.GridTile
     */
    public void install(GridTile tile) {
        EdgeTile = tile;
        if (EdgeTile != null) {
            if (MySignal != null) {
                MySignal.install(EdgeTile);
            }
            if (MyBlock != null) {
                EdgeTile.setSide(MyEdge, true);
            }
            EdgeTile.setBumber(MyEdge, isDistantConnection());
        }
    }
    
    /**
     * removes the SecEdge from the screen.
     */
    public void uninstall() {
        if (EdgeTile != null) {
            if (MySignal != null) {
                MySignal.uninstall();
            }
            if (isBlock()) {
                EdgeTile.setSide(MyEdge, false);
            }
        }
    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String(XML_TAG + " XML Elements do not have Text fields ("
                + eleValue + ").");
    }
    
    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        String resultMsg = null;
        if (SecSignal.XML_TAG.equals(objName)) {
            MySignal = (SecSignal) objValue;
            
            // create a fake panel signal for an intermediate signal
            if ((MySignal.getSigDescription() != null) && (MySignal.getSigIcon() == null)) {
                MySignal.setSigIcon(new IntermediateSignal(MyEdge));
            }
        }
        else if (objName.equals(Block.XML_TAG)) {
            SavedBlock = MyBlock = (Block) objValue;
            if (MyBlock == null) {
                System.out.println("Found an empty block: " + toString());
            }
        }
        else if (objName.equals(SHARED)) {
            DescribeEdge = (Edge) objValue;
            if ((DescribeEdge.EdgeX < 1) || (DescribeEdge.EdgeY < 1)) {
                log.warn("Invalid SHARED section coordinates: (" + DescribeEdge.EdgeX + "," + DescribeEdge.EdgeY
                        + ")");
                DescribeEdge = null;
            }
        }
        else if (objName.equals(SwitchPoints.XML_TAG)) {
            MyPoints = (SwitchPoints) objValue;
        }
        else if (objName.equals(XEdge.XML_TAG)) {
            MyCrossing = (XEdge) objValue;
            PreferPts = false;
        }
        else if (SignalLock.XML_TAG.equals(objName)) {
            MyLocks.put(((SignalLock)objValue).getLock(), ((SignalLock)objValue).getSpec());
        }
        else {
            resultMsg = new String(objName + " is not recognized as an Element of "
                    + objName + " XML Elements.");
        }
        return resultMsg;
    }
    
    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }
    
    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        return null;
    }
    
    /*
     * asks if the state of the Object has been saved to a file.  This will
     * often return false, even after a file has been read in because
     * MyBlock != SavedBlock.  This happens when MyBlock (and SavedBlock)
     * are created while the file is read in, but another Block definition
     * is encountered later that replaces MyBlock.
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved && (MyBlock == SavedBlock) && LocksSaved;
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        int trks;
        SignalLock lockSpec;
        if ( (trks = MySection.countTracks(MyEdge)) > 1) {
            if ((MyPoints == null) && PreferPts) {
                MyPoints = new SwitchPoints();
                MyPoints.setSpur(true);
            }
        }
        else {
            MyPoints = null;
        }
        if (trks > 0) {
            Element thisObject = new Element(XML_TAG);
            thisObject.setAttribute(EDGE, Edge.fromEdge(MyEdge));
            if (MyBlock != null) {
                MyBlock.putXML(thisObject);
            }
            if (MySignal != null) {
                MySignal.putXML(thisObject);
            }
            if ((Link != null) && !isAdjacentPeer()&& (Link.getX() > 0) &&
                (Link.getY() > 0)) {
                  Element se = new Element(SHARED);
                  se.setAttribute(X, String.valueOf(Link.getX()));
                  se.setAttribute(Y, String.valueOf(Link.getY()));
                  se.addContent(Edge.fromEdge(Link.getEdge()));
                  thisObject.addContent(se);
                
            }
            if ((MyCrossing != null) && !PreferPts) {
                MyCrossing.putXML(thisObject);
            }
            else if (MyPoints != null){
                MyPoints.putXML(thisObject);          
            }
            if (MyLocks != null) {
                for (Iterator<EnumerationLocks> iter = MyLocks.keySet().iterator(); iter.hasNext(); ) {
                    EnumerationLocks lock = iter.next();
                    lockSpec = new SignalLock(lock, MyLocks.get(lock));
                    lockSpec.putXML(thisObject);
                }
            }
            parent.addContent(thisObject);
        }
        LocksSaved = true;
        Saved = true;
        SavedBlock = MyBlock;
        return null;
    }
    
    /**
     * registers a SecEdgeFactory with the XMLReader.
     */
    static public void init() {
        XMLReader.registerFactory(XML_TAG, new SecEdgeFactory());
        Edge.init();
        SecSignal.init();
        SwitchPoints.init();
        XEdge.init();
        Block.init();
        SignalLock.init();
    }
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
            SecEdge.class.getName());
}

/**
 * is a Class known only to the SecEdge class for creating SecEdges from
 * an XML document.  Its purpose is to pick up the values in the SecEdge.
 */
class SecEdgeFactory
implements XMLEleFactory {
    private int EdgeId;
    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        EdgeId = CtcFont.NOT_FOUND;
    }
    
    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        if (tag.equals(SecEdge.EDGE)) {
            EdgeId = Edge.toEdge(value);
            if (EdgeId == CtcFont.NOT_FOUND) {
                return new String(value + " is not a valid value for a " +
                        SecEdge.XML_TAG + " XML Attribute.");
            }
            return null;
        }
        return new String(tag + " is not a valid XML Attribute for a " +
                SecEdge.XML_TAG);
    }
    
    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        return new SecEdge(EdgeId);
    }
}
/* @(#)SecEdge.java */
