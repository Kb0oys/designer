/* Name: Track.java
 *
 * What:
 *   This class is the container for all the information about a piece of
 *   Track.
 */

package designer.layout.items;

import designer.gui.CtcFont;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 * is a container for identifying Tracks in an XML document.
 * <p>
 * Title: designer
 * </p>
 * <p>
 * Description: A program for designing dispatcher panels
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public class Track implements XMLEleObject {
	/**
	 * The XML tag for recognizing the Track description.
	 */
	public static final String XML_TAG = "TRACK";

	/**
	 * The XML attribute tag for identifying the authorized speed.
	 */
	public static final String SPEED_TAG = "SPEED";

	/**
	 * The 4 authorized speeds for a Track segment, ordered by fastest to
	 * slowest.
	 */
	public static final String[] TrackSpeed = { "Default", "Normal", "Limited",
			"Medium", "Slow"};

	/**
	 * the name of the Track's type.
	 */
	private String TrackType = null;

	/**
	 * the authorized track speed. It is an index into TrackSpeed.
	 */
	private int Speed;

	/**
	 * constructs the Track from scratch.
	 * 
	 * @param speed
	 *            is the timetable track speed.
	 */
	public Track(int speed) {
		Speed = speed;
	}

	/**
	 * returns the name of the Track's type.
	 * 
	 * @return the name of the Track's type.
	 */
	public String getTrakType() {
		return TrackType;
	}

	/**
	 * returns the prescribed track speed.
	 * 
	 * @return the authorized maximum speed over the track.
	 */
	public int getSpeed() {
		return Speed;
	}

	/*
	 * is the method through which the object receives the text field.
	 * 
	 * @param eleValue is the Text for the Element's value.
	 * 
	 * @return if the value is acceptable, then null; otherwise, an error
	 * string.
	 */
	public String setValue(String eleValue) {
	    String resultMsg = null;
	    TrackType = eleValue;
	    return resultMsg;
	}

	/*
	 * is the method through which the object receives embedded Objects.
	 * 
	 * @param objName is the name of the embedded object @param objValue is the
	 * value of the embedded object
	 * 
	 * @return null if the Object is acceptible or an error String if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		String resultMsg = new String("A " + XML_TAG
				+ " cannot contain an Element (" + objName + ").");
		return resultMsg;
	}

	/*
	 * returns the XML Element tag for the XMLEleObject.
	 * 
	 * @return the name by which XMLReader knows the XMLEleObject (the Element
	 * tag).
	 */
	public String getTag() {
		return new String(XML_TAG);
	}

	/*
	 * tells the XMLEleObject that no more setValue or setObject calls will be
	 * made; thus, it can do any error chacking that it needs.
	 * 
	 * @return null, if it has received everything it needs or an error string
	 * if something isn't correct.
	 */
	public String doneXML() {
		String resultMsg = null;
		int position = CtcFont.findString(TrackType, TrackGroup.TrackName);
		if (position == CtcFont.NOT_FOUND) {
			resultMsg = new String(TrackType
					+ " is not a valid XML text element for " + XML_TAG + ".");
			TrackType = null;
		}
		return resultMsg;
	}

	/**
	 * registers a TrackFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new TrackFactory());
	}
}

/**
 * is a Class known only to the Track class for creating Tracks from an XML
 * document.
 */

class TrackFactory implements XMLEleFactory {

	/**
	 * is the authorized speed across the track.
	 */
	private int Speed = 0;

	/*
	 * tells the factory that an XMLEleObject is to be created. Thus, its
	 * contents can be set from the information in an XML Element description.
	 */
	public void newElement() {
		Speed = 0;
	}

	/*
	 * gives the factory an initialization value for the created XMLEleObject.
	 * 
	 * @param tag is the name of the attribute. @param value is it value.
	 * 
	 * @return null if the tag:value are accepted; otherwise, an error string.
	 */
	public String addAttribute(String tag, String value) {
		String resultMsg = null;
		if (Track.SPEED_TAG.equals(tag)) {
			if ((Speed = CtcFont.findString(value, Track.TrackSpeed)) == CtcFont.NOT_FOUND) {
				Speed = 0;
				resultMsg = new String(tag
						+ " is not a recognized track speed.");
			}
		} else {
			resultMsg = new String(tag + " is not a valid XML Attribute for a "
					+ Track.XML_TAG);
		}
		return resultMsg;
	}

	/*
	 * tells the factory that the attributes have been seen; therefore, return
	 * the XMLEleObject created.
	 * 
	 * @return the newly created XMLEleObject or null (if there was a problem in
	 * creating it).
	 */
	public XMLEleObject getObject() {
		return new Track(Speed);
	}
}
/* @(#)Track.java */
