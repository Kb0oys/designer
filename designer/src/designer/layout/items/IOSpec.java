/*
 * Name: IOSpec.java
 *
 * What:
 *  This is a class for holding the information for triggering something
 *  on the railroad or identifying an event from the railroad.
 */
package designer.layout.items;

import designer.layout.AbstractListElement;
import designer.layout.JmriDevice;
import designer.layout.JmriName;
import designer.layout.xml.*;

import java.io.PrintStream;

import org.jdom2.Element;

/**
 * is a class for holding the information needed to trigger something
 * on the railroad or identifying an event from the railroad.  Currently,
 * the only things triggered or sensed are throwing and closing switches.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2012, 2013, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class IOSpec
    extends AbstractListElement {

  /**
   * is the tag for identifying an IOSpec in the XML file.
   */
  static final String XML_TAG = "IOSPEC";

  /**
   * is the attribute tag for identifying the decoder address.
   */
  static final String DECADDR = "DECADDR";

  /**
   * is the attribute tag for identifying the DCC message type.
   */
  static final String JMRIPREFIX = "JMRIPREFIX";

  /**
   * is the attribute tag for identifying the JMRI device type
   */
  static final String JMRIDEVICE = "JMRIDEVICE";
  
  /**
   * is the text string for a Throw command.
   */
  static final String THROW_TEXT = "throw";

  /**
   * is the text string for a Close command.
   */
  static final String CLOSE_TEXT = "close";

  /**
   * is the XML tag for explicitly sending a command when exiting
   * a state.
   */
  public static final String EXIT_CMD = "EXIT_CMD";

  /**
   * is the XML tag for the UserName attribute.
   */
  public static final String USER_NAME = "USER_NAME";

  /**
   * is the XML tag for the delay between commands.
   */
  public static final String DELAY = "DELAY";
  
  /**
   * is the kind of trigger or event.  True is a "throw" and false is
   * a "close".
   */
  protected boolean ThrowCmd;

  /**
   * is the address to send the trigger to.
   */
  protected String DecAddr;

  /**
   * is the JMRI prefix name used for finding the specific decoder.
   */
  protected String JmriPrefix;

  /**
   * is the user name of the list.  This is equivalent to the JMRI
   * username.
   */
  protected String UserName;

  /**
   * is the JMRI device type associated with the IOSpec
   */
  protected JmriDevice MyDevice;
  
  /**
   * is how long to delay after sending the previous command before
   * sending this one.  The delay in in milliseconds.
   */
  protected int Delay;
  
  /**
   * a true flag means CATS should send the opposite command when
   * changing out of the state.
   * <p>
   * Some devices (such as SE8C) have commands for individual states
   * (e.g. green light on), so sending a command to put it in a
   * different state implicitly turns of the current state.  Other
   * devices (e.g. those that control individual lights) require that
   * the current light be turned off explicitly.
   */
  protected boolean TurnOff;

  /**
   * is the String where the text value is constructed.
   */
  private String TextValue;

  /**
   * the constructor for the XML reader.
   *
   * @param address is the address the command should be sent to.
   */
  public IOSpec(String address) {
	  super();
	  DecAddr = address;
  }

  /**
   * the no argument constructor for the IOSpecChain GUI
   */
  public IOSpec() {
	  TurnOff = false;
	  Saved = false;
	  Delay = 0;
  }
  
  /**
   * is a derived class specific method for retrieving the "name" field
   * from the data structure.
   * @return a String which identifies the data structure to the rest of the world
   */
  public String getElementName() {
      return getUserName();
  }

  /**
   * saves the decoder command.
   *
   * @param throwCmd is the command.  true for a "throw" command and false
   * for a "close" command.
   */
  public void setCommand(boolean throwCmd) {
    ThrowCmd = throwCmd;
  }

  /**
   * retrieves the command.
   *
   * @return true is the command is "throw" and false if it is "close".
   */
  public boolean getCommand() {
    return ThrowCmd;
  }

  /**
   * retrieves the address of the decoder.
   *
   * @return the address.
   */
  public String getAddress() {
    return DecAddr;
  }

  /**
   * stores the address of the decoder.
   * 
   * @param addr is the address.
   */
  public void setAddress(String addr) {
      DecAddr = addr;
  }
  /**
   * sets the flag indicating that a command should be sent when
   * exiting the state.
   *
   * @param exitFlag is the value of the flag.
   */
  public void setExitCommand(boolean exitFlag) {
    TurnOff = exitFlag;
  }

  /**
   * retrieves the flag indicating that a command should be sent
   * when exiting the state.
   *
   * @return true if the complement to ThrowCmd should be sent when
   * exiting the state; false if no command should be sent.
   */
  public boolean getExitcommand() {
    return TurnOff;
  }

  /**
   * saves the JMRI prefix name.
   *
   * @param prefix identifies the kind of decoder being defined.
   * It can be null.
   */
  public void setPrefix(String prefix) {
    JmriPrefix = prefix;
  }

  /**
   * retrieves the JMRI prefix.
   *
   * @return the kind of decoder.  It can be null.
   */
  public String getPrefix() {
    return JmriPrefix;
  }

  /**
   * replaces the UserName field.
   * @param name is the new name.  It can be null or empty.
   */
  public void setUserName(String name) {
      if (name != null) {
          UserName = new String(name);
      }
      else {
          UserName = null;
      }
  }
  
  /**
   * retrieves the UserName field
   * @return UserName.  It can be null or empty.
   */
  public String getUserName() {
      if (UserName != null) {
          return new String(UserName);
      }
      return null;
  }

  /**
   * sets the JMRI device type field
   * @param d is the JMRI device
   */
  public void setDeviceType(JmriDevice d) {
	  MyDevice = d;
  }
  
  /**
   * retrieves the JMRI device type.
   * @return JMRI device type.  It can be null.
   */
  public JmriDevice getDeviceType() {
	  return MyDevice;
  }
  
  /**
   * replaces the Delay value.
   * 
   * @param d is the new delay in milliseconds.
   */
  public void setDelay(int d) {
      Delay = d;
  }
  
  /**
   * retrieves the Delay value.
   * 
   * @return the delay in milliseconds after the previous command
   *  in a chain.
   */
  public int getDelay() {
      return Delay;
  }
 
  /**
   * adds an IOSpec to the list of decoders.
   */
  public void commit() {
      if ((JmriPrefix != null) && (JmriPrefix.length() > 0)
              && !JmriPrefix.equals("none")) {
          if (JmriName.AllDecoders.contains(this)) {
              JmriName.AllDecoders.add(this);
          }
      }
  }
  
  /**
   * removes an IOSpec from the list of decoders.
   */
  public void erase() {
      if ((JmriPrefix != null) && (JmriPrefix.length() > 0)
              && !JmriPrefix.equals("none")) {
          if (JmriName.AllDecoders.contains(this)) {
              JmriName.AllDecoders.removeElement(this);
          }
      }     
  }

  /**
   * is a predicate for determining if the address field has been filled in
   * @return true if it has and false if not
   */
  protected boolean isValidAddress() {
	  return ((DecAddr != null) && !DecAddr.trim().isEmpty());
  }
  
  /**
   * is a predicate for determining if the username field has been filled in
   * @return true if it has and false if not
   */
  protected boolean isValidUserName() {
	  return ((UserName != null) && !UserName.trim().isEmpty());
  }
  
  /**
   * this method checks that enough information has been filled into the IOSpec
   * so that CATS can locate a device.  The required information is:
   * <ul>
   * <li>a prefix and either an address or a username</li>
   * <li>or a JMRI device and username</li>
   * </ul>
   * @return true if it is complete
   */
  public boolean isComplete() {
	  // first, test the case that neither a System Prefix or Device Type is defined.  Though no decoder is specified,
	  // this will be treated as an OK way to end the test.
	  if (((JmriPrefix == null) || JmriPrefix.equals("none")) && ((MyDevice == null) || (MyDevice == JmriDevice.none))) {
		  return true;
	  }
	  // next test for a valid prefix and either an address or username
	  if (JmriName.isValidPrefix(JmriPrefix) && !JmriPrefix.equals("none") && (isValidAddress() || isValidUserName())) {
		  return true;
	  }
	  if ((MyDevice != null) && (MyDevice != JmriDevice.none) && isValidUserName()) {
		  return true;
	  }
	  return false;
//      if (((JmriPrefix == null)  || JmriPrefix.trim().isEmpty() || JmriPrefix.equals("none") ||
//              !JmriName.isValidPrefix(JmriPrefix)) && (MyDevice == null)) {
//          return false;
//      }
//      if (((DecAddr == null) || (DecAddr.trim().isEmpty())) &&
//              ((UserName == null) || UserName.trim().isEmpty())) {
//          return false;
//      }
//      return true;
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
      TextValue = new String(eleValue);
      return null;
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    return new String("A " + XML_TAG + " cannot contain an Element ("
                      + objName + ").");
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return XML_TAG;
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    String resultMsg = null;
    if (TextValue != null) {
      if (THROW_TEXT.equals(TextValue)) {
        ThrowCmd = true;
      }
      else if (CLOSE_TEXT.equals(TextValue)) {
        ThrowCmd = false;
      }
      else {
        resultMsg = new String("A " + XML_TAG +
                               " cannot contain a text field ("
                               + TextValue + ").");
      }
    }
    else {
      resultMsg = new String("A " + XML_TAG + " needs a text field.");
    }
    if ((DECADDR == null) || (DECADDR.equals(""))) {
        resultMsg = new String("A " + XML_TAG + " its missing is address.");
    }
    JmriName.registerPrefix(this);
    Saved = true;
    return resultMsg;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    return Saved;
  }

  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    Element thisObject = new Element(XML_TAG);
    String command;

    if (ThrowCmd) {
      command = new String(THROW_TEXT);
    }
    else {
      command = new String(CLOSE_TEXT);
    }
    if (DecAddr != null) {
    	thisObject.setAttribute(DECADDR, DecAddr);
    }
    if ((JmriPrefix != null) && !JmriPrefix.equals("none")) {
      thisObject.setAttribute(JMRIPREFIX, JmriPrefix);
    }
    if ((MyDevice != null) && (MyDevice != JmriDevice.none)) {
    	thisObject.setAttribute(JMRIDEVICE, MyDevice.name());
    }
    if (TurnOff) {
      thisObject.setAttribute(EXIT_CMD, Block.TRUE);
    }
    if ((UserName != null) && (UserName.length() > 0)) {
        thisObject.setAttribute(USER_NAME, UserName);
    }
    if (Delay != 0) {
        thisObject.setAttribute(DELAY, String.valueOf(Delay));
    }
    thisObject.addContent(command);
    parent.addContent(thisObject);
    Saved = true;
    return null;
  }

  /**
   * tells the Object where to write its state.
   *
   * @param outStream is where to write it
   * @param indent is the indentation level prior to the call
   *
   * @return null if there was no problem writing or an error string
   * if there was.
   */
  public String putXML(PrintStream outStream, String indent) {
    String myIndent = new String(indent + XMLReader.INDENT);
    String command;
    String msgType;
    String exitStr;
    String usrName;
    String deviceName;
    String delay;

    if (ThrowCmd) {
      command = new String(THROW_TEXT);
    }
    else {
      command = new String(CLOSE_TEXT);
    }

    if (TurnOff) {
      exitStr = new String(" " + EXIT_CMD + "=\"" + Block.TRUE + "\" ");
    }
    else {
      exitStr = new String("");
    }
    if (JmriPrefix != null) {
      msgType = " " + JMRIPREFIX + "=\"" + JmriPrefix + "\"";
    }
    else {
      msgType = "";
    }
    if ((UserName != null) && (UserName.length() > 0)) {
        usrName = " " + USER_NAME + "=\"" + UserName + "\"";
    }
    else {
        usrName = "";
    }
    if ((MyDevice != null) && (MyDevice != JmriDevice.none)) {
    	deviceName = " " + JMRIDEVICE + "=\"" + MyDevice + "\"";
    }
    else {
    	deviceName = "";
    }
    if (Delay != 0) {
        delay = " " + DELAY + "=\"" + Delay + "\"";
    }
    else {
        delay = "";
    }
   outStream.println(myIndent + "<" + XML_TAG + " "
                      + DECADDR + "=\"" + DecAddr + "\"" + msgType +
                     exitStr + usrName + deviceName + delay + ">");
    outStream.println(myIndent + XMLReader.INDENT + command);
    outStream.println(myIndent + "</" + XML_TAG + ">");
    Saved = true;
    return null;
  }

  /**
   * makes a copy of itself
   * @return the copy.
   */
  public AbstractListElement copy() {
	  IOSpec newCopy = new IOSpec();
	  copyContents(newCopy);
	  return newCopy;
  }
  
  /**
   * copies the content of this IOSpec into another IOSpec
   * @param is the destination of the copy
   * @return an identical copy of itself.
   */
  protected void copyContents(IOSpec newCopy) {
	  newCopy.DecAddr = DecAddr;
	  newCopy.ThrowCmd = ThrowCmd;
	  newCopy.JmriPrefix = JmriPrefix;
	  newCopy.TurnOff = TurnOff;
	  if (UserName != null) {
		  newCopy.UserName = new String(UserName);
	  }
	  newCopy.Delay = Delay;
	  newCopy.MyDevice = MyDevice;
	  newCopy.Saved = false;
  }
}

/**
 * is a Class known only to the IOSpec class for creating IOSpecs from
 * an XML document.
 */
class IOSpecFactory
    implements XMLEleFactory {

  /**
   * is the address to send the command to.
   */
  String Recipient;

  /**
   * is the JMRI name prefix
   */
  String Prefix;

  /**
   * is the (optional) JMRI Device type
   */
  JmriDevice Device;
  
  /**
   * is a flag for holding the exit command.
   */
  boolean ExitFlag;

  /**
   * is the User Name.
   */
  String UserName;

  /**
   * is the Delay field.
   */
  int Delay;
  
  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    Recipient = "";
    ExitFlag = false;
    Prefix = null;
    UserName = null;
    Device = null;
    Delay = 0;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    if (IOSpec.DECADDR.equals(tag)) {
        Recipient = value;
    }
    else if (IOSpec.JMRIPREFIX.equals(tag)) {
      Prefix = value;
    }
    else if (IOSpec.JMRIDEVICE.equals(tag)) {
    	Device = JmriDevice.valueOf(value);
    }
    else if (IOSpec.EXIT_CMD.equals(tag)) {
      if (Block.TRUE.equals(value)) {
        ExitFlag = true;
      }
    }
    else if (IOSpec.DELAY.equals(tag)) {
        try {
            int addr = Integer.parseInt(value);
            Delay = addr;
          }
          catch (NumberFormatException nfe) {
            System.out.println("Illegal delay for " + IOSpec.XML_TAG
                               + "XML Element:" + value);
          }        
    }
    else if (IOSpec.USER_NAME.equals(tag)) {
        UserName = value;
    }
    else {
      resultMsg = new String("A " + IOSpec.XML_TAG +
                             " XML Element cannot have a " + tag +
                             " attribute.");
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    IOSpec spec = new IOSpec(Recipient);
    if ((Prefix != null) && !Prefix.equals("none")) {
      spec.setPrefix(Prefix);
    }
    spec.setUserName(UserName);
    if (Device != null) {
    	spec.setDeviceType(Device);
    }
    spec.setExitCommand(ExitFlag);
    spec.setDelay(Delay);
    return spec;
  }
}
/* @(#)IOSpec.java */
