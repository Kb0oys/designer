/* Name: RouteInfo.java
 *
 * What;
 *  RouteInfo is a passive data structure for holding the information needed
 *  for aligning turnout points to a particular route.
 */
package designer.layout.items;

import designer.gui.CtcFont;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

import org.jdom2.Element;

/**
 * is a passive data structure for holding the information needed for
 * aligning turnouts to a particular route.   The data structure is:
 * <ul>
 * <li>
 *   the request from a manual switch on the fascia for selecting the route.
 * <li>
 *   the command list, sent to the stationary decoder, to select the route.
 * <li>
 *   the report from a sensor at the turnout, indicating that the turnout
 *   has been aligned for the route.
 * <li>
 *   the report from a sensor at the turnout, indicating that the turnout
 *   has not been aligned for the route.
 * </ul>
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class RouteInfo
    implements XMLEleObject, Savable {

  /**
   * is the tag for identifying a SwitchPoints Object in the XMl file.
   */
  static final String XML_TAG = "ROUTEINFO";

  /**
   * is the attribute tag for identifying the route, by its other end.
   */
  static final String ROUTE_ID = "ROUTEID";

  /**
   * is the tag for identifying the Normal attribute.
   */
  static final String NORMAL_TAG = "NORMAL";

  /**
   * is the String for confirming that the route is the Normal route.
   */
  static final String ISNORMAL = "true";

  /**
   * is a table of labels and XML tags for the decoder reports.
   */
  public static final String[][] RouteName = {
      {
      "Select Route Request", "ROUTEREQUEST"}
      , {
      "Route Selected Report", "SELECTEDREPORT"}
      , {
      "Route Unselected Report", "NOTSELECTEDREPORT"}
  };

  /**
   * is the association between a label and XML tag for the
   * command for moving the switch points.
   */
  public static final String[] RouteCommand = {
      "Select Route Command", "ROUTECOMMAND"
  };

  /**
   * is an index into the above for selecting a label.
   */
  public static final int LAB_INDEX = 0;

  /**
   * is an index into the above for selecting an XML tag.
   */
  public static final int TAG_INDEX = 1;

  /**
   * are the decoder reports.
   */
  private Detector[] RouteSpecs = new Detector[RouteName.length];

  /**
   * is the decoder command.
   */
  private IOSpec RouteCmd;

  /**
   * the SecEdge where the other end of the route terminates.
   */
  private int Destination;

  /**
   * a true flag meaning that this route is the normal or through route.
   * Only one route in the SwitchPoints can be the normal route.
   */
  private boolean NormalRoute = false;

  /**
   *  a true flag means that it has been saved,
   */
  private boolean Saved = true;

  /**
   * the constructor.
   *
   * @param dest is the name of the edge that the other end terminates on.
   */
  public RouteInfo(int dest) {
    Destination = dest;
  }

  /**
   * returns the Route identity.
   *
   * @return the Route identity.  It will be a valid integer between
   * 0 and Edge.EDGENAME.length.
   */
  public int getRouteId() {
    return Destination;
  }

  /**
   * sets a decoder report
   *
   * @param report is a decoder report.  It can be null.
   * @param id is the index of the report in RouteInfo.  If it refers
   * to a non-existent report, nothing happens.
   */
  public void setRouteReport(IOSpec report, int id) {
    if ( (id >= 0) && (id < RouteName.length)) {
      if (report == null) {
        RouteSpecs[id] = null;
      }
      else {
        if (RouteSpecs[id] == null) {
          RouteSpecs[id] = new Detector(RouteName[id][TAG_INDEX]);
        }
        // setSpec copies report !!!!
        RouteSpecs[id].setSpec(report);
      }
      Saved = false;
    }
  }

  /**
   * returns one of the decoder report lists.
   *
   * @param id is the index of the report requested.  If it refers to
   * a non-existent list, nothing happens.
   *
   * @return the list.  It can be null.
   */
  public IOSpec getRouteReport(int id) {
    if ( (id >= 0) && (id < RouteName.length) && (RouteSpecs[id] != null)) {
      return RouteSpecs[id].getSpec();
    }
    return null;
  }

  /**
   * sets the decoder command to move the points to this route.
   *
   * @param cmd is the list of commands.
   */
  public void setRouteCmd(IOSpec cmd) {
      RouteCmd = cmd;
    Saved = false;
  }

  /**
   * returns the list of commands for moving the points.
   *
   * @return the list of commands.
   */
  public IOSpec getRouteCmd() {
    return RouteCmd;
  }

  /**
   * changes the NormalRoute flag.
   *
   * @param normal is true if the route is the normal route or false if
   * it is a diverging route through the turnout.
   */
  public void setNormal(boolean normal) {
    NormalRoute = normal;
  }

  /**
   * retrieves the NormalRoute flag.
   *
   * @return true if the route is the normal route through the turnout or
   * false if it is a diverging route.
   */
  public boolean getNormal() {
    return NormalRoute;
  }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    int field;
    for (field = 0; field < RouteName.length; ++field) {
      if (RouteName[field][TAG_INDEX].equals(objName)) {
        RouteSpecs[field] = (Detector) objValue;
        break;
      }
    }
    if (field == RouteName.length) {
      if (RouteCommand[TAG_INDEX].equals(objName)) {
        RouteCmd = ((Detector) objValue).getSpec();
      }
      else {
        resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                               + objName + ").");
      }
    }
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    Saved = true;
    return null;
  }

  /*
   * asks if the state of the Object has been saved to a file
   *
   * @return true if it has been saved; otherwise return false if it should
   * be written.
   */
  public boolean isSaved() {
    return Saved;
  }

    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
      int io;

      for (io = 0; io < RouteSpecs.length; ++io) {
        if (RouteSpecs[io] != null) {
          break;
        }
      }
      if ( (io < RouteSpecs.length) || NormalRoute || (RouteCmd != null)) {
        Element thisObject = new Element(XML_TAG);
        thisObject.setAttribute(ROUTE_ID, Edge.fromEdge(Destination));
        if (NormalRoute) {
          thisObject.setAttribute(NORMAL_TAG, ISNORMAL);
        }

        for (io = 0; io < RouteSpecs.length; ++io) {
          if (RouteSpecs[io] != null) {
            RouteSpecs[io].putXML(thisObject);
          }
        }
        if (RouteCmd != null) {
            Element rt = new Element(RouteCommand[TAG_INDEX]);
            RouteCmd.putXML(rt);
            thisObject.addContent(rt);
        }
        parent.addContent(thisObject);
      }
      Saved = true;
      return null;
    }

  /**
   * makes a copy of itself.
   *
   * @return the copy.
   */
    public RouteInfo copy() {
        RouteInfo newRoute = new RouteInfo(Destination);
        IOSpec io;
        newRoute.NormalRoute = NormalRoute;
        if (RouteCmd == null) {
            newRoute.RouteCmd = null;
        }
        else {
            newRoute.RouteCmd = (IOSpec) RouteCmd.copy();
        }
        for (int i = 0; i < RouteSpecs.length; ++i) {
            if (RouteSpecs[i] == null) {
                newRoute.RouteSpecs[i] = null;
            }
            else {
                io = RouteSpecs[i].getSpec();
                if (io == null) {
                    newRoute.RouteSpecs[i] = null;
                }
                else {
                    if (newRoute.RouteSpecs[i] == null) {
                        newRoute.RouteSpecs[i] = new Detector(RouteSpecs[i].getTag());
                    }
                    newRoute.RouteSpecs[i].setSpec(io);
                }
            }
        }
        newRoute.Saved = false;
        return newRoute;
    }

  /**
   * makes a shallow copy of itself.  A shallow copy does not include the decoder
   * information.
   *
   * @return the copy.
   */
  public RouteInfo shallowCopy() {
    RouteInfo newRoute = new RouteInfo(Destination);
    newRoute.NormalRoute = NormalRoute;
    newRoute.Saved = false;
    return newRoute;
  }

  /**
   * registers a RouteInfoFactory and the constituent decoders
   * with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new RouteInfoFactory());
    for (int vec = 0; vec < RouteName.length; ++vec) {
      Detector.init(RouteName[vec][TAG_INDEX]);
    }
    Detector.init(RouteCommand[TAG_INDEX]);
  }
}

/**
 * is a Class known only to the RouteInfo class for creating Routes
 * from an XML document.
 */
class RouteInfoFactory
    implements XMLEleFactory {

  /**
   * the index of the edge on which the route terminates.
   */
  int Term;

  /**
   * is true if the normal route attribute is seen.
   */
  boolean Normal;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    Term = -1;
    Normal = false;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    int edge;

    if (RouteInfo.ROUTE_ID.equals(tag)) {
      if ( (edge = Edge.toEdge(value)) != CtcFont.NOT_FOUND) {
        Term = edge;
      }
      else {
        resultMsg = new String(value + " is not a valid value for a " +
                               tag + " attribute.");
      }
    }
    else if (RouteInfo.NORMAL_TAG.equals(tag) &&
             RouteInfo.ISNORMAL.equals(value)) {
      Normal = true;
    }
    else {
      resultMsg = new String("A " + RouteInfo.XML_TAG +
                             " XML Element cannot have a " + tag +
                             " attribute.");
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    RouteInfo route;
    if ( (Term >= 0) && (Term < Edge.EDGENAME.length)) {
      route = new RouteInfo(Term);
      route.setNormal(Normal);
      return route;
    }
    System.out.println("Missing edge attribute for a " + RouteInfo.XML_TAG
                       + " XML element.");
    return null;
  }
}
/* @(#)RouteInfo.java */
