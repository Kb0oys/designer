/*
 * Name: IOSpecChainManager
 *
 * What:
 *  This class provides a repository for holding all the IOSpecChains.
 */
package designer.layout.items;

import designer.gui.jCustom.AbstractManagerTableModel;
import designer.gui.jCustom.ChainManagerModel;
import designer.layout.AbstractListManager;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLReader;

/**
 * Holds all the IOSpecLists.
 * It is roughly based on the JMRI managers.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2006, 2009</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class IOSpecChainManager extends AbstractListManager
implements Savable {
    
    /**
     * the singleton, which is known by all clients.
     */
    public static IOSpecChainManager IOSpecChainKeeper;
    
    /**
     * the constructor.
     */
    public IOSpecChainManager() {
        super();
        IOSpecChainKeeper = this;
    }

    /**
     * generates a copy of the Chains, then uses it to
     * create an AbstractTableModel for editing.
     * @return the AbstractTableModel
     */
    public AbstractManagerTableModel createModel() {
        return new ChainManagerModel(makeCopy());
    }

    public void registerFactory() {
        XMLReader.registerFactory(IOSpecChain.XML_TAG, new IOSpecChainFactory());
        XMLReader.registerFactory(IOSpec.XML_TAG, new IOSpecFactory());
        XMLReader.registerFactory(ReporterSpec.XML_TAG, new ReporterSpecFactory());
    }
}
/* @(#)IOSpecChainManager.java */