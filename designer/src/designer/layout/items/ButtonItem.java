/* Name: ButtonItem.java
 *
 * What:
 * ButtonItem defines a class for placing a button on a CATS panel.  It
 * has the following components:
 * <ol>
 * <li>an IOSpec triggered by "pushing" the button or being notified of it being pushed</li>
 * <li>the image to display when the button is in the primary state</li>
 * <li>the image to display when the button is in the alternate state</li>
 * <li>a timer for implementing a momentary push button</li>
 * </ol>
 */
package designer.layout.items;

import java.io.File;
import org.jdom2.Element;

import designer.gui.GridTile;
import designer.gui.frills.ButtonFrill;
import designer.layout.xml.Savable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 * ButtonItem defines a class for placing a button on a CATS panel.  It
 * has the following components:
 * <ol>
 * <li>an IOSpec triggered by "pushing" the button or being notified of it being pushed</li>
 * <li>the image to display when the button is in the primary state</li>
 * <li>the image to display when the button is in the alternate state</li>
 * <li>a timer for implementing a momentary push button</li>
 * </ol>
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ButtonItem implements Itemizable, Savable {

	/**
	 * The XML tag for identifying a ButtonItem definition.
	 */
	static final String XML_TAG = "BUTTON";

	/**
	 * the XML tag for the trigger
	 */
	static final String XML_TRIGGER = "TRIGGER";

	/**
	 * the XML tag for the location of the primary image
	 */
	static final String XML_PRIMARY = "PRIMARY";

	/**
	 * the XML tag for the location of the alternate image
	 */
	static final String XML_ALTERNATE = "ALTERNATE";

	/**
	 * the XML tag for the delay
	 */
	static final String XML_DELAY = "DELAY";
	
	/**
	 * the XML tag for the display only (do not accept keystrokes) flag
	 */
	static final String XML_STATUS = "STATUS";
	
	/**
	 * the XML tag for scaling mages to fit in a Grid tile
	 */
	static final String XML_FIT = "FIT_TO_GRID";
	
	/**
	 * the maximum delay in seconds
	 */
	public static final int MAXIMUM_DELAY = 5;

	/**
	 * The ImageFrill for painting the current Image.
	 */
	private ButtonFrill MyFrill;

	/**
	 * The Section containing the ImageItem.
	 */
	private Section ButtonSection;

	/**
	 * The GridTile where the Image is painted.
	 */
	private GridTile ButtonTile;

	/**
	 * the IOSpec triggered/received
	 */
	private IOSpec Trigger;

	/**
	 * the image displayed when the primary IOSpec is selected
	 */
	private String PrimaryImage;

	/**
	 * the image displayed when the alternate IOSpec is selected
	 */
	private String AlternateImage;

	/**
	 * the number of seconds to wait after selecting the alternate before flipping
	 * back to the primary.  A value of 0 creates a latching button.  A non-zero
	 * value creates a momentary button (where the delay is the value in seconds)
	 */
	private int MomentaryDelay;
	
	/**
	 * a flag indicating if the button can accept keystrokes or not.  When true, the
	 * button will not accept keystrokes.  It will only change images in response
	 * to events received from the layout; thus, is used for showing status.
	 */
	private boolean StatusOnly;

	/**
	 * a flag indicating if the image should be scaled to the size of a Grid Tile.  True
	 * scales it and false leaves it at its native size.
	 */
	private boolean FitToGrid;
	
	/**
	 *  a true flag means that it has been saved,
	 */
	private boolean Saved = true;

	/**
	 * the ctor
	 */
	public ButtonItem() {
		MyFrill = null;
		ButtonSection = null;
		ButtonTile = null;
		Trigger = null;
		PrimaryImage = null;
		AlternateImage = null;
		MomentaryDelay = 0;
		StatusOnly = false;
		FitToGrid = false;
		Saved = false;
	}

	/********************** getters and putters ********************/

	/**
	 * get the IOSPec
	 * @return the new IOSpec - it could be null
	 */
	public IOSpec getTrigger() {
		return Trigger;
	}

	/**
	 * set the IOSpec
	 * @param trigger is the new IOSpec - it could be null
	 */
	public void setTrigger(final IOSpec trigger) {
		Saved &= (trigger == Trigger);
		Trigger = trigger;
	}

	/**
	 * get the name of the file containing the primary image
	 * @return the filename.  It could be null.
	 */
	public String getPrimaryImage() {
		return PrimaryImage;
	}

	/**
	 * set the filename of the primary image
	 * @param primary is the filename - it could be null
	 */
	public void setPrimaryImage(final String primary) {
		if ((primary == null) || (primary.trim() == "")) {
			if (ButtonSection != null) {
				ButtonSection.replaceButton(null);
			}
		}
		Saved &= (primary == PrimaryImage);
		PrimaryImage = primary;
	}

	/**
	 * get the name of the file containing the alternate image
	 * @return the filename.  It could be null.
	 */
	public String getAlternateImage() {
		return AlternateImage;
	}

	/**
	 * set the filename of the alternate image
	 * @param alternate is the filename - it could be null
	 */
	public void setAlternateImage(final String alternate) {
		Saved &= (alternate == AlternateImage);
		AlternateImage = alternate;
	}

	/**
	 * gets the value of the delay after pushing the button while showing the primary
	 * image.
	 * @return the delay in seconds between 0 (latching) and MAXIMUM_DELAY
	 */
	public int getMomentaryDelay() {
		return MomentaryDelay;
	}

	/**
	 * sets the momentary delay, in seconds.  It must be between 0 and MAXIMUM_DELAY,
	 * inclusively.
	 * @param delay is the new value of the delay
	 */
	public void setMomentaryDelay(final int delay) {
		if ((delay >= 0) && (delay <= Integer.MAX_VALUE)) {
			Saved &= (delay == MomentaryDelay);
			MomentaryDelay = delay;
		}
	}

	/**
	 * gets the value of the status only flag
	 * @return true if the button changes only due to layout events; false, if it
	 * also toggles on keyboard events.
	 */
	public boolean getStatusOnly() {
		return StatusOnly;
	}
	
	/**
	 * sets the value of the status only flag
	 * @param status is true if the button ignores keystrokes and false if it accepts them
	 */
	public void setStatusOnly(final boolean status) {
		Saved &= (status == StatusOnly);
		StatusOnly = status;
	}
	
	/**
	 * gets the value of the FitToGrid flag.
	 * @return true if the images should be scaled to fit in a Grid Tile
	 * and false to retain their native size
	 */
	public boolean getFitToGrid() {
		return FitToGrid;
	}

	/**
	 * sets the FitToGrid flag
	 * @param fit is true to scale an image to fit in a Grid Tile; false leaves the 
	 * image at its native size.
	 */
	public void setFitToGrid(final boolean fit) {
		Saved &= (fit == FitToGrid);
		FitToGrid = fit;
	}
	
	@Override
	public String setValue(String eleValue) {
		return null;
	}

	@Override
	public String setObject(String objName, Object objValue) {
		String resultMsg = null;
		if (XML_TRIGGER.equals(objName)) {
			Trigger = ((Detector) objValue).getSpec();
		}
		else {
			resultMsg = new String(XML_TAG + " XML elements cannot have " +
					objName + " embedded objects.");

		}
		return resultMsg;
	}

	@Override
	public String getTag() {
		return XML_TAG;
	}

	@Override
	public String doneXML() {
		Saved = true;
		return null;
	}

	@Override
	public boolean isSaved() {
		return Saved;
	}

	@Override
	public String putXML(Element parent) {
		Element thisObject = new Element(XML_TAG);
		String resultMsg = null;
		if (PrimaryImage == null) {
			resultMsg = "Button missing primary image file name";
		}
		else if (AlternateImage == null) {
			resultMsg = "Button missing alternate image file name";
		}
		else {
			thisObject.setAttribute(XML_PRIMARY, PrimaryImage);
			thisObject.setAttribute(XML_ALTERNATE, AlternateImage);
			thisObject.setAttribute(XML_DELAY, ((Integer) MomentaryDelay).toString());
			if (Trigger != null) {
				Element trig = new Element(XML_TRIGGER);
				Trigger.putXML(trig);
				thisObject.addContent(trig);
			}
			thisObject.setAttribute(XML_STATUS, ((Boolean) StatusOnly).toString());
			thisObject.setAttribute(XML_FIT, ((Boolean) FitToGrid).toString());
			parent.addContent(thisObject);
			Saved = true;
		}
		return resultMsg;
	}

	@Override
	public void addSelf(Section sec) {
		sec.replaceButton(this);
		ButtonSection = sec;
	}

	@Override
	public Itemizable copy(Section sec) {
		ButtonItem newButton = (ButtonItem) shallowCopy(sec);
		newButton.MyFrill = MyFrill;
		newButton.ButtonSection = sec;
		return newButton;
	}

	@Override
	public Itemizable shallowCopy(Section sec) {
		ButtonItem newButton = new ButtonItem();
		newButton.ButtonTile = ButtonTile;
		Trigger = null;
		newButton.PrimaryImage = PrimaryImage;
		newButton.AlternateImage = AlternateImage;
		newButton.MomentaryDelay = MomentaryDelay;
		newButton.StatusOnly = StatusOnly;
		newButton.FitToGrid = FitToGrid;
		return newButton;
	}

	@Override
	public void install(GridTile tile) {
		File fName;

		if ( (MyFrill == null) && (PrimaryImage != null)) {
			fName = new File(PrimaryImage);
			if (fName.exists()) {
				if (fName.canRead()) {
					MyFrill = new ButtonFrill(PrimaryImage);
					MyFrill.setFitSize(getFitToGrid());
				}
				else {
					log.warn("Cannot read Image file " + PrimaryImage);
					System.out.println("Cannot read Image file " + PrimaryImage);
				}
			}
			else {
				log.warn("Could not find Image file " + fName);
				System.out.println("Could not find Image file " + fName);
			}
		}

		ButtonTile = tile;
		if (MyFrill != null) {
			ButtonTile.addFrill(MyFrill);
		}
	}

	@Override
	public void uninstall() {
		if (MyFrill != null) {
			ButtonTile.delFrill(MyFrill);
			MyFrill = null;
		}
	}

	/**
	 * registers a BlockFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new ButtonFactory());
		Detector.init(XML_TRIGGER);
	}
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
			ButtonItem.class.getName());
}

/**
 * is a Class known only to the ButtonItem class for creating Buttons from
 * an XML document.  Its purpose is to pick up the Image file name.
 */
class ButtonFactory
implements XMLEleFactory {

	/**
	 * these are the XML attributes
	 */
	private String PrimaryAttr;
	private String AlternateAttr;
	private int DelayAttr;
	private boolean StatusAttr;
	private boolean FitAttr;

	@Override
	public void newElement() {
		PrimaryAttr = null;
		AlternateAttr = null;
		DelayAttr = 0;
		StatusAttr = false;
		FitAttr = false;
	}

	@Override
	public String addAttribute(String tag, String value) {
		String resultMsg = null;
		if (ButtonItem.XML_PRIMARY.equals(tag)) {
			PrimaryAttr = value;
		}
		else if (ButtonItem.XML_ALTERNATE.equals(tag)) {
			AlternateAttr = value;
		}
		else if (ButtonItem.XML_DELAY.equals(tag)) {
			try {
				DelayAttr = Integer.parseInt(value);
			}
			catch (NumberFormatException e) {
				resultMsg = new String(value + " is an invalid delay for a " +
						ButtonItem.XML_TAG + " XML Element " + ButtonItem.XML_DELAY +
						" attribute");
			}
		}
		else if (ButtonItem.XML_STATUS.equals(tag)) {
			StatusAttr = Boolean.parseBoolean(value);
		}
		else if (ButtonItem.XML_FIT.equals(tag)) {
			FitAttr = Boolean.parseBoolean(value);
		}
		return resultMsg;
	}

	@Override
	public XMLEleObject getObject() {
		ButtonItem button = new ButtonItem();
		button.setPrimaryImage(PrimaryAttr);
		button.setAlternateImage(AlternateAttr);
		button.setMomentaryDelay(DelayAttr);
		button.setStatusOnly(StatusAttr);
		button.setFitToGrid(FitAttr);
		return button;
	}

}
/* @(#)ButtonItem.java */
