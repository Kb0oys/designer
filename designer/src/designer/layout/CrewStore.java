/*
 * Name: CrewStore
 *
 * What:
 *  This class provides a repository for holding Crew information.  Since
 *  Crew information tends to be very dynamic, changing at each operating
 *  session, there is little need to utilize the content component.  However,
 *  the Field description component is available for editing the presentation
 *  of the Crew assignments in CATS.  This does not prevent anyone from adding
 *  Fields to the Crew content.  The user defined fields will be passed to
 *  CATS.
 */
package designer.layout;

import designer.gui.store.AlignmentList;
import designer.gui.store.FontSpec;
import designer.gui.store.StoreTableModel;
import designer.gui.store.TimeSpec;
import designer.gui.store.TrainList;
import designer.layout.store.FieldInfo;
import designer.layout.store.AbstractStore;
import designer.layout.store.FieldVector;
import designer.layout.store.GenericRecord;
import designer.layout.store.RecordVector;

/**
 *  This class provides a repository for holding Crew information.  Since
 *  Crew information tends to be very dynamic, changing at each operating
 *  session, there is little need to utilize the content component.  However,
 *  the Field description component is available for editing the presentation
 *  of the Crew assignments in CATS.  This does not prevent anyone from adding
 *  Fields to the Crew content.  The user defined fields will be passed to
 *  CATS.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class CrewStore
    extends AbstractStore {

  /**
   * The Tag for identifying the CrewStore in the XML file.
   */
  private static final String XML_TAG = "CREWSTORE";

  /**
   * The Tag for identifying the CrewStore edit control FieldStore.
   */
  private static final String EDIT_TAG = "CREWEDIT";

  /**
   * The Tag for identifying the CrewStore Job Records.
   */
  private static final String DATA_TAG = "CREWDATA";
  /**
   * is the property tag for picking out the Crew's name.
   */
  private static final String CREW_NAME = "CREW_NAME";

  /**
   * is a placeholder for when the crew went on duty.
   */
  private static final String TIME_ON_DUTY = "TIME_ON";
  /**
   * is a placeholder for hours on duty left.
   */
  private static final String TIME_LEFT = "TIME_LEFT";

  /**
   * is the placeholder for when hours expire.
   */
  private static final String EXPIRES = "EXPIRES";
  
  /**
   * is the property tag for attaching a Font to a crew
   */
  private static final String FONT = "FONT";

  /**
   * is the property tag for identifying the Train the crew is assigned to.
   */
  static final String TRAIN_ID = "TRAIN_ID";

  /**
   * is used to locate the fields that describe the Train.  The
   * common.prop class has some useful utilities for pulling out
   * entries.
   */
  protected static String[] CREW_PROPS = {
      CREW_NAME,
      TIME_ON_DUTY,
      TIME_LEFT,
      EXPIRES,
      TRAIN_ID,
      FONT
  };
  
  /**
   * are the default FieldInfos - one FieldInfo for each field.  The order of
   * constructor parameters are:
   * 1. tag
   * 2. visible flag
   * 3. column label on JTable
   * 4. edit flag
   * 5. mandatory flag
   * 6. initial column width
   * 7. default value of Objects of this class
   * 8. class of objects of this class
   * The first field is critical - it references the Object being stored.
   */
  private static final FieldInfo DEFAULT_INFOS[] = {
    new FieldInfo(new String(CREW_NAME), true, new String(CREW_NAME), true, true, FieldInfo.MEDIUM_WIDTH,
            AlignmentList.DEFAULT, "", String.class),
    new FieldInfo(new String(TIME_ON_DUTY), true, new String(TIME_ON_DUTY), true, true, FieldInfo.NARROW_WIDTH,
            AlignmentList.DEFAULT, "", TimeSpec.class),
    new FieldInfo(new String(TIME_LEFT), true, new String(TIME_LEFT), true, true, FieldInfo.NARROW_WIDTH,
            AlignmentList.DEFAULT, "", TimeSpec.class),
    new FieldInfo(new String(EXPIRES), true, new String(EXPIRES), true, true, FieldInfo.NARROW_WIDTH,
            AlignmentList.DEFAULT, "", TimeSpec.class),
    new FieldInfo(new String(TRAIN_ID), true, new String(TRAIN_ID), true, true, FieldInfo.MEDIUM_WIDTH,
            AlignmentList.DEFAULT, "", TrainList.class),
    new FieldInfo(new String(JobStore.FONT), false, new String(JobStore.FONT), true, true, FieldInfo.MEDIUM_WIDTH,
            AlignmentList.DEFAULT, FontList.FONT_LABEL, FontSpec.class)
  };


  /**
   * the singleton, which is known by all clients.
   */
  public static CrewStore CrewKeeper = new CrewStore();

  /**
   * the constructor.
   */
  public CrewStore() {
    super(XML_TAG, EDIT_TAG, DATA_TAG, DEFAULT_INFOS);
    FieldInfoStore.getFieldInfo(CREW_NAME).makeRecordKey();
    init();
  }

  /**
   * constructs a StoreTableModel for the concrete class
   * @param data is a copy of the DataStore
   * @param format is a copy of the FieldInfoStore
   * @return a concrete StoreTableModel for the concrete Store.
   */
  protected StoreTableModel createModel(RecordVector<GenericRecord> data,
          FieldVector format) {
      return new StoreTableModel(data, format, this);
  }
}
/* @(#)CrewStore.java */