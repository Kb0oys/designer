/*
 * Name: JobStore
 *
 * What:
 *  This class provides a repository for holding all the Jobs.
 */
package designer.layout;

import designer.gui.store.AlignmentList;
import designer.gui.store.CrewList;
import designer.gui.store.FontSpec;
import designer.gui.store.StoreTableModel;
import designer.layout.store.FieldInfo;
import designer.layout.store.AbstractStore;
import designer.layout.store.FieldVector;
import designer.layout.store.GenericRecord;
import designer.layout.store.RecordVector;
import designer.layout.xml.*;

/**
 * Holds all the Jobs.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class JobStore
    extends AbstractStore
    implements Savable {

  /**
   * The Tag for identifying the JobStore in the XML file.
   */
  private static final String XML_TAG = "JOBSTORE";

  /**
   * The Tag for identifying the JobStore edit control FieldStore.
   */
  private static final String EDIT_TAG = "JOBEDIT";

  /**
   * The Tag for identifying the JobStore Job Records.
   */
  private static final String DATA_TAG = "JOBDATA";
  /**
   * is the property tag for picking out the Job's name.
   */
  private static final String JOB_NAME = "JOB_NAME";

  /**
   * is the property tag for picking out the Train flag flag.
   */
  private static final String RUNS_TRAIN = "RUNS_TRAIN";

  /**
   * is the placeholder property tag for the Crew column.
   */
  private static final String CREW_NAME = "CREW_NAME";

  /**
   * is the placeholder property tag for the Assistant column.
   */
  private static final String ASSISTANT = "ASSISTANT";
  
  /**
   * is the Font for drawing the job in
   */
  public static final String FONT = "FONT";

  /**
   * are the default FieldInfos - one FieldInfo for each field.  The order of
   * constructor parameters are:
   * 1. tag
   * 2. visible flag
   * 3. column label on JTable
   * 4. edit flag
   * 5. mandatory flag
   * 6. initial column width
   * 7. default value of Objects of this class
   * 8. class of objects of this class
   * The first field is critical - it references the Object being stored.
   */
  private static final FieldInfo DEFAULT_INFOS[] = {
    new FieldInfo(new String(JOB_NAME), true, new String(JOB_NAME), true, true, FieldInfo.MEDIUM_WIDTH,
            AlignmentList.DEFAULT, "", String.class),
    new FieldInfo(new String(RUNS_TRAIN), true, new String(RUNS_TRAIN), true, true, FieldInfo.NARROW_WIDTH,
            AlignmentList.DEFAULT, new Boolean(true), Boolean.class),
    new FieldInfo(new String(CREW_NAME), true, new String(CREW_NAME), true, true, FieldInfo.MEDIUM_WIDTH,
            AlignmentList.DEFAULT, "", CrewList.class),
    new FieldInfo(new String(ASSISTANT), true, new String(ASSISTANT), true, true, FieldInfo.MEDIUM_WIDTH,
            AlignmentList.DEFAULT, "", CrewList.class),
    new FieldInfo(new String(FONT), false, new String(FONT), true, true, FieldInfo.MEDIUM_WIDTH,
            AlignmentList.DEFAULT, FontList.FONT_LABEL, FontSpec.class)
  };

  /**
   * the singleton, which is known by all clients.
   */
  public static JobStore JobsKeeper = new JobStore();

  /**
   * the constructor.
   */
  public JobStore() {
    super(XML_TAG, EDIT_TAG, DATA_TAG, DEFAULT_INFOS);
    FieldInfoStore.getFieldInfo(JOB_NAME).makeRecordKey();
    init();
  }

  /**
   * constructs a StoreTableModel for the concrete class
   * @param data is a copy of the DataStore
   * @param format is a copy of the FieldInfoStore
   * @return a concrete StoreTableModel for the concrete Store.
   */
  protected StoreTableModel createModel(RecordVector<GenericRecord> data,
          FieldVector format) {
      return new StoreTableModel(data, format, this);
  }
}
/* @(#)JobStore.java */