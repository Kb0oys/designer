/* Name: Node.java
 *
 * What:
 *  This file contains the definition of a Node - an Object for gluing
 *  together pieces of the layout.
 */
package designer.layout;

import designer.gui.Ctc;
import designer.gui.GridTile;
import designer.layout.xml.*;
import designer.layout.items.LoadFilter;
import designer.layout.items.Section;
import org.jdom2.Element;


/**
 * This is an Object for gluing together pieces of the layout.  A Node is
 * a point on the Grid that is the dispatcher panel; thus, it has an
 * (X,Y) coordinate and is relatively independent of what it contains.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2009, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class Node
    implements XMLEleObject, Savable {
//  static private int Count = 0;
  /**
   * is the XML tag to identify a Section definition.
   */
  static public final String NODE_TAG = "SECTION";
  static final String X_TAG = "X";
  static final String Y_TAG = "Y";

  /**
   * is the link to the next Node to the right in the Layout.  The
   * link is to the next Node on the same row.
   */
  Node HLink;

  /**
   * is the link to the next Node below this Node.  The link is to
   * the next Node in the same column.
   */
  Node VLink;

  /**
   * is the row or column number for header nodes.  A negative number
   * denotes a track segment Node.
   */
  int Position;

  /**
   * marks a Node as containing a track segment.
   */
  static final int TRACKNODE = -1;

  /**
   * is the Swing element that appears on the screen.
   */
  GridTile MyTile;

  /**
   * is the contents of the Node.
   */
  Section Contents;

  /**
   * is the constructor
   */
  public Node() {
    MyTile = new GridTile(this);
    Contents = new Section(this);
//    Contents.setValue(String.valueOf(Count++));
  }

  /**
   * retrieves the Node to the right.
   *
   * @return the neighbor to this on the same row or null (if node is in
   *         the rightmost column).
   */
  public Node getRight() {
    if ((HLink != null) && (HLink.Position != Node.TRACKNODE)) {
      return null;
    }
    return HLink;
  }

  /**
   * retrieves the Node below.
   *
   * @return the neighbor below this one in the same column or null
   *         (if node is in the bottom row).
   */
  public Node getBelow() {
    if ((VLink != null) && (VLink.Position == Node.TRACKNODE)) {
      return VLink;
    }
    return null;
  }

  /**
   * finds the Node to the left, if there is one.
   *
   * @return the Node to the left or null (if node is in column 1).
   */

  public Node getLeft() {
    Node n = HLink;
    while ((n != null) && (n.HLink != this)) {
      n = n.HLink;
    }
    return n;
  }

  /**
   * finds the Node above, if there is one.
   *
   * @return the Node above or null (if node is in row 1).
   */
  public Node getAbove() {
    Node n = VLink;
    while ((n != null) && (n.VLink != this)) {
      n = n.VLink;
    }
    return n;
  }

  /**
   * retrieves the row number
   *
   * @return the number of the row the Node is in
   */
  public int getRow() {
    Node node;
    for (node = HLink; (node != null) && (node.Position == TRACKNODE);
        node = node.HLink) {
    }
    if (node == null) {
      return -1;
    }
    else {
      return node.Position;
    }
  }

  /**
   * retrieves the column number
   *
   * @return the number of the column the Node is in
   */
  public int getColumn() {
    Node node;
    for (node = VLink; (node !=null) && (node.Position == TRACKNODE);
        node = node.VLink) {
    }
    if (node == null) {
      return -1;
    }
    else {
      return node.Position;
    }
  }

  /**
   * determines how many columns exist between a node and the link
   * back to the row header.
   *
   * @return width of a rectangle
   */
  public int getWidth() {
    int width = 1;
    for (Node link = HLink; (link != null) && (link.HLink.Position ==
                                               TRACKNODE); link = link.HLink) {
      ++width;
    }
    return width;
  }

  /**
   * determines how many rows exist between a node and the link
   * back to the column header.
   *
   * @return height of a rectangle
   */
  public int getHeight() {
    int height = 1;
    for (Node link = VLink; (link != null) && (link.VLink.Position ==
                                               TRACKNODE); link = link.VLink) {
      ++height;
    }
    return height;
  }

  /**
   * retrieves the Tile associated with the Node.
   *
   * @return the Tile.
   *
   * @see designer.gui.GridTile
   */
  public GridTile getTile() {
    return MyTile;
  }

  /**
   * replaces the Section associated with the Node.
   *
   * @param newContents is the replacement.
   *
   * @see designer.layout.items.Section
   */
  public void replaceContents(Section newContents) {
    Contents = newContents;
    if (Contents != null) {
        Contents.replaceNode(this);
    }
  }

  /**
   * retrieves the Section associated with the Node.
   *
   * @return the associated Section.
   *
   * @see designer.layout.items.Section
   */
  public Section getContents() {
    return Contents;
  }

  /**
   * copies the contents (but not the links) of a Node to itself.
   *
   * @param master is the Node being copied.
   */
  void copy(Node master) {
    Position = master.Position;
    MyTile = master.MyTile.copy(this);
    Contents = new Section(this);
    Contents.copy(master.Contents);
  }

  /**
   * copies the contents (but not the links) of a Node to itself.
   *
   * @param master is the Node being copied.
   */
  void shallowCopy(Node master) {
    Position = master.Position;
    MyTile = master.MyTile.copy(this);
    Contents = new Section(this);
    Contents.shallowCopy(master.Contents);
  }

  /**
   * informs the Node that it has been selected by the mouse.
   */
  public void requestSel() {
    Ctc.getLayout().moveCursor(this);
  }

  /**
   * informs the Node that the user has selected it with the shift
   * key down.
   */
  public void requestShiftSel() {
      Ctc.getLayout().newSelBounds(this);
  }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return null;
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    return null;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return NODE_TAG;
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error chacking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    return null;
  }
  /**
   * writes the Object's contents to an XML file.
   *
   * @param parent is the Element that this Object is added to.
   *
   * @return null if the Object was written successfully; otherwise, a String
   *         describing the error.
   */
  public String putXML(Element parent) {
    String resultMsg = null;
    if (Contents.inUse()) {
      Element thisObject = new Element(NODE_TAG);
      thisObject.setAttribute(X_TAG, String.valueOf(getColumn()));
      thisObject.setAttribute(Y_TAG, String.valueOf(getRow()));
      resultMsg = Contents.putXML(thisObject);
      parent.addContent(thisObject);
    }
    return resultMsg;
  }

  /*
   * tells the caller if the state has been saved or not.
   *
   * @return true if the state has not been changed since the last save
   */
  public boolean isSaved() {
    return Contents.isSaved();
  }

  /**
   * registers a NodeFactory and all the factories for creating persistent
   * embedded objects within a Node with the XMLReader
   */
  static public void initNode() {
    XMLReader.registerFactory(NODE_TAG, new NodeFactory());
    Section.init();
  }

}

/**
 * is a Class known only to the Node class for creating Nodes from
 * an XML document.  Its purpose is to pick up the coordinates of the Node
 * and add it to the Layout.
 */
class NodeFactory
    implements XMLEleFactory {
  String RowAttr;
  String ColAttr;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    RowAttr = null;
    ColAttr = null;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    if (tag.equals(Node.X_TAG)) {
      if (ColAttr == null) {
        ColAttr = new String(value);
      }
      else {
        resultMsg = new String(tag + " appears to be duplicated in " +
                               Node.NODE_TAG);
      }
    }
    else if (tag.equals(Node.Y_TAG)) {
      if (RowAttr == null) {
        RowAttr = new String(value);
      }
      else {
        resultMsg = new String(tag + " appears to be duplicated in " +
                               Node.NODE_TAG);
      }
    }
    else {
      resultMsg = new String(tag + " is not a valid attribute for a " +
                             Node.NODE_TAG);
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    Node n;
    if ( (RowAttr != null) && (ColAttr != null)) {
      int row = Integer.parseInt(RowAttr);
      int col = Integer.parseInt(ColAttr);
      if (row < 1) {
        System.out.println(
            "Section row numbers must be greater than 0.");
        RowAttr = null;
      }
      else if (col < 1) {
        log.warn("Section column numbers must be greater than 0.");
        System.out.println(
            "Section column numbers must be greater than 0.");
        ColAttr = null;
      }
      else {
//        if ((n = Layout.RRLayout.locate(row, col)) == null) {
          if ((n = LoadFilter.instance().getLayout().locate(row, col)) == null) {
          log.warn("The section at (" + RowAttr + "," + ColAttr +
                  " is not on the layout.");
          System.out.println("The section at (" + RowAttr + "," + ColAttr +
                            " is not on the layout.");
        }
        else {
          log.debug("Reading Section at row=" + row + ", column=" + col);
          return n.Contents;
        }
      }
    }
    return null;
  }
  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
      NodeFactory.class.getName());
}
/* @(#)Node.java */