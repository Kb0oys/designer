/**
 * Name: OperationsClient
 * 
 * What:
 * This class contains the information for attaching CATS to a JMRI instantiation
 * running Operations.  Operations could be on the same computer or a different one.
 * In either case a network connection is needed to contact it.
 * <p>
 * The information includes:
 * <ol>
 * <li> the IP address of the computer hosting Operations
 * <li> the hostname of the computer hosting Operations
 * <li> the port on the computer hosting Operations
 * <li> the port on this computer to use to connect to the Operations computer
 * <li> a boolean to control whether the connection should be enabled or not
 * </ol>
 * Special Considerations:
 */
package designer.layout;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

import org.jdom2.Element;

import designer.gui.Ctc;
import designer.layout.xml.SimpleSavable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 * This class contains the information for attaching CATS to a JMRI instantiation
 * running Operations.  Operations could be on the same computer or a different one.
 * In either case a network connection is needed to contact it.
 * <p>
 * The information includes:
 * <ol>
 * <li> the IP address of the computer hosting Operations
 * <li> the hostname of the computer hosting Operations
 * <li> the port on the computer hosting Operations
 * <li> the port on this computer to use to connect to the Operations computer
 * <li> a boolean to control whether the connection should be enabled or not
 * </ol>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2011, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class OperationsClient implements XMLEleObject, SimpleSavable {
    /**
     * is the tag for identifying an OperationsClient in the XMl file.
     */
    static final String XML_TAG = "OPERATIONS";

    /**
     * is the XML attribute tag on the hostname
     */
    static final String HOST_TAG = "HOSTNAME";

    /**
     * is the XML attribute tag on the IP address
     */
    static final String IP_TAG = "IPADDRESS";

    /**
     * is the XML attribute tag on the Remote port
     */
    static final String REMOTE_TAG = "REMOTE_PORT";

    /**
     * is the XML attribute tag on the Local port
     */
    static final String LOCAL_TAG = "LOCAL_PORT";

    /**
     * is the XML attribute tag on connect at start up
     */
    static final String CONNECT_TAG = "CONNECT";

    /**
     * the default port to use on the Operations computer.  The default is defined
     * in jmri.jmris.JmriServer.java as "protected int portNo".  However, this does
     * not match.  This was found through netstat.
     * 
     * Try: jmri.jmrix.jmriclient.networkdriver:NetworkDriverAdapter()
     */
    private static final int REMOTE_PORT = 2048;

    /**
     * the default port to use on this computer
     */
    private static final int LOCAL_PORT = 51431;

    /**
     * flags an invalid TCP/IP port number
     */
    public static final int INVALID_PORT = -1;

    /**
     * is the class reference for finding the OperationsClient.
     */
    public static OperationsClient OperationsNetwork;

    /**
     * The internal data being configured
     */
    /**
     * the IP address.  The default value is blank, meaning, use the localhost.
     */
    private String IPAddress = "";

    /**
     * the hostname.  The default value is blank, meaning, use the localhost.
     */
    private String Hostname = "";

    /**
     * the port to use on the Operations computer.  The default is defined above.
     */
    private String RemotePort = "";

    /**
     * the port to use on this computer
     */
    private String LocalPort = "";

    /**
     * a boolean controlling if the connection is established when CATS starts up.
     * The default is disabled.
     */
    private boolean AutoConnect = false;

    /**
     * a boolean used to indicate if any of the above has changed.  true means no
     * changes have been made.
     */
    private boolean Saved = true;

    /**
     * the ctor
     */
    public OperationsClient() {
        Ctc.RootCTC.registerSave(this);
    }
    
    /**
     * is called to retrieve the current value of the hostname.  It will always
     * return a String.
     * @return the hostname.  A blank string ("") indicates that it has not been
     * set.
     */
    public String getHostName() {
        return Hostname;
    }

    /**
     * is called to retrieve the current value of the IP address.  It will always
     * return a String.
     * @return the IP address.  A blank string ("") indicates that it has not been
     * set.
     */
    public String getIPAddress() {
        return IPAddress;
    }

    /**
     * is called to retrieve the current value of the port to use on the Operations
     * computer.
     * @return the port number on the Operations computer
     */
    public String getOperationsPort() {
        return RemotePort;
    }

    /**
     * is called to retrieve the current value of the port to use on the CATS computer.
     * @return the port number on the CATS computer.
     */
    public String getCatsPort() {
        return LocalPort;
    }

    /**
     * is called to retrieve the current value of the AutoConnect flag.  The flag controls
     * is the connection should be established when CATS starts or not.
     * @return the AutoConnect flag.
     */
    public boolean getConnected() {
        return AutoConnect;
    }

    /**
     * converts a String to an InetAddress.  If the String
     * is not formatted for IPv4, null is returned.
     * @param addr is a String representation of an IP address
     * @return the IP address of the String or null if it is invalid
     */
    public static InetAddress toInetAddress(String addr) {
        InetAddress ia = null;
        byte [] result = new byte[4];
        int partial;
        int octet = 0;
        StringTokenizer tokens = new StringTokenizer(addr, ".");
        while (tokens.hasMoreTokens()) {
            try {
                partial = Integer.parseInt(tokens.nextToken());
            }
            catch (NumberFormatException nfe) {
                return null;
            }
            if ((partial < 0) || (partial > 255)) {
                return null;
            }
            if (octet > 3) {
                return null;
            }
            result[octet++] = (byte) partial;
        }
        if (octet != 4) {
            return null;
        }
        try {
            ia = InetAddress.getByAddress(result);
        } catch (UnknownHostException e) {
            return null;
        }
        return ia;
    }

    /**
     * checks a string for being a valid port, using
     * isValidPort().  If the String is null or empty.
     * the default port is returned; otherwise,
     * the results of isValidPort are returned.
     * @param port is the String representation of an IP port
     * @return numeric representation of the port if the String
     * is valid or the default port if not.
     */
    public static int isValidCATSPort(String port) {
        if ((port == null) || (port.trim().equals(""))) {
            return LOCAL_PORT;
        }
        return isValidPort(port);
    }

    /**
     * checks a string for being a valid port, using
     * isValidPort().  If the String is null or empty.
     * the default port is returned; otherwise,
     * the results of isValidPort are returned.
     * @param port is the String representation of an IP port
     * @return numeric representation of the port if the String
     * is valid or the default port if not.
     */
    public static int isValidOperationsPort(String port) {
        if ((port == null) || (port.trim().equals(""))) {
            return REMOTE_PORT;
        }
        return isValidPort(port);
    }
    
    /**
     * checks a String for being a valid IP port.  To be
     * valid, the String must be all numeric characters,
     * greater than or equal to 0, and less than 65536.
     * @param port is the String being verified.
     * @return If the String is valid, then the port
     * number; otherwise, -1.
     */
    private static int isValidPort(String port) {
        int result;
        try {
            result = Integer.parseInt(port);
        }
        catch (NumberFormatException nfe) {
            return INVALID_PORT;
        }
        if ((result >=0) && (result < 65536)) {
            return result;
        }
        return INVALID_PORT;
    }
    
    /**
     * is called to set all the local data.
     * @param hostname is the new name of the Operations host.
     * @param ipaddress is the new IP address of the Operations host.
     * @param operationsPort is the port to use on the Operations computer.
     * @param localPort is the port to use on the CATS computer.
     * @param enabled is true to set up the connection when CATS starts up.
     */
    public void setAll(String hostname, String ipaddress, String operationsPort,
            String localPort, boolean enabled) {
        hostname = hostname.trim();
        if (!Hostname.equals(hostname)) {
            Saved = false;
            Hostname = hostname;
        }
        if (!IPAddress.equals(ipaddress)) {
            Saved = false;
            IPAddress = ipaddress;
        }
        if (!RemotePort.equals(operationsPort)) {
            Saved = false;
            RemotePort = operationsPort;
        }
        if (!LocalPort.equals(localPort)) {
            Saved = false;
            LocalPort = localPort;
        }
        if (AutoConnect != enabled) {
            Saved = false;
            AutoConnect = enabled;
        }
    }

    /**
     * restores the class to its default value
     */
    public void reNew() {
        IPAddress = "";
        Hostname = "";
        RemotePort = "";
        LocalPort = "";
        AutoConnect = false;
    }
    
    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
        return Saved;
    }

    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        Element myObject = new Element(XML_TAG);
        boolean changed = false;
        if (!Hostname.equals("")) {
            myObject.setAttribute(HOST_TAG, Hostname);
            changed = true;
        }
        if (!IPAddress.equals("")) {
            myObject.setAttribute(IP_TAG, IPAddress);
            changed = true;
        }
        if (isValidOperationsPort(RemotePort) != REMOTE_PORT) {
            myObject.setAttribute(REMOTE_TAG, RemotePort);
            changed = true;
        }
        if (isValidCATSPort(LocalPort) != LOCAL_PORT) {
            myObject.setAttribute(LOCAL_TAG, String.valueOf(LocalPort));
            changed = true;
        }
        if (AutoConnect) {
            myObject.setAttribute(CONNECT_TAG, "true");
            changed = true;
        }
        if (changed) {
            parent.addContent(myObject);            
        }
        Saved = true;
        return null;
    }

    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("A " + XML_TAG + " cannot contain a text field ("
                + eleValue + ").");
    }

    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptable or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        return new String("A " + XML_TAG + " cannot contain an Element ("
                + objName + ").");
    }

    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }

    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        Saved = true;
        return null;
    }
    /**
     * registers an OperationsFactory with the XMLReader.
     */
    static public void init() {
        OperationsNetwork = new OperationsClient();
        XMLReader.registerFactory(XML_TAG, new OperationsFactory());
    }
}

/**
 * is a Class known only to the OperationsClient class for creating an OperationsClient
 * from an XML document.
 */
class OperationsFactory
implements XMLEleFactory {

    /**
     * the fields in OperationsClient
     */
    private String Hostname;
    private String IPAddress;
    private String RemotePort;
    private String LocalPort;
    private boolean AutoConnect;

    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        Hostname = OperationsClient.OperationsNetwork.getHostName();
        IPAddress = OperationsClient.OperationsNetwork.getIPAddress();
        RemotePort = OperationsClient.OperationsNetwork.getOperationsPort();
        LocalPort = OperationsClient.OperationsNetwork.getCatsPort();
        AutoConnect = OperationsClient.OperationsNetwork.getConnected();
    }

    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        String resultMsg = null;
        if (tag.equals(OperationsClient.HOST_TAG)) {
            Hostname = value;
        }
        else if (tag.equals(OperationsClient.IP_TAG)) {
            IPAddress = value;
        }
        else if (tag.equals(OperationsClient.REMOTE_TAG)) {
            if (OperationsClient.isValidOperationsPort(value) != OperationsClient.INVALID_PORT) {
                RemotePort = value;
            }
        }
        else if (tag.equals(OperationsClient.LOCAL_TAG)) {
            if (OperationsClient.isValidCATSPort(value) != OperationsClient.INVALID_PORT) {
                LocalPort = value;
            }
        }
        else if (tag.equals(OperationsClient.CONNECT_TAG)) {
            AutoConnect = true;
        }
        else {
            resultMsg = new String("A " + OperationsClient.XML_TAG +
                    " XML Element cannot have a " + tag +
                    " attribute.");
        }
        return resultMsg;
    }

    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        OperationsClient.OperationsNetwork.setAll(Hostname, IPAddress, RemotePort,
                LocalPort, AutoConnect);
        return OperationsClient.OperationsNetwork;
    }
}
/* @(#)OperationsClient.java */