/* Name: FlashRate.java
 *
 * What;
 *  FastRate is a Singleton object with an Enumerated value.  It is set
 *  to the number of milliseconds for the on/off phase of a
 *  flashing light.  All CATS flashing lights use the same
 *  cycle.  JMRI flashing lights used the constant defined
 *  in DefaultSignalHead.
 */
package designer.layout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import org.jdom2.Element;

import designer.gui.DispPanel;
import designer.gui.jCustom.AcceptDialog;
import designer.layout.xml.SimpleSavable;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 *  FastRate is a Singleton object with an Enumerated value.  It is set
 *  to the number of milliseconds for the on/off phase of a
 *  flashing light.  All CATS flashing lights use the same
 *  cycle.  JMRI flashing lights used the constant defined
 *  in DefaultSignalHead.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2005, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class FlashRate implements XMLEleObject, SimpleSavable {

    /**
     * is the tag for identifying a FLashRate Object in the XMl file.
     */
    static final String XML_TAG = "FLASHRATE";

    /**
     * is the index of the default value
     */
    private static final int DEFAULT_RATE = 1;
    
    /**
     * the supported flashrates.
     */
    private static String[] RATES = {
      new String("500"),
      new String("750"),
      new String("1000"),
      new String("1250"),
      new String("1500"),
    };

    /**
     * is the singleton.
     */
    public static FlashRate TheFlashRate;

    /**
     * goes on the drop down menu.
     */
    private JMenuItem RateMenu = new JMenuItem("Flash Rate ...");
    
    /**
     * is the index into Rates of the current selection.
     */
    private int Rate = DEFAULT_RATE;

    /**
     *  a true flag means that it has been saved,
     */
    protected boolean Saved = true;

    /**
     * the constructor.
     *
     * @param parent is the Panel which will hold the JCheckBox.
     */
    public FlashRate(DispPanel parent) {
      if (TheFlashRate == null) {
        init();
        RateMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                JComboBox box = new JComboBox(RATES);
                JPanel p = new JPanel();
                p.add(new JLabel("Select rate (in milliseconds)"));
                box.setSelectedIndex(Rate);
                p.add(box);
                if (AcceptDialog.select(p, "Flash Rate")) {
                    int r = box.getSelectedIndex();
                    if (r != Rate) {
                        Rate = r;
                        Saved = false;
                    }
                }
            }
        }
       );
        TheFlashRate = this;
        parent.addAppearanceItem(RateMenu);
      }
    }

      /**
       * returns the flash rate setting.
       *
       * @return the String representation of number of\
       *  milliseconds in the current flash rate.
       */
      public String getRate() {
        return RATES[Rate];
      }

      /**
       * sets the Flash Rate.
       *
       * @param rate is the new String representation of the 
       * value in milliseconds.  It must be one of the values in RATES.
       */
      public void setRate(String rate) {
          for (int index = 0; index < RATES.length; ++index){
              if (RATES[index].equals(rate)) {
                  Rate = index;
                  Saved = false;
              }
          }
      }

      public void reNew() {
          Rate = DEFAULT_RATE;
      }

    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        setRate(eleValue);
        return null;
    }

    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
      String resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                             + objName + ").");
      return resultMsg;
    }

    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
      return new String(XML_TAG);
    }

    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error chacking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
      Saved = true;
      return null;
    }

    /*
     * asks if the state of the Object has been saved to a file
     *
     * @return true if it has been saved; otherwise return false if it should
     * be written.
     */
    public boolean isSaved() {
      return Saved;
    }

      /**
       * writes the Object's contents to an XML file.
       *
       * @param parent is the Element that this Object is added to.
       *
       * @return null if the Object was written successfully; otherwise, a String
       *         describing the error.
       */
      public String putXML(Element parent) {
        if (Rate != DEFAULT_RATE) {
          Element thisObject = new Element(XML_TAG);
          thisObject.addContent(RATES[Rate]);
          parent.addContent(thisObject);
        }
        Saved = true;
        return null;
      }

    /**
     * registers a FlashRate factory with the XMLReader.
     */
    static public void init() {
      XMLReader.registerFactory(XML_TAG, new FlashRateFactory());
    }
  }

  /**
   * is a Class known only to the FlashRate class for creating a true
   * value from an XML file.
   */
  class FlashRateFactory
      implements XMLEleFactory {

    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
    }

    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        return new String("A " + FlashRate.XML_TAG +
                               " XML Element cannot have a " + tag +
                               " attribute.");
    }

    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
      return FlashRate.TheFlashRate;
    }
}
/* @(#)FlashRate.java */
