/*
 * Name: SimpleSavable.java
 *
 * What:
 *   This file contains an interface for performing "save" operations on simple classes.
 *   These classes are grouped together in Ctc for save operations.
 */
package designer.layout.xml;

/**
 *   This file contains an interface for performing "save" operations on simple classes.
 *   These classes are grouped together in Ctc for save operations.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface SimpleSavable extends Savable {

    /**
     * restores the class to its default value
     *
     */
    public void reNew();
}
/* @(#)SimpleSavable.java */