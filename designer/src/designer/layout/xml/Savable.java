/*
 * Name: Savable.java
 *
 * What:
 *   This file contains an interface for performing "save" operations.
 */
package designer.layout.xml;

import org.jdom2.Element;

/**
 * defines the interface for Objects that can be written to a file.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2003, 2010, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
 public interface Savable {

   /**
    * asks if the state of the Object has been saved to a file
    *
    * @return true if it has been saved; otherwise return false if it should
    * be written.
    */
   public boolean isSaved();

   /**
    * writes the Object's contents to an XML file.
    *
    * @param parent is the Element that this Object is added to.
    *
    * @return null if the Object was written successfully; otherwise, a String
    *         describing the error.
    */
   public String putXML(Element parent);
 }
 /* @(#)Savable.java */