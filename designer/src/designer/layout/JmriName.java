/**
 * Name: JmriName.java
 *
 * What:
 *  This is a class for holding the list of Names defined for
 *  JMRI.
 * 
 */
package designer.layout;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.Vector;

import org.jdom2.Element;

import designer.layout.items.IOSpec;
import designer.layout.items.LoadFilter;
import designer.layout.xml.XMLEleFactory;
import designer.layout.xml.XMLEleObject;
import designer.layout.xml.XMLReader;

/**
 *  This is a class for holding the list of Names defined for
 *  JMRI.  The bad part is that the contents of this file are
 *  tightly coupled to JMRI in 2 ways - the 2 character name
 *  prefixes and the structure of the JMRI classes as to where
 *  the managers reside.  But, we can be abstract for only
 *  so long before we have to do something concrete.
 *  <p>
 *  The good part is that this is quite flexible.  Any part of
 *  any item (prefix, class, or type) can be redefined, which
 *  means it can be easily replace without changing the code.
 *  However, if (when) another type is added to JMRI, this code
 *  (and CATS) will have to be modified.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2006, 2009, 2010, 2012, 2013, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class JmriName extends AbstractListElement {
    /**
     * is the tag for identifying a Jmri Name in the XML file.
     */
    static final String XML_TAG = "JMRINAME";
    
    /**
     * is the XML tag for the 2 character name attribute.
     */
    static final String XML_PREFIX = "JMRIPREFIX";
    
    /**
     * is the attribute of the JMRI type for objects with Prefix.
     */
    static final String XML_TYPE = "XMLTYPE";
    
    /**
     * is the singleton that holds all the known IOSpecs
     */
    public static Vector<IOSpec> AllDecoders;

    /**
     * is the JMRI Name.  This is 2 characters long, following
     * JMRI naming convention.
     */
    private String MyName;
    
    /**
     * is the name of the JMRI class that manages devices of this type.
     */
    private String MyClass;
    
    /**
     * is the JMRI device type
     */
    private String MyType;
    
    /**
     * is true if the definition has been selected for
     * use.
     */
    private boolean Selected = false;
    
    /**
     * is the ctor.
     * 
     * @param name is the JMRI System Prefix.
     * @param c is the JMRI class for objects with the JMRI name
     * @param s is true if the JmriName is created as selected
     * and false if not.
     */
    public JmriName(final String name, final String c, final boolean s) {
    	char device;
        if ((name != null) && (name.length() > 1)) {
            MyName = new String(name);
        	device = name.charAt(MyName.length() - 1);
        	MyType = JmriDevice.mnemonicToDevice(device).toString();
        }
        if (c != null) {
            MyClass = c;
        }
        Selected = s;
    }
    
    /**
     * is the ctor.
     * 
     * @param name is the two character JMRI name.
     * @param c is the JMRI class for objects with the JMRI name
     */
    public JmriName(final String name, final String c) {
        this(name, c,  false);
    }
    
    /**
     * retrieves the System Prefix.
     * @return the JMRI prefix
     */
    public String getJmriID() {
        return MyName;
    }
    
    /**
     * sets the System Prefix.
     * @param id is the System Prefix
     */
    public void setJmriID(final String id) {
    	String s;
    	char device;
        if (id != null) {
            s = id.trim();
            if ((s != null) && !s.isEmpty() && (s.length() > 1)) {
            	MyName = s;
            	device = s.charAt(MyName.length() - 1);
            	MyType = JmriDevice.mnemonicToDevice(device).toString();
            }
        }
        else {
            MyName = null;
            MyType = null;
        }
    }
    
    /**
     * retrieves the fully qualified Jmri class name
     * @return where in the jar file to find the class
     * definition of the manager for the JMRI object.
     */
    public String getJmriClass() {
        return new String(MyClass);
    }
    
    /**
     * sets the class name of the JMRI manager for objects of
     * this decoder.
     * @param c is the JMRI (CATS) class name.
     */
    public void setJmriClass(final String c) {
        if (c != null) {
            MyClass = c;
        }
        else {
            MyClass = null;
        }
    }
    
    /**
     * retrieves the JMRI type definition.  See DefinedTypes.
     * @return the JMRI type that this prefix represents.
     */
    public String getJmriType() {
        return MyType;
    }
    
    /**
     * return the flag indicating the user wants to create
     * decoders of this kind.
     * @return the selection flag.
     */
    public boolean getSelected() {
        return Selected;
    }
    
    /**
     * sets the Selected flag for a JmriName.
     * @param b is the results of the checkbox.
     */
    public void setSelected(final Boolean b) {
        Selected = b.booleanValue();
    }
    
    /**
     * adds or replaces a prefix.  A prefix is replaced if
     * the name and type match an existing
     * prefix definition.
     * 
     * @param newPrefix is the new/replacement prefix
     * definition.
     */
    public static synchronized void addPrefix(final JmriName newPrefix) {
        JmriName existing;
        for (Iterator<Object> iter = JmriNameList.instance().iterator(); iter.hasNext(); ) {
            existing = (JmriName) iter.next();
            if (existing.MyName.equals(newPrefix.MyName)) {
                existing.MyClass = newPrefix.MyClass;
                existing.Selected = newPrefix.Selected;
                return;
            }
        }
        JmriNameList.instance().add(newPrefix);
    }
    
    /**
     * ensures that the JMRI prefix is selected and adds the decoder
     * to the known list.
     * @param spec is the 2 character JMRI identifier
     */
    public static void registerPrefix(final IOSpec spec) {
    	JmriName n;
    	if ((spec.getPrefix() != null) && !spec.getPrefix().isEmpty()) {
    		AllDecoders.add(spec);
    		for (Iterator<Object> iter = JmriNameList.instance().iterator(); iter.hasNext(); ) {
    			n = (JmriName) iter.next();
    			if (n.MyName.equals(spec.getPrefix())) {
    				n.Selected = true;
    				return;
    			}
    		}
    		addPrefix(new JmriName(spec.getPrefix(), "", true));
    	}
    }

    /** is a predicate for searching the list of defined prefixes for
     * a particular String
     * @param prefix is the String being searched for
     * @return true if it is defined and false if not; "none" is a valid prefix.
     */
    public static boolean isValidPrefix(final String prefix) {
        JmriName n;
        if (prefix != null) {
        	for (Iterator<Object> iter = JmriNameList.instance().iterator(); iter.hasNext(); ) {
        		n = (JmriName) iter.next();
        		if (n.MyName.equals(prefix) && n.Selected) {
        			return true;
        		}
        	}
        }
        return false;
    }
    
    /**
     * creates an array of Strings of all JMRI names that have been selected.
     * @return the array of Strings plus "none" added as the last element.
     */
    public static String[] getPrefixes() {
        String[] prefixes;
        JmriName jn;
        int count = 0;
        for (Iterator<Object> iter = JmriNameList.instance().iterator(); iter.hasNext(); ) {
            if (((JmriName) iter.next()).Selected) {
                ++count;
            }
        }
        prefixes = new String[count + 1];
        count = 0;
        for (Iterator<Object> iter = JmriNameList.instance().iterator(); iter.hasNext(); ) {
            jn = (JmriName) iter.next();
            if (jn.Selected) {
                prefixes[count++] = jn.MyName;
            }
        }
        prefixes[count] = "none";
        return prefixes;
    }

    /**
     * creates an array of Strings of all JMRI names of a particular type that have
     * been selected.
     * @param t is the name of a JMRI type
     * @return the array of Strings
     */
    public static String[] getPrefixesForType(String t) {
        String[] prefixes;
        JmriName jn;
        int count = 0;
        for (Iterator<Object> iter = JmriNameList.instance().iterator(); iter.hasNext(); ) {
            jn = (JmriName) iter.next();
            if (jn.Selected && jn.MyType.equals(t)) {
                ++count;
            }
        }
        prefixes = new String[count + 1];
        count = 0;
        for (Iterator<Object> iter = JmriNameList.instance().iterator(); iter.hasNext(); ) {
            jn = (JmriName) iter.next();
            if (jn.Selected && jn.MyType.equals(t)) {
                prefixes[count++] = jn.MyName;
            }
        }
        prefixes[count] = "none";
        return prefixes;        
    }
    
    /**
     * resets the list of prefixes.
     */
    public static synchronized void reset() {
        AllDecoders = new Vector<IOSpec>();
        JmriNameList list = JmriNameList.instance();
        list.init();
        list.add(new JmriName("AL", "jmri.jmrix.acela.AcelaLightManager"));
        list.add(new JmriName("AS", "jmri.jmrix.acela.AcelaSensorManager"));
        list.add(new JmriName("AT", "jmri.jmrix.acela.AcelaTurnoutManager"));
        list.add(new JmriName("CL", "jmri.jmrix.cmri.serial.SerialLightManager"));
        list.add(new JmriName("CS", "jmri.jmrix.cmri.serial.SerialSensorManager"));
        list.add(new JmriName("CT", "jmri.jmrix.cmri.serial.SerialTurnoutManager"));
        list.add(new JmriName("DT", "jmri.jmrix.srcp.SRCPTurnoutManager"));
        list.add(new JmriName("ET", "jmri.jmrix.easydcc.EasyDccTurnoutManager"));
        list.add(new JmriName("GH", "jmri.jmrix.grapevine.SerialSignalHead"));
        list.add(new JmriName("GL", "jmri.jmrix.grapevine.SerialLightManager"));
        list.add(new JmriName("GS", "jmri.jmrix.grapevine.SerialSensorManager"));
        list.add(new JmriName("GT", "jmri.jmrix.grapevine.SerialTurnoutManager"));
        list.add(new JmriName("IC", "cats.layout.items.IOSpecChain"));
        list.add(new JmriName("IH", "cats.jmri.CustomSignalHead"));
        list.add(new JmriName("IM", "jmri.MemoryManager"));
        list.add(new JmriName("IO", "jmri.RouteManager"));
        list.add(new JmriName("IR", "jmri.ReporterManager"));
        list.add(new JmriName("IS", "jmri.managers.InternalSensorManager"));
        list.add(new JmriName("IT", "jmri.managers.InternalTurnoutManager"));
        list.add(new JmriName("LH", "jmri.jmrix.loconet.SE8cSignalHead"));
        list.add(new JmriName("LL", "jmri.jmrix.loconet.LnLightManager"));
        list.add(new JmriName("LR", "jmri.jmrix.loconet.LnReporterManager"));
        list.add(new JmriName("LS", "jmri.jmrix.loconet.LnSensorManager"));
        list.add(new JmriName("LT", "jmri.jmrix.loconet.LnTurnoutManager"));
        list.add(new JmriName("KL", "jmri.jmrix.maple.SerialLightManager"));
        list.add(new JmriName("CS", "jmri.jmrix.maple.SerialSensorManager"));
        list.add(new JmriName("KT", "jmri.jmrix.maple.SerialTurnoutManager"));
        list.add(new JmriName("ML", "cats.jmri.MeterLnLightManager"));
        list.add(new JmriName("MR", "cats.jmri.MeterLnReporterManager"));
        list.add(new JmriName("MS", "cats.jmri.MeterLnSensorManager"));
        list.add(new JmriName("MS", "jmri.jmrix.can.cbus.CbusSensorManager"));
        list.add(new JmriName("MT", "jmri.jmrix.can.cbus.CbusTurnoutManager"));
        list.add(new JmriName("NS", "jmri.jmrix.nce.NceSensorManager"));
        list.add(new JmriName("NT", "jmri.jmrix.nce.NceTurnoutManager"));
        list.add(new JmriName("OL", "jmri.jmrix.oaktree.SerialLightManager"));
        list.add(new JmriName("OS", "jmri.jmrix.oaktree.SerialSensorManager"));
        list.add(new JmriName("OT", "jmri.jmrix.oaktree.SerialTurnoutManager"));
        list.add(new JmriName("PL", "jmri.jmrix.powerline.SerialLightManager"));
        list.add(new JmriName("PS", "jmri.jmrix.powerline.SerialSensorManager"));
        list.add(new JmriName("PT", "jmri.jmrix.powerline.SerialTurnoutManager"));
        list.add(new JmriName("PT", "jmri.jmrix.xpa.XpaTurnoutManager"));
        list.add(new JmriName("RR", "jmri.jmrix.rps.RpsReporterManager"));
        list.add(new JmriName("RS", "jmri.jmrix.rps.RpsSensorManager"));
        list.add(new JmriName("ST", "jmri.jmrix.sprog.SprogTurnoutManager"));
        list.add(new JmriName("TT", "jmri.jmrix.tmcc.SerialTurnoutManager"));
        list.add(new JmriName("UT", "jmri.jmrix.ecos.EcosTurnoutManager"));
        list.add(new JmriName("VL", "jmri.jmrix.secsi.SerialLightManager"));
        list.add(new JmriName("VS", "jmri.jmrix.secsi.SerialSensorManager"));
        list.add(new JmriName("VT", "jmri.jmrix.secsi.SerialTurnoutManager"));
        list.add(new JmriName("XL", "jmri.jmrix.lenz.XNetLightManager"));
        list.add(new JmriName("XS", "jmri.jmrix.lenz.XNetSensorManager"));
        list.add(new JmriName("XT", "jmri.jmrix.lenz.XNetTurnoutManager"));
        list.add(new JmriName("XT", "jmri.jmrix.xpa.XpaTurnoutManager"));
    }
    
    /**
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        MyClass = new String(eleValue);
        return null;
    }
    
    /**
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
        return new String("A " + XML_TAG + " cannot contain an Element ("
                + objName + ").");
        
    }
    
    /**
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
        return new String(XML_TAG);
    }
    
    /**
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
        String result = null;
        if (MyName == null){
            result = new String("A JMRI manager definition is missing the prefix.");
        }
        else if (MyClass == null) {
            result = new String("JMRI Name " + MyName + " is missing its class definition");
        }
        else if (MyType == null) {
            result = new String("JMRI Name " + MyName + " has an invalid JMRI device type");
        }
        else {
            if (LoadFilter.instance().isEnabled(LoadFilter.XML_JMRINAME)) {
                addPrefix(this);
            }
        }
        return result;
    }
    
    /**
     * writes the Object's contents to an XML file.
     *
     * @param parent is the Element that this Object is added to.
     *
     * @return null if the Object was written successfully; otherwise, a String
     *         describing the error.
     */
    public String putXML(Element parent) {
        if (Selected  && (MyName != null) && (MyName.trim().length() != 0)) {
            Element thisObject = new Element(XML_TAG);
            thisObject.setAttribute(XML_PREFIX, MyName.trim());
            if (MyType == null) {
                log.warn("JMRI Name " + MyName + " is missing its JMRI type definition");
            }
            else {
                thisObject.setAttribute(XML_TYPE, MyType);
            }
            if (MyClass == null) {
                log.warn("JMRI Name " + MyName + " is missing its class definition");
            }
            else {
                thisObject.addContent(MyClass);
            }
            parent.addContent(thisObject);
        }
        return null;
    }
    
    /**
     * registers a JmriNameFactory with the XMLReader.
     */
    static public void init() {
        XMLReader.registerFactory(XML_TAG, new JmriNameFactory());
        reset();
    }

    public String getElementName() {
        return getJmriID();
    }

    public AbstractListElement copy() {
        return new JmriName(MyName, MyClass, Selected);
    }

    /**
     * is deprecated
     * @param outStream is where to print the XML element
     * @param indent is the indentation to print before printing the XML element
     * @return null - it always works
     */
    public String putXML(PrintStream outStream, String indent) {
        return null;
    }
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(JmriName.class.getName());
}

/**
 * is a Class known only to the JmriName class for creating PhysicalSignals from
 * an XML document.  Its purpose is to pick up the location of the SecSignal
 * in the GridTile, its orientation, and physical attributes on the layout.
 */
class JmriNameFactory
implements XMLEleFactory {
    
    /**
     * is the 2 character JMRI Name prefix.
     */
    private String Prefix;
    
    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
        Prefix = "";
    }
    
    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
        String resultMsg = null;
        
        if (JmriName.XML_PREFIX.equals(tag)) {
            Prefix = new String(value);
        }
        else if (JmriName.XML_TYPE.equals(tag)) {
//  ignore for legacy files
        }
        else {
            resultMsg = new String("A " + JmriName.XML_TAG +
                    " XML Element cannot have a " + tag +
            " attribute.");
        }
        return resultMsg;
    }
    
    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
        return new JmriName(Prefix, null, true);
    }
}
/* @(#)JmriName.java */
